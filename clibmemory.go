package go_vulkan

import (
	"fmt"
	"unsafe"
)

// #include <stdlib.h>
import "C"

const sizeOfPCharValue = unsafe.Sizeof([1]*C.char{})

type clibMemory struct {
	marks [][]unsafe.Pointer
}

func (c *clibMemory) addPtr(ptr unsafe.Pointer)  {
	if c.marks == nil {
    var currentMark []unsafe.Pointer
    currentMark = append(currentMark, ptr)
    c.marks = append(c.marks, currentMark)
	} else {
		top := len(c.marks) - 1
		currentMark := c.marks[top]
    currentMark = append(currentMark, ptr)
		c.marks[top] = currentMark
	}
}

func (c *clibMemory) allocPCharMemory(n int) unsafe.Pointer {
	return c.Calloc(n, uint64(sizeOfPCharValue))
}

func (c *clibMemory) CString(str string) *C.char {
	r := C.CString(str)
	c.addPtr(unsafe.Pointer(r))
	return r
}

func (c *clibMemory) CStrings(strs []string) **C.char {
	if strs == nil || len(strs) == 0 {
		return nil
	}

	len0 := len(strs)
	mem0 := c.allocPCharMemory(len0)
	h0 := &sliceHeader{
		Data: mem0,
		Cap:  len0,
		Len:  len0,
	}
	v0 := *(*[]*C.char)(unsafe.Pointer(h0))
	for i, gs := range strs {
		v0[i] = c.CString(gs)
	}
	return (**C.char)(mem0)
}

func (c *clibMemory) Calloc(number int, size uint64) unsafe.Pointer {
	mem, err := C.calloc(C.size_t(number), (C.size_t)(size))
	if mem == nil {
		panic(fmt.Sprintln("memory alloc error: ", err))
	}
	c.addPtr(mem)
	return mem
}

func (c *clibMemory) Free(cMemory unsafe.Pointer) {
	for _, m := range c.marks {
		for i, p := range m {
			if p == cMemory {
				C.free(cMemory)
				m[i] = nil
				return
			}
		}
	}
	C.free(cMemory)
}

func (c *clibMemory) Mark() {
	c.marks = append(c.marks, nil)
	// fmt.Println("mark...")
}

func (c *clibMemory) FreeToMark() {
	var freed int
	if len(c.marks) > 0 {
		top := len(c.marks) - 1
		currentMark := c.marks[top]
		c.marks[top] = nil
		for _, p := range currentMark {
			if p != nil {
				freed++
				C.free(p)
			}
		}
		c.marks = c.marks[:top]
	}
	// fmt.Printf("... freed: %d\n", freed)
}

func NewClibClientMemory() VulkanClientMemory {
	return &clibMemory{}
}

