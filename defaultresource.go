package go_vulkan

import (
  "unsafe"
)

type defaultVulkanResource struct {
  mem VulkanClientMemory
}

func (d *defaultVulkanResource) CreateInstance(pCreateInfo *InstanceCreateInfo) (Instance, error) {
  return CreateInstance(d.mem, pCreateInfo)
}

func (d *defaultVulkanResource) DestroyInstance(instance Instance) {
  DestroyInstance(d.mem, instance)
}

func (d *defaultVulkanResource) EnumeratePhysicalDevices(instance Instance) ([]PhysicalDevice, error) {
  return EnumeratePhysicalDevices(d.mem, instance)
}

func (d *defaultVulkanResource) EnumerateInstanceLayerProperties() ([]LayerProperties, error) {
  return EnumerateInstanceLayerProperties(d.mem)
}

func (d *defaultVulkanResource) EnumerateInstanceExtensionProperties(layerName string) ([]ExtensionProperties, error) {
  return EnumerateInstanceExtensionProperties(d.mem, layerName)
}

func (d *defaultVulkanResource) GetPhysicalDeviceProperties(physicalDevice PhysicalDevice) PhysicalDeviceProperties {
  return GetPhysicalDeviceProperties(d.mem, physicalDevice)
}

func (d *defaultVulkanResource) GetPhysicalDeviceQueueFamilyProperties(physicalDevice PhysicalDevice) ([]QueueFamilyProperties, error) {
  return GetPhysicalDeviceQueueFamilyProperties(d.mem, physicalDevice)
}

func (d *defaultVulkanResource) CreateDevice(physicalDevice PhysicalDevice, createInfo *DeviceCreateInfo) (Device, error) {
  return CreateDevice(d.mem, physicalDevice, createInfo)
}

func (d *defaultVulkanResource) GetDeviceQueue(device Device, queueFamilyIndex uint32, queueIndex uint32) Queue {
  return GetDeviceQueue(d.mem, device, queueFamilyIndex, queueIndex)
}

func (d *defaultVulkanResource) DestroyDevice(device Device) {
  DestroyDevice(d.mem, device)
}

func (d *defaultVulkanResource) DestroySurface(instance Instance, surface Surface) {
  DestroySurface(d.mem, instance, surface)
}

func (d *defaultVulkanResource) GetPhysicalDeviceSurfaceSupport(physicalDevice PhysicalDevice, queueFamilyIndex uint32, surface Surface) (bool, error) {
  return GetPhysicalDeviceSurfaceSupport(d.mem, physicalDevice, queueFamilyIndex, surface)
}

func (d *defaultVulkanResource) EnumerateDeviceExtensionProperties(physicalDevice PhysicalDevice, layerName string) ([]ExtensionProperties, error) {
  return EnumerateDeviceExtensionProperties(d.mem, physicalDevice, layerName)
}

func (d *defaultVulkanResource) GetPhysicalDeviceSurfaceCapabilities(physicalDevice PhysicalDevice, surface Surface) (SurfaceCapabilities, error) {
  return GetPhysicalDeviceSurfaceCapabilities(d.mem, physicalDevice, surface)
}

func (d *defaultVulkanResource) GetPhysicalDeviceSurfaceFormats(physicalDevice PhysicalDevice, surface Surface) ([]SurfaceFormat, error) {
  return GetPhysicalDeviceSurfaceFormats(d.mem, physicalDevice, surface)
}

func (d *defaultVulkanResource) GetPhysicalDeviceSurfacePresentModes(physicalDevice PhysicalDevice, surface Surface) ([]PresentMode, error) {
  return GetPhysicalDeviceSurfacePresentModes(d.mem, physicalDevice, surface)
}

func (d *defaultVulkanResource) CreateSwapchain(device Device, pCreateInfo *SwapchainCreateInfo) (Swapchain, error) {
  return CreateSwapchain(d.mem, device, pCreateInfo)
}

func (d *defaultVulkanResource) GetSwapchainImages(device Device, swapchain Swapchain) ([]Image, error) {
  return GetSwapchainImages(d.mem, device, swapchain)
}

func (d *defaultVulkanResource) DestroySwapchain(device Device, swapchain Swapchain) {
  DestroySwapchain(d.mem, device, swapchain)
}

func (d *defaultVulkanResource) DestroyImageView(device Device, imageView ImageView) {
  DestroyImageView(d.mem, device, imageView)
}

func (d *defaultVulkanResource) CreateImageView(device Device, pCreateInfo *ImageViewCreateInfo) (ImageView, error) {
  return CreateImageView(d.mem, device, pCreateInfo)
}

func (d *defaultVulkanResource) CreateShaderModule(device Device, createInfo *ShaderModuleCreateInfo) (ShaderModule, error) {
  return CreateShaderModule(d.mem, device, createInfo)
}

func (d *defaultVulkanResource) DestroyShaderModule(device Device, shaderModule ShaderModule) {
  DestroyShaderModule(d.mem, device, shaderModule)
}

func (d *defaultVulkanResource) CreatePipelineLayout(device Device, createInfo *PipelineLayoutCreateInfo) (PipelineLayout, error) {
  return CreatePipelineLayout(d.mem, device, createInfo)
}

func (d *defaultVulkanResource) DestroyPipelineLayout(device Device, pipelineLayout PipelineLayout) {
  DestroyPipelineLayout(d.mem, device, pipelineLayout)
}

func (d *defaultVulkanResource) CreateRenderPass(device Device, createInfo *RenderPassCreateInfo) (RenderPass, error) {
  return CreateRenderPass(d.mem, device, createInfo)
}

func (d *defaultVulkanResource) DestroyRenderPass(device Device, renderPass RenderPass) {
  DestroyRenderPass(d.mem, device, renderPass)
}

func (d *defaultVulkanResource) CreateFramebuffer(device Device, createInfo *FramebufferCreateInfo) (Framebuffer, error) {
  return CreateFramebuffer(d.mem, device, createInfo)
}

func (d *defaultVulkanResource) DestroyFramebuffer(device Device, framebuffer Framebuffer) {
  DestroyFramebuffer(d.mem, device, framebuffer)
}

func (d *defaultVulkanResource) CreateCommandPool(device Device, pCreateInfo *CommandPoolCreateInfo) (CommandPool, error) {
  return CreateCommandPool(d.mem, device, pCreateInfo)
}

func (d *defaultVulkanResource) DestroyCommandPool(device Device, commandPool CommandPool) {
  DestroyCommandPool(d.mem, device, commandPool)
}

func (d *defaultVulkanResource) AllocateCommandBuffers(device Device, allocateInfo *CommandBufferAllocateInfo) ([]CommandBuffer, error) {
  return AllocateCommandBuffers(d.mem, device, allocateInfo)
}

func (d *defaultVulkanResource) FreeCommandBuffers(device Device, commandPool CommandPool, commandBuffers []CommandBuffer) {
  FreeCommandBuffers(d.mem, device, commandPool, commandBuffers)
}

func (d *defaultVulkanResource) BeginCommandBuffer(commandBuffer CommandBuffer, beginInfo *CommandBufferBeginInfo) error {
  return BeginCommandBuffer(d.mem, commandBuffer, beginInfo)
}

func (d *defaultVulkanResource) EndCommandBuffer(commandBuffer CommandBuffer) error {
  return EndCommandBuffer(d.mem, commandBuffer)
}

func (d *defaultVulkanResource) CmdBeginRenderPass(commandBuffer CommandBuffer, renderPassBegin *RenderPassBeginInfo, contents SubpassContents) {
  CmdBeginRenderPass(d.mem, commandBuffer, renderPassBegin, contents)
}

func (d *defaultVulkanResource) CmdBindPipeline(commandBuffer CommandBuffer, pipelineBindPoint PipelineBindPoint, pipeline Pipeline) {
  CmdBindPipeline(d.mem, commandBuffer, pipelineBindPoint, pipeline)
}

func (d *defaultVulkanResource) CmdDraw(commandBuffer CommandBuffer, vertexCount uint32, instanceCount uint32, firstVertex uint32, firstInstance uint32) {
  CmdDraw(d.mem, commandBuffer, vertexCount, instanceCount, firstVertex, firstInstance)
}

func (d *defaultVulkanResource) CmdEndRenderPass(commandBuffer CommandBuffer) {
  CmdEndRenderPass(d.mem, commandBuffer)
}

func (d *defaultVulkanResource) CreateGraphicsPipelines(device Device, pipelineCache PipelineCache, createInfos []GraphicsPipelineCreateInfo) ([]Pipeline, error) {
  return CreateGraphicsPipelines(d.mem, device, pipelineCache, createInfos)
}

func (d *defaultVulkanResource) DestroyPipeline(device Device, pipeline Pipeline) {
  DestroyPipeline(d.mem, device, pipeline)
}

func (d *defaultVulkanResource) CreateSemaphore(device Device, createInfo *SemaphoreCreateInfo) (Semaphore, error) {
  return CreateSemaphore(d.mem, device, createInfo)
}

func (d *defaultVulkanResource) DestroySemaphore(device Device, semaphore Semaphore) {
  DestroySemaphore(d.mem, device, semaphore)
}

func (d *defaultVulkanResource) AcquireNextImage(device Device, swapchain Swapchain, timeout uint64, semaphore Semaphore, fence Fence) (uint32, error) {
  return AcquireNextImage(d.mem, device, swapchain, timeout, semaphore, fence)
}

func (d *defaultVulkanResource) QueueSubmit(queue Queue, submits []SubmitInfo, fence Fence) error {
  return QueueSubmit(d.mem, queue, submits, fence)
}

func (d *defaultVulkanResource) CreateFence(device Device, createInfo *FenceCreateInfo) (Fence, error) {
  return CreateFence(d.mem, device, createInfo)
}

func (d *defaultVulkanResource) DestroyFence(device Device, fence Fence) {
  DestroyFence(d.mem, device, fence)
}

func (d *defaultVulkanResource) WaitForFences(device Device, fences []Fence, waitAll bool, timeout uint64) error {
  return WaitForFences(d.mem, device, fences, waitAll, timeout)
}

func (d *defaultVulkanResource) ResetFences(device Device, fences []Fence) error {
  return ResetFences(d.mem, device, fences)
}

func (d *defaultVulkanResource) QueuePresent(queue Queue, presentInfo *PresentInfo) error {
  return QueuePresent(d.mem, queue, presentInfo)
}

func (d *defaultVulkanResource) DeviceWaitIdle(device Device) error {
  return DeviceWaitIdle(d.mem, device)
}

func (d *defaultVulkanResource) CreateBuffer(device Device, createInfo *BufferCreateInfo) (Buffer, error) {
  return CreateBuffer(d.mem , device, createInfo)
}

func (d *defaultVulkanResource) DestroyBuffer(device Device, buffer Buffer) {
  DestroyBuffer(d.mem, device, buffer)
}

func (d *defaultVulkanResource) GetBufferMemoryRequirements(device Device, buffer Buffer) MemoryRequirements {
  return GetBufferMemoryRequirements(d.mem, device, buffer)
}

func (d *defaultVulkanResource) GetPhysicalDeviceMemoryProperties(physicalDevice PhysicalDevice) PhysicalDeviceMemoryProperties {
  return GetPhysicalDeviceMemoryProperties(d.mem, physicalDevice)
}

func (d *defaultVulkanResource) AllocateMemory(device Device, allocateInfo *MemoryAllocateInfo) (DeviceMemory, error) {
  return AllocateMemory(d.mem, device, allocateInfo)
}

func (d *defaultVulkanResource) FreeMemory(device Device, memory DeviceMemory) {
  FreeMemory(d.mem, device, memory)
}

func (d *defaultVulkanResource) MapMemory(device Device, memory DeviceMemory, offset DeviceSize, size DeviceSize, flags MemoryMapFlags) (unsafe.Pointer, error) {
  return MapMemory(d.mem, device, memory, offset, size, flags)
}

func (d *defaultVulkanResource) UnmapMemory(device Device, memory DeviceMemory) {
  UnmapMemory(d.mem, device, memory)
}

func (d *defaultVulkanResource) BindBufferMemory(device Device, buffer Buffer, memory DeviceMemory, memoryOffset DeviceSize) error {
  return BindBufferMemory(d.mem, device, buffer, memory, memoryOffset)
}

func (d *defaultVulkanResource) CmdBindVertexBuffers(commandBuffer CommandBuffer, firstBinding uint32, bindingCount uint32, buffers []Buffer, offsets []DeviceSize) {
  CmdBindVertexBuffers(d.mem, commandBuffer, firstBinding, bindingCount, buffers, offsets)
}

func NewVuklanResource(mem VulkanClientMemory) VulkanResource {
  return &defaultVulkanResource{mem: mem}
}
