#include <string.h>
#include <stdint.h>
#include <stdlib.h>


typedef struct arena_chunk {
  struct arena_chunk *prev;
  struct arena_chunk *next;
  uint32_t remaining;
  char *start;
  char *high_waterline;
  char bytes[0];
} arena_chunk;

typedef struct arena {
  uint32_t chunk_size;
  uint32_t max_available;
  arena_chunk *head;
  arena_chunk *current;
} arena;

typedef struct arena_mark {
  arena_chunk *chunk;
  char *high_waterline;
} arena_mark;


#define ROUND_UP_OFFSET(base) ((base % 8) == 0 ? base : (base + (8 - (base % 8))))

static const size_t arena_chunk_header_size = ROUND_UP_OFFSET(sizeof(arena_chunk));
static size_t round_up_offset(size_t base) { return ROUND_UP_OFFSET(base); }

static arena_chunk *arena_alloc_chunk(size_t chunk_size) {
  arena_chunk *chunk = (arena_chunk *)calloc(1, chunk_size);
  char *bytes_start = (char *)round_up_offset((size_t)chunk->bytes);
  uint32_t remaining = chunk_size - (size_t)(bytes_start - (char *)chunk);
  chunk->remaining = remaining;
  chunk->high_waterline = chunk->start = bytes_start;
  return chunk;
}

arena *arena_new(uint32_t chunk_size) {
  size_t full_chunk_size = round_up_offset(arena_chunk_header_size + chunk_size);
  arena_chunk *first_chunk = arena_alloc_chunk(full_chunk_size);
  arena *a = (arena *)calloc(1, sizeof(arena));
  a->chunk_size = full_chunk_size;
  a->max_available = chunk_size;
  a->current = a->head = first_chunk;
  return a;
}

static void *arena_chunk_alloc(arena_chunk *c, size_t adjusted_size) {
  void *addr = c->high_waterline;
  c->high_waterline += adjusted_size;
  size_t bytes_to_clear = (size_t)(c->high_waterline - ((char *)addr));
  memset(addr, 0, bytes_to_clear);
  c->remaining -= adjusted_size;
  return addr;  
}

void *arena_alloc(arena *arena, size_t amount) {
  size_t adjusted_size = round_up_offset(amount);
  if (adjusted_size > arena->max_available) return NULL;
  
  if (arena->current->remaining >= adjusted_size) {
    return arena_chunk_alloc(arena->current, adjusted_size);
  } else {
    if (arena->current->next != NULL) {
      arena->current = arena->current->next;
      return arena_chunk_alloc(arena->current, adjusted_size);
    } else {
      arena_chunk *new_chunk = arena_alloc_chunk(arena->chunk_size);
      new_chunk->prev = arena->current;
      arena->current->next = new_chunk;
      arena->current = new_chunk;
      return arena_chunk_alloc(arena->current, adjusted_size);
    }
  }
}

void arena_destroy(arena *arena) {
  for (arena_chunk *c = arena->head; c != NULL; ) {
    arena_chunk *next = c->next;
    free(c);
    c = next;
  }
  free(arena);
}


void arena_record_mark(arena *arena, arena_mark *mark) {
  mark->chunk = arena->current;
  mark->high_waterline = arena->current->high_waterline;
}

arena_mark *arena_record_mark_new(arena *arena) {
  arena_mark m;
  arena_record_mark(arena, &m);
  arena_mark *mp = arena_alloc(arena, sizeof(arena_mark));
  mp->chunk = m.chunk;
  mp->high_waterline = m.high_waterline;
  return mp;
}

void arena_free_to_mark(arena *arena, arena_mark *mark) {
  arena_chunk *c = arena->current;
  while (c != mark->chunk) {
    c->remaining += (((char*)c->high_waterline) - ((char*)c->start));
    c->high_waterline = c->start;
    c = c->prev;
  }
  c->remaining += (c->high_waterline - mark->high_waterline);
  c->high_waterline = mark->high_waterline;
  arena->current = mark->chunk;
}

#if 0
int main(int argc, char **argv) {
  arena *a = arena_new(4096);
  arena_mark m;
  arena_record_mark(a, &m);
  for (int i = 0; i < 62; i++) {
    void *v00 = arena_alloc(a, 8);
  }
  for (int i = 0; i < 30; i++) {
    void *v1 = arena_alloc(a, 200);
  }
  arena_mark m1;
  arena_record_mark(a, &m1);
  for (int i = 0; i < 30; i++) {
    void *v2 = arena_alloc(a, 300);
  }
  arena_free_to_mark(a, &m1);
  void *v3 = arena_alloc(a, 4096);
  arena_free_to_mark(a, &m);
  
  arena_destroy(a);
  return 0;
}
#endif
