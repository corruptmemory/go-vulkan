package go_vulkan

/*
#cgo CFLAGS: -I. -DVK_NO_PROTOTYPES
#cgo LDFLAGS: -ldl
#include <xcb/xcb.h>
#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>
#include "vulkan-includes/vulkan.h"
#include "vulkan-includes/vulkan_xcb.h"
#include "vulkan-includes/vulkan_xlib.h"
#include "vulkan-includes/vulkan_xlib_xrandr.h"
#include "vulkan-includes/vulkan_wayland.h"
#include "vk_wrapper.h"
#include "vk_bridge.h"
#include <stdlib.h>
*/
import "C"

import (
	"unsafe"
)

// CreateInstance function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkCreateInstance.html
func CreateInstance(mem VulkanClientMemory, pCreateInfo *InstanceCreateInfo) (instance Instance, err error) {
	defer mem.FreeToMark()
	mem.Mark()
	var vkici C.VkInstanceCreateInfo
	pCreateInfo.To(mem, &vkici)
	var cInstance C.VkInstance
	err = VkSuccess(Result(C.callVkCreateInstance(&vkici, nil, &cInstance)))
	if err != nil {
		return
	}
	instance = Instance(cInstance)
	return
}

// DestroyInstance function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkDestroyInstance.html
func DestroyInstance(mem VulkanClientMemory, instance Instance) {
	C.callVkDestroyInstance(instance, nil)
}

// EnumeratePhysicalDevices function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkEnumeratePhysicalDevices.html
func EnumeratePhysicalDevices(mem VulkanClientMemory, instance Instance) ([]PhysicalDevice, error) {
	var cpPhysicalDeviceCount C.uint
	err := VkSuccess(Result(C.callVkEnumeratePhysicalDevices(instance, &cpPhysicalDeviceCount, nil)), Incomplete)
	if err != nil {
		return nil, err
	}
	if cpPhysicalDeviceCount > 0 {
		devices := make([]PhysicalDevice, cpPhysicalDeviceCount)
		err = VkSuccess(Result(C.callVkEnumeratePhysicalDevices(instance, &cpPhysicalDeviceCount, (*C.VkPhysicalDevice)(unsafe.Pointer(&devices[0])))))
		if err != nil {
			return nil, err
		}
		return ([]PhysicalDevice)(devices), nil
	}
	return nil, nil
}

// EnumerateInstanceLayerProperties function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkEnumerateInstanceLayerProperties.html
func EnumerateInstanceLayerProperties(mem VulkanClientMemory) ([]LayerProperties, error) {
	var propertyCount C.uint32_t
	err := VkSuccess(Result(C.callVkEnumerateInstanceLayerProperties(&propertyCount, nil)), Incomplete)
	if err != nil {
		return nil, err
	}
	if propertyCount > 0 {
		properties := make([]C.VkLayerProperties, propertyCount)
		err = VkSuccess(Result(C.callVkEnumerateInstanceLayerProperties(&propertyCount, (*C.VkLayerProperties)(unsafe.Pointer(&properties[0])))))
		if err != nil {
			return nil, err
		}
		rs := make([]LayerProperties, propertyCount)
		for i := range properties {
			rs[i].From(&properties[i])
		}
		return rs, nil
	}
	return nil, nil
}

// EnumerateInstanceExtensionProperties function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkEnumerateInstanceExtensionProperties.html
func EnumerateInstanceExtensionProperties(mem VulkanClientMemory, layerName string) ([]ExtensionProperties, error) {
	defer mem.FreeToMark()
	mem.Mark()
	var ln *C.char
	if len(layerName) > 0 {
		ln = mem.CString(layerName)
	}
	var cPropertyCount C.uint32_t
	err := VkSuccess(Result(C.callVkEnumerateInstanceExtensionProperties(ln, &cPropertyCount, nil)))
	if err != nil {
		return nil, err
	}
	if cPropertyCount > 0 {
		r := make([]C.VkExtensionProperties, cPropertyCount)
		err := VkSuccess(Result(C.callVkEnumerateInstanceExtensionProperties(ln, &cPropertyCount, (*C.VkExtensionProperties)(unsafe.Pointer(&r[0])))))
		if err != nil {
			return nil, err
		}
		rn := make([]ExtensionProperties, cPropertyCount)
		for i, e := range r {
			rn[i].From(&e)
		}
		return rn, nil
	}
	return nil, nil
}

// GetPhysicalDeviceProperties function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkGetPhysicalDeviceProperties.html
func GetPhysicalDeviceProperties(mem VulkanClientMemory, physicalDevice PhysicalDevice) PhysicalDeviceProperties {
	var temp C.VkPhysicalDeviceProperties
	C.callVkGetPhysicalDeviceProperties(C.VkPhysicalDevice(physicalDevice), &temp)
	var pd PhysicalDeviceProperties
	pd.From(&temp)
	return pd
}

// GetPhysicalDeviceQueueFamilyProperties function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkGetPhysicalDeviceQueueFamilyProperties.html
func GetPhysicalDeviceQueueFamilyProperties(mem VulkanClientMemory, physicalDevice PhysicalDevice) ([]QueueFamilyProperties, error) {
	var count C.uint32_t
	C.callVkGetPhysicalDeviceQueueFamilyProperties(C.VkPhysicalDevice(physicalDevice), &count, nil)
	if count > 0 {
		properties := make([]C.VkQueueFamilyProperties, count)
		C.callVkGetPhysicalDeviceQueueFamilyProperties(C.VkPhysicalDevice(physicalDevice), &count, (*C.VkQueueFamilyProperties)(unsafe.Pointer(&properties[0])))
		rs := make([]QueueFamilyProperties, count)
		for i := range properties {
			rs[i].From(&properties[i])
		}
		return rs, nil
	}
	return nil, nil
}

// CreateDevice function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkCreateDevice.html
func CreateDevice(mem VulkanClientMemory, physicalDevice PhysicalDevice, createInfo *DeviceCreateInfo) (Device, error) {
	defer mem.FreeToMark()
	mem.Mark()
	var cCreateInfo C.VkDeviceCreateInfo
	createInfo.To(mem, &cCreateInfo)
	var device C.VkDevice
	err := VkSuccess(Result(C.callVkCreateDevice(C.VkPhysicalDevice(physicalDevice), &cCreateInfo, nil, &device)))
	if err != nil {
		return nil, err
	}
	return Device(device), nil
}

// GetDeviceQueue function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkGetDeviceQueue.html
func GetDeviceQueue(mem VulkanClientMemory, device Device, queueFamilyIndex uint32, queueIndex uint32) Queue {
	var cQueue C.VkQueue
	C.callVkGetDeviceQueue((C.VkDevice)(device), (C.uint)(queueFamilyIndex), (C.uint)(queueIndex), &cQueue)
	return Queue(cQueue)
}

// DestroyDevice function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkDestroyDevice.html
func DestroyDevice(mem VulkanClientMemory, device Device) {
	C.callVkDestroyDevice((C.VkDevice)(device), nil)
}

// DestroySurface function as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDestroySurfaceKHR
func DestroySurface(mem VulkanClientMemory, instance Instance, surface Surface) {
	C.callVkDestroySurfaceKHR((C.VkInstance)(instance), (C.VkSurfaceKHR)(surface), nil)
}

// GetPhysicalDeviceSurfaceSupport function as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkGetPhysicalDeviceSurfaceSupportKHR
func GetPhysicalDeviceSurfaceSupport(mem VulkanClientMemory, physicalDevice PhysicalDevice, queueFamilyIndex uint32, surface Surface) (bool, error) {
	var cSupported C.VkBool32
	err := VkSuccess(Result(C.callVkGetPhysicalDeviceSurfaceSupportKHR(C.VkPhysicalDevice(physicalDevice), (C.uint)(queueFamilyIndex), surface, &cSupported)))
	if err != nil {
		return false, err
	}
	return cSupported == (C.uint)(True), nil
}

// EnumerateDeviceExtensionProperties function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkEnumerateDeviceExtensionProperties.html
func EnumerateDeviceExtensionProperties(mem VulkanClientMemory, physicalDevice PhysicalDevice, layerName string) ([]ExtensionProperties, error) {
	defer mem.FreeToMark()
	mem.Mark()
	var cPropertyCount C.uint32_t
	var pcLayerName *C.char
	if layerName != "" {
		pcLayerName = mem.CString(layerName)
	}
	err := VkSuccess(Result(C.callVkEnumerateDeviceExtensionProperties(C.VkPhysicalDevice(physicalDevice), pcLayerName, &cPropertyCount, nil)), Incomplete)
	if err != nil {
		return nil, err
	}
	if cPropertyCount == 0 {
		return nil, nil
	}
	// Questionable
	ep := make([]C.VkExtensionProperties, cPropertyCount)
	err = VkSuccess(Result(C.callVkEnumerateDeviceExtensionProperties(C.VkPhysicalDevice(physicalDevice), pcLayerName, &cPropertyCount, (*C.VkExtensionProperties)(unsafe.Pointer(&ep[0])))))
	if err != nil {
		return nil, err
	}
	result := make([]ExtensionProperties, cPropertyCount)
	for i, e := range ep {
		result[i].From(&e)
	}
	return result, nil
}

// GetPhysicalDeviceSurfaceCapabilities function as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkGetPhysicalDeviceSurfaceCapabilitiesKHR
func GetPhysicalDeviceSurfaceCapabilities(mem VulkanClientMemory, physicalDevice PhysicalDevice, surface Surface) (SurfaceCapabilities, error) {
	var cSurfaceCapabilities C.VkSurfaceCapabilitiesKHR
	err := VkSuccess(Result(C.callVkGetPhysicalDeviceSurfaceCapabilitiesKHR(C.VkPhysicalDevice(physicalDevice), (C.VkSurfaceKHR)(surface), &cSurfaceCapabilities)))
	if err != nil {
		return SurfaceCapabilities{}, err
	}
	r := SurfaceCapabilities{}
	r.From(&cSurfaceCapabilities)
	return r, nil
}

// GetPhysicalDeviceSurfaceFormats function as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkGetPhysicalDeviceSurfaceFormatsKHR
func GetPhysicalDeviceSurfaceFormats(mem VulkanClientMemory, physicalDevice PhysicalDevice, surface Surface) ([]SurfaceFormat, error) {
	var cSurfaceFormatCount C.uint32_t
	err := VkSuccess(Result(C.callVkGetPhysicalDeviceSurfaceFormatsKHR(C.VkPhysicalDevice(physicalDevice), (C.VkSurfaceKHR)(surface), &cSurfaceFormatCount, nil)), Incomplete)
	if err != nil {
		return nil, err
	}
	sf := make([]C.VkSurfaceFormatKHR, cSurfaceFormatCount)
	err = VkSuccess(Result(C.callVkGetPhysicalDeviceSurfaceFormatsKHR(C.VkPhysicalDevice(physicalDevice), (C.VkSurfaceKHR)(surface), &cSurfaceFormatCount, (*C.VkSurfaceFormatKHR)(unsafe.Pointer(&sf[0])))))
	if err != nil {
		return nil, err
	}
	result := make([]SurfaceFormat, cSurfaceFormatCount)
	for i, e := range sf {
		result[i].From(&e)
	}
	return result, nil
}

// GetPhysicalDeviceSurfacePresentModes function as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkGetPhysicalDeviceSurfacePresentModesKHR
func GetPhysicalDeviceSurfacePresentModes(mem VulkanClientMemory, physicalDevice PhysicalDevice, surface Surface) ([]PresentMode, error) {
	var cPresentModeCount C.uint32_t
	err := VkSuccess(Result(C.callVkGetPhysicalDeviceSurfacePresentModesKHR(C.VkPhysicalDevice(physicalDevice), (C.VkSurfaceKHR)(surface), &cPresentModeCount, nil)), Incomplete)
	if err != nil {
		return nil, err
	}
	pm := make([]C.VkPresentModeKHR, cPresentModeCount)
	err = VkSuccess(Result(C.callVkGetPhysicalDeviceSurfacePresentModesKHR(C.VkPhysicalDevice(physicalDevice), (C.VkSurfaceKHR)(surface), &cPresentModeCount, (*C.VkPresentModeKHR)(unsafe.Pointer(&pm[0])))))
	if err != nil {
		return nil, err
	}
	result := make([]PresentMode, cPresentModeCount)
	for i, e := range pm {
		result[i] = PresentMode(e)
	}
	return result, nil
}

// CreateSwapchain function as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkCreateSwapchainKHR
func CreateSwapchain(mem VulkanClientMemory, device Device, pCreateInfo *SwapchainCreateInfo) (Swapchain, error) {
	defer mem.FreeToMark()
	mem.Mark()
	var cCreateInfo C.VkSwapchainCreateInfoKHR
	var cSwapchain C.VkSwapchainKHR
	err := pCreateInfo.To(mem, &cCreateInfo)
	if err != nil {
		return nil, err
	}
	err = VkSuccess(Result(C.callVkCreateSwapchainKHR(C.VkDevice(device), &cCreateInfo, nil, &cSwapchain)))
	if err != nil {
		return nil, err
	}
	return Swapchain(cSwapchain), nil
}

// GetSwapchainImages function as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkGetSwapchainImagesKHR
func GetSwapchainImages(mem VulkanClientMemory, device Device, swapchain Swapchain) ([]Image, error) {
	var cSwapchainImageCount C.uint32_t
	err := VkSuccess(Result(C.callVkGetSwapchainImagesKHR(C.VkDevice(device), C.VkSwapchainKHR(swapchain), &cSwapchainImageCount, nil)), Incomplete)
	if err != nil {
		return nil, err
	}
	cImage := make([]C.VkImage, cSwapchainImageCount)
	err = VkSuccess(Result(C.callVkGetSwapchainImagesKHR(C.VkDevice(device), C.VkSwapchainKHR(swapchain), &cSwapchainImageCount, (*C.VkImage)(unsafe.Pointer(&cImage[0])))))
	if err != nil {
		return nil, err
	}
	result := make([]Image, cSwapchainImageCount)
	for i := range cImage {
		result[i] = Image(cImage[i])
	}
	return result, nil
}

// DestroySwapchain function as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDestroySwapchainKHR
func DestroySwapchain(mem VulkanClientMemory, device Device, swapchain Swapchain) {
	C.callVkDestroySwapchainKHR(C.VkDevice(device), C.VkSwapchainKHR(swapchain), nil)
}

// DestroyImageView function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkDestroyImageView.html
func DestroyImageView(mem VulkanClientMemory, device Device, imageView ImageView) {
	C.callVkDestroyImageView(C.VkDevice(device), (C.VkImageView)(imageView), nil)
}

// CreateImageView function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkCreateImageView.html
func CreateImageView(mem VulkanClientMemory, device Device, pCreateInfo *ImageViewCreateInfo) (ImageView, error) {
	defer mem.FreeToMark()
	mem.Mark()
	var cCreateInfo C.VkImageViewCreateInfo
	var cView C.VkImageView
	pCreateInfo.To(mem, &cCreateInfo)
	err := VkSuccess(Result(C.callVkCreateImageView(C.VkDevice(device), &cCreateInfo, nil, &cView)))
	if err != nil {
		return nil, err
	}
	return ImageView(cView), nil
}

// CreateShaderModule function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkCreateShaderModule.html
func CreateShaderModule(mem VulkanClientMemory, device Device, createInfo *ShaderModuleCreateInfo) (ShaderModule, error) {
	defer mem.FreeToMark()
	mem.Mark()
	var cCreateInfo C.VkShaderModuleCreateInfo
	err := createInfo.To(mem, &cCreateInfo)
	if err != nil {
		return nil, err
	}
	var cShaderModule C.VkShaderModule
	err = VkSuccess(Result(C.callVkCreateShaderModule(C.VkDevice(device), &cCreateInfo, nil, &cShaderModule)))
	if err != nil {
		return nil, err
	}
	return ShaderModule(cShaderModule), nil
}

// DestroyShaderModule function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkDestroyShaderModule.html
func DestroyShaderModule(mem VulkanClientMemory, device Device, shaderModule ShaderModule) {
	C.callVkDestroyShaderModule(C.VkDevice(device), C.VkShaderModule(shaderModule), nil)
}

// CreatePipelineLayout function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkCreatePipelineLayout.html
func CreatePipelineLayout(mem VulkanClientMemory, device Device, createInfo *PipelineLayoutCreateInfo) (PipelineLayout, error) {
	defer mem.FreeToMark()
	mem.Mark()
	var cCreateInfo C.VkPipelineLayoutCreateInfo
	err := createInfo.To(mem, &cCreateInfo)
	if err != nil {
		return nil, err
	}
	var cPipelineLayout C.VkPipelineLayout
	err = VkSuccess(Result(C.callVkCreatePipelineLayout(C.VkDevice(device), &cCreateInfo, nil, &cPipelineLayout)))
	if err != nil {
		return nil, err
	}
	return PipelineLayout(cPipelineLayout), nil
}

// DestroyPipelineLayout function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkDestroyPipelineLayout.html
func DestroyPipelineLayout(mem VulkanClientMemory, device Device, pipelineLayout PipelineLayout) {
	C.callVkDestroyPipelineLayout(C.VkDevice(device), C.VkPipelineLayout(pipelineLayout), nil)
}

// CreateRenderPass function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkCreateRenderPass.html
func CreateRenderPass(mem VulkanClientMemory, device Device, createInfo *RenderPassCreateInfo) (RenderPass, error) {
	defer mem.FreeToMark()
	mem.Mark()
	var cRenderPass C.VkRenderPass
	var cCreateInfo C.VkRenderPassCreateInfo
	err := createInfo.To(mem, &cCreateInfo)
	if err != nil {
		return nil, err
	}
	err = VkSuccess(Result(C.callVkCreateRenderPass(C.VkDevice(device), &cCreateInfo, nil, &cRenderPass)))
	if err != nil {
		return nil, err
	}
	return RenderPass(cRenderPass), nil
}

// DestroyRenderPass function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkDestroyRenderPass.html
func DestroyRenderPass(mem VulkanClientMemory, device Device, renderPass RenderPass) {
	C.callVkDestroyRenderPass(C.VkDevice(device), C.VkRenderPass(renderPass), nil)
}

// CreateFramebuffer function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkCreateFramebuffer.html
func CreateFramebuffer(mem VulkanClientMemory, device Device, createInfo *FramebufferCreateInfo) (Framebuffer, error) {
	defer mem.FreeToMark()
	mem.Mark()
	var cFramebuffer C.VkFramebuffer
	var cCreateInfo C.VkFramebufferCreateInfo
	err := createInfo.To(mem, &cCreateInfo)
	if err != nil {
		return nil, err
	}
	err = VkSuccess(Result(C.callVkCreateFramebuffer(C.VkDevice(device), &cCreateInfo, nil, &cFramebuffer)))
	if err != nil {
		return nil, err
	}
	return Framebuffer(cFramebuffer), nil
}

// DestroyFramebuffer function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkDestroyFramebuffer.html
func DestroyFramebuffer(mem VulkanClientMemory, device Device, framebuffer Framebuffer) {
	C.callVkDestroyFramebuffer(C.VkDevice(device), C.VkFramebuffer(framebuffer), nil)
}

// CreateCommandPool function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkCreateCommandPool.html
func CreateCommandPool(mem VulkanClientMemory, device Device, pCreateInfo *CommandPoolCreateInfo) (CommandPool, error) {
	defer mem.FreeToMark()
	mem.Mark()
	var cCommandPool C.VkCommandPool
	var cCreateInfo C.VkCommandPoolCreateInfo
	pCreateInfo.To(mem, &cCreateInfo)
	err := VkSuccess(Result(C.callVkCreateCommandPool(C.VkDevice(device), &cCreateInfo, nil, &cCommandPool)))
	if err != nil {
		return nil, err
	}
	return CommandPool(cCommandPool), nil
}

// DestroyCommandPool function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkDestroyCommandPool.html
func DestroyCommandPool(mem VulkanClientMemory, device Device, commandPool CommandPool) {
	C.callVkDestroyCommandPool(C.VkDevice(device), C.VkCommandPool(commandPool), nil)
}

// AllocateCommandBuffers function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkAllocateCommandBuffers.html
func AllocateCommandBuffers(mem VulkanClientMemory, device Device, allocateInfo *CommandBufferAllocateInfo) ([]CommandBuffer, error) {
	defer mem.FreeToMark()
	mem.Mark()
	var cAllocateInfo C.VkCommandBufferAllocateInfo
	cCommandBuffers := make([]C.VkCommandBuffer, allocateInfo.CommandBufferCount)
	allocateInfo.To(mem, &cAllocateInfo)
	err := VkSuccess(Result(C.callVkAllocateCommandBuffers(C.VkDevice(device), &cAllocateInfo, (*C.VkCommandBuffer)(unsafe.Pointer(&cCommandBuffers[0])))))
	if err != nil {
		return nil, err
	}
	r := make([]CommandBuffer, len(cCommandBuffers))
	for i, c := range cCommandBuffers {
		r[i] = CommandBuffer(c)
	}
	return r, nil
}

// FreeCommandBuffers function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkFreeCommandBuffers.html
func FreeCommandBuffers(mem VulkanClientMemory, device Device, commandPool CommandPool, commandBuffers []CommandBuffer) {
	C.callVkFreeCommandBuffers(C.VkDevice(device), C.VkCommandPool(commandPool), C.uint32_t(len(commandBuffers)), (*C.VkCommandBuffer)(unsafe.Pointer(&commandBuffers[0])))
}

// BeginCommandBuffer function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkBeginCommandBuffer.html
func BeginCommandBuffer(mem VulkanClientMemory, commandBuffer CommandBuffer, beginInfo *CommandBufferBeginInfo) error {
	defer mem.FreeToMark()
	mem.Mark()
	var cBeginInfo C.VkCommandBufferBeginInfo
	beginInfo.To(mem, &cBeginInfo)
	return VkSuccess(Result(C.callVkBeginCommandBuffer(C.VkCommandBuffer(commandBuffer), &cBeginInfo)))
}

// EndCommandBuffer function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkEndCommandBuffer.html
func EndCommandBuffer(mem VulkanClientMemory, commandBuffer CommandBuffer) error {
	return VkSuccess(Result(C.callVkEndCommandBuffer(C.VkCommandBuffer(commandBuffer))))
}

// CmdBeginRenderPass function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkCmdBeginRenderPass.html
func CmdBeginRenderPass(mem VulkanClientMemory, commandBuffer CommandBuffer, renderPassBegin *RenderPassBeginInfo, contents SubpassContents) {
	defer mem.FreeToMark()
	mem.Mark()
	var cRenderPassBegin C.VkRenderPassBeginInfo
	renderPassBegin.To(mem, &cRenderPassBegin)
	C.callVkCmdBeginRenderPass(C.VkCommandBuffer(commandBuffer), &cRenderPassBegin, C.VkSubpassContents(contents))
}

// CmdBindPipeline function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkCmdBindPipeline.html
func CmdBindPipeline(mem VulkanClientMemory, commandBuffer CommandBuffer, pipelineBindPoint PipelineBindPoint, pipeline Pipeline) {
	C.callVkCmdBindPipeline(C.VkCommandBuffer(commandBuffer), C.VkPipelineBindPoint(pipelineBindPoint), C.VkPipeline(pipeline))
}

// CmdDraw function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkCmdDraw.html
func CmdDraw(mem VulkanClientMemory, commandBuffer CommandBuffer, vertexCount uint32, instanceCount uint32, firstVertex uint32, firstInstance uint32) {
	C.callVkCmdDraw(C.VkCommandBuffer(commandBuffer), C.uint32_t(vertexCount), C.uint32_t(instanceCount), C.uint32_t(firstVertex), C.uint32_t(firstInstance))
}

// CmdEndRenderPass function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkCmdEndRenderPass.html
func CmdEndRenderPass(mem VulkanClientMemory, commandBuffer CommandBuffer) {
	C.callVkCmdEndRenderPass(C.VkCommandBuffer(commandBuffer))
}

// CreateGraphicsPipelines function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkCreateGraphicsPipelines.html
func CreateGraphicsPipelines(mem VulkanClientMemory, device Device, pipelineCache PipelineCache, createInfos []GraphicsPipelineCreateInfo) ([]Pipeline, error) {
	defer mem.FreeToMark()
	mem.Mark()
	cCreateInfos := make([]C.VkGraphicsPipelineCreateInfo, len(createInfos))
	for i, ci := range createInfos {
		err := ci.To(mem, &cCreateInfos[i])
		if err != nil {
			return nil, err
		}
	}
	cPipelines := make([]C.VkPipeline, len(createInfos))
	err := VkSuccess(Result(C.callVkCreateGraphicsPipelines(C.VkDevice(device),
		C.VkPipelineCache(pipelineCache),
		C.uint32_t(len(createInfos)),
		(*C.VkGraphicsPipelineCreateInfo)(unsafe.Pointer(&cCreateInfos[0])),
		nil,
		(*C.VkPipeline)(unsafe.Pointer(&cPipelines[0])))))
	if err != nil {
		return nil, err
	}
	r := make([]Pipeline, len(createInfos))
	for i, p := range cPipelines {
		r[i] = Pipeline(p)
	}
	return r, nil
}

// DestroyPipeline function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkDestroyPipeline.html
func DestroyPipeline(mem VulkanClientMemory, device Device, pipeline Pipeline) {
	C.callVkDestroyPipeline(C.VkDevice(device), C.VkPipeline(pipeline), nil)
}

// CreateSemaphore function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkCreateSemaphore.html
func CreateSemaphore(mem VulkanClientMemory, device Device, createInfo *SemaphoreCreateInfo) (Semaphore, error) {
	defer mem.FreeToMark()
	mem.Mark()
	var cSemaphore C.VkSemaphore
	var cCreateInfo C.VkSemaphoreCreateInfo
	createInfo.To(mem, &cCreateInfo)
	err := VkSuccess(Result(C.callVkCreateSemaphore(C.VkDevice(device), &cCreateInfo, nil, &cSemaphore)))
	if err != nil {
		return nil, err
	}
	return Semaphore(cSemaphore), nil
}

// DestroySemaphore function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkDestroySemaphore.html
func DestroySemaphore(mem VulkanClientMemory, device Device, semaphore Semaphore) {
	C.callVkDestroySemaphore(C.VkDevice(device), C.VkSemaphore(semaphore), nil)
}

// AcquireNextImage function as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAcquireNextImageKHR
func AcquireNextImage(mem VulkanClientMemory, device Device, swapchain Swapchain, timeout uint64, semaphore Semaphore, fence Fence) (uint32, error) {
	var cImageIndex C.uint32_t
	err := VkSuccess(Result(C.callVkAcquireNextImageKHR(C.VkDevice(device), C.VkSwapchainKHR(swapchain), C.uint64_t(timeout), C.VkSemaphore(semaphore), C.VkFence(fence), &cImageIndex)))
	if err != nil {
		return 0, err
	}
	return uint32(cImageIndex), nil
}

// QueueSubmit function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkQueueSubmit.html
func QueueSubmit(mem VulkanClientMemory, queue Queue, submits []SubmitInfo, fence Fence) error {
	defer mem.FreeToMark()
	mem.Mark()
	submitCount := len(submits)
	cSubmits := make([]C.VkSubmitInfo, len(submits))
	for i, s := range submits {
		err := s.To(mem, &cSubmits[i])
		if err != nil {
			return err
		}
	}
	return VkSuccess(Result(C.callVkQueueSubmit(C.VkQueue(queue), C.uint32_t(submitCount), (*C.VkSubmitInfo)(unsafe.Pointer(&cSubmits[0])), C.VkFence(fence))))
}

// CreateFence function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkCreateFence.html
func CreateFence(mem VulkanClientMemory, device Device, createInfo *FenceCreateInfo) (Fence, error) {
	defer mem.FreeToMark()
	mem.Mark()
	var cCreateInfo C.VkFenceCreateInfo
	createInfo.To(mem, &cCreateInfo)
	var cFence C.VkFence
	err := VkSuccess(Result(C.callVkCreateFence(C.VkDevice(device), &cCreateInfo, nil, &cFence)))
	if err != nil {
		return nil, err
	}
	return Fence(cFence), nil
}

// DestroyFence function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkDestroyFence.html
func DestroyFence(mem VulkanClientMemory, device Device, fence Fence) {
	C.callVkDestroyFence(C.VkDevice(device), C.VkFence(fence), nil)
}

// WaitForFences function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkWaitForFences.html
func WaitForFences(mem VulkanClientMemory, device Device, fences []Fence, waitAll bool, timeout uint64) error {
	cFences := make([]C.VkFence, len(fences))
	for i, f := range fences {
		cFences[i] = C.VkFence(f)
	}

	return VkSuccess(Result(C.callVkWaitForFences(C.VkDevice(device), C.uint32_t(len(fences)), (*C.VkFence)(unsafe.Pointer(&cFences[0])), ToBool32(waitAll), C.uint64_t(timeout))))
}

// ResetFences function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkResetFences.html
func ResetFences(mem VulkanClientMemory, device Device, fences []Fence) error {
	cFences := make([]C.VkFence, len(fences))
	for i, f := range fences {
		cFences[i] = C.VkFence(f)
	}

	return VkSuccess(Result(C.callVkResetFences(C.VkDevice(device), C.uint32_t(len(fences)), (*C.VkFence)(unsafe.Pointer(&cFences[0])))))
}

// QueuePresent function as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkQueuePresentKHR
func QueuePresent(mem VulkanClientMemory, queue Queue, presentInfo *PresentInfo) error {
	defer mem.FreeToMark()
	mem.Mark()
	var cPresentInfo C.VkPresentInfoKHR
	err := presentInfo.To(mem, &cPresentInfo)
	if err != nil {
		return err
	}
	return VkSuccess(Result(C.callVkQueuePresentKHR(C.VkQueue(queue), &cPresentInfo)))
}

// DeviceWaitIdle function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkDeviceWaitIdle.html
func DeviceWaitIdle(mem VulkanClientMemory, device Device) error {
	return VkSuccess(Result(C.callVkDeviceWaitIdle(C.VkDevice(device))))
}

// CreateBuffer function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkCreateBuffer.html
func CreateBuffer(mem VulkanClientMemory, device Device, createInfo *BufferCreateInfo) (Buffer, error) {
	defer mem.FreeToMark()
	mem.Mark()
	var cBufferCreateInfo C.VkBufferCreateInfo
	createInfo.To(mem, &cBufferCreateInfo)
	var cBuffer C.VkBuffer
	err := VkSuccess(Result(C.callVkCreateBuffer(C.VkDevice(device), &cBufferCreateInfo, nil, &cBuffer)))
	if err != nil {
		return nil, err
	}
	return (Buffer)(cBuffer),nil
}

// DestroyBuffer function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkDestroyBuffer.html
func DestroyBuffer(mem VulkanClientMemory, device Device, buffer Buffer) {
	C.callVkDestroyBuffer(C.VkDevice(device), C.VkBuffer(buffer), nil)
}

// GetBufferMemoryRequirements function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkGetBufferMemoryRequirements.html
func GetBufferMemoryRequirements(mem VulkanClientMemory, device Device, buffer Buffer) MemoryRequirements {
	var cMemoryRequirements C.VkMemoryRequirements
	C.callVkGetBufferMemoryRequirements(C.VkDevice(device), C.VkBuffer(buffer), &cMemoryRequirements)
	var r MemoryRequirements
	r.From(&cMemoryRequirements)
	return r
}

// GetPhysicalDeviceMemoryProperties function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkGetPhysicalDeviceMemoryProperties.html
func GetPhysicalDeviceMemoryProperties(mem VulkanClientMemory, physicalDevice PhysicalDevice) PhysicalDeviceMemoryProperties {
	var cMemoryProperties C.VkPhysicalDeviceMemoryProperties
	C.callVkGetPhysicalDeviceMemoryProperties(C.VkPhysicalDevice(physicalDevice), &cMemoryProperties)
	var r PhysicalDeviceMemoryProperties
	r.From(&cMemoryProperties)
	return r
}

// AllocateMemory function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkAllocateMemory.html
func AllocateMemory(mem VulkanClientMemory, device Device, allocateInfo *MemoryAllocateInfo) (DeviceMemory, error) {
	var cAllocateInfo C.VkMemoryAllocateInfo
	allocateInfo.To(&cAllocateInfo)
	var cMemory C.VkDeviceMemory
	err := VkSuccess(Result(C.callVkAllocateMemory(C.VkDevice(device), &cAllocateInfo, nil, &cMemory)))
	if err != nil {
		return nil, err
	}
	return DeviceMemory(cMemory), nil
}

// FreeMemory function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkFreeMemory.html
func FreeMemory(mem VulkanClientMemory, device Device, memory DeviceMemory) {
	C.callVkFreeMemory(C.VkDevice(device), C.VkDeviceMemory(memory), nil)
}

// MapMemory function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkMapMemory.html
func MapMemory(mem VulkanClientMemory, device Device, memory DeviceMemory, offset DeviceSize, size DeviceSize, flags MemoryMapFlags) (unsafe.Pointer, error) {
	var pData unsafe.Pointer
	err := VkSuccess(Result(C.callVkMapMemory(C.VkDevice(device), C.VkDeviceMemory(memory), C.VkDeviceSize(offset), C.VkDeviceSize(size), C.VkMemoryMapFlags(flags), &pData)))
	if err != nil {
		return nil, err
	}
	return pData, nil
}

// UnmapMemory function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkUnmapMemory.html
func UnmapMemory(mem VulkanClientMemory, device Device, memory DeviceMemory) {
	C.callVkUnmapMemory(C.VkDevice(device), C.VkDeviceMemory(memory))
}


// BindBufferMemory function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkBindBufferMemory.html
func BindBufferMemory(mem VulkanClientMemory, device Device, buffer Buffer, memory DeviceMemory, memoryOffset DeviceSize) error {
	return VkSuccess(Result(C.callVkBindBufferMemory(C.VkDevice(device), C.VkBuffer(buffer), C.VkDeviceMemory(memory), C.VkDeviceSize(memoryOffset))))
}

// CmdBindVertexBuffers function as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkCmdBindVertexBuffers.html
func CmdBindVertexBuffers(mem VulkanClientMemory, commandBuffer CommandBuffer, firstBinding uint32, bindingCount uint32, buffers []Buffer, offsets []DeviceSize) {
	cBuffers := make([]C.VkBuffer, len(buffers))
	cOffsets := make([]C.VkDeviceSize, len(offsets))
	for i := 0; i < len(buffers); i++ {
		cBuffers[i] = C.VkBuffer(buffers[i])
		cOffsets[i] = C.VkDeviceSize(offsets[i])
	}
	C.callVkCmdBindVertexBuffers(C.VkCommandBuffer(commandBuffer), C.uint32_t(firstBinding), C.uint32_t(bindingCount), (*C.VkBuffer)(unsafe.Pointer(&cBuffers[0])), (*C.VkDeviceSize)(unsafe.Pointer(&cOffsets[0])))
}
