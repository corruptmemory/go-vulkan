#include <stdint.h>
#include <stdlib.h>

typedef struct arena arena;
typedef struct arena_mark arena_mark;

extern arena *arena_new(uint32_t chunk_size);
extern void *arena_alloc(arena *arena, size_t amount);
extern void arena_destroy(arena *arena);
extern void arena_record_mark(arena *arena, arena_mark *mark);
extern arena_mark *arena_record_mark_new(arena *arena);
extern void arena_free_to_mark(arena *arena, arena_mark *mark);

