package go_vulkan

import (
	"fmt"
)

var (
	ErrNotReady                                 = fmt.Errorf("error: NotReady:%d", NotReady)
	ErrTimeout                                  = fmt.Errorf("error: Timeout:%d", Timeout)
	ErrEventSet                                 = fmt.Errorf("error: EventSet:%d", EventSet)
	ErrEventReset                               = fmt.Errorf("error: EventReset:%d", EventReset)
	ErrIncomplete                               = fmt.Errorf("error: Incomplete:%d", Incomplete)
	ErrErrorOutOfHostMemory                     = fmt.Errorf("error: ErrorOutOfHostMemory:%d", ErrorOutOfHostMemory)
	ErrErrorOutOfDeviceMemory                   = fmt.Errorf("error: ErrorOutOfDeviceMemory:%d", ErrorOutOfDeviceMemory)
	ErrErrorInitializationFailed                = fmt.Errorf("error: ErrorInitializationFailed:%d", ErrorInitializationFailed)
	ErrErrorDeviceLost                          = fmt.Errorf("error: ErrorDeviceLost:%d", ErrorDeviceLost)
	ErrErrorMemoryMapFailed                     = fmt.Errorf("error: ErrorMemoryMapFailed:%d", ErrorMemoryMapFailed)
	ErrErrorLayerNotPresent                     = fmt.Errorf("error: ErrorLayerNotPresent:%d", ErrorLayerNotPresent)
	ErrErrorExtensionNotPresent                 = fmt.Errorf("error: ErrorExtensionNotPresent:%d", ErrorExtensionNotPresent)
	ErrErrorFeatureNotPresent                   = fmt.Errorf("error: ErrorFeatureNotPresent:%d", ErrorFeatureNotPresent)
	ErrErrorIncompatibleDriver                  = fmt.Errorf("error: ErrorIncompatibleDriver:%d", ErrorIncompatibleDriver)
	ErrErrorTooManyObjects                      = fmt.Errorf("error: ErrorTooManyObjects:%d", ErrorTooManyObjects)
	ErrErrorFormatNotSupported                  = fmt.Errorf("error: ErrorFormatNotSupported:%d", ErrorFormatNotSupported)
	ErrErrorFragmentedPool                      = fmt.Errorf("error: ErrorFragmentedPool:%d", ErrorFragmentedPool)
	ErrErrorOutOfPoolMemory                     = fmt.Errorf("error: ErrorOutOfPoolMemory:%d", ErrorOutOfPoolMemory)
	ErrErrorInvalidExternalHandle               = fmt.Errorf("error: ErrorInvalidExternalHandle:%d", ErrorInvalidExternalHandle)
	ErrErrorSurfaceLost                         = fmt.Errorf("error: ErrorSurfaceLost:%d", ErrorSurfaceLost)
	ErrErrorNativeWindowInUse                   = fmt.Errorf("error: ErrorNativeWindowInUse:%d", ErrorNativeWindowInUse)
	ErrSuboptimal                               = fmt.Errorf("error: Suboptimal:%d", Suboptimal)
	ErrErrorOutOfDate                           = fmt.Errorf("error: ErrorOutOfDate:%d", ErrorOutOfDate)
	ErrErrorIncompatibleDisplay                 = fmt.Errorf("error: ErrorIncompatibleDisplay:%d", ErrorIncompatibleDisplay)
	ErrErrorValidationFailed                    = fmt.Errorf("error: ErrorValidationFailed:%d", ErrorValidationFailed)
	ErrErrorInvalidShaderNv                     = fmt.Errorf("error: ErrorInvalidShaderNv:%d", ErrorInvalidShaderNv)
	ErrErrorInvalidDrmFormatModifierPlaneLayout = fmt.Errorf("error: ErrorInvalidDrmFormatModifierPlaneLayout:%d", ErrorInvalidDrmFormatModifierPlaneLayout)
	ErrErrorFragmentation                       = fmt.Errorf("error: ErrorFragmentation:%d", ErrorFragmentation)
	ErrErrorNotPermitted                        = fmt.Errorf("error: ErrorNotPermitted:%d", ErrorNotPermitted)

	VkErrorMap = map[Result]error{
		NotReady:                                 ErrNotReady,
		Timeout:                                  ErrTimeout,
		EventSet:                                 ErrEventSet,
		EventReset:                               ErrEventReset,
		Incomplete:                               ErrIncomplete,
		ErrorOutOfHostMemory:                     ErrErrorOutOfHostMemory,
		ErrorOutOfDeviceMemory:                   ErrErrorOutOfDeviceMemory,
		ErrorInitializationFailed:                ErrErrorInitializationFailed,
		ErrorDeviceLost:                          ErrErrorDeviceLost,
		ErrorMemoryMapFailed:                     ErrErrorMemoryMapFailed,
		ErrorLayerNotPresent:                     ErrErrorLayerNotPresent,
		ErrorExtensionNotPresent:                 ErrErrorExtensionNotPresent,
		ErrorFeatureNotPresent:                   ErrErrorFeatureNotPresent,
		ErrorIncompatibleDriver:                  ErrErrorIncompatibleDriver,
		ErrorTooManyObjects:                      ErrErrorTooManyObjects,
		ErrorFormatNotSupported:                  ErrErrorFormatNotSupported,
		ErrorFragmentedPool:                      ErrErrorFragmentedPool,
		ErrorOutOfPoolMemory:                     ErrErrorOutOfPoolMemory,
		ErrorInvalidExternalHandle:               ErrErrorInvalidExternalHandle,
		ErrorSurfaceLost:                         ErrErrorSurfaceLost,
		ErrorNativeWindowInUse:                   ErrErrorNativeWindowInUse,
		Suboptimal:                               ErrSuboptimal,
		ErrorOutOfDate:                           ErrErrorOutOfDate,
		ErrorIncompatibleDisplay:                 ErrErrorIncompatibleDisplay,
		ErrorValidationFailed:                    ErrErrorValidationFailed,
		ErrorInvalidShaderNv:                     ErrErrorInvalidShaderNv,
		ErrorInvalidDrmFormatModifierPlaneLayout: ErrErrorInvalidDrmFormatModifierPlaneLayout,
		ErrorFragmentation:                       ErrErrorFragmentation,
		ErrorNotPermitted:                        ErrErrorNotPermitted,
	}
)

func VkSuccess(in Result, others ...Result) error {
	if in == Success {
		return nil
	}

	for _, v := range others {
		if v == in {
			return nil
		}
	}

	err, ok := VkErrorMap[in]
	if ok {
		return err
	}
	return fmt.Errorf("error: unknown Result value: %d", in)
}
