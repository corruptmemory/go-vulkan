package go_vulkan

import "unsafe"
import "C"

type VulkanClientMemory interface {
	CString(str string) *C.char
	CStrings(str []string) **C.char
	Calloc(number int, size uint64) unsafe.Pointer
	Free(cMemory unsafe.Pointer)
	Mark()
	FreeToMark()
}

type VulkanResource interface {
	CreateInstance(pCreateInfo *InstanceCreateInfo) (Instance, error)
	DestroyInstance(instance Instance)
	EnumeratePhysicalDevices(instance Instance) ([]PhysicalDevice, error)
	EnumerateInstanceLayerProperties() ([]LayerProperties, error)
	EnumerateInstanceExtensionProperties(layerName string) ([]ExtensionProperties, error)
	GetPhysicalDeviceProperties(physicalDevice PhysicalDevice) PhysicalDeviceProperties
	GetPhysicalDeviceQueueFamilyProperties(physicalDevice PhysicalDevice) ([]QueueFamilyProperties, error)
	CreateDevice(physicalDevice PhysicalDevice, createInfo *DeviceCreateInfo) (Device, error)
	GetDeviceQueue(device Device, queueFamilyIndex uint32, queueIndex uint32) Queue
	DestroyDevice(device Device)
	DestroySurface(instance Instance, surface Surface)
	GetPhysicalDeviceSurfaceSupport(physicalDevice PhysicalDevice, queueFamilyIndex uint32, surface Surface) (bool, error)
	EnumerateDeviceExtensionProperties(physicalDevice PhysicalDevice, layerName string) ([]ExtensionProperties, error)
	GetPhysicalDeviceSurfaceCapabilities(physicalDevice PhysicalDevice, surface Surface) (SurfaceCapabilities, error)
	GetPhysicalDeviceSurfaceFormats(physicalDevice PhysicalDevice, surface Surface) ([]SurfaceFormat, error)
	GetPhysicalDeviceSurfacePresentModes(physicalDevice PhysicalDevice, surface Surface) ([]PresentMode, error)
	CreateSwapchain(device Device, pCreateInfo *SwapchainCreateInfo) (Swapchain, error)
	GetSwapchainImages(device Device, swapchain Swapchain) ([]Image, error)
	DestroySwapchain(device Device, swapchain Swapchain)
	DestroyImageView(device Device, imageView ImageView)
	CreateImageView(device Device, pCreateInfo *ImageViewCreateInfo) (ImageView, error)
	CreateShaderModule(device Device, createInfo *ShaderModuleCreateInfo) (ShaderModule, error)
	DestroyShaderModule(device Device, shaderModule ShaderModule)
	CreatePipelineLayout(device Device, createInfo *PipelineLayoutCreateInfo) (PipelineLayout, error)
	DestroyPipelineLayout(device Device, pipelineLayout PipelineLayout)
	CreateRenderPass(device Device, createInfo *RenderPassCreateInfo) (RenderPass, error)
	DestroyRenderPass(device Device, renderPass RenderPass)
	CreateFramebuffer(device Device, createInfo *FramebufferCreateInfo) (Framebuffer, error)
	DestroyFramebuffer(device Device, framebuffer Framebuffer)
	CreateCommandPool(device Device, pCreateInfo *CommandPoolCreateInfo) (CommandPool, error)
	DestroyCommandPool(device Device, commandPool CommandPool)
	AllocateCommandBuffers(device Device, allocateInfo *CommandBufferAllocateInfo) ([]CommandBuffer, error)
	FreeCommandBuffers(device Device, commandPool CommandPool, commandBuffers []CommandBuffer)
	BeginCommandBuffer(commandBuffer CommandBuffer, beginInfo *CommandBufferBeginInfo) error
	EndCommandBuffer(commandBuffer CommandBuffer) error
	CmdBeginRenderPass(commandBuffer CommandBuffer, renderPassBegin *RenderPassBeginInfo, contents SubpassContents)
	CmdBindPipeline(commandBuffer CommandBuffer, pipelineBindPoint PipelineBindPoint, pipeline Pipeline)
	CmdDraw(commandBuffer CommandBuffer, vertexCount uint32, instanceCount uint32, firstVertex uint32, firstInstance uint32)
	CmdEndRenderPass(commandBuffer CommandBuffer)
	CreateGraphicsPipelines(device Device, pipelineCache PipelineCache, createInfos []GraphicsPipelineCreateInfo) ([]Pipeline, error)
	DestroyPipeline(device Device, pipeline Pipeline)
	CreateSemaphore(device Device, createInfo *SemaphoreCreateInfo) (Semaphore, error)
	DestroySemaphore(device Device, semaphore Semaphore)
	AcquireNextImage(device Device, swapchain Swapchain, timeout uint64, semaphore Semaphore, fence Fence) (uint32, error)
	QueueSubmit(queue Queue, submits []SubmitInfo, fence Fence) error
	CreateFence(device Device, createInfo *FenceCreateInfo) (Fence, error)
	DestroyFence(device Device, fence Fence)
	WaitForFences(device Device, fences []Fence, waitAll bool, timeout uint64) error
	ResetFences(device Device, fences []Fence) error
	QueuePresent(queue Queue, presentInfo *PresentInfo) error
	DeviceWaitIdle(device Device) error
	CreateBuffer(device Device, createInfo *BufferCreateInfo) (Buffer, error)
	DestroyBuffer(device Device, buffer Buffer)
	GetBufferMemoryRequirements(device Device, buffer Buffer) MemoryRequirements
	GetPhysicalDeviceMemoryProperties(physicalDevice PhysicalDevice) PhysicalDeviceMemoryProperties
	AllocateMemory(device Device, allocateInfo *MemoryAllocateInfo) (DeviceMemory, error)
	FreeMemory(device Device, memory DeviceMemory)
	MapMemory(device Device, memory DeviceMemory, offset DeviceSize, size DeviceSize, flags MemoryMapFlags) (unsafe.Pointer, error)
	UnmapMemory(device Device, memory DeviceMemory)
	BindBufferMemory(device Device, buffer Buffer, memory DeviceMemory, memoryOffset DeviceSize) error
	CmdBindVertexBuffers(commandBuffer CommandBuffer, firstBinding uint32, bindingCount uint32, buffers []Buffer, offsets []DeviceSize)
}
