package go_vulkan

import (
	"fmt"
	"unsafe"
)

/*
 #include "arena.h"
 #include <stdlib.h>
 #include <stdint.h>
*/
import "C"

type arenaMemory struct {
	arena *C.arena
	marks []*C.arena_mark
}

func (a *arenaMemory) pushMark(mark *C.arena_mark) {
	a.marks = append(a.marks, mark)
}

func (a *arenaMemory) popMark() *C.arena_mark {
	if len(a.marks) > 0 {
		r := a.marks[0]
		a.marks = a.marks[1:]
		return r
	}
	return nil
}

func (a *arenaMemory) allocPCharMemory(n int) unsafe.Pointer {
	return a.Calloc(n, uint64(sizeOfPCharValue))
}

func (a *arenaMemory) CString(str string) *C.char {
	var buf []byte
	*(*string)(unsafe.Pointer(&buf)) = str
	(*sliceHeader)(unsafe.Pointer(&buf)).Cap = len(str)
	cstr := a.Calloc(len(str)+1, 1)
	Memcopy(cstr, buf)
	return (*C.char)(cstr)
}

func (a *arenaMemory) CStrings(strs []string) **C.char {
	if strs == nil || len(strs) == 0 {
		return nil
	}

	len0 := len(strs)
	mem0 := a.allocPCharMemory(len0)
	h0 := &sliceHeader{
		Data: mem0,
		Cap:  len0,
		Len:  len0,
	}
	v0 := *(*[]*C.char)(unsafe.Pointer(h0))
	for i, gs := range strs {
		v0[i] = a.CString(gs)
	}
	return (**C.char)(mem0)
}

func (a *arenaMemory) Calloc(number int, size uint64) unsafe.Pointer {
	if size == 0 {
		return nil
	}

	mem := C.arena_alloc(a.arena, C.size_t(uint64(number)*size))
	if mem == nil {
		panic(fmt.Sprintln("Failed to allocate memory!!"))
	}
	return mem
}

func (a *arenaMemory) Free(cMemory unsafe.Pointer) {
}

func (a *arenaMemory) Mark() {
	mark := C.arena_record_mark_new(a.arena)
	a.pushMark(mark)
}

func (a *arenaMemory) FreeToMark() {
	mark := a.popMark()
	if mark != nil {
		C.arena_free_to_mark(a.arena, mark)
	}
}

func NewArenaMemory(chunkSize uint32) VulkanClientMemory {
	return &arenaMemory{
		arena: C.arena_new(C.uint32_t(chunkSize)),
	}
}
