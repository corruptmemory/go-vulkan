package go_vulkan

const (
	// NoPrototypes as defined in vulkan/<predefine>:24
	NoPrototypes = 1
	// Version10 as defined in vulkan/vulkan_core.h:22
	Version10 = 1
	// Use64BitPtrDefines as defined in vulkan/vulkan_core.h:30
	Use64BitPtrDefines = 1
	// HeaderVersion as defined in vulkan/vulkan_core.h:75
	HeaderVersion = 177
	// AttachmentUnused as defined in vulkan/vulkan_core.h:123
	AttachmentUnused = (^uint32(0))
	// False as defined in vulkan/vulkan_core.h:124
	False = uint32(0)
	// LodClampNone as defined in vulkan/vulkan_core.h:125
	LodClampNone = 1000.0
	// QueueFamilyIgnored as defined in vulkan/vulkan_core.h:126
	QueueFamilyIgnored = (^uint32(0))
	// RemainingArrayLayers as defined in vulkan/vulkan_core.h:127
	RemainingArrayLayers = (^uint32(0))
	// RemainingMipLevels as defined in vulkan/vulkan_core.h:128
	RemainingMipLevels = (^uint32(0))
	// SubpassExternal as defined in vulkan/vulkan_core.h:129
	SubpassExternal = (^uint32(0))
	// True as defined in vulkan/vulkan_core.h:130
	True = uint32(1)
	// WholeSize as defined in vulkan/vulkan_core.h:131
	WholeSize = (^uint64(0))
	// MaxMemoryTypes as defined in vulkan/vulkan_core.h:132
	MaxMemoryTypes = uint32(32)
	// MaxMemoryHeaps as defined in vulkan/vulkan_core.h:133
	MaxMemoryHeaps = uint32(16)
	// MaxPhysicalDeviceNameSize as defined in vulkan/vulkan_core.h:134
	MaxPhysicalDeviceNameSize = uint32(256)
	// UuidSize as defined in vulkan/vulkan_core.h:135
	UuidSize = uint32(16)
	// MaxExtensionNameSize as defined in vulkan/vulkan_core.h:136
	MaxExtensionNameSize = uint32(256)
	// MaxDescriptionSize as defined in vulkan/vulkan_core.h:137
	MaxDescriptionSize = uint32(256)
	// Version11 as defined in vulkan/vulkan_core.h:4359
	Version11 = 1
	// MaxDeviceGroupSize as defined in vulkan/vulkan_core.h:4365
	MaxDeviceGroupSize = uint32(32)
	// LuidSize as defined in vulkan/vulkan_core.h:4366
	LuidSize = uint32(8)
	// QueueFamilyExternal as defined in vulkan/vulkan_core.h:4367
	QueueFamilyExternal = (^uint32(1))
	// Version12 as defined in vulkan/vulkan_core.h:5223
	Version12 = 1
	// MaxDriverNameSize as defined in vulkan/vulkan_core.h:5227
	MaxDriverNameSize = uint32(256)
	// MaxDriverInfoSize as defined in vulkan/vulkan_core.h:5228
	MaxDriverInfoSize = uint32(256)
	// KhrSurface as defined in vulkan/vulkan_core.h:5967
	KhrSurface = 1
	// KhrSurfaceSpecVersion as defined in vulkan/vulkan_core.h:5969
	KhrSurfaceSpecVersion = 25
	// KhrSurfaceExtensionName as defined in vulkan/vulkan_core.h:5970
	KhrSurfaceExtensionName = "VK_KHR_surface"
	// KhrSwapchain as defined in vulkan/vulkan_core.h:6081
	KhrSwapchain = 1
	// KhrSwapchainSpecVersion as defined in vulkan/vulkan_core.h:6083
	KhrSwapchainSpecVersion = 70
	// KhrSwapchainExtensionName as defined in vulkan/vulkan_core.h:6084
	KhrSwapchainExtensionName = "VK_KHR_swapchain"
	// KhrDisplay as defined in vulkan/vulkan_core.h:6240
	KhrDisplay = 1
	// KhrDisplaySpecVersion as defined in vulkan/vulkan_core.h:6243
	KhrDisplaySpecVersion = 23
	// KhrDisplayExtensionName as defined in vulkan/vulkan_core.h:6244
	KhrDisplayExtensionName = "VK_KHR_display"
	// KhrDisplaySwapchain as defined in vulkan/vulkan_core.h:6365
	KhrDisplaySwapchain = 1
	// KhrDisplaySwapchainSpecVersion as defined in vulkan/vulkan_core.h:6366
	KhrDisplaySwapchainSpecVersion = 10
	// KhrDisplaySwapchainExtensionName as defined in vulkan/vulkan_core.h:6367
	KhrDisplaySwapchainExtensionName = "VK_KHR_display_swapchain"
	// KhrSamplerMirrorClampToEdge as defined in vulkan/vulkan_core.h:6388
	KhrSamplerMirrorClampToEdge = 1
	// KhrSamplerMirrorClampToEdgeSpecVersion as defined in vulkan/vulkan_core.h:6389
	KhrSamplerMirrorClampToEdgeSpecVersion = 3
	// KhrSamplerMirrorClampToEdgeExtensionName as defined in vulkan/vulkan_core.h:6390
	KhrSamplerMirrorClampToEdgeExtensionName = "VK_KHR_sampler_mirror_clamp_to_edge"
	// KhrMultiview as defined in vulkan/vulkan_core.h:6393
	KhrMultiview = 1
	// KhrMultiviewSpecVersion as defined in vulkan/vulkan_core.h:6394
	KhrMultiviewSpecVersion = 1
	// KhrMultiviewExtensionName as defined in vulkan/vulkan_core.h:6395
	KhrMultiviewExtensionName = "VK_KHR_multiview"
	// KhrGetPhysicalDeviceProperties2 as defined in vulkan/vulkan_core.h:6404
	KhrGetPhysicalDeviceProperties2 = 1
	// KhrGetPhysicalDeviceProperties2SpecVersion as defined in vulkan/vulkan_core.h:6405
	KhrGetPhysicalDeviceProperties2SpecVersion = 2
	// KhrGetPhysicalDeviceProperties2ExtensionName as defined in vulkan/vulkan_core.h:6406
	KhrGetPhysicalDeviceProperties2ExtensionName = "VK_KHR_get_physical_device_properties2"
	// KhrDeviceGroup as defined in vulkan/vulkan_core.h:6469
	KhrDeviceGroup = 1
	// KhrDeviceGroupSpecVersion as defined in vulkan/vulkan_core.h:6470
	KhrDeviceGroupSpecVersion = 4
	// KhrDeviceGroupExtensionName as defined in vulkan/vulkan_core.h:6471
	KhrDeviceGroupExtensionName = "VK_KHR_device_group"
	// KhrShaderDrawParameters as defined in vulkan/vulkan_core.h:6521
	KhrShaderDrawParameters = 1
	// KhrShaderDrawParametersSpecVersion as defined in vulkan/vulkan_core.h:6522
	KhrShaderDrawParametersSpecVersion = 1
	// KhrShaderDrawParametersExtensionName as defined in vulkan/vulkan_core.h:6523
	KhrShaderDrawParametersExtensionName = "VK_KHR_shader_draw_parameters"
	// KhrMaintenance1 as defined in vulkan/vulkan_core.h:6526
	KhrMaintenance1 = 1
	// KhrMaintenance1SpecVersion as defined in vulkan/vulkan_core.h:6527
	KhrMaintenance1SpecVersion = 2
	// KhrMaintenance1ExtensionName as defined in vulkan/vulkan_core.h:6528
	KhrMaintenance1ExtensionName = "VK_KHR_maintenance1"
	// KhrDeviceGroupCreation as defined in vulkan/vulkan_core.h:6541
	KhrDeviceGroupCreation = 1
	// KhrDeviceGroupCreationSpecVersion as defined in vulkan/vulkan_core.h:6542
	KhrDeviceGroupCreationSpecVersion = 1
	// KhrDeviceGroupCreationExtensionName as defined in vulkan/vulkan_core.h:6543
	KhrDeviceGroupCreationExtensionName = "VK_KHR_device_group_creation"
	// KhrExternalMemoryCapabilities as defined in vulkan/vulkan_core.h:6559
	KhrExternalMemoryCapabilities = 1
	// KhrExternalMemoryCapabilitiesSpecVersion as defined in vulkan/vulkan_core.h:6560
	KhrExternalMemoryCapabilitiesSpecVersion = 1
	// KhrExternalMemoryCapabilitiesExtensionName as defined in vulkan/vulkan_core.h:6561
	KhrExternalMemoryCapabilitiesExtensionName = "VK_KHR_external_memory_capabilities"
	// KhrExternalMemory as defined in vulkan/vulkan_core.h:6593
	KhrExternalMemory = 1
	// KhrExternalMemorySpecVersion as defined in vulkan/vulkan_core.h:6594
	KhrExternalMemorySpecVersion = 1
	// KhrExternalMemoryExtensionName as defined in vulkan/vulkan_core.h:6595
	KhrExternalMemoryExtensionName = "VK_KHR_external_memory"
	// KhrExternalMemoryFd as defined in vulkan/vulkan_core.h:6605
	KhrExternalMemoryFd = 1
	// KhrExternalMemoryFdSpecVersion as defined in vulkan/vulkan_core.h:6606
	KhrExternalMemoryFdSpecVersion = 1
	// KhrExternalMemoryFdExtensionName as defined in vulkan/vulkan_core.h:6607
	KhrExternalMemoryFdExtensionName = "VK_KHR_external_memory_fd"
	// KhrExternalSemaphoreCapabilities as defined in vulkan/vulkan_core.h:6645
	KhrExternalSemaphoreCapabilities = 1
	// KhrExternalSemaphoreCapabilitiesSpecVersion as defined in vulkan/vulkan_core.h:6646
	KhrExternalSemaphoreCapabilitiesSpecVersion = 1
	// KhrExternalSemaphoreCapabilitiesExtensionName as defined in vulkan/vulkan_core.h:6647
	KhrExternalSemaphoreCapabilitiesExtensionName = "VK_KHR_external_semaphore_capabilities"
	// KhrExternalSemaphore as defined in vulkan/vulkan_core.h:6670
	KhrExternalSemaphore = 1
	// KhrExternalSemaphoreSpecVersion as defined in vulkan/vulkan_core.h:6671
	KhrExternalSemaphoreSpecVersion = 1
	// KhrExternalSemaphoreExtensionName as defined in vulkan/vulkan_core.h:6672
	KhrExternalSemaphoreExtensionName = "VK_KHR_external_semaphore"
	// KhrExternalSemaphoreFd as defined in vulkan/vulkan_core.h:6681
	KhrExternalSemaphoreFd = 1
	// KhrExternalSemaphoreFdSpecVersion as defined in vulkan/vulkan_core.h:6682
	KhrExternalSemaphoreFdSpecVersion = 1
	// KhrExternalSemaphoreFdExtensionName as defined in vulkan/vulkan_core.h:6683
	KhrExternalSemaphoreFdExtensionName = "VK_KHR_external_semaphore_fd"
	// KhrPushDescriptor as defined in vulkan/vulkan_core.h:6715
	KhrPushDescriptor = 1
	// KhrPushDescriptorSpecVersion as defined in vulkan/vulkan_core.h:6716
	KhrPushDescriptorSpecVersion = 2
	// KhrPushDescriptorExtensionName as defined in vulkan/vulkan_core.h:6717
	KhrPushDescriptorExtensionName = "VK_KHR_push_descriptor"
	// KhrShaderFloat16Int8 as defined in vulkan/vulkan_core.h:6745
	KhrShaderFloat16Int8 = 1
	// KhrShaderFloat16Int8SpecVersion as defined in vulkan/vulkan_core.h:6746
	KhrShaderFloat16Int8SpecVersion = 1
	// KhrShaderFloat16Int8ExtensionName as defined in vulkan/vulkan_core.h:6747
	KhrShaderFloat16Int8ExtensionName = "VK_KHR_shader_float16_int8"
	// Khr16bitStorage as defined in vulkan/vulkan_core.h:6754
	Khr16bitStorage = 1
	// Khr16bitStorageSpecVersion as defined in vulkan/vulkan_core.h:6755
	Khr16bitStorageSpecVersion = 1
	// Khr16bitStorageExtensionName as defined in vulkan/vulkan_core.h:6756
	Khr16bitStorageExtensionName = "VK_KHR_16bit_storage"
	// KhrIncrementalPresent as defined in vulkan/vulkan_core.h:6761
	KhrIncrementalPresent = 1
	// KhrIncrementalPresentSpecVersion as defined in vulkan/vulkan_core.h:6762
	KhrIncrementalPresentSpecVersion = 2
	// KhrIncrementalPresentExtensionName as defined in vulkan/vulkan_core.h:6763
	KhrIncrementalPresentExtensionName = "VK_KHR_incremental_present"
	// KhrDescriptorUpdateTemplate as defined in vulkan/vulkan_core.h:6784
	KhrDescriptorUpdateTemplate = 1
	// KhrDescriptorUpdateTemplateSpecVersion as defined in vulkan/vulkan_core.h:6787
	KhrDescriptorUpdateTemplateSpecVersion = 1
	// KhrDescriptorUpdateTemplateExtensionName as defined in vulkan/vulkan_core.h:6788
	KhrDescriptorUpdateTemplateExtensionName = "VK_KHR_descriptor_update_template"
	// KhrImagelessFramebuffer as defined in vulkan/vulkan_core.h:6821
	KhrImagelessFramebuffer = 1
	// KhrImagelessFramebufferSpecVersion as defined in vulkan/vulkan_core.h:6822
	KhrImagelessFramebufferSpecVersion = 1
	// KhrImagelessFramebufferExtensionName as defined in vulkan/vulkan_core.h:6823
	KhrImagelessFramebufferExtensionName = "VK_KHR_imageless_framebuffer"
	// KhrCreateRenderpass2 as defined in vulkan/vulkan_core.h:6834
	KhrCreateRenderpass2 = 1
	// KhrCreateRenderpass2SpecVersion as defined in vulkan/vulkan_core.h:6835
	KhrCreateRenderpass2SpecVersion = 1
	// KhrCreateRenderpass2ExtensionName as defined in vulkan/vulkan_core.h:6836
	KhrCreateRenderpass2ExtensionName = "VK_KHR_create_renderpass2"
	// KhrSharedPresentableImage as defined in vulkan/vulkan_core.h:6879
	KhrSharedPresentableImage = 1
	// KhrSharedPresentableImageSpecVersion as defined in vulkan/vulkan_core.h:6880
	KhrSharedPresentableImageSpecVersion = 1
	// KhrSharedPresentableImageExtensionName as defined in vulkan/vulkan_core.h:6881
	KhrSharedPresentableImageExtensionName = "VK_KHR_shared_presentable_image"
	// KhrExternalFenceCapabilities as defined in vulkan/vulkan_core.h:6897
	KhrExternalFenceCapabilities = 1
	// KhrExternalFenceCapabilitiesSpecVersion as defined in vulkan/vulkan_core.h:6898
	KhrExternalFenceCapabilitiesSpecVersion = 1
	// KhrExternalFenceCapabilitiesExtensionName as defined in vulkan/vulkan_core.h:6899
	KhrExternalFenceCapabilitiesExtensionName = "VK_KHR_external_fence_capabilities"
	// KhrExternalFence as defined in vulkan/vulkan_core.h:6922
	KhrExternalFence = 1
	// KhrExternalFenceSpecVersion as defined in vulkan/vulkan_core.h:6923
	KhrExternalFenceSpecVersion = 1
	// KhrExternalFenceExtensionName as defined in vulkan/vulkan_core.h:6924
	KhrExternalFenceExtensionName = "VK_KHR_external_fence"
	// KhrExternalFenceFd as defined in vulkan/vulkan_core.h:6933
	KhrExternalFenceFd = 1
	// KhrExternalFenceFdSpecVersion as defined in vulkan/vulkan_core.h:6934
	KhrExternalFenceFdSpecVersion = 1
	// KhrExternalFenceFdExtensionName as defined in vulkan/vulkan_core.h:6935
	KhrExternalFenceFdExtensionName = "VK_KHR_external_fence_fd"
	// KhrPerformanceQuery as defined in vulkan/vulkan_core.h:6967
	KhrPerformanceQuery = 1
	// KhrPerformanceQuerySpecVersion as defined in vulkan/vulkan_core.h:6968
	KhrPerformanceQuerySpecVersion = 1
	// KhrPerformanceQueryExtensionName as defined in vulkan/vulkan_core.h:6969
	KhrPerformanceQueryExtensionName = "VK_KHR_performance_query"
	// KhrMaintenance2 as defined in vulkan/vulkan_core.h:7107
	KhrMaintenance2 = 1
	// KhrMaintenance2SpecVersion as defined in vulkan/vulkan_core.h:7108
	KhrMaintenance2SpecVersion = 1
	// KhrMaintenance2ExtensionName as defined in vulkan/vulkan_core.h:7109
	KhrMaintenance2ExtensionName = "VK_KHR_maintenance2"
	// KhrGetSurfaceCapabilities2 as defined in vulkan/vulkan_core.h:7126
	KhrGetSurfaceCapabilities2 = 1
	// KhrGetSurfaceCapabilities2SpecVersion as defined in vulkan/vulkan_core.h:7127
	KhrGetSurfaceCapabilities2SpecVersion = 1
	// KhrGetSurfaceCapabilities2ExtensionName as defined in vulkan/vulkan_core.h:7128
	KhrGetSurfaceCapabilities2ExtensionName = "VK_KHR_get_surface_capabilities2"
	// KhrVariablePointers as defined in vulkan/vulkan_core.h:7164
	KhrVariablePointers = 1
	// KhrVariablePointersSpecVersion as defined in vulkan/vulkan_core.h:7165
	KhrVariablePointersSpecVersion = 1
	// KhrVariablePointersExtensionName as defined in vulkan/vulkan_core.h:7166
	KhrVariablePointersExtensionName = "VK_KHR_variable_pointers"
	// KhrGetDisplayProperties2 as defined in vulkan/vulkan_core.h:7173
	KhrGetDisplayProperties2 = 1
	// KhrGetDisplayProperties2SpecVersion as defined in vulkan/vulkan_core.h:7174
	KhrGetDisplayProperties2SpecVersion = 1
	// KhrGetDisplayProperties2ExtensionName as defined in vulkan/vulkan_core.h:7175
	KhrGetDisplayProperties2ExtensionName = "VK_KHR_get_display_properties2"
	// KhrDedicatedAllocation as defined in vulkan/vulkan_core.h:7236
	KhrDedicatedAllocation = 1
	// KhrDedicatedAllocationSpecVersion as defined in vulkan/vulkan_core.h:7237
	KhrDedicatedAllocationSpecVersion = 3
	// KhrDedicatedAllocationExtensionName as defined in vulkan/vulkan_core.h:7238
	KhrDedicatedAllocationExtensionName = "VK_KHR_dedicated_allocation"
	// KhrStorageBufferStorageClass as defined in vulkan/vulkan_core.h:7245
	KhrStorageBufferStorageClass = 1
	// KhrStorageBufferStorageClassSpecVersion as defined in vulkan/vulkan_core.h:7246
	KhrStorageBufferStorageClassSpecVersion = 1
	// KhrStorageBufferStorageClassExtensionName as defined in vulkan/vulkan_core.h:7247
	KhrStorageBufferStorageClassExtensionName = "VK_KHR_storage_buffer_storage_class"
	// KhrRelaxedBlockLayout as defined in vulkan/vulkan_core.h:7250
	KhrRelaxedBlockLayout = 1
	// KhrRelaxedBlockLayoutSpecVersion as defined in vulkan/vulkan_core.h:7251
	KhrRelaxedBlockLayoutSpecVersion = 1
	// KhrRelaxedBlockLayoutExtensionName as defined in vulkan/vulkan_core.h:7252
	KhrRelaxedBlockLayoutExtensionName = "VK_KHR_relaxed_block_layout"
	// KhrGetMemoryRequirements2 as defined in vulkan/vulkan_core.h:7255
	KhrGetMemoryRequirements2 = 1
	// KhrGetMemoryRequirements2SpecVersion as defined in vulkan/vulkan_core.h:7256
	KhrGetMemoryRequirements2SpecVersion = 1
	// KhrGetMemoryRequirements2ExtensionName as defined in vulkan/vulkan_core.h:7257
	KhrGetMemoryRequirements2ExtensionName = "VK_KHR_get_memory_requirements2"
	// KhrImageFormatList as defined in vulkan/vulkan_core.h:7291
	KhrImageFormatList = 1
	// KhrImageFormatListSpecVersion as defined in vulkan/vulkan_core.h:7292
	KhrImageFormatListSpecVersion = 1
	// KhrImageFormatListExtensionName as defined in vulkan/vulkan_core.h:7293
	KhrImageFormatListExtensionName = "VK_KHR_image_format_list"
	// KhrSamplerYcbcrConversion as defined in vulkan/vulkan_core.h:7298
	KhrSamplerYcbcrConversion = 1
	// KhrSamplerYcbcrConversionSpecVersion as defined in vulkan/vulkan_core.h:7301
	KhrSamplerYcbcrConversionSpecVersion = 14
	// KhrSamplerYcbcrConversionExtensionName as defined in vulkan/vulkan_core.h:7302
	KhrSamplerYcbcrConversionExtensionName = "VK_KHR_sampler_ycbcr_conversion"
	// KhrBindMemory2 as defined in vulkan/vulkan_core.h:7338
	KhrBindMemory2 = 1
	// KhrBindMemory2SpecVersion as defined in vulkan/vulkan_core.h:7339
	KhrBindMemory2SpecVersion = 1
	// KhrBindMemory2ExtensionName as defined in vulkan/vulkan_core.h:7340
	KhrBindMemory2ExtensionName = "VK_KHR_bind_memory2"
	// KhrMaintenance3 as defined in vulkan/vulkan_core.h:7361
	KhrMaintenance3 = 1
	// KhrMaintenance3SpecVersion as defined in vulkan/vulkan_core.h:7362
	KhrMaintenance3SpecVersion = 1
	// KhrMaintenance3ExtensionName as defined in vulkan/vulkan_core.h:7363
	KhrMaintenance3ExtensionName = "VK_KHR_maintenance3"
	// KhrDrawIndirectCount as defined in vulkan/vulkan_core.h:7378
	KhrDrawIndirectCount = 1
	// KhrDrawIndirectCountSpecVersion as defined in vulkan/vulkan_core.h:7379
	KhrDrawIndirectCountSpecVersion = 1
	// KhrDrawIndirectCountExtensionName as defined in vulkan/vulkan_core.h:7380
	KhrDrawIndirectCountExtensionName = "VK_KHR_draw_indirect_count"
	// KhrShaderSubgroupExtendedTypes as defined in vulkan/vulkan_core.h:7405
	KhrShaderSubgroupExtendedTypes = 1
	// KhrShaderSubgroupExtendedTypesSpecVersion as defined in vulkan/vulkan_core.h:7406
	KhrShaderSubgroupExtendedTypesSpecVersion = 1
	// KhrShaderSubgroupExtendedTypesExtensionName as defined in vulkan/vulkan_core.h:7407
	KhrShaderSubgroupExtendedTypesExtensionName = "VK_KHR_shader_subgroup_extended_types"
	// Khr8bitStorage as defined in vulkan/vulkan_core.h:7412
	Khr8bitStorage = 1
	// Khr8bitStorageSpecVersion as defined in vulkan/vulkan_core.h:7413
	Khr8bitStorageSpecVersion = 1
	// Khr8bitStorageExtensionName as defined in vulkan/vulkan_core.h:7414
	Khr8bitStorageExtensionName = "VK_KHR_8bit_storage"
	// KhrShaderAtomicInt64 as defined in vulkan/vulkan_core.h:7419
	KhrShaderAtomicInt64 = 1
	// KhrShaderAtomicInt64SpecVersion as defined in vulkan/vulkan_core.h:7420
	KhrShaderAtomicInt64SpecVersion = 1
	// KhrShaderAtomicInt64ExtensionName as defined in vulkan/vulkan_core.h:7421
	KhrShaderAtomicInt64ExtensionName = "VK_KHR_shader_atomic_int64"
	// KhrShaderClock as defined in vulkan/vulkan_core.h:7426
	KhrShaderClock = 1
	// KhrShaderClockSpecVersion as defined in vulkan/vulkan_core.h:7427
	KhrShaderClockSpecVersion = 1
	// KhrShaderClockExtensionName as defined in vulkan/vulkan_core.h:7428
	KhrShaderClockExtensionName = "VK_KHR_shader_clock"
	// KhrDriverProperties as defined in vulkan/vulkan_core.h:7438
	KhrDriverProperties = 1
	// KhrDriverPropertiesSpecVersion as defined in vulkan/vulkan_core.h:7439
	KhrDriverPropertiesSpecVersion = 1
	// KhrDriverPropertiesExtensionName as defined in vulkan/vulkan_core.h:7440
	KhrDriverPropertiesExtensionName = "VK_KHR_driver_properties"
	// KhrShaderFloatControls as defined in vulkan/vulkan_core.h:7451
	KhrShaderFloatControls = 1
	// KhrShaderFloatControlsSpecVersion as defined in vulkan/vulkan_core.h:7452
	KhrShaderFloatControlsSpecVersion = 4
	// KhrShaderFloatControlsExtensionName as defined in vulkan/vulkan_core.h:7453
	KhrShaderFloatControlsExtensionName = "VK_KHR_shader_float_controls"
	// KhrDepthStencilResolve as defined in vulkan/vulkan_core.h:7460
	KhrDepthStencilResolve = 1
	// KhrDepthStencilResolveSpecVersion as defined in vulkan/vulkan_core.h:7461
	KhrDepthStencilResolveSpecVersion = 1
	// KhrDepthStencilResolveExtensionName as defined in vulkan/vulkan_core.h:7462
	KhrDepthStencilResolveExtensionName = "VK_KHR_depth_stencil_resolve"
	// KhrSwapchainMutableFormat as defined in vulkan/vulkan_core.h:7473
	KhrSwapchainMutableFormat = 1
	// KhrSwapchainMutableFormatSpecVersion as defined in vulkan/vulkan_core.h:7474
	KhrSwapchainMutableFormatSpecVersion = 1
	// KhrSwapchainMutableFormatExtensionName as defined in vulkan/vulkan_core.h:7475
	KhrSwapchainMutableFormatExtensionName = "VK_KHR_swapchain_mutable_format"
	// KhrTimelineSemaphore as defined in vulkan/vulkan_core.h:7478
	KhrTimelineSemaphore = 1
	// KhrTimelineSemaphoreSpecVersion as defined in vulkan/vulkan_core.h:7479
	KhrTimelineSemaphoreSpecVersion = 2
	// KhrTimelineSemaphoreExtensionName as defined in vulkan/vulkan_core.h:7480
	KhrTimelineSemaphoreExtensionName = "VK_KHR_timeline_semaphore"
	// KhrVulkanMemoryModel as defined in vulkan/vulkan_core.h:7520
	KhrVulkanMemoryModel = 1
	// KhrVulkanMemoryModelSpecVersion as defined in vulkan/vulkan_core.h:7521
	KhrVulkanMemoryModelSpecVersion = 3
	// KhrVulkanMemoryModelExtensionName as defined in vulkan/vulkan_core.h:7522
	KhrVulkanMemoryModelExtensionName = "VK_KHR_vulkan_memory_model"
	// KhrShaderTerminateInvocation as defined in vulkan/vulkan_core.h:7527
	KhrShaderTerminateInvocation = 1
	// KhrShaderTerminateInvocationSpecVersion as defined in vulkan/vulkan_core.h:7528
	KhrShaderTerminateInvocationSpecVersion = 1
	// KhrShaderTerminateInvocationExtensionName as defined in vulkan/vulkan_core.h:7529
	KhrShaderTerminateInvocationExtensionName = "VK_KHR_shader_terminate_invocation"
	// KhrFragmentShadingRate as defined in vulkan/vulkan_core.h:7538
	KhrFragmentShadingRate = 1
	// KhrFragmentShadingRateSpecVersion as defined in vulkan/vulkan_core.h:7539
	KhrFragmentShadingRateSpecVersion = 1
	// KhrFragmentShadingRateExtensionName as defined in vulkan/vulkan_core.h:7540
	KhrFragmentShadingRateExtensionName = "VK_KHR_fragment_shading_rate"
	// KhrSpirv14 as defined in vulkan/vulkan_core.h:7617
	KhrSpirv14 = 1
	// KhrSpirv14SpecVersion as defined in vulkan/vulkan_core.h:7618
	KhrSpirv14SpecVersion = 1
	// KhrSpirv14ExtensionName as defined in vulkan/vulkan_core.h:7619
	KhrSpirv14ExtensionName = "VK_KHR_spirv_1_4"
	// KhrSurfaceProtectedCapabilities as defined in vulkan/vulkan_core.h:7622
	KhrSurfaceProtectedCapabilities = 1
	// KhrSurfaceProtectedCapabilitiesSpecVersion as defined in vulkan/vulkan_core.h:7623
	KhrSurfaceProtectedCapabilitiesSpecVersion = 1
	// KhrSurfaceProtectedCapabilitiesExtensionName as defined in vulkan/vulkan_core.h:7624
	KhrSurfaceProtectedCapabilitiesExtensionName = "VK_KHR_surface_protected_capabilities"
	// KhrSeparateDepthStencilLayouts as defined in vulkan/vulkan_core.h:7633
	KhrSeparateDepthStencilLayouts = 1
	// KhrSeparateDepthStencilLayoutsSpecVersion as defined in vulkan/vulkan_core.h:7634
	KhrSeparateDepthStencilLayoutsSpecVersion = 1
	// KhrSeparateDepthStencilLayoutsExtensionName as defined in vulkan/vulkan_core.h:7635
	KhrSeparateDepthStencilLayoutsExtensionName = "VK_KHR_separate_depth_stencil_layouts"
	// KhrUniformBufferStandardLayout as defined in vulkan/vulkan_core.h:7644
	KhrUniformBufferStandardLayout = 1
	// KhrUniformBufferStandardLayoutSpecVersion as defined in vulkan/vulkan_core.h:7645
	KhrUniformBufferStandardLayoutSpecVersion = 1
	// KhrUniformBufferStandardLayoutExtensionName as defined in vulkan/vulkan_core.h:7646
	KhrUniformBufferStandardLayoutExtensionName = "VK_KHR_uniform_buffer_standard_layout"
	// KhrBufferDeviceAddress as defined in vulkan/vulkan_core.h:7651
	KhrBufferDeviceAddress = 1
	// KhrBufferDeviceAddressSpecVersion as defined in vulkan/vulkan_core.h:7652
	KhrBufferDeviceAddressSpecVersion = 1
	// KhrBufferDeviceAddressExtensionName as defined in vulkan/vulkan_core.h:7653
	KhrBufferDeviceAddressExtensionName = "VK_KHR_buffer_device_address"
	// KhrDeferredHostOperations as defined in vulkan/vulkan_core.h:7683
	KhrDeferredHostOperations = 1
	// KhrDeferredHostOperationsSpecVersion as defined in vulkan/vulkan_core.h:7685
	KhrDeferredHostOperationsSpecVersion = 4
	// KhrDeferredHostOperationsExtensionName as defined in vulkan/vulkan_core.h:7686
	KhrDeferredHostOperationsExtensionName = "VK_KHR_deferred_host_operations"
	// KhrPipelineExecutableProperties as defined in vulkan/vulkan_core.h:7718
	KhrPipelineExecutableProperties = 1
	// KhrPipelineExecutablePropertiesSpecVersion as defined in vulkan/vulkan_core.h:7719
	KhrPipelineExecutablePropertiesSpecVersion = 1
	// KhrPipelineExecutablePropertiesExtensionName as defined in vulkan/vulkan_core.h:7720
	KhrPipelineExecutablePropertiesExtensionName = "VK_KHR_pipeline_executable_properties"
	// KhrPipelineLibrary as defined in vulkan/vulkan_core.h:7808
	KhrPipelineLibrary = 1
	// KhrPipelineLibrarySpecVersion as defined in vulkan/vulkan_core.h:7809
	KhrPipelineLibrarySpecVersion = 1
	// KhrPipelineLibraryExtensionName as defined in vulkan/vulkan_core.h:7810
	KhrPipelineLibraryExtensionName = "VK_KHR_pipeline_library"
	// KhrShaderNonSemanticInfo as defined in vulkan/vulkan_core.h:7820
	KhrShaderNonSemanticInfo = 1
	// KhrShaderNonSemanticInfoSpecVersion as defined in vulkan/vulkan_core.h:7821
	KhrShaderNonSemanticInfoSpecVersion = 1
	// KhrShaderNonSemanticInfoExtensionName as defined in vulkan/vulkan_core.h:7822
	KhrShaderNonSemanticInfoExtensionName = "VK_KHR_shader_non_semantic_info"
	// KhrSynchronization2 as defined in vulkan/vulkan_core.h:7825
	KhrSynchronization2 = 1
	// KhrSynchronization2SpecVersion as defined in vulkan/vulkan_core.h:7827
	KhrSynchronization2SpecVersion = 1
	// KhrSynchronization2ExtensionName as defined in vulkan/vulkan_core.h:7828
	KhrSynchronization2ExtensionName = "VK_KHR_synchronization2"
	// KhrZeroInitializeWorkgroupMemory as defined in vulkan/vulkan_core.h:8089
	KhrZeroInitializeWorkgroupMemory = 1
	// KhrZeroInitializeWorkgroupMemorySpecVersion as defined in vulkan/vulkan_core.h:8090
	KhrZeroInitializeWorkgroupMemorySpecVersion = 1
	// KhrZeroInitializeWorkgroupMemoryExtensionName as defined in vulkan/vulkan_core.h:8091
	KhrZeroInitializeWorkgroupMemoryExtensionName = "VK_KHR_zero_initialize_workgroup_memory"
	// KhrWorkgroupMemoryExplicitLayout as defined in vulkan/vulkan_core.h:8100
	KhrWorkgroupMemoryExplicitLayout = 1
	// KhrWorkgroupMemoryExplicitLayoutSpecVersion as defined in vulkan/vulkan_core.h:8101
	KhrWorkgroupMemoryExplicitLayoutSpecVersion = 1
	// KhrWorkgroupMemoryExplicitLayoutExtensionName as defined in vulkan/vulkan_core.h:8102
	KhrWorkgroupMemoryExplicitLayoutExtensionName = "VK_KHR_workgroup_memory_explicit_layout"
	// KhrCopyCommands2 as defined in vulkan/vulkan_core.h:8114
	KhrCopyCommands2 = 1
	// KhrCopyCommands2SpecVersion as defined in vulkan/vulkan_core.h:8115
	KhrCopyCommands2SpecVersion = 1
	// KhrCopyCommands2ExtensionName as defined in vulkan/vulkan_core.h:8116
	KhrCopyCommands2ExtensionName = "VK_KHR_copy_commands2"
	// ExtDebugReport as defined in vulkan/vulkan_core.h:8262
	ExtDebugReport = 1
	// ExtDebugReportSpecVersion as defined in vulkan/vulkan_core.h:8264
	ExtDebugReportSpecVersion = 10
	// ExtDebugReportExtensionName as defined in vulkan/vulkan_core.h:8265
	ExtDebugReportExtensionName = "VK_EXT_debug_report"
	// NvGlslShader as defined in vulkan/vulkan_core.h:8366
	NvGlslShader = 1
	// NvGlslShaderSpecVersion as defined in vulkan/vulkan_core.h:8367
	NvGlslShaderSpecVersion = 1
	// NvGlslShaderExtensionName as defined in vulkan/vulkan_core.h:8368
	NvGlslShaderExtensionName = "VK_NV_glsl_shader"
	// ExtDepthRangeUnrestricted as defined in vulkan/vulkan_core.h:8371
	ExtDepthRangeUnrestricted = 1
	// ExtDepthRangeUnrestrictedSpecVersion as defined in vulkan/vulkan_core.h:8372
	ExtDepthRangeUnrestrictedSpecVersion = 1
	// ExtDepthRangeUnrestrictedExtensionName as defined in vulkan/vulkan_core.h:8373
	ExtDepthRangeUnrestrictedExtensionName = "VK_EXT_depth_range_unrestricted"
	// ImgFilterCubic as defined in vulkan/vulkan_core.h:8376
	ImgFilterCubic = 1
	// ImgFilterCubicSpecVersion as defined in vulkan/vulkan_core.h:8377
	ImgFilterCubicSpecVersion = 1
	// ImgFilterCubicExtensionName as defined in vulkan/vulkan_core.h:8378
	ImgFilterCubicExtensionName = "VK_IMG_filter_cubic"
	// AmdRasterizationOrder as defined in vulkan/vulkan_core.h:8381
	AmdRasterizationOrder = 1
	// AmdRasterizationOrderSpecVersion as defined in vulkan/vulkan_core.h:8382
	AmdRasterizationOrderSpecVersion = 1
	// AmdRasterizationOrderExtensionName as defined in vulkan/vulkan_core.h:8383
	AmdRasterizationOrderExtensionName = "VK_AMD_rasterization_order"
	// AmdShaderTrinaryMinmax as defined in vulkan/vulkan_core.h:8398
	AmdShaderTrinaryMinmax = 1
	// AmdShaderTrinaryMinmaxSpecVersion as defined in vulkan/vulkan_core.h:8399
	AmdShaderTrinaryMinmaxSpecVersion = 1
	// AmdShaderTrinaryMinmaxExtensionName as defined in vulkan/vulkan_core.h:8400
	AmdShaderTrinaryMinmaxExtensionName = "VK_AMD_shader_trinary_minmax"
	// AmdShaderExplicitVertexParameter as defined in vulkan/vulkan_core.h:8403
	AmdShaderExplicitVertexParameter = 1
	// AmdShaderExplicitVertexParameterSpecVersion as defined in vulkan/vulkan_core.h:8404
	AmdShaderExplicitVertexParameterSpecVersion = 1
	// AmdShaderExplicitVertexParameterExtensionName as defined in vulkan/vulkan_core.h:8405
	AmdShaderExplicitVertexParameterExtensionName = "VK_AMD_shader_explicit_vertex_parameter"
	// ExtDebugMarker as defined in vulkan/vulkan_core.h:8408
	ExtDebugMarker = 1
	// ExtDebugMarkerSpecVersion as defined in vulkan/vulkan_core.h:8409
	ExtDebugMarkerSpecVersion = 4
	// ExtDebugMarkerExtensionName as defined in vulkan/vulkan_core.h:8410
	ExtDebugMarkerExtensionName = "VK_EXT_debug_marker"
	// AmdGcnShader as defined in vulkan/vulkan_core.h:8464
	AmdGcnShader = 1
	// AmdGcnShaderSpecVersion as defined in vulkan/vulkan_core.h:8465
	AmdGcnShaderSpecVersion = 1
	// AmdGcnShaderExtensionName as defined in vulkan/vulkan_core.h:8466
	AmdGcnShaderExtensionName = "VK_AMD_gcn_shader"
	// NvDedicatedAllocation as defined in vulkan/vulkan_core.h:8469
	NvDedicatedAllocation = 1
	// NvDedicatedAllocationSpecVersion as defined in vulkan/vulkan_core.h:8470
	NvDedicatedAllocationSpecVersion = 1
	// NvDedicatedAllocationExtensionName as defined in vulkan/vulkan_core.h:8471
	NvDedicatedAllocationExtensionName = "VK_NV_dedicated_allocation"
	// ExtTransformFeedback as defined in vulkan/vulkan_core.h:8493
	ExtTransformFeedback = 1
	// ExtTransformFeedbackSpecVersion as defined in vulkan/vulkan_core.h:8494
	ExtTransformFeedbackSpecVersion = 1
	// ExtTransformFeedbackExtensionName as defined in vulkan/vulkan_core.h:8495
	ExtTransformFeedbackExtensionName = "VK_EXT_transform_feedback"
	// NvxImageViewHandle as defined in vulkan/vulkan_core.h:8580
	NvxImageViewHandle = 1
	// NvxImageViewHandleSpecVersion as defined in vulkan/vulkan_core.h:8581
	NvxImageViewHandleSpecVersion = 2
	// NvxImageViewHandleExtensionName as defined in vulkan/vulkan_core.h:8582
	NvxImageViewHandleExtensionName = "VK_NVX_image_view_handle"
	// AmdDrawIndirectCount as defined in vulkan/vulkan_core.h:8613
	AmdDrawIndirectCount = 1
	// AmdDrawIndirectCountSpecVersion as defined in vulkan/vulkan_core.h:8614
	AmdDrawIndirectCountSpecVersion = 2
	// AmdDrawIndirectCountExtensionName as defined in vulkan/vulkan_core.h:8615
	AmdDrawIndirectCountExtensionName = "VK_AMD_draw_indirect_count"
	// AmdNegativeViewportHeight as defined in vulkan/vulkan_core.h:8640
	AmdNegativeViewportHeight = 1
	// AmdNegativeViewportHeightSpecVersion as defined in vulkan/vulkan_core.h:8641
	AmdNegativeViewportHeightSpecVersion = 1
	// AmdNegativeViewportHeightExtensionName as defined in vulkan/vulkan_core.h:8642
	AmdNegativeViewportHeightExtensionName = "VK_AMD_negative_viewport_height"
	// AmdGpuShaderHalfFloat as defined in vulkan/vulkan_core.h:8645
	AmdGpuShaderHalfFloat = 1
	// AmdGpuShaderHalfFloatSpecVersion as defined in vulkan/vulkan_core.h:8646
	AmdGpuShaderHalfFloatSpecVersion = 2
	// AmdGpuShaderHalfFloatExtensionName as defined in vulkan/vulkan_core.h:8647
	AmdGpuShaderHalfFloatExtensionName = "VK_AMD_gpu_shader_half_float"
	// AmdShaderBallot as defined in vulkan/vulkan_core.h:8650
	AmdShaderBallot = 1
	// AmdShaderBallotSpecVersion as defined in vulkan/vulkan_core.h:8651
	AmdShaderBallotSpecVersion = 1
	// AmdShaderBallotExtensionName as defined in vulkan/vulkan_core.h:8652
	AmdShaderBallotExtensionName = "VK_AMD_shader_ballot"
	// AmdTextureGatherBiasLod as defined in vulkan/vulkan_core.h:8655
	AmdTextureGatherBiasLod = 1
	// AmdTextureGatherBiasLodSpecVersion as defined in vulkan/vulkan_core.h:8656
	AmdTextureGatherBiasLodSpecVersion = 1
	// AmdTextureGatherBiasLodExtensionName as defined in vulkan/vulkan_core.h:8657
	AmdTextureGatherBiasLodExtensionName = "VK_AMD_texture_gather_bias_lod"
	// AmdShaderInfo as defined in vulkan/vulkan_core.h:8666
	AmdShaderInfo = 1
	// AmdShaderInfoSpecVersion as defined in vulkan/vulkan_core.h:8667
	AmdShaderInfoSpecVersion = 1
	// AmdShaderInfoExtensionName as defined in vulkan/vulkan_core.h:8668
	AmdShaderInfoExtensionName = "VK_AMD_shader_info"
	// AmdShaderImageLoadStoreLod as defined in vulkan/vulkan_core.h:8707
	AmdShaderImageLoadStoreLod = 1
	// AmdShaderImageLoadStoreLodSpecVersion as defined in vulkan/vulkan_core.h:8708
	AmdShaderImageLoadStoreLodSpecVersion = 1
	// AmdShaderImageLoadStoreLodExtensionName as defined in vulkan/vulkan_core.h:8709
	AmdShaderImageLoadStoreLodExtensionName = "VK_AMD_shader_image_load_store_lod"
	// NvCornerSampledImage as defined in vulkan/vulkan_core.h:8712
	NvCornerSampledImage = 1
	// NvCornerSampledImageSpecVersion as defined in vulkan/vulkan_core.h:8713
	NvCornerSampledImageSpecVersion = 2
	// NvCornerSampledImageExtensionName as defined in vulkan/vulkan_core.h:8714
	NvCornerSampledImageExtensionName = "VK_NV_corner_sampled_image"
	// ImgFormatPvrtc as defined in vulkan/vulkan_core.h:8723
	ImgFormatPvrtc = 1
	// ImgFormatPvrtcSpecVersion as defined in vulkan/vulkan_core.h:8724
	ImgFormatPvrtcSpecVersion = 1
	// ImgFormatPvrtcExtensionName as defined in vulkan/vulkan_core.h:8725
	ImgFormatPvrtcExtensionName = "VK_IMG_format_pvrtc"
	// NvExternalMemoryCapabilities as defined in vulkan/vulkan_core.h:8728
	NvExternalMemoryCapabilities = 1
	// NvExternalMemoryCapabilitiesSpecVersion as defined in vulkan/vulkan_core.h:8729
	NvExternalMemoryCapabilitiesSpecVersion = 1
	// NvExternalMemoryCapabilitiesExtensionName as defined in vulkan/vulkan_core.h:8730
	NvExternalMemoryCapabilitiesExtensionName = "VK_NV_external_memory_capabilities"
	// NvExternalMemory as defined in vulkan/vulkan_core.h:8770
	NvExternalMemory = 1
	// NvExternalMemorySpecVersion as defined in vulkan/vulkan_core.h:8771
	NvExternalMemorySpecVersion = 1
	// NvExternalMemoryExtensionName as defined in vulkan/vulkan_core.h:8772
	NvExternalMemoryExtensionName = "VK_NV_external_memory"
	// ExtValidationFlags as defined in vulkan/vulkan_core.h:8787
	ExtValidationFlags = 1
	// ExtValidationFlagsSpecVersion as defined in vulkan/vulkan_core.h:8788
	ExtValidationFlagsSpecVersion = 2
	// ExtValidationFlagsExtensionName as defined in vulkan/vulkan_core.h:8789
	ExtValidationFlagsExtensionName = "VK_EXT_validation_flags"
	// ExtShaderSubgroupBallot as defined in vulkan/vulkan_core.h:8805
	ExtShaderSubgroupBallot = 1
	// ExtShaderSubgroupBallotSpecVersion as defined in vulkan/vulkan_core.h:8806
	ExtShaderSubgroupBallotSpecVersion = 1
	// ExtShaderSubgroupBallotExtensionName as defined in vulkan/vulkan_core.h:8807
	ExtShaderSubgroupBallotExtensionName = "VK_EXT_shader_subgroup_ballot"
	// ExtShaderSubgroupVote as defined in vulkan/vulkan_core.h:8810
	ExtShaderSubgroupVote = 1
	// ExtShaderSubgroupVoteSpecVersion as defined in vulkan/vulkan_core.h:8811
	ExtShaderSubgroupVoteSpecVersion = 1
	// ExtShaderSubgroupVoteExtensionName as defined in vulkan/vulkan_core.h:8812
	ExtShaderSubgroupVoteExtensionName = "VK_EXT_shader_subgroup_vote"
	// ExtTextureCompressionAstcHdr as defined in vulkan/vulkan_core.h:8815
	ExtTextureCompressionAstcHdr = 1
	// ExtTextureCompressionAstcHdrSpecVersion as defined in vulkan/vulkan_core.h:8816
	ExtTextureCompressionAstcHdrSpecVersion = 1
	// ExtTextureCompressionAstcHdrExtensionName as defined in vulkan/vulkan_core.h:8817
	ExtTextureCompressionAstcHdrExtensionName = "VK_EXT_texture_compression_astc_hdr"
	// ExtAstcDecodeMode as defined in vulkan/vulkan_core.h:8826
	ExtAstcDecodeMode = 1
	// ExtAstcDecodeModeSpecVersion as defined in vulkan/vulkan_core.h:8827
	ExtAstcDecodeModeSpecVersion = 1
	// ExtAstcDecodeModeExtensionName as defined in vulkan/vulkan_core.h:8828
	ExtAstcDecodeModeExtensionName = "VK_EXT_astc_decode_mode"
	// ExtConditionalRendering as defined in vulkan/vulkan_core.h:8843
	ExtConditionalRendering = 1
	// ExtConditionalRenderingSpecVersion as defined in vulkan/vulkan_core.h:8844
	ExtConditionalRenderingSpecVersion = 2
	// ExtConditionalRenderingExtensionName as defined in vulkan/vulkan_core.h:8845
	ExtConditionalRenderingExtensionName = "VK_EXT_conditional_rendering"
	// NvClipSpaceWScaling as defined in vulkan/vulkan_core.h:8886
	NvClipSpaceWScaling = 1
	// NvClipSpaceWScalingSpecVersion as defined in vulkan/vulkan_core.h:8887
	NvClipSpaceWScalingSpecVersion = 1
	// NvClipSpaceWScalingExtensionName as defined in vulkan/vulkan_core.h:8888
	NvClipSpaceWScalingExtensionName = "VK_NV_clip_space_w_scaling"
	// ExtDirectModeDisplay as defined in vulkan/vulkan_core.h:8913
	ExtDirectModeDisplay = 1
	// ExtDirectModeDisplaySpecVersion as defined in vulkan/vulkan_core.h:8914
	ExtDirectModeDisplaySpecVersion = 1
	// ExtDirectModeDisplayExtensionName as defined in vulkan/vulkan_core.h:8915
	ExtDirectModeDisplayExtensionName = "VK_EXT_direct_mode_display"
	// ExtDisplaySurfaceCounter as defined in vulkan/vulkan_core.h:8925
	ExtDisplaySurfaceCounter = 1
	// ExtDisplaySurfaceCounterSpecVersion as defined in vulkan/vulkan_core.h:8926
	ExtDisplaySurfaceCounterSpecVersion = 1
	// ExtDisplaySurfaceCounterExtensionName as defined in vulkan/vulkan_core.h:8927
	ExtDisplaySurfaceCounterExtensionName = "VK_EXT_display_surface_counter"
	// ExtDisplayControl as defined in vulkan/vulkan_core.h:8961
	ExtDisplayControl = 1
	// ExtDisplayControlSpecVersion as defined in vulkan/vulkan_core.h:8962
	ExtDisplayControlSpecVersion = 1
	// ExtDisplayControlExtensionName as defined in vulkan/vulkan_core.h:8963
	ExtDisplayControlExtensionName = "VK_EXT_display_control"
	// GoogleDisplayTiming as defined in vulkan/vulkan_core.h:9037
	GoogleDisplayTiming = 1
	// GoogleDisplayTimingSpecVersion as defined in vulkan/vulkan_core.h:9038
	GoogleDisplayTimingSpecVersion = 1
	// GoogleDisplayTimingExtensionName as defined in vulkan/vulkan_core.h:9039
	GoogleDisplayTimingExtensionName = "VK_GOOGLE_display_timing"
	// NvSampleMaskOverrideCoverage as defined in vulkan/vulkan_core.h:9081
	NvSampleMaskOverrideCoverage = 1
	// NvSampleMaskOverrideCoverageSpecVersion as defined in vulkan/vulkan_core.h:9082
	NvSampleMaskOverrideCoverageSpecVersion = 1
	// NvSampleMaskOverrideCoverageExtensionName as defined in vulkan/vulkan_core.h:9083
	NvSampleMaskOverrideCoverageExtensionName = "VK_NV_sample_mask_override_coverage"
	// NvGeometryShaderPassthrough as defined in vulkan/vulkan_core.h:9086
	NvGeometryShaderPassthrough = 1
	// NvGeometryShaderPassthroughSpecVersion as defined in vulkan/vulkan_core.h:9087
	NvGeometryShaderPassthroughSpecVersion = 1
	// NvGeometryShaderPassthroughExtensionName as defined in vulkan/vulkan_core.h:9088
	NvGeometryShaderPassthroughExtensionName = "VK_NV_geometry_shader_passthrough"
	// NvViewportArray2 as defined in vulkan/vulkan_core.h:9091
	NvViewportArray2 = 1
	// NvViewportArray2SpecVersion as defined in vulkan/vulkan_core.h:9092
	NvViewportArray2SpecVersion = 1
	// NvViewportArray2ExtensionName as defined in vulkan/vulkan_core.h:9093
	NvViewportArray2ExtensionName = "VK_NV_viewport_array2"
	// NvxMultiviewPerViewAttributes as defined in vulkan/vulkan_core.h:9096
	NvxMultiviewPerViewAttributes = 1
	// NvxMultiviewPerViewAttributesSpecVersion as defined in vulkan/vulkan_core.h:9097
	NvxMultiviewPerViewAttributesSpecVersion = 1
	// NvxMultiviewPerViewAttributesExtensionName as defined in vulkan/vulkan_core.h:9098
	NvxMultiviewPerViewAttributesExtensionName = "VK_NVX_multiview_per_view_attributes"
	// NvViewportSwizzle as defined in vulkan/vulkan_core.h:9107
	NvViewportSwizzle = 1
	// NvViewportSwizzleSpecVersion as defined in vulkan/vulkan_core.h:9108
	NvViewportSwizzleSpecVersion = 1
	// NvViewportSwizzleExtensionName as defined in vulkan/vulkan_core.h:9109
	NvViewportSwizzleExtensionName = "VK_NV_viewport_swizzle"
	// ExtDiscardRectangles as defined in vulkan/vulkan_core.h:9140
	ExtDiscardRectangles = 1
	// ExtDiscardRectanglesSpecVersion as defined in vulkan/vulkan_core.h:9141
	ExtDiscardRectanglesSpecVersion = 1
	// ExtDiscardRectanglesExtensionName as defined in vulkan/vulkan_core.h:9142
	ExtDiscardRectanglesExtensionName = "VK_EXT_discard_rectangles"
	// ExtConservativeRasterization as defined in vulkan/vulkan_core.h:9176
	ExtConservativeRasterization = 1
	// ExtConservativeRasterizationSpecVersion as defined in vulkan/vulkan_core.h:9177
	ExtConservativeRasterizationSpecVersion = 1
	// ExtConservativeRasterizationExtensionName as defined in vulkan/vulkan_core.h:9178
	ExtConservativeRasterizationExtensionName = "VK_EXT_conservative_rasterization"
	// ExtDepthClipEnable as defined in vulkan/vulkan_core.h:9211
	ExtDepthClipEnable = 1
	// ExtDepthClipEnableSpecVersion as defined in vulkan/vulkan_core.h:9212
	ExtDepthClipEnableSpecVersion = 1
	// ExtDepthClipEnableExtensionName as defined in vulkan/vulkan_core.h:9213
	ExtDepthClipEnableExtensionName = "VK_EXT_depth_clip_enable"
	// ExtSwapchainColorspace as defined in vulkan/vulkan_core.h:9230
	ExtSwapchainColorspace = 1
	// ExtSwapchainColorSpaceSpecVersion as defined in vulkan/vulkan_core.h:9231
	ExtSwapchainColorSpaceSpecVersion = 4
	// ExtSwapchainColorSpaceExtensionName as defined in vulkan/vulkan_core.h:9232
	ExtSwapchainColorSpaceExtensionName = "VK_EXT_swapchain_colorspace"
	// ExtHdrMetadata as defined in vulkan/vulkan_core.h:9235
	ExtHdrMetadata = 1
	// ExtHdrMetadataSpecVersion as defined in vulkan/vulkan_core.h:9236
	ExtHdrMetadataSpecVersion = 2
	// ExtHdrMetadataExtensionName as defined in vulkan/vulkan_core.h:9237
	ExtHdrMetadataExtensionName = "VK_EXT_hdr_metadata"
	// ExtExternalMemoryDmaBuf as defined in vulkan/vulkan_core.h:9267
	ExtExternalMemoryDmaBuf = 1
	// ExtExternalMemoryDmaBufSpecVersion as defined in vulkan/vulkan_core.h:9268
	ExtExternalMemoryDmaBufSpecVersion = 1
	// ExtExternalMemoryDmaBufExtensionName as defined in vulkan/vulkan_core.h:9269
	ExtExternalMemoryDmaBufExtensionName = "VK_EXT_external_memory_dma_buf"
	// ExtQueueFamilyForeign as defined in vulkan/vulkan_core.h:9272
	ExtQueueFamilyForeign = 1
	// ExtQueueFamilyForeignSpecVersion as defined in vulkan/vulkan_core.h:9273
	ExtQueueFamilyForeignSpecVersion = 1
	// ExtQueueFamilyForeignExtensionName as defined in vulkan/vulkan_core.h:9274
	ExtQueueFamilyForeignExtensionName = "VK_EXT_queue_family_foreign"
	// QueueFamilyForeign as defined in vulkan/vulkan_core.h:9275
	QueueFamilyForeign = (^uint32(2))
	// ExtDebugUtils as defined in vulkan/vulkan_core.h:9278
	ExtDebugUtils = 1
	// ExtDebugUtilsSpecVersion as defined in vulkan/vulkan_core.h:9280
	ExtDebugUtilsSpecVersion = 2
	// ExtDebugUtilsExtensionName as defined in vulkan/vulkan_core.h:9281
	ExtDebugUtilsExtensionName = "VK_EXT_debug_utils"
	// ExtSamplerFilterMinmax as defined in vulkan/vulkan_core.h:9419
	ExtSamplerFilterMinmax = 1
	// ExtSamplerFilterMinmaxSpecVersion as defined in vulkan/vulkan_core.h:9420
	ExtSamplerFilterMinmaxSpecVersion = 2
	// ExtSamplerFilterMinmaxExtensionName as defined in vulkan/vulkan_core.h:9421
	ExtSamplerFilterMinmaxExtensionName = "VK_EXT_sampler_filter_minmax"
	// AmdGpuShaderInt16 as defined in vulkan/vulkan_core.h:9430
	AmdGpuShaderInt16 = 1
	// AmdGpuShaderInt16SpecVersion as defined in vulkan/vulkan_core.h:9431
	AmdGpuShaderInt16SpecVersion = 2
	// AmdGpuShaderInt16ExtensionName as defined in vulkan/vulkan_core.h:9432
	AmdGpuShaderInt16ExtensionName = "VK_AMD_gpu_shader_int16"
	// AmdMixedAttachmentSamples as defined in vulkan/vulkan_core.h:9435
	AmdMixedAttachmentSamples = 1
	// AmdMixedAttachmentSamplesSpecVersion as defined in vulkan/vulkan_core.h:9436
	AmdMixedAttachmentSamplesSpecVersion = 1
	// AmdMixedAttachmentSamplesExtensionName as defined in vulkan/vulkan_core.h:9437
	AmdMixedAttachmentSamplesExtensionName = "VK_AMD_mixed_attachment_samples"
	// AmdShaderFragmentMask as defined in vulkan/vulkan_core.h:9440
	AmdShaderFragmentMask = 1
	// AmdShaderFragmentMaskSpecVersion as defined in vulkan/vulkan_core.h:9441
	AmdShaderFragmentMaskSpecVersion = 1
	// AmdShaderFragmentMaskExtensionName as defined in vulkan/vulkan_core.h:9442
	AmdShaderFragmentMaskExtensionName = "VK_AMD_shader_fragment_mask"
	// ExtInlineUniformBlock as defined in vulkan/vulkan_core.h:9445
	ExtInlineUniformBlock = 1
	// ExtInlineUniformBlockSpecVersion as defined in vulkan/vulkan_core.h:9446
	ExtInlineUniformBlockSpecVersion = 1
	// ExtInlineUniformBlockExtensionName as defined in vulkan/vulkan_core.h:9447
	ExtInlineUniformBlockExtensionName = "VK_EXT_inline_uniform_block"
	// ExtShaderStencilExport as defined in vulkan/vulkan_core.h:9480
	ExtShaderStencilExport = 1
	// ExtShaderStencilExportSpecVersion as defined in vulkan/vulkan_core.h:9481
	ExtShaderStencilExportSpecVersion = 1
	// ExtShaderStencilExportExtensionName as defined in vulkan/vulkan_core.h:9482
	ExtShaderStencilExportExtensionName = "VK_EXT_shader_stencil_export"
	// ExtSampleLocations as defined in vulkan/vulkan_core.h:9485
	ExtSampleLocations = 1
	// ExtSampleLocationsSpecVersion as defined in vulkan/vulkan_core.h:9486
	ExtSampleLocationsSpecVersion = 1
	// ExtSampleLocationsExtensionName as defined in vulkan/vulkan_core.h:9487
	ExtSampleLocationsExtensionName = "VK_EXT_sample_locations"
	// ExtBlendOperationAdvanced as defined in vulkan/vulkan_core.h:9559
	ExtBlendOperationAdvanced = 1
	// ExtBlendOperationAdvancedSpecVersion as defined in vulkan/vulkan_core.h:9560
	ExtBlendOperationAdvancedSpecVersion = 2
	// ExtBlendOperationAdvancedExtensionName as defined in vulkan/vulkan_core.h:9561
	ExtBlendOperationAdvancedExtensionName = "VK_EXT_blend_operation_advanced"
	// NvFragmentCoverageToColor as defined in vulkan/vulkan_core.h:9596
	NvFragmentCoverageToColor = 1
	// NvFragmentCoverageToColorSpecVersion as defined in vulkan/vulkan_core.h:9597
	NvFragmentCoverageToColorSpecVersion = 1
	// NvFragmentCoverageToColorExtensionName as defined in vulkan/vulkan_core.h:9598
	NvFragmentCoverageToColorExtensionName = "VK_NV_fragment_coverage_to_color"
	// NvFramebufferMixedSamples as defined in vulkan/vulkan_core.h:9610
	NvFramebufferMixedSamples = 1
	// NvFramebufferMixedSamplesSpecVersion as defined in vulkan/vulkan_core.h:9611
	NvFramebufferMixedSamplesSpecVersion = 1
	// NvFramebufferMixedSamplesExtensionName as defined in vulkan/vulkan_core.h:9612
	NvFramebufferMixedSamplesExtensionName = "VK_NV_framebuffer_mixed_samples"
	// NvFillRectangle as defined in vulkan/vulkan_core.h:9634
	NvFillRectangle = 1
	// NvFillRectangleSpecVersion as defined in vulkan/vulkan_core.h:9635
	NvFillRectangleSpecVersion = 1
	// NvFillRectangleExtensionName as defined in vulkan/vulkan_core.h:9636
	NvFillRectangleExtensionName = "VK_NV_fill_rectangle"
	// NvShaderSmBuiltins as defined in vulkan/vulkan_core.h:9639
	NvShaderSmBuiltins = 1
	// NvShaderSmBuiltinsSpecVersion as defined in vulkan/vulkan_core.h:9640
	NvShaderSmBuiltinsSpecVersion = 1
	// NvShaderSmBuiltinsExtensionName as defined in vulkan/vulkan_core.h:9641
	NvShaderSmBuiltinsExtensionName = "VK_NV_shader_sm_builtins"
	// ExtPostDepthCoverage as defined in vulkan/vulkan_core.h:9657
	ExtPostDepthCoverage = 1
	// ExtPostDepthCoverageSpecVersion as defined in vulkan/vulkan_core.h:9658
	ExtPostDepthCoverageSpecVersion = 1
	// ExtPostDepthCoverageExtensionName as defined in vulkan/vulkan_core.h:9659
	ExtPostDepthCoverageExtensionName = "VK_EXT_post_depth_coverage"
	// ExtImageDrmFormatModifier as defined in vulkan/vulkan_core.h:9662
	ExtImageDrmFormatModifier = 1
	// ExtImageDrmFormatModifierSpecVersion as defined in vulkan/vulkan_core.h:9663
	ExtImageDrmFormatModifierSpecVersion = 1
	// ExtImageDrmFormatModifierExtensionName as defined in vulkan/vulkan_core.h:9664
	ExtImageDrmFormatModifierExtensionName = "VK_EXT_image_drm_format_modifier"
	// ExtValidationCache as defined in vulkan/vulkan_core.h:9718
	ExtValidationCache = 1
	// ExtValidationCacheSpecVersion as defined in vulkan/vulkan_core.h:9720
	ExtValidationCacheSpecVersion = 1
	// ExtValidationCacheExtensionName as defined in vulkan/vulkan_core.h:9721
	ExtValidationCacheExtensionName = "VK_EXT_validation_cache"
	// ExtDescriptorIndexing as defined in vulkan/vulkan_core.h:9773
	ExtDescriptorIndexing = 1
	// ExtDescriptorIndexingSpecVersion as defined in vulkan/vulkan_core.h:9774
	ExtDescriptorIndexingSpecVersion = 2
	// ExtDescriptorIndexingExtensionName as defined in vulkan/vulkan_core.h:9775
	ExtDescriptorIndexingExtensionName = "VK_EXT_descriptor_indexing"
	// ExtShaderViewportIndexLayer as defined in vulkan/vulkan_core.h:9792
	ExtShaderViewportIndexLayer = 1
	// ExtShaderViewportIndexLayerSpecVersion as defined in vulkan/vulkan_core.h:9793
	ExtShaderViewportIndexLayerSpecVersion = 1
	// ExtShaderViewportIndexLayerExtensionName as defined in vulkan/vulkan_core.h:9794
	ExtShaderViewportIndexLayerExtensionName = "VK_EXT_shader_viewport_index_layer"
	// NvShadingRateImage as defined in vulkan/vulkan_core.h:9797
	NvShadingRateImage = 1
	// NvShadingRateImageSpecVersion as defined in vulkan/vulkan_core.h:9798
	NvShadingRateImageSpecVersion = 3
	// NvShadingRateImageExtensionName as defined in vulkan/vulkan_core.h:9799
	NvShadingRateImageExtensionName = "VK_NV_shading_rate_image"
	// NvRayTracing as defined in vulkan/vulkan_core.h:9897
	NvRayTracing = 1
	// NvRayTracingSpecVersion as defined in vulkan/vulkan_core.h:9899
	NvRayTracingSpecVersion = 3
	// NvRayTracingExtensionName as defined in vulkan/vulkan_core.h:9900
	NvRayTracingExtensionName = "VK_NV_ray_tracing"
	// ShaderUnused as defined in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkVK_SHADER_UNUSED_KHR
	ShaderUnused = (^uint32(0))
	// ShaderUnusedNv as defined in vulkan/vulkan_core.h:9902
	ShaderUnusedNv = ShaderUnused
	// NvRepresentativeFragmentTest as defined in vulkan/vulkan_core.h:10264
	NvRepresentativeFragmentTest = 1
	// NvRepresentativeFragmentTestSpecVersion as defined in vulkan/vulkan_core.h:10265
	NvRepresentativeFragmentTestSpecVersion = 2
	// NvRepresentativeFragmentTestExtensionName as defined in vulkan/vulkan_core.h:10266
	NvRepresentativeFragmentTestExtensionName = "VK_NV_representative_fragment_test"
	// ExtFilterCubic as defined in vulkan/vulkan_core.h:10281
	ExtFilterCubic = 1
	// ExtFilterCubicSpecVersion as defined in vulkan/vulkan_core.h:10282
	ExtFilterCubicSpecVersion = 3
	// ExtFilterCubicExtensionName as defined in vulkan/vulkan_core.h:10283
	ExtFilterCubicExtensionName = "VK_EXT_filter_cubic"
	// QcomRenderPassShaderResolve as defined in vulkan/vulkan_core.h:10299
	QcomRenderPassShaderResolve = 1
	// QcomRenderPassShaderResolveSpecVersion as defined in vulkan/vulkan_core.h:10300
	QcomRenderPassShaderResolveSpecVersion = 4
	// QcomRenderPassShaderResolveExtensionName as defined in vulkan/vulkan_core.h:10301
	QcomRenderPassShaderResolveExtensionName = "VK_QCOM_render_pass_shader_resolve"
	// ExtGlobalPriority as defined in vulkan/vulkan_core.h:10304
	ExtGlobalPriority = 1
	// ExtGlobalPrioritySpecVersion as defined in vulkan/vulkan_core.h:10305
	ExtGlobalPrioritySpecVersion = 2
	// ExtGlobalPriorityExtensionName as defined in vulkan/vulkan_core.h:10306
	ExtGlobalPriorityExtensionName = "VK_EXT_global_priority"
	// ExtExternalMemoryHost as defined in vulkan/vulkan_core.h:10323
	ExtExternalMemoryHost = 1
	// ExtExternalMemoryHostSpecVersion as defined in vulkan/vulkan_core.h:10324
	ExtExternalMemoryHostSpecVersion = 1
	// ExtExternalMemoryHostExtensionName as defined in vulkan/vulkan_core.h:10325
	ExtExternalMemoryHostExtensionName = "VK_EXT_external_memory_host"
	// AmdBufferMarker as defined in vulkan/vulkan_core.h:10356
	AmdBufferMarker = 1
	// AmdBufferMarkerSpecVersion as defined in vulkan/vulkan_core.h:10357
	AmdBufferMarkerSpecVersion = 1
	// AmdBufferMarkerExtensionName as defined in vulkan/vulkan_core.h:10358
	AmdBufferMarkerExtensionName = "VK_AMD_buffer_marker"
	// AmdPipelineCompilerControl as defined in vulkan/vulkan_core.h:10371
	AmdPipelineCompilerControl = 1
	// AmdPipelineCompilerControlSpecVersion as defined in vulkan/vulkan_core.h:10372
	AmdPipelineCompilerControlSpecVersion = 1
	// AmdPipelineCompilerControlExtensionName as defined in vulkan/vulkan_core.h:10373
	AmdPipelineCompilerControlExtensionName = "VK_AMD_pipeline_compiler_control"
	// ExtCalibratedTimestamps as defined in vulkan/vulkan_core.h:10387
	ExtCalibratedTimestamps = 1
	// ExtCalibratedTimestampsSpecVersion as defined in vulkan/vulkan_core.h:10388
	ExtCalibratedTimestampsSpecVersion = 2
	// ExtCalibratedTimestampsExtensionName as defined in vulkan/vulkan_core.h:10389
	ExtCalibratedTimestampsExtensionName = "VK_EXT_calibrated_timestamps"
	// AmdShaderCoreProperties as defined in vulkan/vulkan_core.h:10422
	AmdShaderCoreProperties = 1
	// AmdShaderCorePropertiesSpecVersion as defined in vulkan/vulkan_core.h:10423
	AmdShaderCorePropertiesSpecVersion = 2
	// AmdShaderCorePropertiesExtensionName as defined in vulkan/vulkan_core.h:10424
	AmdShaderCorePropertiesExtensionName = "VK_AMD_shader_core_properties"
	// AmdMemoryOverallocationBehavior as defined in vulkan/vulkan_core.h:10446
	AmdMemoryOverallocationBehavior = 1
	// AmdMemoryOverallocationBehaviorSpecVersion as defined in vulkan/vulkan_core.h:10447
	AmdMemoryOverallocationBehaviorSpecVersion = 1
	// AmdMemoryOverallocationBehaviorExtensionName as defined in vulkan/vulkan_core.h:10448
	AmdMemoryOverallocationBehaviorExtensionName = "VK_AMD_memory_overallocation_behavior"
	// ExtVertexAttributeDivisor as defined in vulkan/vulkan_core.h:10464
	ExtVertexAttributeDivisor = 1
	// ExtVertexAttributeDivisorSpecVersion as defined in vulkan/vulkan_core.h:10465
	ExtVertexAttributeDivisorSpecVersion = 3
	// ExtVertexAttributeDivisorExtensionName as defined in vulkan/vulkan_core.h:10466
	ExtVertexAttributeDivisorExtensionName = "VK_EXT_vertex_attribute_divisor"
	// ExtPipelineCreationFeedback as defined in vulkan/vulkan_core.h:10494
	ExtPipelineCreationFeedback = 1
	// ExtPipelineCreationFeedbackSpecVersion as defined in vulkan/vulkan_core.h:10495
	ExtPipelineCreationFeedbackSpecVersion = 1
	// ExtPipelineCreationFeedbackExtensionName as defined in vulkan/vulkan_core.h:10496
	ExtPipelineCreationFeedbackExtensionName = "VK_EXT_pipeline_creation_feedback"
	// NvShaderSubgroupPartitioned as defined in vulkan/vulkan_core.h:10520
	NvShaderSubgroupPartitioned = 1
	// NvShaderSubgroupPartitionedSpecVersion as defined in vulkan/vulkan_core.h:10521
	NvShaderSubgroupPartitionedSpecVersion = 1
	// NvShaderSubgroupPartitionedExtensionName as defined in vulkan/vulkan_core.h:10522
	NvShaderSubgroupPartitionedExtensionName = "VK_NV_shader_subgroup_partitioned"
	// NvComputeShaderDerivatives as defined in vulkan/vulkan_core.h:10525
	NvComputeShaderDerivatives = 1
	// NvComputeShaderDerivativesSpecVersion as defined in vulkan/vulkan_core.h:10526
	NvComputeShaderDerivativesSpecVersion = 1
	// NvComputeShaderDerivativesExtensionName as defined in vulkan/vulkan_core.h:10527
	NvComputeShaderDerivativesExtensionName = "VK_NV_compute_shader_derivatives"
	// NvMeshShader as defined in vulkan/vulkan_core.h:10537
	NvMeshShader = 1
	// NvMeshShaderSpecVersion as defined in vulkan/vulkan_core.h:10538
	NvMeshShaderSpecVersion = 1
	// NvMeshShaderExtensionName as defined in vulkan/vulkan_core.h:10539
	NvMeshShaderExtensionName = "VK_NV_mesh_shader"
	// NvFragmentShaderBarycentric as defined in vulkan/vulkan_core.h:10598
	NvFragmentShaderBarycentric = 1
	// NvFragmentShaderBarycentricSpecVersion as defined in vulkan/vulkan_core.h:10599
	NvFragmentShaderBarycentricSpecVersion = 1
	// NvFragmentShaderBarycentricExtensionName as defined in vulkan/vulkan_core.h:10600
	NvFragmentShaderBarycentricExtensionName = "VK_NV_fragment_shader_barycentric"
	// NvShaderImageFootprint as defined in vulkan/vulkan_core.h:10609
	NvShaderImageFootprint = 1
	// NvShaderImageFootprintSpecVersion as defined in vulkan/vulkan_core.h:10610
	NvShaderImageFootprintSpecVersion = 2
	// NvShaderImageFootprintExtensionName as defined in vulkan/vulkan_core.h:10611
	NvShaderImageFootprintExtensionName = "VK_NV_shader_image_footprint"
	// NvScissorExclusive as defined in vulkan/vulkan_core.h:10620
	NvScissorExclusive = 1
	// NvScissorExclusiveSpecVersion as defined in vulkan/vulkan_core.h:10621
	NvScissorExclusiveSpecVersion = 1
	// NvScissorExclusiveExtensionName as defined in vulkan/vulkan_core.h:10622
	NvScissorExclusiveExtensionName = "VK_NV_scissor_exclusive"
	// NvDeviceDiagnosticCheckpoints as defined in vulkan/vulkan_core.h:10647
	NvDeviceDiagnosticCheckpoints = 1
	// NvDeviceDiagnosticCheckpointsSpecVersion as defined in vulkan/vulkan_core.h:10648
	NvDeviceDiagnosticCheckpointsSpecVersion = 2
	// NvDeviceDiagnosticCheckpointsExtensionName as defined in vulkan/vulkan_core.h:10649
	NvDeviceDiagnosticCheckpointsExtensionName = "VK_NV_device_diagnostic_checkpoints"
	// IntelShaderIntegerFunctions2 as defined in vulkan/vulkan_core.h:10678
	IntelShaderIntegerFunctions2 = 1
	// IntelShaderIntegerFunctions2SpecVersion as defined in vulkan/vulkan_core.h:10679
	IntelShaderIntegerFunctions2SpecVersion = 1
	// IntelShaderIntegerFunctions2ExtensionName as defined in vulkan/vulkan_core.h:10680
	IntelShaderIntegerFunctions2ExtensionName = "VK_INTEL_shader_integer_functions2"
	// IntelPerformanceQuery as defined in vulkan/vulkan_core.h:10689
	IntelPerformanceQuery = 1
	// IntelPerformanceQuerySpecVersion as defined in vulkan/vulkan_core.h:10691
	IntelPerformanceQuerySpecVersion = 2
	// IntelPerformanceQueryExtensionName as defined in vulkan/vulkan_core.h:10692
	IntelPerformanceQueryExtensionName = "VK_INTEL_performance_query"
	// ExtPciBusInfo as defined in vulkan/vulkan_core.h:10827
	ExtPciBusInfo = 1
	// ExtPciBusInfoSpecVersion as defined in vulkan/vulkan_core.h:10828
	ExtPciBusInfoSpecVersion = 2
	// ExtPciBusInfoExtensionName as defined in vulkan/vulkan_core.h:10829
	ExtPciBusInfoExtensionName = "VK_EXT_pci_bus_info"
	// AmdDisplayNativeHdr as defined in vulkan/vulkan_core.h:10841
	AmdDisplayNativeHdr = 1
	// AmdDisplayNativeHdrSpecVersion as defined in vulkan/vulkan_core.h:10842
	AmdDisplayNativeHdrSpecVersion = 1
	// AmdDisplayNativeHdrExtensionName as defined in vulkan/vulkan_core.h:10843
	AmdDisplayNativeHdrExtensionName = "VK_AMD_display_native_hdr"
	// ExtFragmentDensityMap as defined in vulkan/vulkan_core.h:10866
	ExtFragmentDensityMap = 1
	// ExtFragmentDensityMapSpecVersion as defined in vulkan/vulkan_core.h:10867
	ExtFragmentDensityMapSpecVersion = 1
	// ExtFragmentDensityMapExtensionName as defined in vulkan/vulkan_core.h:10868
	ExtFragmentDensityMapExtensionName = "VK_EXT_fragment_density_map"
	// ExtScalarBlockLayout as defined in vulkan/vulkan_core.h:10893
	ExtScalarBlockLayout = 1
	// ExtScalarBlockLayoutSpecVersion as defined in vulkan/vulkan_core.h:10894
	ExtScalarBlockLayoutSpecVersion = 1
	// ExtScalarBlockLayoutExtensionName as defined in vulkan/vulkan_core.h:10895
	ExtScalarBlockLayoutExtensionName = "VK_EXT_scalar_block_layout"
	// GoogleHlslFunctionality1 as defined in vulkan/vulkan_core.h:10900
	GoogleHlslFunctionality1 = 1
	// GoogleHlslFunctionality1SpecVersion as defined in vulkan/vulkan_core.h:10901
	GoogleHlslFunctionality1SpecVersion = 1
	// GoogleHlslFunctionality1ExtensionName as defined in vulkan/vulkan_core.h:10902
	GoogleHlslFunctionality1ExtensionName = "VK_GOOGLE_hlsl_functionality1"
	// GoogleDecorateString as defined in vulkan/vulkan_core.h:10905
	GoogleDecorateString = 1
	// GoogleDecorateStringSpecVersion as defined in vulkan/vulkan_core.h:10906
	GoogleDecorateStringSpecVersion = 1
	// GoogleDecorateStringExtensionName as defined in vulkan/vulkan_core.h:10907
	GoogleDecorateStringExtensionName = "VK_GOOGLE_decorate_string"
	// ExtSubgroupSizeControl as defined in vulkan/vulkan_core.h:10910
	ExtSubgroupSizeControl = 1
	// ExtSubgroupSizeControlSpecVersion as defined in vulkan/vulkan_core.h:10911
	ExtSubgroupSizeControlSpecVersion = 2
	// ExtSubgroupSizeControlExtensionName as defined in vulkan/vulkan_core.h:10912
	ExtSubgroupSizeControlExtensionName = "VK_EXT_subgroup_size_control"
	// AmdShaderCoreProperties2 as defined in vulkan/vulkan_core.h:10937
	AmdShaderCoreProperties2 = 1
	// AmdShaderCoreProperties2SpecVersion as defined in vulkan/vulkan_core.h:10938
	AmdShaderCoreProperties2SpecVersion = 1
	// AmdShaderCoreProperties2ExtensionName as defined in vulkan/vulkan_core.h:10939
	AmdShaderCoreProperties2ExtensionName = "VK_AMD_shader_core_properties2"
	// AmdDeviceCoherentMemory as defined in vulkan/vulkan_core.h:10954
	AmdDeviceCoherentMemory = 1
	// AmdDeviceCoherentMemorySpecVersion as defined in vulkan/vulkan_core.h:10955
	AmdDeviceCoherentMemorySpecVersion = 1
	// AmdDeviceCoherentMemoryExtensionName as defined in vulkan/vulkan_core.h:10956
	AmdDeviceCoherentMemoryExtensionName = "VK_AMD_device_coherent_memory"
	// ExtShaderImageAtomicInt64 as defined in vulkan/vulkan_core.h:10965
	ExtShaderImageAtomicInt64 = 1
	// ExtShaderImageAtomicInt64SpecVersion as defined in vulkan/vulkan_core.h:10966
	ExtShaderImageAtomicInt64SpecVersion = 1
	// ExtShaderImageAtomicInt64ExtensionName as defined in vulkan/vulkan_core.h:10967
	ExtShaderImageAtomicInt64ExtensionName = "VK_EXT_shader_image_atomic_int64"
	// ExtMemoryBudget as defined in vulkan/vulkan_core.h:10977
	ExtMemoryBudget = 1
	// ExtMemoryBudgetSpecVersion as defined in vulkan/vulkan_core.h:10978
	ExtMemoryBudgetSpecVersion = 1
	// ExtMemoryBudgetExtensionName as defined in vulkan/vulkan_core.h:10979
	ExtMemoryBudgetExtensionName = "VK_EXT_memory_budget"
	// ExtMemoryPriority as defined in vulkan/vulkan_core.h:10989
	ExtMemoryPriority = 1
	// ExtMemoryPrioritySpecVersion as defined in vulkan/vulkan_core.h:10990
	ExtMemoryPrioritySpecVersion = 1
	// ExtMemoryPriorityExtensionName as defined in vulkan/vulkan_core.h:10991
	ExtMemoryPriorityExtensionName = "VK_EXT_memory_priority"
	// NvDedicatedAllocationImageAliasing as defined in vulkan/vulkan_core.h:11006
	NvDedicatedAllocationImageAliasing = 1
	// NvDedicatedAllocationImageAliasingSpecVersion as defined in vulkan/vulkan_core.h:11007
	NvDedicatedAllocationImageAliasingSpecVersion = 1
	// NvDedicatedAllocationImageAliasingExtensionName as defined in vulkan/vulkan_core.h:11008
	NvDedicatedAllocationImageAliasingExtensionName = "VK_NV_dedicated_allocation_image_aliasing"
	// ExtBufferDeviceAddress as defined in vulkan/vulkan_core.h:11017
	ExtBufferDeviceAddress = 1
	// ExtBufferDeviceAddressSpecVersion as defined in vulkan/vulkan_core.h:11018
	ExtBufferDeviceAddressSpecVersion = 2
	// ExtBufferDeviceAddressExtensionName as defined in vulkan/vulkan_core.h:11019
	ExtBufferDeviceAddressExtensionName = "VK_EXT_buffer_device_address"
	// ExtToolingInfo as defined in vulkan/vulkan_core.h:11047
	ExtToolingInfo = 1
	// ExtToolingInfoSpecVersion as defined in vulkan/vulkan_core.h:11048
	ExtToolingInfoSpecVersion = 1
	// ExtToolingInfoExtensionName as defined in vulkan/vulkan_core.h:11049
	ExtToolingInfoExtensionName = "VK_EXT_tooling_info"
	// ExtSeparateStencilUsage as defined in vulkan/vulkan_core.h:11082
	ExtSeparateStencilUsage = 1
	// ExtSeparateStencilUsageSpecVersion as defined in vulkan/vulkan_core.h:11083
	ExtSeparateStencilUsageSpecVersion = 1
	// ExtSeparateStencilUsageExtensionName as defined in vulkan/vulkan_core.h:11084
	ExtSeparateStencilUsageExtensionName = "VK_EXT_separate_stencil_usage"
	// ExtValidationFeatures as defined in vulkan/vulkan_core.h:11089
	ExtValidationFeatures = 1
	// ExtValidationFeaturesSpecVersion as defined in vulkan/vulkan_core.h:11090
	ExtValidationFeaturesSpecVersion = 4
	// ExtValidationFeaturesExtensionName as defined in vulkan/vulkan_core.h:11091
	ExtValidationFeaturesExtensionName = "VK_EXT_validation_features"
	// NvCooperativeMatrix as defined in vulkan/vulkan_core.h:11123
	NvCooperativeMatrix = 1
	// NvCooperativeMatrixSpecVersion as defined in vulkan/vulkan_core.h:11124
	NvCooperativeMatrixSpecVersion = 1
	// NvCooperativeMatrixExtensionName as defined in vulkan/vulkan_core.h:11125
	NvCooperativeMatrixExtensionName = "VK_NV_cooperative_matrix"
	// NvCoverageReductionMode as defined in vulkan/vulkan_core.h:11185
	NvCoverageReductionMode = 1
	// NvCoverageReductionModeSpecVersion as defined in vulkan/vulkan_core.h:11186
	NvCoverageReductionModeSpecVersion = 1
	// NvCoverageReductionModeExtensionName as defined in vulkan/vulkan_core.h:11187
	NvCoverageReductionModeExtensionName = "VK_NV_coverage_reduction_mode"
	// ExtFragmentShaderInterlock as defined in vulkan/vulkan_core.h:11227
	ExtFragmentShaderInterlock = 1
	// ExtFragmentShaderInterlockSpecVersion as defined in vulkan/vulkan_core.h:11228
	ExtFragmentShaderInterlockSpecVersion = 1
	// ExtFragmentShaderInterlockExtensionName as defined in vulkan/vulkan_core.h:11229
	ExtFragmentShaderInterlockExtensionName = "VK_EXT_fragment_shader_interlock"
	// ExtYcbcrImageArrays as defined in vulkan/vulkan_core.h:11240
	ExtYcbcrImageArrays = 1
	// ExtYcbcrImageArraysSpecVersion as defined in vulkan/vulkan_core.h:11241
	ExtYcbcrImageArraysSpecVersion = 1
	// ExtYcbcrImageArraysExtensionName as defined in vulkan/vulkan_core.h:11242
	ExtYcbcrImageArraysExtensionName = "VK_EXT_ycbcr_image_arrays"
	// ExtProvokingVertex as defined in vulkan/vulkan_core.h:11251
	ExtProvokingVertex = 1
	// ExtProvokingVertexSpecVersion as defined in vulkan/vulkan_core.h:11252
	ExtProvokingVertexSpecVersion = 1
	// ExtProvokingVertexExtensionName as defined in vulkan/vulkan_core.h:11253
	ExtProvokingVertexExtensionName = "VK_EXT_provoking_vertex"
	// ExtHeadlessSurface as defined in vulkan/vulkan_core.h:11282
	ExtHeadlessSurface = 1
	// ExtHeadlessSurfaceSpecVersion as defined in vulkan/vulkan_core.h:11283
	ExtHeadlessSurfaceSpecVersion = 1
	// ExtHeadlessSurfaceExtensionName as defined in vulkan/vulkan_core.h:11284
	ExtHeadlessSurfaceExtensionName = "VK_EXT_headless_surface"
	// ExtLineRasterization as defined in vulkan/vulkan_core.h:11303
	ExtLineRasterization = 1
	// ExtLineRasterizationSpecVersion as defined in vulkan/vulkan_core.h:11304
	ExtLineRasterizationSpecVersion = 1
	// ExtLineRasterizationExtensionName as defined in vulkan/vulkan_core.h:11305
	ExtLineRasterizationExtensionName = "VK_EXT_line_rasterization"
	// ExtShaderAtomicFloat as defined in vulkan/vulkan_core.h:11350
	ExtShaderAtomicFloat = 1
	// ExtShaderAtomicFloatSpecVersion as defined in vulkan/vulkan_core.h:11351
	ExtShaderAtomicFloatSpecVersion = 1
	// ExtShaderAtomicFloatExtensionName as defined in vulkan/vulkan_core.h:11352
	ExtShaderAtomicFloatExtensionName = "VK_EXT_shader_atomic_float"
	// ExtHostQueryReset as defined in vulkan/vulkan_core.h:11372
	ExtHostQueryReset = 1
	// ExtHostQueryResetSpecVersion as defined in vulkan/vulkan_core.h:11373
	ExtHostQueryResetSpecVersion = 1
	// ExtHostQueryResetExtensionName as defined in vulkan/vulkan_core.h:11374
	ExtHostQueryResetExtensionName = "VK_EXT_host_query_reset"
	// ExtIndexTypeUint8 as defined in vulkan/vulkan_core.h:11388
	ExtIndexTypeUint8 = 1
	// ExtIndexTypeUint8SpecVersion as defined in vulkan/vulkan_core.h:11389
	ExtIndexTypeUint8SpecVersion = 1
	// ExtIndexTypeUint8ExtensionName as defined in vulkan/vulkan_core.h:11390
	ExtIndexTypeUint8ExtensionName = "VK_EXT_index_type_uint8"
	// ExtExtendedDynamicState as defined in vulkan/vulkan_core.h:11399
	ExtExtendedDynamicState = 1
	// ExtExtendedDynamicStateSpecVersion as defined in vulkan/vulkan_core.h:11400
	ExtExtendedDynamicStateSpecVersion = 1
	// ExtExtendedDynamicStateExtensionName as defined in vulkan/vulkan_core.h:11401
	ExtExtendedDynamicStateExtensionName = "VK_EXT_extended_dynamic_state"
	// ExtShaderDemoteToHelperInvocation as defined in vulkan/vulkan_core.h:11483
	ExtShaderDemoteToHelperInvocation = 1
	// ExtShaderDemoteToHelperInvocationSpecVersion as defined in vulkan/vulkan_core.h:11484
	ExtShaderDemoteToHelperInvocationSpecVersion = 1
	// ExtShaderDemoteToHelperInvocationExtensionName as defined in vulkan/vulkan_core.h:11485
	ExtShaderDemoteToHelperInvocationExtensionName = "VK_EXT_shader_demote_to_helper_invocation"
	// NvDeviceGeneratedCommands as defined in vulkan/vulkan_core.h:11494
	NvDeviceGeneratedCommands = 1
	// NvDeviceGeneratedCommandsSpecVersion as defined in vulkan/vulkan_core.h:11496
	NvDeviceGeneratedCommandsSpecVersion = 3
	// NvDeviceGeneratedCommandsExtensionName as defined in vulkan/vulkan_core.h:11497
	NvDeviceGeneratedCommandsExtensionName = "VK_NV_device_generated_commands"
	// NvInheritedViewportScissor as defined in vulkan/vulkan_core.h:11684
	NvInheritedViewportScissor = 1
	// NvInheritedViewportScissorSpecVersion as defined in vulkan/vulkan_core.h:11685
	NvInheritedViewportScissorSpecVersion = 1
	// NvInheritedViewportScissorExtensionName as defined in vulkan/vulkan_core.h:11686
	NvInheritedViewportScissorExtensionName = "VK_NV_inherited_viewport_scissor"
	// ExtTexelBufferAlignment as defined in vulkan/vulkan_core.h:11703
	ExtTexelBufferAlignment = 1
	// ExtTexelBufferAlignmentSpecVersion as defined in vulkan/vulkan_core.h:11704
	ExtTexelBufferAlignmentSpecVersion = 1
	// ExtTexelBufferAlignmentExtensionName as defined in vulkan/vulkan_core.h:11705
	ExtTexelBufferAlignmentExtensionName = "VK_EXT_texel_buffer_alignment"
	// QcomRenderPassTransform as defined in vulkan/vulkan_core.h:11723
	QcomRenderPassTransform = 1
	// QcomRenderPassTransformSpecVersion as defined in vulkan/vulkan_core.h:11724
	QcomRenderPassTransformSpecVersion = 2
	// QcomRenderPassTransformExtensionName as defined in vulkan/vulkan_core.h:11725
	QcomRenderPassTransformExtensionName = "VK_QCOM_render_pass_transform"
	// ExtDeviceMemoryReport as defined in vulkan/vulkan_core.h:11741
	ExtDeviceMemoryReport = 1
	// ExtDeviceMemoryReportSpecVersion as defined in vulkan/vulkan_core.h:11742
	ExtDeviceMemoryReportSpecVersion = 2
	// ExtDeviceMemoryReportExtensionName as defined in vulkan/vulkan_core.h:11743
	ExtDeviceMemoryReportExtensionName = "VK_EXT_device_memory_report"
	// ExtRobustness2 as defined in vulkan/vulkan_core.h:11786
	ExtRobustness2 = 1
	// ExtRobustness2SpecVersion as defined in vulkan/vulkan_core.h:11787
	ExtRobustness2SpecVersion = 1
	// ExtRobustness2ExtensionName as defined in vulkan/vulkan_core.h:11788
	ExtRobustness2ExtensionName = "VK_EXT_robustness2"
	// ExtCustomBorderColor as defined in vulkan/vulkan_core.h:11806
	ExtCustomBorderColor = 1
	// ExtCustomBorderColorSpecVersion as defined in vulkan/vulkan_core.h:11807
	ExtCustomBorderColorSpecVersion = 12
	// ExtCustomBorderColorExtensionName as defined in vulkan/vulkan_core.h:11808
	ExtCustomBorderColorExtensionName = "VK_EXT_custom_border_color"
	// GoogleUserType as defined in vulkan/vulkan_core.h:11831
	GoogleUserType = 1
	// GoogleUserTypeSpecVersion as defined in vulkan/vulkan_core.h:11832
	GoogleUserTypeSpecVersion = 1
	// GoogleUserTypeExtensionName as defined in vulkan/vulkan_core.h:11833
	GoogleUserTypeExtensionName = "VK_GOOGLE_user_type"
	// ExtPrivateData as defined in vulkan/vulkan_core.h:11836
	ExtPrivateData = 1
	// ExtPrivateDataSpecVersion as defined in vulkan/vulkan_core.h:11838
	ExtPrivateDataSpecVersion = 1
	// ExtPrivateDataExtensionName as defined in vulkan/vulkan_core.h:11839
	ExtPrivateDataExtensionName = "VK_EXT_private_data"
	// ExtPipelineCreationCacheControl as defined in vulkan/vulkan_core.h:11896
	ExtPipelineCreationCacheControl = 1
	// ExtPipelineCreationCacheControlSpecVersion as defined in vulkan/vulkan_core.h:11897
	ExtPipelineCreationCacheControlSpecVersion = 3
	// ExtPipelineCreationCacheControlExtensionName as defined in vulkan/vulkan_core.h:11898
	ExtPipelineCreationCacheControlExtensionName = "VK_EXT_pipeline_creation_cache_control"
	// NvDeviceDiagnosticsConfig as defined in vulkan/vulkan_core.h:11907
	NvDeviceDiagnosticsConfig = 1
	// NvDeviceDiagnosticsConfigSpecVersion as defined in vulkan/vulkan_core.h:11908
	NvDeviceDiagnosticsConfigSpecVersion = 1
	// NvDeviceDiagnosticsConfigExtensionName as defined in vulkan/vulkan_core.h:11909
	NvDeviceDiagnosticsConfigExtensionName = "VK_NV_device_diagnostics_config"
	// QcomRenderPassStoreOps as defined in vulkan/vulkan_core.h:11932
	QcomRenderPassStoreOps = 1
	// QcomRenderPassStoreOpsSpecVersion as defined in vulkan/vulkan_core.h:11933
	QcomRenderPassStoreOpsSpecVersion = 2
	// QcomRenderPassStoreOpsExtensionName as defined in vulkan/vulkan_core.h:11934
	QcomRenderPassStoreOpsExtensionName = "VK_QCOM_render_pass_store_ops"
	// NvFragmentShadingRateEnums as defined in vulkan/vulkan_core.h:11937
	NvFragmentShadingRateEnums = 1
	// NvFragmentShadingRateEnumsSpecVersion as defined in vulkan/vulkan_core.h:11938
	NvFragmentShadingRateEnumsSpecVersion = 1
	// NvFragmentShadingRateEnumsExtensionName as defined in vulkan/vulkan_core.h:11939
	NvFragmentShadingRateEnumsExtensionName = "VK_NV_fragment_shading_rate_enums"
	// ExtYcbcr2plane444Formats as defined in vulkan/vulkan_core.h:11994
	ExtYcbcr2plane444Formats = 1
	// ExtYcbcr2plane444FormatsSpecVersion as defined in vulkan/vulkan_core.h:11995
	ExtYcbcr2plane444FormatsSpecVersion = 1
	// ExtYcbcr2plane444FormatsExtensionName as defined in vulkan/vulkan_core.h:11996
	ExtYcbcr2plane444FormatsExtensionName = "VK_EXT_ycbcr_2plane_444_formats"
	// ExtFragmentDensityMap2 as defined in vulkan/vulkan_core.h:12005
	ExtFragmentDensityMap2 = 1
	// ExtFragmentDensityMap2SpecVersion as defined in vulkan/vulkan_core.h:12006
	ExtFragmentDensityMap2SpecVersion = 1
	// ExtFragmentDensityMap2ExtensionName as defined in vulkan/vulkan_core.h:12007
	ExtFragmentDensityMap2ExtensionName = "VK_EXT_fragment_density_map2"
	// QcomRotatedCopyCommands as defined in vulkan/vulkan_core.h:12025
	QcomRotatedCopyCommands = 1
	// QcomRotatedCopyCommandsSpecVersion as defined in vulkan/vulkan_core.h:12026
	QcomRotatedCopyCommandsSpecVersion = 1
	// QcomRotatedCopyCommandsExtensionName as defined in vulkan/vulkan_core.h:12027
	QcomRotatedCopyCommandsExtensionName = "VK_QCOM_rotated_copy_commands"
	// ExtImageRobustness as defined in vulkan/vulkan_core.h:12036
	ExtImageRobustness = 1
	// ExtImageRobustnessSpecVersion as defined in vulkan/vulkan_core.h:12037
	ExtImageRobustnessSpecVersion = 1
	// ExtImageRobustnessExtensionName as defined in vulkan/vulkan_core.h:12038
	ExtImageRobustnessExtensionName = "VK_EXT_image_robustness"
	// Ext4444Formats as defined in vulkan/vulkan_core.h:12047
	Ext4444Formats = 1
	// Ext4444FormatsSpecVersion as defined in vulkan/vulkan_core.h:12048
	Ext4444FormatsSpecVersion = 1
	// Ext4444FormatsExtensionName as defined in vulkan/vulkan_core.h:12049
	Ext4444FormatsExtensionName = "VK_EXT_4444_formats"
	// NvAcquireWinrtDisplay as defined in vulkan/vulkan_core.h:12059
	NvAcquireWinrtDisplay = 1
	// NvAcquireWinrtDisplaySpecVersion as defined in vulkan/vulkan_core.h:12060
	NvAcquireWinrtDisplaySpecVersion = 1
	// NvAcquireWinrtDisplayExtensionName as defined in vulkan/vulkan_core.h:12061
	NvAcquireWinrtDisplayExtensionName = "VK_NV_acquire_winrt_display"
	// ValveMutableDescriptorType as defined in vulkan/vulkan_core.h:12077
	ValveMutableDescriptorType = 1
	// ValveMutableDescriptorTypeSpecVersion as defined in vulkan/vulkan_core.h:12078
	ValveMutableDescriptorTypeSpecVersion = 1
	// ValveMutableDescriptorTypeExtensionName as defined in vulkan/vulkan_core.h:12079
	ValveMutableDescriptorTypeExtensionName = "VK_VALVE_mutable_descriptor_type"
	// ExtVertexInputDynamicState as defined in vulkan/vulkan_core.h:12100
	ExtVertexInputDynamicState = 1
	// ExtVertexInputDynamicStateSpecVersion as defined in vulkan/vulkan_core.h:12101
	ExtVertexInputDynamicStateSpecVersion = 2
	// ExtVertexInputDynamicStateExtensionName as defined in vulkan/vulkan_core.h:12102
	ExtVertexInputDynamicStateExtensionName = "VK_EXT_vertex_input_dynamic_state"
	// ExtExtendedDynamicState2 as defined in vulkan/vulkan_core.h:12139
	ExtExtendedDynamicState2 = 1
	// ExtExtendedDynamicState2SpecVersion as defined in vulkan/vulkan_core.h:12140
	ExtExtendedDynamicState2SpecVersion = 1
	// ExtExtendedDynamicState2ExtensionName as defined in vulkan/vulkan_core.h:12141
	ExtExtendedDynamicState2ExtensionName = "VK_EXT_extended_dynamic_state2"
	// ExtColorWriteEnable as defined in vulkan/vulkan_core.h:12179
	ExtColorWriteEnable = 1
	// ExtColorWriteEnableSpecVersion as defined in vulkan/vulkan_core.h:12180
	ExtColorWriteEnableSpecVersion = 1
	// ExtColorWriteEnableExtensionName as defined in vulkan/vulkan_core.h:12181
	ExtColorWriteEnableExtensionName = "VK_EXT_color_write_enable"
	// KhrAccelerationStructure as defined in vulkan/vulkan_core.h:12205
	KhrAccelerationStructure = 1
	// KhrAccelerationStructureSpecVersion as defined in vulkan/vulkan_core.h:12207
	KhrAccelerationStructureSpecVersion = 11
	// KhrAccelerationStructureExtensionName as defined in vulkan/vulkan_core.h:12208
	KhrAccelerationStructureExtensionName = "VK_KHR_acceleration_structure"
	// KhrRayTracingPipeline as defined in vulkan/vulkan_core.h:12502
	KhrRayTracingPipeline = 1
	// KhrRayTracingPipelineSpecVersion as defined in vulkan/vulkan_core.h:12503
	KhrRayTracingPipelineSpecVersion = 1
	// KhrRayTracingPipelineExtensionName as defined in vulkan/vulkan_core.h:12504
	KhrRayTracingPipelineExtensionName = "VK_KHR_ray_tracing_pipeline"
	// KhrRayQuery as defined in vulkan/vulkan_core.h:12638
	KhrRayQuery = 1
	// KhrRayQuerySpecVersion as defined in vulkan/vulkan_core.h:12639
	KhrRayQuerySpecVersion = 1
	// KhrRayQueryExtensionName as defined in vulkan/vulkan_core.h:12640
	KhrRayQueryExtensionName = "VK_KHR_ray_query"
	// KhrXcbSurface as defined in vulkan/vulkan_xcb.h:22
	KhrXcbSurface = 1
	// KhrXcbSurfaceSpecVersion as defined in vulkan/vulkan_xcb.h:23
	KhrXcbSurfaceSpecVersion = 6
	// KhrXcbSurfaceExtensionName as defined in vulkan/vulkan_xcb.h:24
	KhrXcbSurfaceExtensionName = "VK_KHR_xcb_surface"
	// KhrXlibSurface as defined in vulkan/vulkan_xlib.h:22
	KhrXlibSurface = 1
	// KhrXlibSurfaceSpecVersion as defined in vulkan/vulkan_xlib.h:23
	KhrXlibSurfaceSpecVersion = 6
	// KhrXlibSurfaceExtensionName as defined in vulkan/vulkan_xlib.h:24
	KhrXlibSurfaceExtensionName = "VK_KHR_xlib_surface"
	// ExtAcquireXlibDisplay as defined in vulkan/vulkan_xlib_xrandr.h:22
	ExtAcquireXlibDisplay = 1
	// ExtAcquireXlibDisplaySpecVersion as defined in vulkan/vulkan_xlib_xrandr.h:23
	ExtAcquireXlibDisplaySpecVersion = 1
	// ExtAcquireXlibDisplayExtensionName as defined in vulkan/vulkan_xlib_xrandr.h:24
	ExtAcquireXlibDisplayExtensionName = "VK_EXT_acquire_xlib_display"
	// KhrWaylandSurface as defined in vulkan/vulkan_wayland.h:22
	KhrWaylandSurface = 1
	// KhrWaylandSurfaceSpecVersion as defined in vulkan/vulkan_wayland.h:23
	KhrWaylandSurfaceSpecVersion = 6
	// KhrWaylandSurfaceExtensionName as defined in vulkan/vulkan_wayland.h:24
	KhrWaylandSurfaceExtensionName = "VK_KHR_wayland_surface"
)

// Result as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkResult.html
type Result int32

// Result enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkResult.html
const (
	Success                                  Result = iota
	NotReady                                 Result = 1
	Timeout                                  Result = 2
	EventSet                                 Result = 3
	EventReset                               Result = 4
	Incomplete                               Result = 5
	ErrorOutOfHostMemory                     Result = -1
	ErrorOutOfDeviceMemory                   Result = -2
	ErrorInitializationFailed                Result = -3
	ErrorDeviceLost                          Result = -4
	ErrorMemoryMapFailed                     Result = -5
	ErrorLayerNotPresent                     Result = -6
	ErrorExtensionNotPresent                 Result = -7
	ErrorFeatureNotPresent                   Result = -8
	ErrorIncompatibleDriver                  Result = -9
	ErrorTooManyObjects                      Result = -10
	ErrorFormatNotSupported                  Result = -11
	ErrorFragmentedPool                      Result = -12
	ErrorUnknown                             Result = -13
	ErrorOutOfPoolMemory                     Result = -1000069000
	ErrorInvalidExternalHandle               Result = -1000072003
	ErrorFragmentation                       Result = -1000161000
	ErrorInvalidOpaqueCaptureAddress         Result = -1000257000
	ErrorSurfaceLost                         Result = -1000000000
	ErrorNativeWindowInUse                   Result = -1000000001
	Suboptimal                               Result = 1000001003
	ErrorOutOfDate                           Result = -1000001004
	ErrorIncompatibleDisplay                 Result = -1000003001
	ErrorValidationFailed                    Result = -1000011001
	ErrorInvalidShaderNv                     Result = -1000012000
	ErrorInvalidDrmFormatModifierPlaneLayout Result = -1000158000
	ErrorNotPermitted                        Result = -1000174001
	ErrorFullScreenExclusiveModeLost         Result = -1000255000
	ThreadIdle                               Result = 1000268000
	ThreadDone                               Result = 1000268001
	OperationDeferred                        Result = 1000268002
	OperationNotDeferred                     Result = 1000268003
	PipelineCompileRequired                  Result = 1000297000
	ErrorInvalidDeviceAddress                Result = -1000257000
	ErrorPipelineCompileRequired             Result = 1000297000
	ResultMaxEnum                            Result = 2147483647
)

// StructureType as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkStructureType.html
type StructureType int32

// StructureType enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkStructureType.html
const (
	StructureTypeApplicationInfo                                          StructureType = iota
	StructureTypeInstanceCreateInfo                                       StructureType = 1
	StructureTypeDeviceQueueCreateInfo                                    StructureType = 2
	StructureTypeDeviceCreateInfo                                         StructureType = 3
	StructureTypeSubmitInfo                                               StructureType = 4
	StructureTypeMemoryAllocateInfo                                       StructureType = 5
	StructureTypeMappedMemoryRange                                        StructureType = 6
	StructureTypeBindSparseInfo                                           StructureType = 7
	StructureTypeFenceCreateInfo                                          StructureType = 8
	StructureTypeSemaphoreCreateInfo                                      StructureType = 9
	StructureTypeEventCreateInfo                                          StructureType = 10
	StructureTypeQueryPoolCreateInfo                                      StructureType = 11
	StructureTypeBufferCreateInfo                                         StructureType = 12
	StructureTypeBufferViewCreateInfo                                     StructureType = 13
	StructureTypeImageCreateInfo                                          StructureType = 14
	StructureTypeImageViewCreateInfo                                      StructureType = 15
	StructureTypeShaderModuleCreateInfo                                   StructureType = 16
	StructureTypePipelineCacheCreateInfo                                  StructureType = 17
	StructureTypePipelineShaderStageCreateInfo                            StructureType = 18
	StructureTypePipelineVertexInputStateCreateInfo                       StructureType = 19
	StructureTypePipelineInputAssemblyStateCreateInfo                     StructureType = 20
	StructureTypePipelineTessellationStateCreateInfo                      StructureType = 21
	StructureTypePipelineViewportStateCreateInfo                          StructureType = 22
	StructureTypePipelineRasterizationStateCreateInfo                     StructureType = 23
	StructureTypePipelineMultisampleStateCreateInfo                       StructureType = 24
	StructureTypePipelineDepthStencilStateCreateInfo                      StructureType = 25
	StructureTypePipelineColorBlendStateCreateInfo                        StructureType = 26
	StructureTypePipelineDynamicStateCreateInfo                           StructureType = 27
	StructureTypeGraphicsPipelineCreateInfo                               StructureType = 28
	StructureTypeComputePipelineCreateInfo                                StructureType = 29
	StructureTypePipelineLayoutCreateInfo                                 StructureType = 30
	StructureTypeSamplerCreateInfo                                        StructureType = 31
	StructureTypeDescriptorSetLayoutCreateInfo                            StructureType = 32
	StructureTypeDescriptorPoolCreateInfo                                 StructureType = 33
	StructureTypeDescriptorSetAllocateInfo                                StructureType = 34
	StructureTypeWriteDescriptorSet                                       StructureType = 35
	StructureTypeCopyDescriptorSet                                        StructureType = 36
	StructureTypeFramebufferCreateInfo                                    StructureType = 37
	StructureTypeRenderPassCreateInfo                                     StructureType = 38
	StructureTypeCommandPoolCreateInfo                                    StructureType = 39
	StructureTypeCommandBufferAllocateInfo                                StructureType = 40
	StructureTypeCommandBufferInheritanceInfo                             StructureType = 41
	StructureTypeCommandBufferBeginInfo                                   StructureType = 42
	StructureTypeRenderPassBeginInfo                                      StructureType = 43
	StructureTypeBufferMemoryBarrier                                      StructureType = 44
	StructureTypeImageMemoryBarrier                                       StructureType = 45
	StructureTypeMemoryBarrier                                            StructureType = 46
	StructureTypeLoaderInstanceCreateInfo                                 StructureType = 47
	StructureTypeLoaderDeviceCreateInfo                                   StructureType = 48
	StructureTypePhysicalDeviceSubgroupProperties                         StructureType = 1000094000
	StructureTypeBindBufferMemoryInfo                                     StructureType = 1000157000
	StructureTypeBindImageMemoryInfo                                      StructureType = 1000157001
	StructureTypePhysicalDevice16bitStorageFeatures                       StructureType = 1000083000
	StructureTypeMemoryDedicatedRequirements                              StructureType = 1000127000
	StructureTypeMemoryDedicatedAllocateInfo                              StructureType = 1000127001
	StructureTypeMemoryAllocateFlagsInfo                                  StructureType = 1000060000
	StructureTypeDeviceGroupRenderPassBeginInfo                           StructureType = 1000060003
	StructureTypeDeviceGroupCommandBufferBeginInfo                        StructureType = 1000060004
	StructureTypeDeviceGroupSubmitInfo                                    StructureType = 1000060005
	StructureTypeDeviceGroupBindSparseInfo                                StructureType = 1000060006
	StructureTypeBindBufferMemoryDeviceGroupInfo                          StructureType = 1000060013
	StructureTypeBindImageMemoryDeviceGroupInfo                           StructureType = 1000060014
	StructureTypePhysicalDeviceGroupProperties                            StructureType = 1000070000
	StructureTypeDeviceGroupDeviceCreateInfo                              StructureType = 1000070001
	StructureTypeBufferMemoryRequirementsInfo2                            StructureType = 1000146000
	StructureTypeImageMemoryRequirementsInfo2                             StructureType = 1000146001
	StructureTypeImageSparseMemoryRequirementsInfo2                       StructureType = 1000146002
	StructureTypeMemoryRequirements2                                      StructureType = 1000146003
	StructureTypeSparseImageMemoryRequirements2                           StructureType = 1000146004
	StructureTypePhysicalDeviceFeatures2                                  StructureType = 1000059000
	StructureTypePhysicalDeviceProperties2                                StructureType = 1000059001
	StructureTypeFormatProperties2                                        StructureType = 1000059002
	StructureTypeImageFormatProperties2                                   StructureType = 1000059003
	StructureTypePhysicalDeviceImageFormatInfo2                           StructureType = 1000059004
	StructureTypeQueueFamilyProperties2                                   StructureType = 1000059005
	StructureTypePhysicalDeviceMemoryProperties2                          StructureType = 1000059006
	StructureTypeSparseImageFormatProperties2                             StructureType = 1000059007
	StructureTypePhysicalDeviceSparseImageFormatInfo2                     StructureType = 1000059008
	StructureTypePhysicalDevicePointClippingProperties                    StructureType = 1000117000
	StructureTypeRenderPassInputAttachmentAspectCreateInfo                StructureType = 1000117001
	StructureTypeImageViewUsageCreateInfo                                 StructureType = 1000117002
	StructureTypePipelineTessellationDomainOriginStateCreateInfo          StructureType = 1000117003
	StructureTypeRenderPassMultiviewCreateInfo                            StructureType = 1000053000
	StructureTypePhysicalDeviceMultiviewFeatures                          StructureType = 1000053001
	StructureTypePhysicalDeviceMultiviewProperties                        StructureType = 1000053002
	StructureTypePhysicalDeviceVariablePointersFeatures                   StructureType = 1000120000
	StructureTypeProtectedSubmitInfo                                      StructureType = 1000145000
	StructureTypePhysicalDeviceProtectedMemoryFeatures                    StructureType = 1000145001
	StructureTypePhysicalDeviceProtectedMemoryProperties                  StructureType = 1000145002
	StructureTypeDeviceQueueInfo2                                         StructureType = 1000145003
	StructureTypeSamplerYcbcrConversionCreateInfo                         StructureType = 1000156000
	StructureTypeSamplerYcbcrConversionInfo                               StructureType = 1000156001
	StructureTypeBindImagePlaneMemoryInfo                                 StructureType = 1000156002
	StructureTypeImagePlaneMemoryRequirementsInfo                         StructureType = 1000156003
	StructureTypePhysicalDeviceSamplerYcbcrConversionFeatures             StructureType = 1000156004
	StructureTypeSamplerYcbcrConversionImageFormatProperties              StructureType = 1000156005
	StructureTypeDescriptorUpdateTemplateCreateInfo                       StructureType = 1000085000
	StructureTypePhysicalDeviceExternalImageFormatInfo                    StructureType = 1000071000
	StructureTypeExternalImageFormatProperties                            StructureType = 1000071001
	StructureTypePhysicalDeviceExternalBufferInfo                         StructureType = 1000071002
	StructureTypeExternalBufferProperties                                 StructureType = 1000071003
	StructureTypePhysicalDeviceIdProperties                               StructureType = 1000071004
	StructureTypeExternalMemoryBufferCreateInfo                           StructureType = 1000072000
	StructureTypeExternalMemoryImageCreateInfo                            StructureType = 1000072001
	StructureTypeExportMemoryAllocateInfo                                 StructureType = 1000072002
	StructureTypePhysicalDeviceExternalFenceInfo                          StructureType = 1000112000
	StructureTypeExternalFenceProperties                                  StructureType = 1000112001
	StructureTypeExportFenceCreateInfo                                    StructureType = 1000113000
	StructureTypeExportSemaphoreCreateInfo                                StructureType = 1000077000
	StructureTypePhysicalDeviceExternalSemaphoreInfo                      StructureType = 1000076000
	StructureTypeExternalSemaphoreProperties                              StructureType = 1000076001
	StructureTypePhysicalDeviceMaintenance3Properties                     StructureType = 1000168000
	StructureTypeDescriptorSetLayoutSupport                               StructureType = 1000168001
	StructureTypePhysicalDeviceShaderDrawParametersFeatures               StructureType = 1000063000
	StructureTypePhysicalDeviceVulkan11Features                           StructureType = 49
	StructureTypePhysicalDeviceVulkan11Properties                         StructureType = 50
	StructureTypePhysicalDeviceVulkan12Features                           StructureType = 51
	StructureTypePhysicalDeviceVulkan12Properties                         StructureType = 52
	StructureTypeImageFormatListCreateInfo                                StructureType = 1000147000
	StructureTypeAttachmentDescription2                                   StructureType = 1000109000
	StructureTypeAttachmentReference2                                     StructureType = 1000109001
	StructureTypeSubpassDescription2                                      StructureType = 1000109002
	StructureTypeSubpassDependency2                                       StructureType = 1000109003
	StructureTypeRenderPassCreateInfo2                                    StructureType = 1000109004
	StructureTypeSubpassBeginInfo                                         StructureType = 1000109005
	StructureTypeSubpassEndInfo                                           StructureType = 1000109006
	StructureTypePhysicalDevice8bitStorageFeatures                        StructureType = 1000177000
	StructureTypePhysicalDeviceDriverProperties                           StructureType = 1000196000
	StructureTypePhysicalDeviceShaderAtomicInt64Features                  StructureType = 1000180000
	StructureTypePhysicalDeviceShaderFloat16Int8Features                  StructureType = 1000082000
	StructureTypePhysicalDeviceFloatControlsProperties                    StructureType = 1000197000
	StructureTypeDescriptorSetLayoutBindingFlagsCreateInfo                StructureType = 1000161000
	StructureTypePhysicalDeviceDescriptorIndexingFeatures                 StructureType = 1000161001
	StructureTypePhysicalDeviceDescriptorIndexingProperties               StructureType = 1000161002
	StructureTypeDescriptorSetVariableDescriptorCountAllocateInfo         StructureType = 1000161003
	StructureTypeDescriptorSetVariableDescriptorCountLayoutSupport        StructureType = 1000161004
	StructureTypePhysicalDeviceDepthStencilResolveProperties              StructureType = 1000199000
	StructureTypeSubpassDescriptionDepthStencilResolve                    StructureType = 1000199001
	StructureTypePhysicalDeviceScalarBlockLayoutFeatures                  StructureType = 1000221000
	StructureTypeImageStencilUsageCreateInfo                              StructureType = 1000246000
	StructureTypePhysicalDeviceSamplerFilterMinmaxProperties              StructureType = 1000130000
	StructureTypeSamplerReductionModeCreateInfo                           StructureType = 1000130001
	StructureTypePhysicalDeviceVulkanMemoryModelFeatures                  StructureType = 1000211000
	StructureTypePhysicalDeviceImagelessFramebufferFeatures               StructureType = 1000108000
	StructureTypeFramebufferAttachmentsCreateInfo                         StructureType = 1000108001
	StructureTypeFramebufferAttachmentImageInfo                           StructureType = 1000108002
	StructureTypeRenderPassAttachmentBeginInfo                            StructureType = 1000108003
	StructureTypePhysicalDeviceUniformBufferStandardLayoutFeatures        StructureType = 1000253000
	StructureTypePhysicalDeviceShaderSubgroupExtendedTypesFeatures        StructureType = 1000175000
	StructureTypePhysicalDeviceSeparateDepthStencilLayoutsFeatures        StructureType = 1000241000
	StructureTypeAttachmentReferenceStencilLayout                         StructureType = 1000241001
	StructureTypeAttachmentDescriptionStencilLayout                       StructureType = 1000241002
	StructureTypePhysicalDeviceHostQueryResetFeatures                     StructureType = 1000261000
	StructureTypePhysicalDeviceTimelineSemaphoreFeatures                  StructureType = 1000207000
	StructureTypePhysicalDeviceTimelineSemaphoreProperties                StructureType = 1000207001
	StructureTypeSemaphoreTypeCreateInfo                                  StructureType = 1000207002
	StructureTypeTimelineSemaphoreSubmitInfo                              StructureType = 1000207003
	StructureTypeSemaphoreWaitInfo                                        StructureType = 1000207004
	StructureTypeSemaphoreSignalInfo                                      StructureType = 1000207005
	StructureTypePhysicalDeviceBufferDeviceAddressFeatures                StructureType = 1000257000
	StructureTypeBufferDeviceAddressInfo                                  StructureType = 1000244001
	StructureTypeBufferOpaqueCaptureAddressCreateInfo                     StructureType = 1000257002
	StructureTypeMemoryOpaqueCaptureAddressAllocateInfo                   StructureType = 1000257003
	StructureTypeDeviceMemoryOpaqueCaptureAddressInfo                     StructureType = 1000257004
	StructureTypeSwapchainCreateInfo                                      StructureType = 1000001000
	StructureTypePresentInfo                                              StructureType = 1000001001
	StructureTypeDeviceGroupPresentCapabilities                           StructureType = 1000060007
	StructureTypeImageSwapchainCreateInfo                                 StructureType = 1000060008
	StructureTypeBindImageMemorySwapchainInfo                             StructureType = 1000060009
	StructureTypeAcquireNextImageInfo                                     StructureType = 1000060010
	StructureTypeDeviceGroupPresentInfo                                   StructureType = 1000060011
	StructureTypeDeviceGroupSwapchainCreateInfo                           StructureType = 1000060012
	StructureTypeDisplayModeCreateInfo                                    StructureType = 1000002000
	StructureTypeDisplaySurfaceCreateInfo                                 StructureType = 1000002001
	StructureTypeDisplayPresentInfo                                       StructureType = 1000003000
	StructureTypeXlibSurfaceCreateInfo                                    StructureType = 1000004000
	StructureTypeXcbSurfaceCreateInfo                                     StructureType = 1000005000
	StructureTypeWaylandSurfaceCreateInfo                                 StructureType = 1000006000
	StructureTypeAndroidSurfaceCreateInfo                                 StructureType = 1000008000
	StructureTypeWin32SurfaceCreateInfo                                   StructureType = 1000009000
	StructureTypeDebugReportCallbackCreateInfo                            StructureType = 1000011000
	StructureTypePipelineRasterizationStateRasterizationOrderAmd          StructureType = 1000018000
	StructureTypeDebugMarkerObjectNameInfo                                StructureType = 1000022000
	StructureTypeDebugMarkerObjectTagInfo                                 StructureType = 1000022001
	StructureTypeDebugMarkerMarkerInfo                                    StructureType = 1000022002
	StructureTypeDedicatedAllocationImageCreateInfoNv                     StructureType = 1000026000
	StructureTypeDedicatedAllocationBufferCreateInfoNv                    StructureType = 1000026001
	StructureTypeDedicatedAllocationMemoryAllocateInfoNv                  StructureType = 1000026002
	StructureTypePhysicalDeviceTransformFeedbackFeatures                  StructureType = 1000028000
	StructureTypePhysicalDeviceTransformFeedbackProperties                StructureType = 1000028001
	StructureTypePipelineRasterizationStateStreamCreateInfo               StructureType = 1000028002
	StructureTypeImageViewHandleInfoNvx                                   StructureType = 1000030000
	StructureTypeImageViewAddressPropertiesNvx                            StructureType = 1000030001
	StructureTypeTextureLodGatherFormatPropertiesAmd                      StructureType = 1000041000
	StructureTypeStreamDescriptorSurfaceCreateInfoGgp                     StructureType = 1000049000
	StructureTypePhysicalDeviceCornerSampledImageFeaturesNv               StructureType = 1000050000
	StructureTypeExternalMemoryImageCreateInfoNv                          StructureType = 1000056000
	StructureTypeExportMemoryAllocateInfoNv                               StructureType = 1000056001
	StructureTypeImportMemoryWin32HandleInfoNv                            StructureType = 1000057000
	StructureTypeExportMemoryWin32HandleInfoNv                            StructureType = 1000057001
	StructureTypeWin32KeyedMutexAcquireReleaseInfoNv                      StructureType = 1000058000
	StructureTypeValidationFlags                                          StructureType = 1000061000
	StructureTypeViSurfaceCreateInfoNn                                    StructureType = 1000062000
	StructureTypePhysicalDeviceTextureCompressionAstcHdrFeatures          StructureType = 1000066000
	StructureTypeImageViewAstcDecodeMode                                  StructureType = 1000067000
	StructureTypePhysicalDeviceAstcDecodeFeatures                         StructureType = 1000067001
	StructureTypeImportMemoryWin32HandleInfo                              StructureType = 1000073000
	StructureTypeExportMemoryWin32HandleInfo                              StructureType = 1000073001
	StructureTypeMemoryWin32HandleProperties                              StructureType = 1000073002
	StructureTypeMemoryGetWin32HandleInfo                                 StructureType = 1000073003
	StructureTypeImportMemoryFdInfo                                       StructureType = 1000074000
	StructureTypeMemoryFdProperties                                       StructureType = 1000074001
	StructureTypeMemoryGetFdInfo                                          StructureType = 1000074002
	StructureTypeWin32KeyedMutexAcquireReleaseInfo                        StructureType = 1000075000
	StructureTypeImportSemaphoreWin32HandleInfo                           StructureType = 1000078000
	StructureTypeExportSemaphoreWin32HandleInfo                           StructureType = 1000078001
	StructureTypeD3d12FenceSubmitInfo                                     StructureType = 1000078002
	StructureTypeSemaphoreGetWin32HandleInfo                              StructureType = 1000078003
	StructureTypeImportSemaphoreFdInfo                                    StructureType = 1000079000
	StructureTypeSemaphoreGetFdInfo                                       StructureType = 1000079001
	StructureTypePhysicalDevicePushDescriptorProperties                   StructureType = 1000080000
	StructureTypeCommandBufferInheritanceConditionalRenderingInfo         StructureType = 1000081000
	StructureTypePhysicalDeviceConditionalRenderingFeatures               StructureType = 1000081001
	StructureTypeConditionalRenderingBeginInfo                            StructureType = 1000081002
	StructureTypePresentRegions                                           StructureType = 1000084000
	StructureTypePipelineViewportWScalingStateCreateInfoNv                StructureType = 1000087000
	StructureTypeSurfaceCapabilities2                                     StructureType = 1000090000
	StructureTypeDisplayPowerInfo                                         StructureType = 1000091000
	StructureTypeDeviceEventInfo                                          StructureType = 1000091001
	StructureTypeDisplayEventInfo                                         StructureType = 1000091002
	StructureTypeSwapchainCounterCreateInfo                               StructureType = 1000091003
	StructureTypePresentTimesInfoGoogle                                   StructureType = 1000092000
	StructureTypePhysicalDeviceMultiviewPerViewAttributesPropertiesNvx    StructureType = 1000097000
	StructureTypePipelineViewportSwizzleStateCreateInfoNv                 StructureType = 1000098000
	StructureTypePhysicalDeviceDiscardRectangleProperties                 StructureType = 1000099000
	StructureTypePipelineDiscardRectangleStateCreateInfo                  StructureType = 1000099001
	StructureTypePhysicalDeviceConservativeRasterizationProperties        StructureType = 1000101000
	StructureTypePipelineRasterizationConservativeStateCreateInfo         StructureType = 1000101001
	StructureTypePhysicalDeviceDepthClipEnableFeatures                    StructureType = 1000102000
	StructureTypePipelineRasterizationDepthClipStateCreateInfo            StructureType = 1000102001
	StructureTypeHdrMetadata                                              StructureType = 1000105000
	StructureTypeSharedPresentSurfaceCapabilities                         StructureType = 1000111000
	StructureTypeImportFenceWin32HandleInfo                               StructureType = 1000114000
	StructureTypeExportFenceWin32HandleInfo                               StructureType = 1000114001
	StructureTypeFenceGetWin32HandleInfo                                  StructureType = 1000114002
	StructureTypeImportFenceFdInfo                                        StructureType = 1000115000
	StructureTypeFenceGetFdInfo                                           StructureType = 1000115001
	StructureTypePhysicalDevicePerformanceQueryFeatures                   StructureType = 1000116000
	StructureTypePhysicalDevicePerformanceQueryProperties                 StructureType = 1000116001
	StructureTypeQueryPoolPerformanceCreateInfo                           StructureType = 1000116002
	StructureTypePerformanceQuerySubmitInfo                               StructureType = 1000116003
	StructureTypeAcquireProfilingLockInfo                                 StructureType = 1000116004
	StructureTypePerformanceCounter                                       StructureType = 1000116005
	StructureTypePerformanceCounterDescription                            StructureType = 1000116006
	StructureTypePhysicalDeviceSurfaceInfo2                               StructureType = 1000119000
	StructureTypeSurfaceFormat2                                           StructureType = 1000119002
	StructureTypeDisplayProperties2                                       StructureType = 1000121000
	StructureTypeDisplayPlaneProperties2                                  StructureType = 1000121001
	StructureTypeDisplayModeProperties2                                   StructureType = 1000121002
	StructureTypeDisplayPlaneInfo2                                        StructureType = 1000121003
	StructureTypeDisplayPlaneCapabilities2                                StructureType = 1000121004
	StructureTypeIosSurfaceCreateInfoMvk                                  StructureType = 1000122000
	StructureTypeMacosSurfaceCreateInfoMvk                                StructureType = 1000123000
	StructureTypeDebugUtilsObjectNameInfo                                 StructureType = 1000128000
	StructureTypeDebugUtilsObjectTagInfo                                  StructureType = 1000128001
	StructureTypeDebugUtilsLabel                                          StructureType = 1000128002
	StructureTypeDebugUtilsMessengerCallbackData                          StructureType = 1000128003
	StructureTypeDebugUtilsMessengerCreateInfo                            StructureType = 1000128004
	StructureTypeAndroidHardwareBufferUsageAndroid                        StructureType = 1000129000
	StructureTypeAndroidHardwareBufferPropertiesAndroid                   StructureType = 1000129001
	StructureTypeAndroidHardwareBufferFormatPropertiesAndroid             StructureType = 1000129002
	StructureTypeImportAndroidHardwareBufferInfoAndroid                   StructureType = 1000129003
	StructureTypeMemoryGetAndroidHardwareBufferInfoAndroid                StructureType = 1000129004
	StructureTypeExternalFormatAndroid                                    StructureType = 1000129005
	StructureTypePhysicalDeviceInlineUniformBlockFeatures                 StructureType = 1000138000
	StructureTypePhysicalDeviceInlineUniformBlockProperties               StructureType = 1000138001
	StructureTypeWriteDescriptorSetInlineUniformBlock                     StructureType = 1000138002
	StructureTypeDescriptorPoolInlineUniformBlockCreateInfo               StructureType = 1000138003
	StructureTypeSampleLocationsInfo                                      StructureType = 1000143000
	StructureTypeRenderPassSampleLocationsBeginInfo                       StructureType = 1000143001
	StructureTypePipelineSampleLocationsStateCreateInfo                   StructureType = 1000143002
	StructureTypePhysicalDeviceSampleLocationsProperties                  StructureType = 1000143003
	StructureTypeMultisampleProperties                                    StructureType = 1000143004
	StructureTypePhysicalDeviceBlendOperationAdvancedFeatures             StructureType = 1000148000
	StructureTypePhysicalDeviceBlendOperationAdvancedProperties           StructureType = 1000148001
	StructureTypePipelineColorBlendAdvancedStateCreateInfo                StructureType = 1000148002
	StructureTypePipelineCoverageToColorStateCreateInfoNv                 StructureType = 1000149000
	StructureTypeWriteDescriptorSetAccelerationStructure                  StructureType = 1000150007
	StructureTypeAccelerationStructureBuildGeometryInfo                   StructureType = 1000150000
	StructureTypeAccelerationStructureDeviceAddressInfo                   StructureType = 1000150002
	StructureTypeAccelerationStructureGeometryAabbsData                   StructureType = 1000150003
	StructureTypeAccelerationStructureGeometryInstancesData               StructureType = 1000150004
	StructureTypeAccelerationStructureGeometryTrianglesData               StructureType = 1000150005
	StructureTypeAccelerationStructureGeometry                            StructureType = 1000150006
	StructureTypeAccelerationStructureVersionInfo                         StructureType = 1000150009
	StructureTypeCopyAccelerationStructureInfo                            StructureType = 1000150010
	StructureTypeCopyAccelerationStructureToMemoryInfo                    StructureType = 1000150011
	StructureTypeCopyMemoryToAccelerationStructureInfo                    StructureType = 1000150012
	StructureTypePhysicalDeviceAccelerationStructureFeatures              StructureType = 1000150013
	StructureTypePhysicalDeviceAccelerationStructureProperties            StructureType = 1000150014
	StructureTypeAccelerationStructureCreateInfo                          StructureType = 1000150017
	StructureTypeAccelerationStructureBuildSizesInfo                      StructureType = 1000150020
	StructureTypePhysicalDeviceRayTracingPipelineFeatures                 StructureType = 1000347000
	StructureTypePhysicalDeviceRayTracingPipelineProperties               StructureType = 1000347001
	StructureTypeRayTracingPipelineCreateInfo                             StructureType = 1000150015
	StructureTypeRayTracingShaderGroupCreateInfo                          StructureType = 1000150016
	StructureTypeRayTracingPipelineInterfaceCreateInfo                    StructureType = 1000150018
	StructureTypePhysicalDeviceRayQueryFeatures                           StructureType = 1000348013
	StructureTypePipelineCoverageModulationStateCreateInfoNv              StructureType = 1000152000
	StructureTypePhysicalDeviceShaderSmBuiltinsFeaturesNv                 StructureType = 1000154000
	StructureTypePhysicalDeviceShaderSmBuiltinsPropertiesNv               StructureType = 1000154001
	StructureTypeDrmFormatModifierPropertiesList                          StructureType = 1000158000
	StructureTypePhysicalDeviceImageDrmFormatModifierInfo                 StructureType = 1000158002
	StructureTypeImageDrmFormatModifierListCreateInfo                     StructureType = 1000158003
	StructureTypeImageDrmFormatModifierExplicitCreateInfo                 StructureType = 1000158004
	StructureTypeImageDrmFormatModifierProperties                         StructureType = 1000158005
	StructureTypeValidationCacheCreateInfo                                StructureType = 1000160000
	StructureTypeShaderModuleValidationCacheCreateInfo                    StructureType = 1000160001
	StructureTypePipelineViewportShadingRateImageStateCreateInfoNv        StructureType = 1000164000
	StructureTypePhysicalDeviceShadingRateImageFeaturesNv                 StructureType = 1000164001
	StructureTypePhysicalDeviceShadingRateImagePropertiesNv               StructureType = 1000164002
	StructureTypePipelineViewportCoarseSampleOrderStateCreateInfoNv       StructureType = 1000164005
	StructureTypeRayTracingPipelineCreateInfoNv                           StructureType = 1000165000
	StructureTypeAccelerationStructureCreateInfoNv                        StructureType = 1000165001
	StructureTypeGeometryNv                                               StructureType = 1000165003
	StructureTypeGeometryTrianglesNv                                      StructureType = 1000165004
	StructureTypeGeometryAabbNv                                           StructureType = 1000165005
	StructureTypeBindAccelerationStructureMemoryInfoNv                    StructureType = 1000165006
	StructureTypeWriteDescriptorSetAccelerationStructureNv                StructureType = 1000165007
	StructureTypeAccelerationStructureMemoryRequirementsInfoNv            StructureType = 1000165008
	StructureTypePhysicalDeviceRayTracingPropertiesNv                     StructureType = 1000165009
	StructureTypeRayTracingShaderGroupCreateInfoNv                        StructureType = 1000165011
	StructureTypeAccelerationStructureInfoNv                              StructureType = 1000165012
	StructureTypePhysicalDeviceRepresentativeFragmentTestFeaturesNv       StructureType = 1000166000
	StructureTypePipelineRepresentativeFragmentTestStateCreateInfoNv      StructureType = 1000166001
	StructureTypePhysicalDeviceImageViewImageFormatInfo                   StructureType = 1000170000
	StructureTypeFilterCubicImageViewImageFormatProperties                StructureType = 1000170001
	StructureTypeDeviceQueueGlobalPriorityCreateInfo                      StructureType = 1000174000
	StructureTypeImportMemoryHostPointerInfo                              StructureType = 1000178000
	StructureTypeMemoryHostPointerProperties                              StructureType = 1000178001
	StructureTypePhysicalDeviceExternalMemoryHostProperties               StructureType = 1000178002
	StructureTypePhysicalDeviceShaderClockFeatures                        StructureType = 1000181000
	StructureTypePipelineCompilerControlCreateInfoAmd                     StructureType = 1000183000
	StructureTypeCalibratedTimestampInfo                                  StructureType = 1000184000
	StructureTypePhysicalDeviceShaderCorePropertiesAmd                    StructureType = 1000185000
	StructureTypeDeviceMemoryOverallocationCreateInfoAmd                  StructureType = 1000189000
	StructureTypePhysicalDeviceVertexAttributeDivisorProperties           StructureType = 1000190000
	StructureTypePipelineVertexInputDivisorStateCreateInfo                StructureType = 1000190001
	StructureTypePhysicalDeviceVertexAttributeDivisorFeatures             StructureType = 1000190002
	StructureTypePresentFrameTokenGgp                                     StructureType = 1000191000
	StructureTypePipelineCreationFeedbackCreateInfo                       StructureType = 1000192000
	StructureTypePhysicalDeviceComputeShaderDerivativesFeaturesNv         StructureType = 1000201000
	StructureTypePhysicalDeviceMeshShaderFeaturesNv                       StructureType = 1000202000
	StructureTypePhysicalDeviceMeshShaderPropertiesNv                     StructureType = 1000202001
	StructureTypePhysicalDeviceFragmentShaderBarycentricFeaturesNv        StructureType = 1000203000
	StructureTypePhysicalDeviceShaderImageFootprintFeaturesNv             StructureType = 1000204000
	StructureTypePipelineViewportExclusiveScissorStateCreateInfoNv        StructureType = 1000205000
	StructureTypePhysicalDeviceExclusiveScissorFeaturesNv                 StructureType = 1000205002
	StructureTypeCheckpointDataNv                                         StructureType = 1000206000
	StructureTypeQueueFamilyCheckpointPropertiesNv                        StructureType = 1000206001
	StructureTypePhysicalDeviceShaderIntegerFunctions2FeaturesIntel       StructureType = 1000209000
	StructureTypeQueryPoolPerformanceQueryCreateInfoIntel                 StructureType = 1000210000
	StructureTypeInitializePerformanceApiInfoIntel                        StructureType = 1000210001
	StructureTypePerformanceMarkerInfoIntel                               StructureType = 1000210002
	StructureTypePerformanceStreamMarkerInfoIntel                         StructureType = 1000210003
	StructureTypePerformanceOverrideInfoIntel                             StructureType = 1000210004
	StructureTypePerformanceConfigurationAcquireInfoIntel                 StructureType = 1000210005
	StructureTypePhysicalDevicePciBusInfoProperties                       StructureType = 1000212000
	StructureTypeDisplayNativeHdrSurfaceCapabilitiesAmd                   StructureType = 1000213000
	StructureTypeSwapchainDisplayNativeHdrCreateInfoAmd                   StructureType = 1000213001
	StructureTypeImagepipeSurfaceCreateInfoFuchsia                        StructureType = 1000214000
	StructureTypePhysicalDeviceShaderTerminateInvocationFeatures          StructureType = 1000215000
	StructureTypeMetalSurfaceCreateInfo                                   StructureType = 1000217000
	StructureTypePhysicalDeviceFragmentDensityMapFeatures                 StructureType = 1000218000
	StructureTypePhysicalDeviceFragmentDensityMapProperties               StructureType = 1000218001
	StructureTypeRenderPassFragmentDensityMapCreateInfo                   StructureType = 1000218002
	StructureTypePhysicalDeviceSubgroupSizeControlProperties              StructureType = 1000225000
	StructureTypePipelineShaderStageRequiredSubgroupSizeCreateInfo        StructureType = 1000225001
	StructureTypePhysicalDeviceSubgroupSizeControlFeatures                StructureType = 1000225002
	StructureTypeFragmentShadingRateAttachmentInfo                        StructureType = 1000226000
	StructureTypePipelineFragmentShadingRateStateCreateInfo               StructureType = 1000226001
	StructureTypePhysicalDeviceFragmentShadingRateProperties              StructureType = 1000226002
	StructureTypePhysicalDeviceFragmentShadingRateFeatures                StructureType = 1000226003
	StructureTypePhysicalDeviceFragmentShadingRate                        StructureType = 1000226004
	StructureTypePhysicalDeviceShaderCoreProperties2Amd                   StructureType = 1000227000
	StructureTypePhysicalDeviceCoherentMemoryFeaturesAmd                  StructureType = 1000229000
	StructureTypePhysicalDeviceShaderImageAtomicInt64Features             StructureType = 1000234000
	StructureTypePhysicalDeviceMemoryBudgetProperties                     StructureType = 1000237000
	StructureTypePhysicalDeviceMemoryPriorityFeatures                     StructureType = 1000238000
	StructureTypeMemoryPriorityAllocateInfo                               StructureType = 1000238001
	StructureTypeSurfaceProtectedCapabilities                             StructureType = 1000239000
	StructureTypePhysicalDeviceDedicatedAllocationImageAliasingFeaturesNv StructureType = 1000240000
	StructureTypeBufferDeviceAddressCreateInfo                            StructureType = 1000244002
	StructureTypePhysicalDeviceToolProperties                             StructureType = 1000245000
	StructureTypeValidationFeatures                                       StructureType = 1000247000
	StructureTypePhysicalDeviceCooperativeMatrixFeaturesNv                StructureType = 1000249000
	StructureTypeCooperativeMatrixPropertiesNv                            StructureType = 1000249001
	StructureTypePhysicalDeviceCooperativeMatrixPropertiesNv              StructureType = 1000249002
	StructureTypePhysicalDeviceCoverageReductionModeFeaturesNv            StructureType = 1000250000
	StructureTypePipelineCoverageReductionStateCreateInfoNv               StructureType = 1000250001
	StructureTypeFramebufferMixedSamplesCombinationNv                     StructureType = 1000250002
	StructureTypePhysicalDeviceFragmentShaderInterlockFeatures            StructureType = 1000251000
	StructureTypePhysicalDeviceYcbcrImageArraysFeatures                   StructureType = 1000252000
	StructureTypePhysicalDeviceProvokingVertexFeatures                    StructureType = 1000254000
	StructureTypePipelineRasterizationProvokingVertexStateCreateInfo      StructureType = 1000254001
	StructureTypePhysicalDeviceProvokingVertexProperties                  StructureType = 1000254002
	StructureTypeSurfaceFullScreenExclusiveInfo                           StructureType = 1000255000
	StructureTypeSurfaceCapabilitiesFullScreenExclusive                   StructureType = 1000255002
	StructureTypeSurfaceFullScreenExclusiveWin32Info                      StructureType = 1000255001
	StructureTypeHeadlessSurfaceCreateInfo                                StructureType = 1000256000
	StructureTypePhysicalDeviceLineRasterizationFeatures                  StructureType = 1000259000
	StructureTypePipelineRasterizationLineStateCreateInfo                 StructureType = 1000259001
	StructureTypePhysicalDeviceLineRasterizationProperties                StructureType = 1000259002
	StructureTypePhysicalDeviceShaderAtomicFloatFeatures                  StructureType = 1000260000
	StructureTypePhysicalDeviceIndexTypeUint8Features                     StructureType = 1000265000
	StructureTypePhysicalDeviceExtendedDynamicStateFeatures               StructureType = 1000267000
	StructureTypePhysicalDevicePipelineExecutablePropertiesFeatures       StructureType = 1000269000
	StructureTypePipelineInfo                                             StructureType = 1000269001
	StructureTypePipelineExecutableProperties                             StructureType = 1000269002
	StructureTypePipelineExecutableInfo                                   StructureType = 1000269003
	StructureTypePipelineExecutableStatistic                              StructureType = 1000269004
	StructureTypePipelineExecutableInternalRepresentation                 StructureType = 1000269005
	StructureTypePhysicalDeviceShaderDemoteToHelperInvocationFeatures     StructureType = 1000276000
	StructureTypePhysicalDeviceDeviceGeneratedCommandsPropertiesNv        StructureType = 1000277000
	StructureTypeGraphicsShaderGroupCreateInfoNv                          StructureType = 1000277001
	StructureTypeGraphicsPipelineShaderGroupsCreateInfoNv                 StructureType = 1000277002
	StructureTypeIndirectCommandsLayoutTokenNv                            StructureType = 1000277003
	StructureTypeIndirectCommandsLayoutCreateInfoNv                       StructureType = 1000277004
	StructureTypeGeneratedCommandsInfoNv                                  StructureType = 1000277005
	StructureTypeGeneratedCommandsMemoryRequirementsInfoNv                StructureType = 1000277006
	StructureTypePhysicalDeviceDeviceGeneratedCommandsFeaturesNv          StructureType = 1000277007
	StructureTypePhysicalDeviceInheritedViewportScissorFeaturesNv         StructureType = 1000278000
	StructureTypeCommandBufferInheritanceViewportScissorInfoNv            StructureType = 1000278001
	StructureTypePhysicalDeviceTexelBufferAlignmentFeatures               StructureType = 1000281000
	StructureTypePhysicalDeviceTexelBufferAlignmentProperties             StructureType = 1000281001
	StructureTypeCommandBufferInheritanceRenderPassTransformInfoQcom      StructureType = 1000282000
	StructureTypeRenderPassTransformBeginInfoQcom                         StructureType = 1000282001
	StructureTypePhysicalDeviceDeviceMemoryReportFeatures                 StructureType = 1000284000
	StructureTypeDeviceDeviceMemoryReportCreateInfo                       StructureType = 1000284001
	StructureTypeDeviceMemoryReportCallbackData                           StructureType = 1000284002
	StructureTypePhysicalDeviceRobustness2Features                        StructureType = 1000286000
	StructureTypePhysicalDeviceRobustness2Properties                      StructureType = 1000286001
	StructureTypeSamplerCustomBorderColorCreateInfo                       StructureType = 1000287000
	StructureTypePhysicalDeviceCustomBorderColorProperties                StructureType = 1000287001
	StructureTypePhysicalDeviceCustomBorderColorFeatures                  StructureType = 1000287002
	StructureTypePipelineLibraryCreateInfo                                StructureType = 1000290000
	StructureTypePhysicalDevicePrivateDataFeatures                        StructureType = 1000295000
	StructureTypeDevicePrivateDataCreateInfo                              StructureType = 1000295001
	StructureTypePrivateDataSlotCreateInfo                                StructureType = 1000295002
	StructureTypePhysicalDevicePipelineCreationCacheControlFeatures       StructureType = 1000297000
	StructureTypePhysicalDeviceDiagnosticsConfigFeaturesNv                StructureType = 1000300000
	StructureTypeDeviceDiagnosticsConfigCreateInfoNv                      StructureType = 1000300001
	StructureTypeMemoryBarrier2                                           StructureType = 1000314000
	StructureTypeBufferMemoryBarrier2                                     StructureType = 1000314001
	StructureTypeImageMemoryBarrier2                                      StructureType = 1000314002
	StructureTypeDependencyInfo                                           StructureType = 1000314003
	StructureTypeSubmitInfo2                                              StructureType = 1000314004
	StructureTypeSemaphoreSubmitInfo                                      StructureType = 1000314005
	StructureTypeCommandBufferSubmitInfo                                  StructureType = 1000314006
	StructureTypePhysicalDeviceSynchronization2Features                   StructureType = 1000314007
	StructureTypeQueueFamilyCheckpointProperties2Nv                       StructureType = 1000314008
	StructureTypeCheckpointData2Nv                                        StructureType = 1000314009
	StructureTypePhysicalDeviceZeroInitializeWorkgroupMemoryFeatures      StructureType = 1000325000
	StructureTypePhysicalDeviceFragmentShadingRateEnumsPropertiesNv       StructureType = 1000326000
	StructureTypePhysicalDeviceFragmentShadingRateEnumsFeaturesNv         StructureType = 1000326001
	StructureTypePipelineFragmentShadingRateEnumStateCreateInfoNv         StructureType = 1000326002
	StructureTypePhysicalDeviceYcbcr2Plane444FormatsFeatures              StructureType = 1000330000
	StructureTypePhysicalDeviceFragmentDensityMap2Features                StructureType = 1000332000
	StructureTypePhysicalDeviceFragmentDensityMap2Properties              StructureType = 1000332001
	StructureTypeCopyCommandTransformInfoQcom                             StructureType = 1000333000
	StructureTypePhysicalDeviceImageRobustnessFeatures                    StructureType = 1000335000
	StructureTypePhysicalDeviceWorkgroupMemoryExplicitLayoutFeatures      StructureType = 1000336000
	StructureTypeCopyBufferInfo2                                          StructureType = 1000337000
	StructureTypeCopyImageInfo2                                           StructureType = 1000337001
	StructureTypeCopyBufferToImageInfo2                                   StructureType = 1000337002
	StructureTypeCopyImageToBufferInfo2                                   StructureType = 1000337003
	StructureTypeBlitImageInfo2                                           StructureType = 1000337004
	StructureTypeResolveImageInfo2                                        StructureType = 1000337005
	StructureTypeBufferCopy2                                              StructureType = 1000337006
	StructureTypeImageCopy2                                               StructureType = 1000337007
	StructureTypeImageBlit2                                               StructureType = 1000337008
	StructureTypeBufferImageCopy2                                         StructureType = 1000337009
	StructureTypeImageResolve2                                            StructureType = 1000337010
	StructureTypePhysicalDevice4444FormatsFeatures                        StructureType = 1000340000
	StructureTypeDirectfbSurfaceCreateInfo                                StructureType = 1000346000
	StructureTypePhysicalDeviceMutableDescriptorTypeFeaturesValve         StructureType = 1000351000
	StructureTypeMutableDescriptorTypeCreateInfoValve                     StructureType = 1000351002
	StructureTypePhysicalDeviceVertexInputDynamicStateFeatures            StructureType = 1000352000
	StructureTypeVertexInputBindingDescription2                           StructureType = 1000352001
	StructureTypeVertexInputAttributeDescription2                         StructureType = 1000352002
	StructureTypeImportMemoryZirconHandleInfoFuchsia                      StructureType = 1000364000
	StructureTypeMemoryZirconHandlePropertiesFuchsia                      StructureType = 1000364001
	StructureTypeMemoryGetZirconHandleInfoFuchsia                         StructureType = 1000364002
	StructureTypeImportSemaphoreZirconHandleInfoFuchsia                   StructureType = 1000365000
	StructureTypeSemaphoreGetZirconHandleInfoFuchsia                      StructureType = 1000365001
	StructureTypePhysicalDeviceExtendedDynamicState2Features              StructureType = 1000377000
	StructureTypeScreenSurfaceCreateInfoQnx                               StructureType = 1000378000
	StructureTypePhysicalDeviceColorWriteEnableFeatures                   StructureType = 1000381000
	StructureTypePipelineColorWriteCreateInfo                             StructureType = 1000381001
	StructureTypePhysicalDeviceVariablePointerFeatures                    StructureType = 1000120000
	StructureTypePhysicalDeviceShaderDrawParameterFeatures                StructureType = 1000063000
	StructureTypeDebugReportCreateInfo                                    StructureType = 1000011000
	StructureTypePhysicalDeviceFloat16Int8Features                        StructureType = 1000082000
	StructureTypeQueryPoolCreateInfoIntel                                 StructureType = 1000210000
	StructureTypePhysicalDeviceBufferAddressFeatures                      StructureType = 1000244000
	StructureTypeMaxEnum                                                  StructureType = 2147483647
)

// ImageLayout as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageLayout.html
type ImageLayout int32

// ImageLayout enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageLayout.html
const (
	ImageLayoutUndefined                             ImageLayout = iota
	ImageLayoutGeneral                               ImageLayout = 1
	ImageLayoutColorAttachmentOptimal                ImageLayout = 2
	ImageLayoutDepthStencilAttachmentOptimal         ImageLayout = 3
	ImageLayoutDepthStencilReadOnlyOptimal           ImageLayout = 4
	ImageLayoutShaderReadOnlyOptimal                 ImageLayout = 5
	ImageLayoutTransferSrcOptimal                    ImageLayout = 6
	ImageLayoutTransferDstOptimal                    ImageLayout = 7
	ImageLayoutPreinitialized                        ImageLayout = 8
	ImageLayoutDepthReadOnlyStencilAttachmentOptimal ImageLayout = 1000117000
	ImageLayoutDepthAttachmentStencilReadOnlyOptimal ImageLayout = 1000117001
	ImageLayoutDepthAttachmentOptimal                ImageLayout = 1000241000
	ImageLayoutDepthReadOnlyOptimal                  ImageLayout = 1000241001
	ImageLayoutStencilAttachmentOptimal              ImageLayout = 1000241002
	ImageLayoutStencilReadOnlyOptimal                ImageLayout = 1000241003
	ImageLayoutPresentSrc                            ImageLayout = 1000001002
	ImageLayoutSharedPresent                         ImageLayout = 1000111000
	ImageLayoutShadingRateOptimalNv                  ImageLayout = 1000164003
	ImageLayoutFragmentDensityMapOptimal             ImageLayout = 1000218000
	ImageLayoutReadOnlyOptimal                       ImageLayout = 1000314000
	ImageLayoutAttachmentOptimal                     ImageLayout = 1000314001
	ImageLayoutFragmentShadingRateAttachmentOptimal  ImageLayout = 1000164003
	ImageLayoutMaxEnum                               ImageLayout = 2147483647
)

// ObjectType as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkObjectType.html
type ObjectType int32

// ObjectType enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkObjectType.html
const (
	ObjectTypeUnknown                       ObjectType = iota
	ObjectTypeInstance                      ObjectType = 1
	ObjectTypePhysicalDevice                ObjectType = 2
	ObjectTypeDevice                        ObjectType = 3
	ObjectTypeQueue                         ObjectType = 4
	ObjectTypeSemaphore                     ObjectType = 5
	ObjectTypeCommandBuffer                 ObjectType = 6
	ObjectTypeFence                         ObjectType = 7
	ObjectTypeDeviceMemory                  ObjectType = 8
	ObjectTypeBuffer                        ObjectType = 9
	ObjectTypeImage                         ObjectType = 10
	ObjectTypeEvent                         ObjectType = 11
	ObjectTypeQueryPool                     ObjectType = 12
	ObjectTypeBufferView                    ObjectType = 13
	ObjectTypeImageView                     ObjectType = 14
	ObjectTypeShaderModule                  ObjectType = 15
	ObjectTypePipelineCache                 ObjectType = 16
	ObjectTypePipelineLayout                ObjectType = 17
	ObjectTypeRenderPass                    ObjectType = 18
	ObjectTypePipeline                      ObjectType = 19
	ObjectTypeDescriptorSetLayout           ObjectType = 20
	ObjectTypeSampler                       ObjectType = 21
	ObjectTypeDescriptorPool                ObjectType = 22
	ObjectTypeDescriptorSet                 ObjectType = 23
	ObjectTypeFramebuffer                   ObjectType = 24
	ObjectTypeCommandPool                   ObjectType = 25
	ObjectTypeSamplerYcbcrConversion        ObjectType = 1000156000
	ObjectTypeDescriptorUpdateTemplate      ObjectType = 1000085000
	ObjectTypeSurface                       ObjectType = 1000000000
	ObjectTypeSwapchain                     ObjectType = 1000001000
	ObjectTypeDisplay                       ObjectType = 1000002000
	ObjectTypeDisplayMode                   ObjectType = 1000002001
	ObjectTypeDebugReportCallback           ObjectType = 1000011000
	ObjectTypeDebugUtilsMessenger           ObjectType = 1000128000
	ObjectTypeAccelerationStructure         ObjectType = 1000150000
	ObjectTypeValidationCache               ObjectType = 1000160000
	ObjectTypeAccelerationStructureNv       ObjectType = 1000165000
	ObjectTypePerformanceConfigurationIntel ObjectType = 1000210000
	ObjectTypeDeferredOperation             ObjectType = 1000268000
	ObjectTypeIndirectCommandsLayoutNv      ObjectType = 1000277000
	ObjectTypePrivateDataSlot               ObjectType = 1000295000
	ObjectTypeMaxEnum                       ObjectType = 2147483647
)

// VendorId as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkVendorId.html
type VendorId int32

// VendorId enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkVendorId.html
const (
	VendorIdViv      VendorId = 65537
	VendorIdVsi      VendorId = 65538
	VendorIdKazan    VendorId = 65539
	VendorIdCodeplay VendorId = 65540
	VendorIdMesa     VendorId = 65541
	VendorIdPocl     VendorId = 65542
	VendorIdMaxEnum  VendorId = 2147483647
)

// PipelineCacheHeaderVersion as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCacheHeaderVersion.html
type PipelineCacheHeaderVersion int32

// PipelineCacheHeaderVersion enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCacheHeaderVersion.html
const (
	PipelineCacheHeaderVersionOne     PipelineCacheHeaderVersion = 1
	PipelineCacheHeaderVersionMaxEnum PipelineCacheHeaderVersion = 2147483647
)

// SystemAllocationScope as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSystemAllocationScope.html
type SystemAllocationScope int32

// SystemAllocationScope enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSystemAllocationScope.html
const (
	SystemAllocationScopeCommand  SystemAllocationScope = iota
	SystemAllocationScopeObject   SystemAllocationScope = 1
	SystemAllocationScopeCache    SystemAllocationScope = 2
	SystemAllocationScopeDevice   SystemAllocationScope = 3
	SystemAllocationScopeInstance SystemAllocationScope = 4
	SystemAllocationScopeMaxEnum  SystemAllocationScope = 2147483647
)

// InternalAllocationType as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkInternalAllocationType.html
type InternalAllocationType int32

// InternalAllocationType enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkInternalAllocationType.html
const (
	InternalAllocationTypeExecutable InternalAllocationType = iota
	InternalAllocationTypeMaxEnum    InternalAllocationType = 2147483647
)

// Format as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFormat.html
type Format int32

// Format enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFormat.html
const (
	FormatUndefined                            Format = iota
	FormatR4g4UnormPack8                       Format = 1
	FormatR4g4b4a4UnormPack16                  Format = 2
	FormatB4g4r4a4UnormPack16                  Format = 3
	FormatR5g6b5UnormPack16                    Format = 4
	FormatB5g6r5UnormPack16                    Format = 5
	FormatR5g5b5a1UnormPack16                  Format = 6
	FormatB5g5r5a1UnormPack16                  Format = 7
	FormatA1r5g5b5UnormPack16                  Format = 8
	FormatR8Unorm                              Format = 9
	FormatR8Snorm                              Format = 10
	FormatR8Uscaled                            Format = 11
	FormatR8Sscaled                            Format = 12
	FormatR8Uint                               Format = 13
	FormatR8Sint                               Format = 14
	FormatR8Srgb                               Format = 15
	FormatR8g8Unorm                            Format = 16
	FormatR8g8Snorm                            Format = 17
	FormatR8g8Uscaled                          Format = 18
	FormatR8g8Sscaled                          Format = 19
	FormatR8g8Uint                             Format = 20
	FormatR8g8Sint                             Format = 21
	FormatR8g8Srgb                             Format = 22
	FormatR8g8b8Unorm                          Format = 23
	FormatR8g8b8Snorm                          Format = 24
	FormatR8g8b8Uscaled                        Format = 25
	FormatR8g8b8Sscaled                        Format = 26
	FormatR8g8b8Uint                           Format = 27
	FormatR8g8b8Sint                           Format = 28
	FormatR8g8b8Srgb                           Format = 29
	FormatB8g8r8Unorm                          Format = 30
	FormatB8g8r8Snorm                          Format = 31
	FormatB8g8r8Uscaled                        Format = 32
	FormatB8g8r8Sscaled                        Format = 33
	FormatB8g8r8Uint                           Format = 34
	FormatB8g8r8Sint                           Format = 35
	FormatB8g8r8Srgb                           Format = 36
	FormatR8g8b8a8Unorm                        Format = 37
	FormatR8g8b8a8Snorm                        Format = 38
	FormatR8g8b8a8Uscaled                      Format = 39
	FormatR8g8b8a8Sscaled                      Format = 40
	FormatR8g8b8a8Uint                         Format = 41
	FormatR8g8b8a8Sint                         Format = 42
	FormatR8g8b8a8Srgb                         Format = 43
	FormatB8g8r8a8Unorm                        Format = 44
	FormatB8g8r8a8Snorm                        Format = 45
	FormatB8g8r8a8Uscaled                      Format = 46
	FormatB8g8r8a8Sscaled                      Format = 47
	FormatB8g8r8a8Uint                         Format = 48
	FormatB8g8r8a8Sint                         Format = 49
	FormatB8g8r8a8Srgb                         Format = 50
	FormatA8b8g8r8UnormPack32                  Format = 51
	FormatA8b8g8r8SnormPack32                  Format = 52
	FormatA8b8g8r8UscaledPack32                Format = 53
	FormatA8b8g8r8SscaledPack32                Format = 54
	FormatA8b8g8r8UintPack32                   Format = 55
	FormatA8b8g8r8SintPack32                   Format = 56
	FormatA8b8g8r8SrgbPack32                   Format = 57
	FormatA2r10g10b10UnormPack32               Format = 58
	FormatA2r10g10b10SnormPack32               Format = 59
	FormatA2r10g10b10UscaledPack32             Format = 60
	FormatA2r10g10b10SscaledPack32             Format = 61
	FormatA2r10g10b10UintPack32                Format = 62
	FormatA2r10g10b10SintPack32                Format = 63
	FormatA2b10g10r10UnormPack32               Format = 64
	FormatA2b10g10r10SnormPack32               Format = 65
	FormatA2b10g10r10UscaledPack32             Format = 66
	FormatA2b10g10r10SscaledPack32             Format = 67
	FormatA2b10g10r10UintPack32                Format = 68
	FormatA2b10g10r10SintPack32                Format = 69
	FormatR16Unorm                             Format = 70
	FormatR16Snorm                             Format = 71
	FormatR16Uscaled                           Format = 72
	FormatR16Sscaled                           Format = 73
	FormatR16Uint                              Format = 74
	FormatR16Sint                              Format = 75
	FormatR16Sfloat                            Format = 76
	FormatR16g16Unorm                          Format = 77
	FormatR16g16Snorm                          Format = 78
	FormatR16g16Uscaled                        Format = 79
	FormatR16g16Sscaled                        Format = 80
	FormatR16g16Uint                           Format = 81
	FormatR16g16Sint                           Format = 82
	FormatR16g16Sfloat                         Format = 83
	FormatR16g16b16Unorm                       Format = 84
	FormatR16g16b16Snorm                       Format = 85
	FormatR16g16b16Uscaled                     Format = 86
	FormatR16g16b16Sscaled                     Format = 87
	FormatR16g16b16Uint                        Format = 88
	FormatR16g16b16Sint                        Format = 89
	FormatR16g16b16Sfloat                      Format = 90
	FormatR16g16b16a16Unorm                    Format = 91
	FormatR16g16b16a16Snorm                    Format = 92
	FormatR16g16b16a16Uscaled                  Format = 93
	FormatR16g16b16a16Sscaled                  Format = 94
	FormatR16g16b16a16Uint                     Format = 95
	FormatR16g16b16a16Sint                     Format = 96
	FormatR16g16b16a16Sfloat                   Format = 97
	FormatR32Uint                              Format = 98
	FormatR32Sint                              Format = 99
	FormatR32Sfloat                            Format = 100
	FormatR32g32Uint                           Format = 101
	FormatR32g32Sint                           Format = 102
	FormatR32g32Sfloat                         Format = 103
	FormatR32g32b32Uint                        Format = 104
	FormatR32g32b32Sint                        Format = 105
	FormatR32g32b32Sfloat                      Format = 106
	FormatR32g32b32a32Uint                     Format = 107
	FormatR32g32b32a32Sint                     Format = 108
	FormatR32g32b32a32Sfloat                   Format = 109
	FormatR64Uint                              Format = 110
	FormatR64Sint                              Format = 111
	FormatR64Sfloat                            Format = 112
	FormatR64g64Uint                           Format = 113
	FormatR64g64Sint                           Format = 114
	FormatR64g64Sfloat                         Format = 115
	FormatR64g64b64Uint                        Format = 116
	FormatR64g64b64Sint                        Format = 117
	FormatR64g64b64Sfloat                      Format = 118
	FormatR64g64b64a64Uint                     Format = 119
	FormatR64g64b64a64Sint                     Format = 120
	FormatR64g64b64a64Sfloat                   Format = 121
	FormatB10g11r11UfloatPack32                Format = 122
	FormatE5b9g9r9UfloatPack32                 Format = 123
	FormatD16Unorm                             Format = 124
	FormatX8D24UnormPack32                     Format = 125
	FormatD32Sfloat                            Format = 126
	FormatS8Uint                               Format = 127
	FormatD16UnormS8Uint                       Format = 128
	FormatD24UnormS8Uint                       Format = 129
	FormatD32SfloatS8Uint                      Format = 130
	FormatBc1RgbUnormBlock                     Format = 131
	FormatBc1RgbSrgbBlock                      Format = 132
	FormatBc1RgbaUnormBlock                    Format = 133
	FormatBc1RgbaSrgbBlock                     Format = 134
	FormatBc2UnormBlock                        Format = 135
	FormatBc2SrgbBlock                         Format = 136
	FormatBc3UnormBlock                        Format = 137
	FormatBc3SrgbBlock                         Format = 138
	FormatBc4UnormBlock                        Format = 139
	FormatBc4SnormBlock                        Format = 140
	FormatBc5UnormBlock                        Format = 141
	FormatBc5SnormBlock                        Format = 142
	FormatBc6hUfloatBlock                      Format = 143
	FormatBc6hSfloatBlock                      Format = 144
	FormatBc7UnormBlock                        Format = 145
	FormatBc7SrgbBlock                         Format = 146
	FormatEtc2R8g8b8UnormBlock                 Format = 147
	FormatEtc2R8g8b8SrgbBlock                  Format = 148
	FormatEtc2R8g8b8a1UnormBlock               Format = 149
	FormatEtc2R8g8b8a1SrgbBlock                Format = 150
	FormatEtc2R8g8b8a8UnormBlock               Format = 151
	FormatEtc2R8g8b8a8SrgbBlock                Format = 152
	FormatEacR11UnormBlock                     Format = 153
	FormatEacR11SnormBlock                     Format = 154
	FormatEacR11g11UnormBlock                  Format = 155
	FormatEacR11g11SnormBlock                  Format = 156
	FormatAstc4x4UnormBlock                    Format = 157
	FormatAstc4x4SrgbBlock                     Format = 158
	FormatAstc5x4UnormBlock                    Format = 159
	FormatAstc5x4SrgbBlock                     Format = 160
	FormatAstc5x5UnormBlock                    Format = 161
	FormatAstc5x5SrgbBlock                     Format = 162
	FormatAstc6x5UnormBlock                    Format = 163
	FormatAstc6x5SrgbBlock                     Format = 164
	FormatAstc6x6UnormBlock                    Format = 165
	FormatAstc6x6SrgbBlock                     Format = 166
	FormatAstc8x5UnormBlock                    Format = 167
	FormatAstc8x5SrgbBlock                     Format = 168
	FormatAstc8x6UnormBlock                    Format = 169
	FormatAstc8x6SrgbBlock                     Format = 170
	FormatAstc8x8UnormBlock                    Format = 171
	FormatAstc8x8SrgbBlock                     Format = 172
	FormatAstc10x5UnormBlock                   Format = 173
	FormatAstc10x5SrgbBlock                    Format = 174
	FormatAstc10x6UnormBlock                   Format = 175
	FormatAstc10x6SrgbBlock                    Format = 176
	FormatAstc10x8UnormBlock                   Format = 177
	FormatAstc10x8SrgbBlock                    Format = 178
	FormatAstc10x10UnormBlock                  Format = 179
	FormatAstc10x10SrgbBlock                   Format = 180
	FormatAstc12x10UnormBlock                  Format = 181
	FormatAstc12x10SrgbBlock                   Format = 182
	FormatAstc12x12UnormBlock                  Format = 183
	FormatAstc12x12SrgbBlock                   Format = 184
	FormatG8b8g8r8422Unorm                     Format = 1000156000
	FormatB8g8r8g8422Unorm                     Format = 1000156001
	FormatG8B8R83plane420Unorm                 Format = 1000156002
	FormatG8B8r82plane420Unorm                 Format = 1000156003
	FormatG8B8R83plane422Unorm                 Format = 1000156004
	FormatG8B8r82plane422Unorm                 Format = 1000156005
	FormatG8B8R83plane444Unorm                 Format = 1000156006
	FormatR10x6UnormPack16                     Format = 1000156007
	FormatR10x6g10x6Unorm2pack16               Format = 1000156008
	FormatR10x6g10x6b10x6a10x6Unorm4pack16     Format = 1000156009
	FormatG10x6b10x6g10x6r10x6422Unorm4pack16  Format = 1000156010
	FormatB10x6g10x6r10x6g10x6422Unorm4pack16  Format = 1000156011
	FormatG10x6B10x6R10x63plane420Unorm3pack16 Format = 1000156012
	FormatG10x6B10x6r10x62plane420Unorm3pack16 Format = 1000156013
	FormatG10x6B10x6R10x63plane422Unorm3pack16 Format = 1000156014
	FormatG10x6B10x6r10x62plane422Unorm3pack16 Format = 1000156015
	FormatG10x6B10x6R10x63plane444Unorm3pack16 Format = 1000156016
	FormatR12x4UnormPack16                     Format = 1000156017
	FormatR12x4g12x4Unorm2pack16               Format = 1000156018
	FormatR12x4g12x4b12x4a12x4Unorm4pack16     Format = 1000156019
	FormatG12x4b12x4g12x4r12x4422Unorm4pack16  Format = 1000156020
	FormatB12x4g12x4r12x4g12x4422Unorm4pack16  Format = 1000156021
	FormatG12x4B12x4R12x43plane420Unorm3pack16 Format = 1000156022
	FormatG12x4B12x4r12x42plane420Unorm3pack16 Format = 1000156023
	FormatG12x4B12x4R12x43plane422Unorm3pack16 Format = 1000156024
	FormatG12x4B12x4r12x42plane422Unorm3pack16 Format = 1000156025
	FormatG12x4B12x4R12x43plane444Unorm3pack16 Format = 1000156026
	FormatG16b16g16r16422Unorm                 Format = 1000156027
	FormatB16g16r16g16422Unorm                 Format = 1000156028
	FormatG16B16R163plane420Unorm              Format = 1000156029
	FormatG16B16r162plane420Unorm              Format = 1000156030
	FormatG16B16R163plane422Unorm              Format = 1000156031
	FormatG16B16r162plane422Unorm              Format = 1000156032
	FormatG16B16R163plane444Unorm              Format = 1000156033
	FormatPvrtc12bppUnormBlockImg              Format = 1000054000
	FormatPvrtc14bppUnormBlockImg              Format = 1000054001
	FormatPvrtc22bppUnormBlockImg              Format = 1000054002
	FormatPvrtc24bppUnormBlockImg              Format = 1000054003
	FormatPvrtc12bppSrgbBlockImg               Format = 1000054004
	FormatPvrtc14bppSrgbBlockImg               Format = 1000054005
	FormatPvrtc22bppSrgbBlockImg               Format = 1000054006
	FormatPvrtc24bppSrgbBlockImg               Format = 1000054007
	FormatAstc4x4SfloatBlock                   Format = 1000066000
	FormatAstc5x4SfloatBlock                   Format = 1000066001
	FormatAstc5x5SfloatBlock                   Format = 1000066002
	FormatAstc6x5SfloatBlock                   Format = 1000066003
	FormatAstc6x6SfloatBlock                   Format = 1000066004
	FormatAstc8x5SfloatBlock                   Format = 1000066005
	FormatAstc8x6SfloatBlock                   Format = 1000066006
	FormatAstc8x8SfloatBlock                   Format = 1000066007
	FormatAstc10x5SfloatBlock                  Format = 1000066008
	FormatAstc10x6SfloatBlock                  Format = 1000066009
	FormatAstc10x8SfloatBlock                  Format = 1000066010
	FormatAstc10x10SfloatBlock                 Format = 1000066011
	FormatAstc12x10SfloatBlock                 Format = 1000066012
	FormatAstc12x12SfloatBlock                 Format = 1000066013
	FormatG8B8r82plane444Unorm                 Format = 1000330000
	FormatG10x6B10x6r10x62plane444Unorm3pack16 Format = 1000330001
	FormatG12x4B12x4r12x42plane444Unorm3pack16 Format = 1000330002
	FormatG16B16r162plane444Unorm              Format = 1000330003
	FormatA4r4g4b4UnormPack16                  Format = 1000340000
	FormatA4b4g4r4UnormPack16                  Format = 1000340001
	FormatMaxEnum                              Format = 2147483647
)

// ImageTiling as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageTiling.html
type ImageTiling int32

// ImageTiling enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageTiling.html
const (
	ImageTilingOptimal           ImageTiling = iota
	ImageTilingLinear            ImageTiling = 1
	ImageTilingDrmFormatModifier ImageTiling = 1000158000
	ImageTilingMaxEnum           ImageTiling = 2147483647
)

// ImageType as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageType.html
type ImageType int32

// ImageType enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageType.html
const (
	ImageType1d      ImageType = iota
	ImageType2d      ImageType = 1
	ImageType3d      ImageType = 2
	ImageTypeMaxEnum ImageType = 2147483647
)

// PhysicalDeviceType as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceType.html
type PhysicalDeviceType int32

// PhysicalDeviceType enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceType.html
const (
	PhysicalDeviceTypeOther         PhysicalDeviceType = iota
	PhysicalDeviceTypeIntegratedGpu PhysicalDeviceType = 1
	PhysicalDeviceTypeDiscreteGpu   PhysicalDeviceType = 2
	PhysicalDeviceTypeVirtualGpu    PhysicalDeviceType = 3
	PhysicalDeviceTypeCpu           PhysicalDeviceType = 4
	PhysicalDeviceTypeMaxEnum       PhysicalDeviceType = 2147483647
)

// QueryType as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueryType.html
type QueryType int32

// QueryType enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueryType.html
const (
	QueryTypeOcclusion                              QueryType = iota
	QueryTypePipelineStatistics                     QueryType = 1
	QueryTypeTimestamp                              QueryType = 2
	QueryTypeTransformFeedbackStream                QueryType = 1000028004
	QueryTypePerformanceQuery                       QueryType = 1000116000
	QueryTypeAccelerationStructureCompactedSize     QueryType = 1000150000
	QueryTypeAccelerationStructureSerializationSize QueryType = 1000150001
	QueryTypeAccelerationStructureCompactedSizeNv   QueryType = 1000165000
	QueryTypePerformanceQueryIntel                  QueryType = 1000210000
	QueryTypeMaxEnum                                QueryType = 2147483647
)

// SharingMode as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSharingMode.html
type SharingMode int32

// SharingMode enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSharingMode.html
const (
	SharingModeExclusive  SharingMode = iota
	SharingModeConcurrent SharingMode = 1
	SharingModeMaxEnum    SharingMode = 2147483647
)

// ComponentSwizzle as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkComponentSwizzle.html
type ComponentSwizzle int32

// ComponentSwizzle enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkComponentSwizzle.html
const (
	ComponentSwizzleIdentity ComponentSwizzle = iota
	ComponentSwizzleZero     ComponentSwizzle = 1
	ComponentSwizzleOne      ComponentSwizzle = 2
	ComponentSwizzleR        ComponentSwizzle = 3
	ComponentSwizzleG        ComponentSwizzle = 4
	ComponentSwizzleB        ComponentSwizzle = 5
	ComponentSwizzleA        ComponentSwizzle = 6
	ComponentSwizzleMaxEnum  ComponentSwizzle = 2147483647
)

// ImageViewType as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageViewType.html
type ImageViewType int32

// ImageViewType enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageViewType.html
const (
	ImageViewType1d        ImageViewType = iota
	ImageViewType2d        ImageViewType = 1
	ImageViewType3d        ImageViewType = 2
	ImageViewTypeCube      ImageViewType = 3
	ImageViewType1dArray   ImageViewType = 4
	ImageViewType2dArray   ImageViewType = 5
	ImageViewTypeCubeArray ImageViewType = 6
	ImageViewTypeMaxEnum   ImageViewType = 2147483647
)

// BlendFactor as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBlendFactor.html
type BlendFactor int32

// BlendFactor enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBlendFactor.html
const (
	BlendFactorZero                  BlendFactor = iota
	BlendFactorOne                   BlendFactor = 1
	BlendFactorSrcColor              BlendFactor = 2
	BlendFactorOneMinusSrcColor      BlendFactor = 3
	BlendFactorDstColor              BlendFactor = 4
	BlendFactorOneMinusDstColor      BlendFactor = 5
	BlendFactorSrcAlpha              BlendFactor = 6
	BlendFactorOneMinusSrcAlpha      BlendFactor = 7
	BlendFactorDstAlpha              BlendFactor = 8
	BlendFactorOneMinusDstAlpha      BlendFactor = 9
	BlendFactorConstantColor         BlendFactor = 10
	BlendFactorOneMinusConstantColor BlendFactor = 11
	BlendFactorConstantAlpha         BlendFactor = 12
	BlendFactorOneMinusConstantAlpha BlendFactor = 13
	BlendFactorSrcAlphaSaturate      BlendFactor = 14
	BlendFactorSrc1Color             BlendFactor = 15
	BlendFactorOneMinusSrc1Color     BlendFactor = 16
	BlendFactorSrc1Alpha             BlendFactor = 17
	BlendFactorOneMinusSrc1Alpha     BlendFactor = 18
	BlendFactorMaxEnum               BlendFactor = 2147483647
)

// BlendOp as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBlendOp.html
type BlendOp int32

// BlendOp enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBlendOp.html
const (
	BlendOpAdd              BlendOp = iota
	BlendOpSubtract         BlendOp = 1
	BlendOpReverseSubtract  BlendOp = 2
	BlendOpMin              BlendOp = 3
	BlendOpMax              BlendOp = 4
	BlendOpZero             BlendOp = 1000148000
	BlendOpSrc              BlendOp = 1000148001
	BlendOpDst              BlendOp = 1000148002
	BlendOpSrcOver          BlendOp = 1000148003
	BlendOpDstOver          BlendOp = 1000148004
	BlendOpSrcIn            BlendOp = 1000148005
	BlendOpDstIn            BlendOp = 1000148006
	BlendOpSrcOut           BlendOp = 1000148007
	BlendOpDstOut           BlendOp = 1000148008
	BlendOpSrcAtop          BlendOp = 1000148009
	BlendOpDstAtop          BlendOp = 1000148010
	BlendOpXor              BlendOp = 1000148011
	BlendOpMultiply         BlendOp = 1000148012
	BlendOpScreen           BlendOp = 1000148013
	BlendOpOverlay          BlendOp = 1000148014
	BlendOpDarken           BlendOp = 1000148015
	BlendOpLighten          BlendOp = 1000148016
	BlendOpColordodge       BlendOp = 1000148017
	BlendOpColorburn        BlendOp = 1000148018
	BlendOpHardlight        BlendOp = 1000148019
	BlendOpSoftlight        BlendOp = 1000148020
	BlendOpDifference       BlendOp = 1000148021
	BlendOpExclusion        BlendOp = 1000148022
	BlendOpInvert           BlendOp = 1000148023
	BlendOpInvertRgb        BlendOp = 1000148024
	BlendOpLineardodge      BlendOp = 1000148025
	BlendOpLinearburn       BlendOp = 1000148026
	BlendOpVividlight       BlendOp = 1000148027
	BlendOpLinearlight      BlendOp = 1000148028
	BlendOpPinlight         BlendOp = 1000148029
	BlendOpHardmix          BlendOp = 1000148030
	BlendOpHslHue           BlendOp = 1000148031
	BlendOpHslSaturation    BlendOp = 1000148032
	BlendOpHslColor         BlendOp = 1000148033
	BlendOpHslLuminosity    BlendOp = 1000148034
	BlendOpPlus             BlendOp = 1000148035
	BlendOpPlusClamped      BlendOp = 1000148036
	BlendOpPlusClampedAlpha BlendOp = 1000148037
	BlendOpPlusDarker       BlendOp = 1000148038
	BlendOpMinus            BlendOp = 1000148039
	BlendOpMinusClamped     BlendOp = 1000148040
	BlendOpContrast         BlendOp = 1000148041
	BlendOpInvertOvg        BlendOp = 1000148042
	BlendOpRed              BlendOp = 1000148043
	BlendOpGreen            BlendOp = 1000148044
	BlendOpBlue             BlendOp = 1000148045
	BlendOpMaxEnum          BlendOp = 2147483647
)

// CompareOp as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCompareOp.html
type CompareOp int32

// CompareOp enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCompareOp.html
const (
	CompareOpNever          CompareOp = iota
	CompareOpLess           CompareOp = 1
	CompareOpEqual          CompareOp = 2
	CompareOpLessOrEqual    CompareOp = 3
	CompareOpGreater        CompareOp = 4
	CompareOpNotEqual       CompareOp = 5
	CompareOpGreaterOrEqual CompareOp = 6
	CompareOpAlways         CompareOp = 7
	CompareOpMaxEnum        CompareOp = 2147483647
)

// DynamicState as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDynamicState.html
type DynamicState int32

// DynamicState enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDynamicState.html
const (
	DynamicStateViewport                     DynamicState = iota
	DynamicStateScissor                      DynamicState = 1
	DynamicStateLineWidth                    DynamicState = 2
	DynamicStateDepthBias                    DynamicState = 3
	DynamicStateBlendConstants               DynamicState = 4
	DynamicStateDepthBounds                  DynamicState = 5
	DynamicStateStencilCompareMask           DynamicState = 6
	DynamicStateStencilWriteMask             DynamicState = 7
	DynamicStateStencilReference             DynamicState = 8
	DynamicStateViewportWScalingNv           DynamicState = 1000087000
	DynamicStateDiscardRectangle             DynamicState = 1000099000
	DynamicStateSampleLocations              DynamicState = 1000143000
	DynamicStateRayTracingPipelineStackSize  DynamicState = 1000347000
	DynamicStateViewportShadingRatePaletteNv DynamicState = 1000164004
	DynamicStateViewportCoarseSampleOrderNv  DynamicState = 1000164006
	DynamicStateExclusiveScissorNv           DynamicState = 1000205001
	DynamicStateFragmentShadingRate          DynamicState = 1000226000
	DynamicStateLineStipple                  DynamicState = 1000259000
	DynamicStateCullMode                     DynamicState = 1000267000
	DynamicStateFrontFace                    DynamicState = 1000267001
	DynamicStatePrimitiveTopology            DynamicState = 1000267002
	DynamicStateViewportWithCount            DynamicState = 1000267003
	DynamicStateScissorWithCount             DynamicState = 1000267004
	DynamicStateVertexInputBindingStride     DynamicState = 1000267005
	DynamicStateDepthTestEnable              DynamicState = 1000267006
	DynamicStateDepthWriteEnable             DynamicState = 1000267007
	DynamicStateDepthCompareOp               DynamicState = 1000267008
	DynamicStateDepthBoundsTestEnable        DynamicState = 1000267009
	DynamicStateStencilTestEnable            DynamicState = 1000267010
	DynamicStateStencilOp                    DynamicState = 1000267011
	DynamicStateVertexInput                  DynamicState = 1000352000
	DynamicStatePatchControlPoints           DynamicState = 1000377000
	DynamicStateRasterizerDiscardEnable      DynamicState = 1000377001
	DynamicStateDepthBiasEnable              DynamicState = 1000377002
	DynamicStateLogicOp                      DynamicState = 1000377003
	DynamicStatePrimitiveRestartEnable       DynamicState = 1000377004
	DynamicStateColorWriteEnable             DynamicState = 1000381000
	DynamicStateMaxEnum                      DynamicState = 2147483647
)

// FrontFace as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFrontFace.html
type FrontFace int32

// FrontFace enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFrontFace.html
const (
	FrontFaceCounterClockwise FrontFace = iota
	FrontFaceClockwise        FrontFace = 1
	FrontFaceMaxEnum          FrontFace = 2147483647
)

// VertexInputRate as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkVertexInputRate.html
type VertexInputRate int32

// VertexInputRate enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkVertexInputRate.html
const (
	VertexInputRateVertex   VertexInputRate = iota
	VertexInputRateInstance VertexInputRate = 1
	VertexInputRateMaxEnum  VertexInputRate = 2147483647
)

// PrimitiveTopology as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPrimitiveTopology.html
type PrimitiveTopology int32

// PrimitiveTopology enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPrimitiveTopology.html
const (
	PrimitiveTopologyPointList                  PrimitiveTopology = iota
	PrimitiveTopologyLineList                   PrimitiveTopology = 1
	PrimitiveTopologyLineStrip                  PrimitiveTopology = 2
	PrimitiveTopologyTriangleList               PrimitiveTopology = 3
	PrimitiveTopologyTriangleStrip              PrimitiveTopology = 4
	PrimitiveTopologyTriangleFan                PrimitiveTopology = 5
	PrimitiveTopologyLineListWithAdjacency      PrimitiveTopology = 6
	PrimitiveTopologyLineStripWithAdjacency     PrimitiveTopology = 7
	PrimitiveTopologyTriangleListWithAdjacency  PrimitiveTopology = 8
	PrimitiveTopologyTriangleStripWithAdjacency PrimitiveTopology = 9
	PrimitiveTopologyPatchList                  PrimitiveTopology = 10
	PrimitiveTopologyMaxEnum                    PrimitiveTopology = 2147483647
)

// PolygonMode as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPolygonMode.html
type PolygonMode int32

// PolygonMode enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPolygonMode.html
const (
	PolygonModeFill            PolygonMode = iota
	PolygonModeLine            PolygonMode = 1
	PolygonModePoint           PolygonMode = 2
	PolygonModeFillRectangleNv PolygonMode = 1000153000
	PolygonModeMaxEnum         PolygonMode = 2147483647
)

// StencilOp as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkStencilOp.html
type StencilOp int32

// StencilOp enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkStencilOp.html
const (
	StencilOpKeep              StencilOp = iota
	StencilOpZero              StencilOp = 1
	StencilOpReplace           StencilOp = 2
	StencilOpIncrementAndClamp StencilOp = 3
	StencilOpDecrementAndClamp StencilOp = 4
	StencilOpInvert            StencilOp = 5
	StencilOpIncrementAndWrap  StencilOp = 6
	StencilOpDecrementAndWrap  StencilOp = 7
	StencilOpMaxEnum           StencilOp = 2147483647
)

// LogicOp as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkLogicOp.html
type LogicOp int32

// LogicOp enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkLogicOp.html
const (
	LogicOpClear        LogicOp = iota
	LogicOpAnd          LogicOp = 1
	LogicOpAndReverse   LogicOp = 2
	LogicOpCopy         LogicOp = 3
	LogicOpAndInverted  LogicOp = 4
	LogicOpNoOp         LogicOp = 5
	LogicOpXor          LogicOp = 6
	LogicOpOr           LogicOp = 7
	LogicOpNor          LogicOp = 8
	LogicOpEquivalent   LogicOp = 9
	LogicOpInvert       LogicOp = 10
	LogicOpOrReverse    LogicOp = 11
	LogicOpCopyInverted LogicOp = 12
	LogicOpOrInverted   LogicOp = 13
	LogicOpNand         LogicOp = 14
	LogicOpSet          LogicOp = 15
	LogicOpMaxEnum      LogicOp = 2147483647
)

// BorderColor as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBorderColor.html
type BorderColor int32

// BorderColor enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBorderColor.html
const (
	BorderColorFloatTransparentBlack BorderColor = iota
	BorderColorIntTransparentBlack   BorderColor = 1
	BorderColorFloatOpaqueBlack      BorderColor = 2
	BorderColorIntOpaqueBlack        BorderColor = 3
	BorderColorFloatOpaqueWhite      BorderColor = 4
	BorderColorIntOpaqueWhite        BorderColor = 5
	BorderColorFloatCustom           BorderColor = 1000287003
	BorderColorIntCustom             BorderColor = 1000287004
	BorderColorMaxEnum               BorderColor = 2147483647
)

// Filter as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFilter.html
type Filter int32

// Filter enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFilter.html
const (
	FilterNearest  Filter = iota
	FilterLinear   Filter = 1
	FilterCubicImg Filter = 1000015000
	FilterCubic    Filter = 1000015000
	FilterMaxEnum  Filter = 2147483647
)

// SamplerAddressMode as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerAddressMode.html
type SamplerAddressMode int32

// SamplerAddressMode enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerAddressMode.html
const (
	SamplerAddressModeRepeat            SamplerAddressMode = iota
	SamplerAddressModeMirroredRepeat    SamplerAddressMode = 1
	SamplerAddressModeClampToEdge       SamplerAddressMode = 2
	SamplerAddressModeClampToBorder     SamplerAddressMode = 3
	SamplerAddressModeMirrorClampToEdge SamplerAddressMode = 4
	SamplerAddressModeMaxEnum           SamplerAddressMode = 2147483647
)

// SamplerMipmapMode as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerMipmapMode.html
type SamplerMipmapMode int32

// SamplerMipmapMode enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerMipmapMode.html
const (
	SamplerMipmapModeNearest SamplerMipmapMode = iota
	SamplerMipmapModeLinear  SamplerMipmapMode = 1
	SamplerMipmapModeMaxEnum SamplerMipmapMode = 2147483647
)

// DescriptorType as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorType.html
type DescriptorType int32

// DescriptorType enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorType.html
const (
	DescriptorTypeSampler                 DescriptorType = iota
	DescriptorTypeCombinedImageSampler    DescriptorType = 1
	DescriptorTypeSampledImage            DescriptorType = 2
	DescriptorTypeStorageImage            DescriptorType = 3
	DescriptorTypeUniformTexelBuffer      DescriptorType = 4
	DescriptorTypeStorageTexelBuffer      DescriptorType = 5
	DescriptorTypeUniformBuffer           DescriptorType = 6
	DescriptorTypeStorageBuffer           DescriptorType = 7
	DescriptorTypeUniformBufferDynamic    DescriptorType = 8
	DescriptorTypeStorageBufferDynamic    DescriptorType = 9
	DescriptorTypeInputAttachment         DescriptorType = 10
	DescriptorTypeInlineUniformBlock      DescriptorType = 1000138000
	DescriptorTypeAccelerationStructure   DescriptorType = 1000150000
	DescriptorTypeAccelerationStructureNv DescriptorType = 1000165000
	DescriptorTypeMutableValve            DescriptorType = 1000351000
	DescriptorTypeMaxEnum                 DescriptorType = 2147483647
)

// AttachmentLoadOp as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAttachmentLoadOp.html
type AttachmentLoadOp int32

// AttachmentLoadOp enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAttachmentLoadOp.html
const (
	AttachmentLoadOpLoad     AttachmentLoadOp = iota
	AttachmentLoadOpClear    AttachmentLoadOp = 1
	AttachmentLoadOpDontCare AttachmentLoadOp = 2
	AttachmentLoadOpMaxEnum  AttachmentLoadOp = 2147483647
)

// AttachmentStoreOp as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAttachmentStoreOp.html
type AttachmentStoreOp int32

// AttachmentStoreOp enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAttachmentStoreOp.html
const (
	AttachmentStoreOpStore    AttachmentStoreOp = iota
	AttachmentStoreOpDontCare AttachmentStoreOp = 1
	AttachmentStoreOpNoneQcom AttachmentStoreOp = 1000301000
	AttachmentStoreOpMaxEnum  AttachmentStoreOp = 2147483647
)

// PipelineBindPoint as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineBindPoint.html
type PipelineBindPoint int32

// PipelineBindPoint enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineBindPoint.html
const (
	PipelineBindPointGraphics     PipelineBindPoint = iota
	PipelineBindPointCompute      PipelineBindPoint = 1
	PipelineBindPointRayTracing   PipelineBindPoint = 1000165000
	PipelineBindPointRayTracingNv PipelineBindPoint = 1000165000
	PipelineBindPointMaxEnum      PipelineBindPoint = 2147483647
)

// CommandBufferLevel as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandBufferLevel.html
type CommandBufferLevel int32

// CommandBufferLevel enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandBufferLevel.html
const (
	CommandBufferLevelPrimary   CommandBufferLevel = iota
	CommandBufferLevelSecondary CommandBufferLevel = 1
	CommandBufferLevelMaxEnum   CommandBufferLevel = 2147483647
)

// IndexType as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkIndexType.html
type IndexType int32

// IndexType enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkIndexType.html
const (
	IndexTypeUint16  IndexType = iota
	IndexTypeUint32  IndexType = 1
	IndexTypeNone    IndexType = 1000165000
	IndexTypeUint8   IndexType = 1000265000
	IndexTypeNoneNv  IndexType = 1000165000
	IndexTypeMaxEnum IndexType = 2147483647
)

// SubpassContents as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSubpassContents.html
type SubpassContents int32

// SubpassContents enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSubpassContents.html
const (
	SubpassContentsInline                  SubpassContents = iota
	SubpassContentsSecondaryCommandBuffers SubpassContents = 1
	SubpassContentsMaxEnum                 SubpassContents = 2147483647
)

// AccessFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAccessFlagBits.html
type AccessFlagBits int32

// AccessFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAccessFlagBits.html
const (
	AccessIndirectCommandReadBit               AccessFlagBits = 1
	AccessIndexReadBit                         AccessFlagBits = 2
	AccessVertexAttributeReadBit               AccessFlagBits = 4
	AccessUniformReadBit                       AccessFlagBits = 8
	AccessInputAttachmentReadBit               AccessFlagBits = 16
	AccessShaderReadBit                        AccessFlagBits = 32
	AccessShaderWriteBit                       AccessFlagBits = 64
	AccessColorAttachmentReadBit               AccessFlagBits = 128
	AccessColorAttachmentWriteBit              AccessFlagBits = 256
	AccessDepthStencilAttachmentReadBit        AccessFlagBits = 512
	AccessDepthStencilAttachmentWriteBit       AccessFlagBits = 1024
	AccessTransferReadBit                      AccessFlagBits = 2048
	AccessTransferWriteBit                     AccessFlagBits = 4096
	AccessHostReadBit                          AccessFlagBits = 8192
	AccessHostWriteBit                         AccessFlagBits = 16384
	AccessMemoryReadBit                        AccessFlagBits = 32768
	AccessMemoryWriteBit                       AccessFlagBits = 65536
	AccessTransformFeedbackWriteBit            AccessFlagBits = 33554432
	AccessTransformFeedbackCounterReadBit      AccessFlagBits = 67108864
	AccessTransformFeedbackCounterWriteBit     AccessFlagBits = 134217728
	AccessConditionalRenderingReadBit          AccessFlagBits = 1048576
	AccessColorAttachmentReadNoncoherentBit    AccessFlagBits = 524288
	AccessAccelerationStructureReadBit         AccessFlagBits = 2097152
	AccessAccelerationStructureWriteBit        AccessFlagBits = 4194304
	AccessShadingRateImageReadBitNv            AccessFlagBits = 8388608
	AccessFragmentDensityMapReadBit            AccessFlagBits = 16777216
	AccessCommandPreprocessReadBitNv           AccessFlagBits = 131072
	AccessCommandPreprocessWriteBitNv          AccessFlagBits = 262144
	AccessNone                                 AccessFlagBits = 0
	AccessAccelerationStructureReadBitNv       AccessFlagBits = 2097152
	AccessAccelerationStructureWriteBitNv      AccessFlagBits = 4194304
	AccessFragmentShadingRateAttachmentReadBit AccessFlagBits = 8388608
	AccessFlagBitsMaxEnum                      AccessFlagBits = 2147483647
)

// ImageAspectFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageAspectFlagBits.html
type ImageAspectFlagBits int32

// ImageAspectFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageAspectFlagBits.html
const (
	ImageAspectColorBit        ImageAspectFlagBits = 1
	ImageAspectDepthBit        ImageAspectFlagBits = 2
	ImageAspectStencilBit      ImageAspectFlagBits = 4
	ImageAspectMetadataBit     ImageAspectFlagBits = 8
	ImageAspectPlane0Bit       ImageAspectFlagBits = 16
	ImageAspectPlane1Bit       ImageAspectFlagBits = 32
	ImageAspectPlane2Bit       ImageAspectFlagBits = 64
	ImageAspectMemoryPlane0Bit ImageAspectFlagBits = 128
	ImageAspectMemoryPlane1Bit ImageAspectFlagBits = 256
	ImageAspectMemoryPlane2Bit ImageAspectFlagBits = 512
	ImageAspectMemoryPlane3Bit ImageAspectFlagBits = 1024
	ImageAspectFlagBitsMaxEnum ImageAspectFlagBits = 2147483647
)

// FormatFeatureFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFormatFeatureFlagBits.html
type FormatFeatureFlagBits int32

// FormatFeatureFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFormatFeatureFlagBits.html
const (
	FormatFeatureSampledImageBit                                                     FormatFeatureFlagBits = 1
	FormatFeatureStorageImageBit                                                     FormatFeatureFlagBits = 2
	FormatFeatureStorageImageAtomicBit                                               FormatFeatureFlagBits = 4
	FormatFeatureUniformTexelBufferBit                                               FormatFeatureFlagBits = 8
	FormatFeatureStorageTexelBufferBit                                               FormatFeatureFlagBits = 16
	FormatFeatureStorageTexelBufferAtomicBit                                         FormatFeatureFlagBits = 32
	FormatFeatureVertexBufferBit                                                     FormatFeatureFlagBits = 64
	FormatFeatureColorAttachmentBit                                                  FormatFeatureFlagBits = 128
	FormatFeatureColorAttachmentBlendBit                                             FormatFeatureFlagBits = 256
	FormatFeatureDepthStencilAttachmentBit                                           FormatFeatureFlagBits = 512
	FormatFeatureBlitSrcBit                                                          FormatFeatureFlagBits = 1024
	FormatFeatureBlitDstBit                                                          FormatFeatureFlagBits = 2048
	FormatFeatureSampledImageFilterLinearBit                                         FormatFeatureFlagBits = 4096
	FormatFeatureTransferSrcBit                                                      FormatFeatureFlagBits = 16384
	FormatFeatureTransferDstBit                                                      FormatFeatureFlagBits = 32768
	FormatFeatureMidpointChromaSamplesBit                                            FormatFeatureFlagBits = 131072
	FormatFeatureSampledImageYcbcrConversionLinearFilterBit                          FormatFeatureFlagBits = 262144
	FormatFeatureSampledImageYcbcrConversionSeparateReconstructionFilterBit          FormatFeatureFlagBits = 524288
	FormatFeatureSampledImageYcbcrConversionChromaReconstructionExplicitBit          FormatFeatureFlagBits = 1048576
	FormatFeatureSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit FormatFeatureFlagBits = 2097152
	FormatFeatureDisjointBit                                                         FormatFeatureFlagBits = 4194304
	FormatFeatureCositedChromaSamplesBit                                             FormatFeatureFlagBits = 8388608
	FormatFeatureSampledImageFilterMinmaxBit                                         FormatFeatureFlagBits = 65536
	FormatFeatureSampledImageFilterCubicBitImg                                       FormatFeatureFlagBits = 8192
	FormatFeatureAccelerationStructureVertexBufferBit                                FormatFeatureFlagBits = 536870912
	FormatFeatureFragmentDensityMapBit                                               FormatFeatureFlagBits = 16777216
	FormatFeatureFragmentShadingRateAttachmentBit                                    FormatFeatureFlagBits = 1073741824
	FormatFeatureSampledImageFilterCubicBit                                          FormatFeatureFlagBits = 8192
	FormatFeatureFlagBitsMaxEnum                                                     FormatFeatureFlagBits = 2147483647
)

// ImageCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageCreateFlagBits.html
type ImageCreateFlagBits int32

// ImageCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageCreateFlagBits.html
const (
	ImageCreateSparseBindingBit                  ImageCreateFlagBits = 1
	ImageCreateSparseResidencyBit                ImageCreateFlagBits = 2
	ImageCreateSparseAliasedBit                  ImageCreateFlagBits = 4
	ImageCreateMutableFormatBit                  ImageCreateFlagBits = 8
	ImageCreateCubeCompatibleBit                 ImageCreateFlagBits = 16
	ImageCreateAliasBit                          ImageCreateFlagBits = 1024
	ImageCreateSplitInstanceBindRegionsBit       ImageCreateFlagBits = 64
	ImageCreate2dArrayCompatibleBit              ImageCreateFlagBits = 32
	ImageCreateBlockTexelViewCompatibleBit       ImageCreateFlagBits = 128
	ImageCreateExtendedUsageBit                  ImageCreateFlagBits = 256
	ImageCreateProtectedBit                      ImageCreateFlagBits = 2048
	ImageCreateDisjointBit                       ImageCreateFlagBits = 512
	ImageCreateCornerSampledBitNv                ImageCreateFlagBits = 8192
	ImageCreateSampleLocationsCompatibleDepthBit ImageCreateFlagBits = 4096
	ImageCreateSubsampledBit                     ImageCreateFlagBits = 16384
	ImageCreateFlagBitsMaxEnum                   ImageCreateFlagBits = 2147483647
)

// SampleCountFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSampleCountFlagBits.html
type SampleCountFlagBits int32

// SampleCountFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSampleCountFlagBits.html
const (
	SampleCount1Bit            SampleCountFlagBits = 1
	SampleCount2Bit            SampleCountFlagBits = 2
	SampleCount4Bit            SampleCountFlagBits = 4
	SampleCount8Bit            SampleCountFlagBits = 8
	SampleCount16Bit           SampleCountFlagBits = 16
	SampleCount32Bit           SampleCountFlagBits = 32
	SampleCount64Bit           SampleCountFlagBits = 64
	SampleCountFlagBitsMaxEnum SampleCountFlagBits = 2147483647
)

// ImageUsageFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageUsageFlagBits.html
type ImageUsageFlagBits int32

// ImageUsageFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageUsageFlagBits.html
const (
	ImageUsageTransferSrcBit                   ImageUsageFlagBits = 1
	ImageUsageTransferDstBit                   ImageUsageFlagBits = 2
	ImageUsageSampledBit                       ImageUsageFlagBits = 4
	ImageUsageStorageBit                       ImageUsageFlagBits = 8
	ImageUsageColorAttachmentBit               ImageUsageFlagBits = 16
	ImageUsageDepthStencilAttachmentBit        ImageUsageFlagBits = 32
	ImageUsageTransientAttachmentBit           ImageUsageFlagBits = 64
	ImageUsageInputAttachmentBit               ImageUsageFlagBits = 128
	ImageUsageShadingRateImageBitNv            ImageUsageFlagBits = 256
	ImageUsageFragmentDensityMapBit            ImageUsageFlagBits = 512
	ImageUsageFragmentShadingRateAttachmentBit ImageUsageFlagBits = 256
	ImageUsageFlagBitsMaxEnum                  ImageUsageFlagBits = 2147483647
)

// MemoryHeapFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryHeapFlagBits.html
type MemoryHeapFlagBits int32

// MemoryHeapFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryHeapFlagBits.html
const (
	MemoryHeapDeviceLocalBit   MemoryHeapFlagBits = 1
	MemoryHeapMultiInstanceBit MemoryHeapFlagBits = 2
	MemoryHeapFlagBitsMaxEnum  MemoryHeapFlagBits = 2147483647
)

// MemoryPropertyFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryPropertyFlagBits.html
type MemoryPropertyFlagBits int32

// MemoryPropertyFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryPropertyFlagBits.html
const (
	MemoryPropertyDeviceLocalBit       MemoryPropertyFlagBits = 1
	MemoryPropertyHostVisibleBit       MemoryPropertyFlagBits = 2
	MemoryPropertyHostCoherentBit      MemoryPropertyFlagBits = 4
	MemoryPropertyHostCachedBit        MemoryPropertyFlagBits = 8
	MemoryPropertyLazilyAllocatedBit   MemoryPropertyFlagBits = 16
	MemoryPropertyProtectedBit         MemoryPropertyFlagBits = 32
	MemoryPropertyDeviceCoherentBitAmd MemoryPropertyFlagBits = 64
	MemoryPropertyDeviceUncachedBitAmd MemoryPropertyFlagBits = 128
	MemoryPropertyFlagBitsMaxEnum      MemoryPropertyFlagBits = 2147483647
)

// QueueFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueueFlagBits.html
type QueueFlagBits int32

// QueueFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueueFlagBits.html
const (
	QueueGraphicsBit      QueueFlagBits = 1
	QueueComputeBit       QueueFlagBits = 2
	QueueTransferBit      QueueFlagBits = 4
	QueueSparseBindingBit QueueFlagBits = 8
	QueueProtectedBit     QueueFlagBits = 16
	QueueFlagBitsMaxEnum  QueueFlagBits = 2147483647
)

// DeviceQueueCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceQueueCreateFlagBits.html
type DeviceQueueCreateFlagBits int32

// DeviceQueueCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceQueueCreateFlagBits.html
const (
	DeviceQueueCreateProtectedBit    DeviceQueueCreateFlagBits = 1
	DeviceQueueCreateFlagBitsMaxEnum DeviceQueueCreateFlagBits = 2147483647
)

// PipelineStageFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineStageFlagBits.html
type PipelineStageFlagBits int32

// PipelineStageFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineStageFlagBits.html
const (
	PipelineStageTopOfPipeBit                     PipelineStageFlagBits = 1
	PipelineStageDrawIndirectBit                  PipelineStageFlagBits = 2
	PipelineStageVertexInputBit                   PipelineStageFlagBits = 4
	PipelineStageVertexShaderBit                  PipelineStageFlagBits = 8
	PipelineStageTessellationControlShaderBit     PipelineStageFlagBits = 16
	PipelineStageTessellationEvaluationShaderBit  PipelineStageFlagBits = 32
	PipelineStageGeometryShaderBit                PipelineStageFlagBits = 64
	PipelineStageFragmentShaderBit                PipelineStageFlagBits = 128
	PipelineStageEarlyFragmentTestsBit            PipelineStageFlagBits = 256
	PipelineStageLateFragmentTestsBit             PipelineStageFlagBits = 512
	PipelineStageColorAttachmentOutputBit         PipelineStageFlagBits = 1024
	PipelineStageComputeShaderBit                 PipelineStageFlagBits = 2048
	PipelineStageTransferBit                      PipelineStageFlagBits = 4096
	PipelineStageBottomOfPipeBit                  PipelineStageFlagBits = 8192
	PipelineStageHostBit                          PipelineStageFlagBits = 16384
	PipelineStageAllGraphicsBit                   PipelineStageFlagBits = 32768
	PipelineStageAllCommandsBit                   PipelineStageFlagBits = 65536
	PipelineStageTransformFeedbackBit             PipelineStageFlagBits = 16777216
	PipelineStageConditionalRenderingBit          PipelineStageFlagBits = 262144
	PipelineStageAccelerationStructureBuildBit    PipelineStageFlagBits = 33554432
	PipelineStageRayTracingShaderBit              PipelineStageFlagBits = 2097152
	PipelineStageShadingRateImageBitNv            PipelineStageFlagBits = 4194304
	PipelineStageTaskShaderBitNv                  PipelineStageFlagBits = 524288
	PipelineStageMeshShaderBitNv                  PipelineStageFlagBits = 1048576
	PipelineStageFragmentDensityProcessBit        PipelineStageFlagBits = 8388608
	PipelineStageCommandPreprocessBitNv           PipelineStageFlagBits = 131072
	PipelineStageNone                             PipelineStageFlagBits = 0
	PipelineStageRayTracingShaderBitNv            PipelineStageFlagBits = 2097152
	PipelineStageAccelerationStructureBuildBitNv  PipelineStageFlagBits = 33554432
	PipelineStageFragmentShadingRateAttachmentBit PipelineStageFlagBits = 4194304
	PipelineStageFlagBitsMaxEnum                  PipelineStageFlagBits = 2147483647
)

// SparseMemoryBindFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSparseMemoryBindFlagBits.html
type SparseMemoryBindFlagBits int32

// SparseMemoryBindFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSparseMemoryBindFlagBits.html
const (
	SparseMemoryBindMetadataBit     SparseMemoryBindFlagBits = 1
	SparseMemoryBindFlagBitsMaxEnum SparseMemoryBindFlagBits = 2147483647
)

// SparseImageFormatFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSparseImageFormatFlagBits.html
type SparseImageFormatFlagBits int32

// SparseImageFormatFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSparseImageFormatFlagBits.html
const (
	SparseImageFormatSingleMiptailBit        SparseImageFormatFlagBits = 1
	SparseImageFormatAlignedMipSizeBit       SparseImageFormatFlagBits = 2
	SparseImageFormatNonstandardBlockSizeBit SparseImageFormatFlagBits = 4
	SparseImageFormatFlagBitsMaxEnum         SparseImageFormatFlagBits = 2147483647
)

// FenceCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFenceCreateFlagBits.html
type FenceCreateFlagBits int32

// FenceCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFenceCreateFlagBits.html
const (
	FenceCreateSignaledBit     FenceCreateFlagBits = 1
	FenceCreateFlagBitsMaxEnum FenceCreateFlagBits = 2147483647
)

// EventCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkEventCreateFlagBits.html
type EventCreateFlagBits int32

// EventCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkEventCreateFlagBits.html
const (
	EventCreateDeviceOnlyBit   EventCreateFlagBits = 1
	EventCreateFlagBitsMaxEnum EventCreateFlagBits = 2147483647
)

// QueryPipelineStatisticFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueryPipelineStatisticFlagBits.html
type QueryPipelineStatisticFlagBits int32

// QueryPipelineStatisticFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueryPipelineStatisticFlagBits.html
const (
	QueryPipelineStatisticInputAssemblyVerticesBit                   QueryPipelineStatisticFlagBits = 1
	QueryPipelineStatisticInputAssemblyPrimitivesBit                 QueryPipelineStatisticFlagBits = 2
	QueryPipelineStatisticVertexShaderInvocationsBit                 QueryPipelineStatisticFlagBits = 4
	QueryPipelineStatisticGeometryShaderInvocationsBit               QueryPipelineStatisticFlagBits = 8
	QueryPipelineStatisticGeometryShaderPrimitivesBit                QueryPipelineStatisticFlagBits = 16
	QueryPipelineStatisticClippingInvocationsBit                     QueryPipelineStatisticFlagBits = 32
	QueryPipelineStatisticClippingPrimitivesBit                      QueryPipelineStatisticFlagBits = 64
	QueryPipelineStatisticFragmentShaderInvocationsBit               QueryPipelineStatisticFlagBits = 128
	QueryPipelineStatisticTessellationControlShaderPatchesBit        QueryPipelineStatisticFlagBits = 256
	QueryPipelineStatisticTessellationEvaluationShaderInvocationsBit QueryPipelineStatisticFlagBits = 512
	QueryPipelineStatisticComputeShaderInvocationsBit                QueryPipelineStatisticFlagBits = 1024
	QueryPipelineStatisticFlagBitsMaxEnum                            QueryPipelineStatisticFlagBits = 2147483647
)

// QueryResultFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueryResultFlagBits.html
type QueryResultFlagBits int32

// QueryResultFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueryResultFlagBits.html
const (
	QueryResult64Bit               QueryResultFlagBits = 1
	QueryResultWaitBit             QueryResultFlagBits = 2
	QueryResultWithAvailabilityBit QueryResultFlagBits = 4
	QueryResultPartialBit          QueryResultFlagBits = 8
	QueryResultFlagBitsMaxEnum     QueryResultFlagBits = 2147483647
)

// BufferCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBufferCreateFlagBits.html
type BufferCreateFlagBits int32

// BufferCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBufferCreateFlagBits.html
const (
	BufferCreateSparseBindingBit              BufferCreateFlagBits = 1
	BufferCreateSparseResidencyBit            BufferCreateFlagBits = 2
	BufferCreateSparseAliasedBit              BufferCreateFlagBits = 4
	BufferCreateProtectedBit                  BufferCreateFlagBits = 8
	BufferCreateDeviceAddressCaptureReplayBit BufferCreateFlagBits = 16
	BufferCreateFlagBitsMaxEnum               BufferCreateFlagBits = 2147483647
)

// BufferUsageFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBufferUsageFlagBits.html
type BufferUsageFlagBits int32

// BufferUsageFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBufferUsageFlagBits.html
const (
	BufferUsageTransferSrcBit                             BufferUsageFlagBits = 1
	BufferUsageTransferDstBit                             BufferUsageFlagBits = 2
	BufferUsageUniformTexelBufferBit                      BufferUsageFlagBits = 4
	BufferUsageStorageTexelBufferBit                      BufferUsageFlagBits = 8
	BufferUsageUniformBufferBit                           BufferUsageFlagBits = 16
	BufferUsageStorageBufferBit                           BufferUsageFlagBits = 32
	BufferUsageIndexBufferBit                             BufferUsageFlagBits = 64
	BufferUsageVertexBufferBit                            BufferUsageFlagBits = 128
	BufferUsageIndirectBufferBit                          BufferUsageFlagBits = 256
	BufferUsageShaderDeviceAddressBit                     BufferUsageFlagBits = 131072
	BufferUsageTransformFeedbackBufferBit                 BufferUsageFlagBits = 2048
	BufferUsageTransformFeedbackCounterBufferBit          BufferUsageFlagBits = 4096
	BufferUsageConditionalRenderingBit                    BufferUsageFlagBits = 512
	BufferUsageAccelerationStructureBuildInputReadOnlyBit BufferUsageFlagBits = 524288
	BufferUsageAccelerationStructureStorageBit            BufferUsageFlagBits = 1048576
	BufferUsageShaderBindingTableBit                      BufferUsageFlagBits = 1024
	BufferUsageRayTracingBitNv                            BufferUsageFlagBits = 1024
	BufferUsageFlagBitsMaxEnum                            BufferUsageFlagBits = 2147483647
)

// ImageViewCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageViewCreateFlagBits.html
type ImageViewCreateFlagBits int32

// ImageViewCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageViewCreateFlagBits.html
const (
	ImageViewCreateFragmentDensityMapDynamicBit  ImageViewCreateFlagBits = 1
	ImageViewCreateFragmentDensityMapDeferredBit ImageViewCreateFlagBits = 2
	ImageViewCreateFlagBitsMaxEnum               ImageViewCreateFlagBits = 2147483647
)

// ShaderModuleCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkShaderModuleCreateFlagBits.html
type ShaderModuleCreateFlagBits int32

// ShaderModuleCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkShaderModuleCreateFlagBits.html
const (
	ShaderModuleCreateFlagBitsMaxEnum ShaderModuleCreateFlagBits = 2147483647
)

// PipelineCacheCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCacheCreateFlagBits.html
type PipelineCacheCreateFlagBits int32

// PipelineCacheCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCacheCreateFlagBits.html
const (
	PipelineCacheCreateExternallySynchronizedBit PipelineCacheCreateFlagBits = 1
	PipelineCacheCreateFlagBitsMaxEnum           PipelineCacheCreateFlagBits = 2147483647
)

// ColorComponentFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkColorComponentFlagBits.html
type ColorComponentFlagBits int32

// ColorComponentFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkColorComponentFlagBits.html
const (
	ColorComponentRBit            ColorComponentFlagBits = 1
	ColorComponentGBit            ColorComponentFlagBits = 2
	ColorComponentBBit            ColorComponentFlagBits = 4
	ColorComponentABit            ColorComponentFlagBits = 8
	ColorComponentFlagBitsMaxEnum ColorComponentFlagBits = 2147483647
)

// PipelineCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCreateFlagBits.html
type PipelineCreateFlagBits int32

// PipelineCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCreateFlagBits.html
const (
	PipelineCreateDisableOptimizationBit                      PipelineCreateFlagBits = 1
	PipelineCreateAllowDerivativesBit                         PipelineCreateFlagBits = 2
	PipelineCreateDerivativeBit                               PipelineCreateFlagBits = 4
	PipelineCreateViewIndexFromDeviceIndexBit                 PipelineCreateFlagBits = 8
	PipelineCreateDispatchBaseBit                             PipelineCreateFlagBits = 16
	PipelineCreateRayTracingNoNullAnyHitShadersBit            PipelineCreateFlagBits = 16384
	PipelineCreateRayTracingNoNullClosestHitShadersBit        PipelineCreateFlagBits = 32768
	PipelineCreateRayTracingNoNullMissShadersBit              PipelineCreateFlagBits = 65536
	PipelineCreateRayTracingNoNullIntersectionShadersBit      PipelineCreateFlagBits = 131072
	PipelineCreateRayTracingSkipTrianglesBit                  PipelineCreateFlagBits = 4096
	PipelineCreateRayTracingSkipAabbsBit                      PipelineCreateFlagBits = 8192
	PipelineCreateRayTracingShaderGroupHandleCaptureReplayBit PipelineCreateFlagBits = 524288
	PipelineCreateDeferCompileBitNv                           PipelineCreateFlagBits = 32
	PipelineCreateCaptureStatisticsBit                        PipelineCreateFlagBits = 64
	PipelineCreateCaptureInternalRepresentationsBit           PipelineCreateFlagBits = 128
	PipelineCreateIndirectBindableBitNv                       PipelineCreateFlagBits = 262144
	PipelineCreateLibraryBit                                  PipelineCreateFlagBits = 2048
	PipelineCreateFailOnPipelineCompileRequiredBit            PipelineCreateFlagBits = 256
	PipelineCreateEarlyReturnOnFailureBit                     PipelineCreateFlagBits = 512
	PipelineCreateDispatchBase                                PipelineCreateFlagBits = 16
	PipelineCreateFlagBitsMaxEnum                             PipelineCreateFlagBits = 2147483647
)

// PipelineShaderStageCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineShaderStageCreateFlagBits.html
type PipelineShaderStageCreateFlagBits int32

// PipelineShaderStageCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineShaderStageCreateFlagBits.html
const (
	PipelineShaderStageCreateAllowVaryingSubgroupSizeBit PipelineShaderStageCreateFlagBits = 1
	PipelineShaderStageCreateRequireFullSubgroupsBit     PipelineShaderStageCreateFlagBits = 2
	PipelineShaderStageCreateFlagBitsMaxEnum             PipelineShaderStageCreateFlagBits = 2147483647
)

// ShaderStageFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkShaderStageFlagBits.html
type ShaderStageFlagBits int32

// ShaderStageFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkShaderStageFlagBits.html
const (
	ShaderStageVertexBit                 ShaderStageFlagBits = 1
	ShaderStageTessellationControlBit    ShaderStageFlagBits = 2
	ShaderStageTessellationEvaluationBit ShaderStageFlagBits = 4
	ShaderStageGeometryBit               ShaderStageFlagBits = 8
	ShaderStageFragmentBit               ShaderStageFlagBits = 16
	ShaderStageComputeBit                ShaderStageFlagBits = 32
	ShaderStageAllGraphics               ShaderStageFlagBits = 31
	ShaderStageAll                       ShaderStageFlagBits = 2147483647
	ShaderStageRaygenBit                 ShaderStageFlagBits = 256
	ShaderStageAnyHitBit                 ShaderStageFlagBits = 512
	ShaderStageClosestHitBit             ShaderStageFlagBits = 1024
	ShaderStageMissBit                   ShaderStageFlagBits = 2048
	ShaderStageIntersectionBit           ShaderStageFlagBits = 4096
	ShaderStageCallableBit               ShaderStageFlagBits = 8192
	ShaderStageTaskBitNv                 ShaderStageFlagBits = 64
	ShaderStageMeshBitNv                 ShaderStageFlagBits = 128
	ShaderStageRaygenBitNv               ShaderStageFlagBits = 256
	ShaderStageAnyHitBitNv               ShaderStageFlagBits = 512
	ShaderStageClosestHitBitNv           ShaderStageFlagBits = 1024
	ShaderStageMissBitNv                 ShaderStageFlagBits = 2048
	ShaderStageIntersectionBitNv         ShaderStageFlagBits = 4096
	ShaderStageCallableBitNv             ShaderStageFlagBits = 8192
	ShaderStageFlagBitsMaxEnum           ShaderStageFlagBits = 2147483647
)

// CullModeFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCullModeFlagBits.html
type CullModeFlagBits int32

// CullModeFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCullModeFlagBits.html
const (
	CullModeNone            CullModeFlagBits = iota
	CullModeFrontBit        CullModeFlagBits = 1
	CullModeBackBit         CullModeFlagBits = 2
	CullModeFrontAndBack    CullModeFlagBits = 3
	CullModeFlagBitsMaxEnum CullModeFlagBits = 2147483647
)

// SamplerCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerCreateFlagBits.html
type SamplerCreateFlagBits int32

// SamplerCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerCreateFlagBits.html
const (
	SamplerCreateSubsampledBit                     SamplerCreateFlagBits = 1
	SamplerCreateSubsampledCoarseReconstructionBit SamplerCreateFlagBits = 2
	SamplerCreateFlagBitsMaxEnum                   SamplerCreateFlagBits = 2147483647
)

// DescriptorPoolCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorPoolCreateFlagBits.html
type DescriptorPoolCreateFlagBits int32

// DescriptorPoolCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorPoolCreateFlagBits.html
const (
	DescriptorPoolCreateFreeDescriptorSetBit DescriptorPoolCreateFlagBits = 1
	DescriptorPoolCreateUpdateAfterBindBit   DescriptorPoolCreateFlagBits = 2
	DescriptorPoolCreateHostOnlyBitValve     DescriptorPoolCreateFlagBits = 4
	DescriptorPoolCreateFlagBitsMaxEnum      DescriptorPoolCreateFlagBits = 2147483647
)

// DescriptorSetLayoutCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorSetLayoutCreateFlagBits.html
type DescriptorSetLayoutCreateFlagBits int32

// DescriptorSetLayoutCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorSetLayoutCreateFlagBits.html
const (
	DescriptorSetLayoutCreateUpdateAfterBindPoolBit DescriptorSetLayoutCreateFlagBits = 2
	DescriptorSetLayoutCreatePushDescriptorBit      DescriptorSetLayoutCreateFlagBits = 1
	DescriptorSetLayoutCreateHostOnlyPoolBitValve   DescriptorSetLayoutCreateFlagBits = 4
	DescriptorSetLayoutCreateFlagBitsMaxEnum        DescriptorSetLayoutCreateFlagBits = 2147483647
)

// AttachmentDescriptionFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAttachmentDescriptionFlagBits.html
type AttachmentDescriptionFlagBits int32

// AttachmentDescriptionFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAttachmentDescriptionFlagBits.html
const (
	AttachmentDescriptionMayAliasBit     AttachmentDescriptionFlagBits = 1
	AttachmentDescriptionFlagBitsMaxEnum AttachmentDescriptionFlagBits = 2147483647
)

// DependencyFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDependencyFlagBits.html
type DependencyFlagBits int32

// DependencyFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDependencyFlagBits.html
const (
	DependencyByRegionBit     DependencyFlagBits = 1
	DependencyDeviceGroupBit  DependencyFlagBits = 4
	DependencyViewLocalBit    DependencyFlagBits = 2
	DependencyFlagBitsMaxEnum DependencyFlagBits = 2147483647
)

// FramebufferCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFramebufferCreateFlagBits.html
type FramebufferCreateFlagBits int32

// FramebufferCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFramebufferCreateFlagBits.html
const (
	FramebufferCreateImagelessBit    FramebufferCreateFlagBits = 1
	FramebufferCreateFlagBitsMaxEnum FramebufferCreateFlagBits = 2147483647
)

// RenderPassCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkRenderPassCreateFlagBits.html
type RenderPassCreateFlagBits int32

// RenderPassCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkRenderPassCreateFlagBits.html
const (
	RenderPassCreateTransformBitQcom RenderPassCreateFlagBits = 2
	RenderPassCreateFlagBitsMaxEnum  RenderPassCreateFlagBits = 2147483647
)

// SubpassDescriptionFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSubpassDescriptionFlagBits.html
type SubpassDescriptionFlagBits int32

// SubpassDescriptionFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSubpassDescriptionFlagBits.html
const (
	SubpassDescriptionPerViewAttributesBitNvx    SubpassDescriptionFlagBits = 1
	SubpassDescriptionPerViewPositionXOnlyBitNvx SubpassDescriptionFlagBits = 2
	SubpassDescriptionFragmentRegionBitQcom      SubpassDescriptionFlagBits = 4
	SubpassDescriptionShaderResolveBitQcom       SubpassDescriptionFlagBits = 8
	SubpassDescriptionFlagBitsMaxEnum            SubpassDescriptionFlagBits = 2147483647
)

// CommandPoolCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandPoolCreateFlagBits.html
type CommandPoolCreateFlagBits int32

// CommandPoolCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandPoolCreateFlagBits.html
const (
	CommandPoolCreateTransientBit          CommandPoolCreateFlagBits = 1
	CommandPoolCreateResetCommandBufferBit CommandPoolCreateFlagBits = 2
	CommandPoolCreateProtectedBit          CommandPoolCreateFlagBits = 4
	CommandPoolCreateFlagBitsMaxEnum       CommandPoolCreateFlagBits = 2147483647
)

// CommandPoolResetFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandPoolResetFlagBits.html
type CommandPoolResetFlagBits int32

// CommandPoolResetFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandPoolResetFlagBits.html
const (
	CommandPoolResetReleaseResourcesBit CommandPoolResetFlagBits = 1
	CommandPoolResetFlagBitsMaxEnum     CommandPoolResetFlagBits = 2147483647
)

// CommandBufferUsageFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandBufferUsageFlagBits.html
type CommandBufferUsageFlagBits int32

// CommandBufferUsageFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandBufferUsageFlagBits.html
const (
	CommandBufferUsageOneTimeSubmitBit      CommandBufferUsageFlagBits = 1
	CommandBufferUsageRenderPassContinueBit CommandBufferUsageFlagBits = 2
	CommandBufferUsageSimultaneousUseBit    CommandBufferUsageFlagBits = 4
	CommandBufferUsageFlagBitsMaxEnum       CommandBufferUsageFlagBits = 2147483647
)

// QueryControlFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueryControlFlagBits.html
type QueryControlFlagBits int32

// QueryControlFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueryControlFlagBits.html
const (
	QueryControlPreciseBit      QueryControlFlagBits = 1
	QueryControlFlagBitsMaxEnum QueryControlFlagBits = 2147483647
)

// CommandBufferResetFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandBufferResetFlagBits.html
type CommandBufferResetFlagBits int32

// CommandBufferResetFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandBufferResetFlagBits.html
const (
	CommandBufferResetReleaseResourcesBit CommandBufferResetFlagBits = 1
	CommandBufferResetFlagBitsMaxEnum     CommandBufferResetFlagBits = 2147483647
)

// StencilFaceFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkStencilFaceFlagBits.html
type StencilFaceFlagBits int32

// StencilFaceFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkStencilFaceFlagBits.html
const (
	StencilFaceFrontBit        StencilFaceFlagBits = 1
	StencilFaceBackBit         StencilFaceFlagBits = 2
	StencilFaceFrontAndBack    StencilFaceFlagBits = 3
	StencilFrontAndBack        StencilFaceFlagBits = 3
	StencilFaceFlagBitsMaxEnum StencilFaceFlagBits = 2147483647
)

// PointClippingBehavior as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPointClippingBehavior.html
type PointClippingBehavior int32

// PointClippingBehavior enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPointClippingBehavior.html
const (
	PointClippingBehaviorAllClipPlanes      PointClippingBehavior = iota
	PointClippingBehaviorUserClipPlanesOnly PointClippingBehavior = 1
	PointClippingBehaviorMaxEnum            PointClippingBehavior = 2147483647
)

// TessellationDomainOrigin as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkTessellationDomainOrigin.html
type TessellationDomainOrigin int32

// TessellationDomainOrigin enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkTessellationDomainOrigin.html
const (
	TessellationDomainOriginUpperLeft TessellationDomainOrigin = iota
	TessellationDomainOriginLowerLeft TessellationDomainOrigin = 1
	TessellationDomainOriginMaxEnum   TessellationDomainOrigin = 2147483647
)

// SamplerYcbcrModelConversion as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerYcbcrModelConversion.html
type SamplerYcbcrModelConversion int32

// SamplerYcbcrModelConversion enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerYcbcrModelConversion.html
const (
	SamplerYcbcrModelConversionRgbIdentity   SamplerYcbcrModelConversion = iota
	SamplerYcbcrModelConversionYcbcrIdentity SamplerYcbcrModelConversion = 1
	SamplerYcbcrModelConversionYcbcr709      SamplerYcbcrModelConversion = 2
	SamplerYcbcrModelConversionYcbcr601      SamplerYcbcrModelConversion = 3
	SamplerYcbcrModelConversionYcbcr2020     SamplerYcbcrModelConversion = 4
	SamplerYcbcrModelConversionMaxEnum       SamplerYcbcrModelConversion = 2147483647
)

// SamplerYcbcrRange as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerYcbcrRange.html
type SamplerYcbcrRange int32

// SamplerYcbcrRange enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerYcbcrRange.html
const (
	SamplerYcbcrRangeItuFull   SamplerYcbcrRange = iota
	SamplerYcbcrRangeItuNarrow SamplerYcbcrRange = 1
	SamplerYcbcrRangeMaxEnum   SamplerYcbcrRange = 2147483647
)

// ChromaLocation as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkChromaLocation.html
type ChromaLocation int32

// ChromaLocation enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkChromaLocation.html
const (
	ChromaLocationCositedEven ChromaLocation = iota
	ChromaLocationMidpoint    ChromaLocation = 1
	ChromaLocationMaxEnum     ChromaLocation = 2147483647
)

// DescriptorUpdateTemplateType as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorUpdateTemplateType.html
type DescriptorUpdateTemplateType int32

// DescriptorUpdateTemplateType enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorUpdateTemplateType.html
const (
	DescriptorUpdateTemplateTypeDescriptorSet   DescriptorUpdateTemplateType = iota
	DescriptorUpdateTemplateTypePushDescriptors DescriptorUpdateTemplateType = 1
	DescriptorUpdateTemplateTypeMaxEnum         DescriptorUpdateTemplateType = 2147483647
)

// SubgroupFeatureFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSubgroupFeatureFlagBits.html
type SubgroupFeatureFlagBits int32

// SubgroupFeatureFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSubgroupFeatureFlagBits.html
const (
	SubgroupFeatureBasicBit           SubgroupFeatureFlagBits = 1
	SubgroupFeatureVoteBit            SubgroupFeatureFlagBits = 2
	SubgroupFeatureArithmeticBit      SubgroupFeatureFlagBits = 4
	SubgroupFeatureBallotBit          SubgroupFeatureFlagBits = 8
	SubgroupFeatureShuffleBit         SubgroupFeatureFlagBits = 16
	SubgroupFeatureShuffleRelativeBit SubgroupFeatureFlagBits = 32
	SubgroupFeatureClusteredBit       SubgroupFeatureFlagBits = 64
	SubgroupFeatureQuadBit            SubgroupFeatureFlagBits = 128
	SubgroupFeaturePartitionedBitNv   SubgroupFeatureFlagBits = 256
	SubgroupFeatureFlagBitsMaxEnum    SubgroupFeatureFlagBits = 2147483647
)

// PeerMemoryFeatureFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPeerMemoryFeatureFlagBits.html
type PeerMemoryFeatureFlagBits int32

// PeerMemoryFeatureFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPeerMemoryFeatureFlagBits.html
const (
	PeerMemoryFeatureCopySrcBit      PeerMemoryFeatureFlagBits = 1
	PeerMemoryFeatureCopyDstBit      PeerMemoryFeatureFlagBits = 2
	PeerMemoryFeatureGenericSrcBit   PeerMemoryFeatureFlagBits = 4
	PeerMemoryFeatureGenericDstBit   PeerMemoryFeatureFlagBits = 8
	PeerMemoryFeatureFlagBitsMaxEnum PeerMemoryFeatureFlagBits = 2147483647
)

// MemoryAllocateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryAllocateFlagBits.html
type MemoryAllocateFlagBits int32

// MemoryAllocateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryAllocateFlagBits.html
const (
	MemoryAllocateDeviceMaskBit                 MemoryAllocateFlagBits = 1
	MemoryAllocateDeviceAddressBit              MemoryAllocateFlagBits = 2
	MemoryAllocateDeviceAddressCaptureReplayBit MemoryAllocateFlagBits = 4
	MemoryAllocateFlagBitsMaxEnum               MemoryAllocateFlagBits = 2147483647
)

// ExternalMemoryHandleTypeFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalMemoryHandleTypeFlagBits.html
type ExternalMemoryHandleTypeFlagBits int32

// ExternalMemoryHandleTypeFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalMemoryHandleTypeFlagBits.html
const (
	ExternalMemoryHandleTypeOpaqueFdBit                     ExternalMemoryHandleTypeFlagBits = 1
	ExternalMemoryHandleTypeOpaqueWin32Bit                  ExternalMemoryHandleTypeFlagBits = 2
	ExternalMemoryHandleTypeOpaqueWin32KmtBit               ExternalMemoryHandleTypeFlagBits = 4
	ExternalMemoryHandleTypeD3d11TextureBit                 ExternalMemoryHandleTypeFlagBits = 8
	ExternalMemoryHandleTypeD3d11TextureKmtBit              ExternalMemoryHandleTypeFlagBits = 16
	ExternalMemoryHandleTypeD3d12HeapBit                    ExternalMemoryHandleTypeFlagBits = 32
	ExternalMemoryHandleTypeD3d12ResourceBit                ExternalMemoryHandleTypeFlagBits = 64
	ExternalMemoryHandleTypeDmaBufBit                       ExternalMemoryHandleTypeFlagBits = 512
	ExternalMemoryHandleTypeAndroidHardwareBufferBitAndroid ExternalMemoryHandleTypeFlagBits = 1024
	ExternalMemoryHandleTypeHostAllocationBit               ExternalMemoryHandleTypeFlagBits = 128
	ExternalMemoryHandleTypeHostMappedForeignMemoryBit      ExternalMemoryHandleTypeFlagBits = 256
	ExternalMemoryHandleTypeZirconVmoBitFuchsia             ExternalMemoryHandleTypeFlagBits = 2048
	ExternalMemoryHandleTypeFlagBitsMaxEnum                 ExternalMemoryHandleTypeFlagBits = 2147483647
)

// ExternalMemoryFeatureFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalMemoryFeatureFlagBits.html
type ExternalMemoryFeatureFlagBits int32

// ExternalMemoryFeatureFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalMemoryFeatureFlagBits.html
const (
	ExternalMemoryFeatureDedicatedOnlyBit ExternalMemoryFeatureFlagBits = 1
	ExternalMemoryFeatureExportableBit    ExternalMemoryFeatureFlagBits = 2
	ExternalMemoryFeatureImportableBit    ExternalMemoryFeatureFlagBits = 4
	ExternalMemoryFeatureFlagBitsMaxEnum  ExternalMemoryFeatureFlagBits = 2147483647
)

// ExternalFenceHandleTypeFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalFenceHandleTypeFlagBits.html
type ExternalFenceHandleTypeFlagBits int32

// ExternalFenceHandleTypeFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalFenceHandleTypeFlagBits.html
const (
	ExternalFenceHandleTypeOpaqueFdBit       ExternalFenceHandleTypeFlagBits = 1
	ExternalFenceHandleTypeOpaqueWin32Bit    ExternalFenceHandleTypeFlagBits = 2
	ExternalFenceHandleTypeOpaqueWin32KmtBit ExternalFenceHandleTypeFlagBits = 4
	ExternalFenceHandleTypeSyncFdBit         ExternalFenceHandleTypeFlagBits = 8
	ExternalFenceHandleTypeFlagBitsMaxEnum   ExternalFenceHandleTypeFlagBits = 2147483647
)

// ExternalFenceFeatureFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalFenceFeatureFlagBits.html
type ExternalFenceFeatureFlagBits int32

// ExternalFenceFeatureFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalFenceFeatureFlagBits.html
const (
	ExternalFenceFeatureExportableBit   ExternalFenceFeatureFlagBits = 1
	ExternalFenceFeatureImportableBit   ExternalFenceFeatureFlagBits = 2
	ExternalFenceFeatureFlagBitsMaxEnum ExternalFenceFeatureFlagBits = 2147483647
)

// FenceImportFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFenceImportFlagBits.html
type FenceImportFlagBits int32

// FenceImportFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFenceImportFlagBits.html
const (
	FenceImportTemporaryBit    FenceImportFlagBits = 1
	FenceImportFlagBitsMaxEnum FenceImportFlagBits = 2147483647
)

// SemaphoreImportFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSemaphoreImportFlagBits.html
type SemaphoreImportFlagBits int32

// SemaphoreImportFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSemaphoreImportFlagBits.html
const (
	SemaphoreImportTemporaryBit    SemaphoreImportFlagBits = 1
	SemaphoreImportFlagBitsMaxEnum SemaphoreImportFlagBits = 2147483647
)

// ExternalSemaphoreHandleTypeFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalSemaphoreHandleTypeFlagBits.html
type ExternalSemaphoreHandleTypeFlagBits int32

// ExternalSemaphoreHandleTypeFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalSemaphoreHandleTypeFlagBits.html
const (
	ExternalSemaphoreHandleTypeOpaqueFdBit           ExternalSemaphoreHandleTypeFlagBits = 1
	ExternalSemaphoreHandleTypeOpaqueWin32Bit        ExternalSemaphoreHandleTypeFlagBits = 2
	ExternalSemaphoreHandleTypeOpaqueWin32KmtBit     ExternalSemaphoreHandleTypeFlagBits = 4
	ExternalSemaphoreHandleTypeD3d12FenceBit         ExternalSemaphoreHandleTypeFlagBits = 8
	ExternalSemaphoreHandleTypeSyncFdBit             ExternalSemaphoreHandleTypeFlagBits = 16
	ExternalSemaphoreHandleTypeZirconEventBitFuchsia ExternalSemaphoreHandleTypeFlagBits = 128
	ExternalSemaphoreHandleTypeD3d11FenceBit         ExternalSemaphoreHandleTypeFlagBits = 8
	ExternalSemaphoreHandleTypeFlagBitsMaxEnum       ExternalSemaphoreHandleTypeFlagBits = 2147483647
)

// ExternalSemaphoreFeatureFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalSemaphoreFeatureFlagBits.html
type ExternalSemaphoreFeatureFlagBits int32

// ExternalSemaphoreFeatureFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalSemaphoreFeatureFlagBits.html
const (
	ExternalSemaphoreFeatureExportableBit   ExternalSemaphoreFeatureFlagBits = 1
	ExternalSemaphoreFeatureImportableBit   ExternalSemaphoreFeatureFlagBits = 2
	ExternalSemaphoreFeatureFlagBitsMaxEnum ExternalSemaphoreFeatureFlagBits = 2147483647
)

// DriverId as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDriverId.html
type DriverId int32

// DriverId enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDriverId.html
const (
	DriverIdAmdProprietary          DriverId = 1
	DriverIdAmdOpenSource           DriverId = 2
	DriverIdMesaRadv                DriverId = 3
	DriverIdNvidiaProprietary       DriverId = 4
	DriverIdIntelProprietaryWindows DriverId = 5
	DriverIdIntelOpenSourceMesa     DriverId = 6
	DriverIdImaginationProprietary  DriverId = 7
	DriverIdQualcommProprietary     DriverId = 8
	DriverIdArmProprietary          DriverId = 9
	DriverIdGoogleSwiftshader       DriverId = 10
	DriverIdGgpProprietary          DriverId = 11
	DriverIdBroadcomProprietary     DriverId = 12
	DriverIdMesaLlvmpipe            DriverId = 13
	DriverIdMoltenvk                DriverId = 14
	DriverIdCoreaviProprietary      DriverId = 15
	DriverIdMaxEnum                 DriverId = 2147483647
)

// ShaderFloatControlsIndependence as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkShaderFloatControlsIndependence.html
type ShaderFloatControlsIndependence int32

// ShaderFloatControlsIndependence enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkShaderFloatControlsIndependence.html
const (
	ShaderFloatControlsIndependence32BitOnly ShaderFloatControlsIndependence = iota
	ShaderFloatControlsIndependenceAll       ShaderFloatControlsIndependence = 1
	ShaderFloatControlsIndependenceNone      ShaderFloatControlsIndependence = 2
	ShaderFloatControlsIndependenceMaxEnum   ShaderFloatControlsIndependence = 2147483647
)

// SamplerReductionMode as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerReductionMode.html
type SamplerReductionMode int32

// SamplerReductionMode enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerReductionMode.html
const (
	SamplerReductionModeWeightedAverage SamplerReductionMode = iota
	SamplerReductionModeMin             SamplerReductionMode = 1
	SamplerReductionModeMax             SamplerReductionMode = 2
	SamplerReductionModeMaxEnum         SamplerReductionMode = 2147483647
)

// SemaphoreType as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSemaphoreType.html
type SemaphoreType int32

// SemaphoreType enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSemaphoreType.html
const (
	SemaphoreTypeBinary   SemaphoreType = iota
	SemaphoreTypeTimeline SemaphoreType = 1
	SemaphoreTypeMaxEnum  SemaphoreType = 2147483647
)

// ResolveModeFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkResolveModeFlagBits.html
type ResolveModeFlagBits int32

// ResolveModeFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkResolveModeFlagBits.html
const (
	ResolveModeNone            ResolveModeFlagBits = iota
	ResolveModeSampleZeroBit   ResolveModeFlagBits = 1
	ResolveModeAverageBit      ResolveModeFlagBits = 2
	ResolveModeMinBit          ResolveModeFlagBits = 4
	ResolveModeMaxBit          ResolveModeFlagBits = 8
	ResolveModeFlagBitsMaxEnum ResolveModeFlagBits = 2147483647
)

// DescriptorBindingFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorBindingFlagBits.html
type DescriptorBindingFlagBits int32

// DescriptorBindingFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorBindingFlagBits.html
const (
	DescriptorBindingUpdateAfterBindBit          DescriptorBindingFlagBits = 1
	DescriptorBindingUpdateUnusedWhilePendingBit DescriptorBindingFlagBits = 2
	DescriptorBindingPartiallyBoundBit           DescriptorBindingFlagBits = 4
	DescriptorBindingVariableDescriptorCountBit  DescriptorBindingFlagBits = 8
	DescriptorBindingFlagBitsMaxEnum             DescriptorBindingFlagBits = 2147483647
)

// SemaphoreWaitFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSemaphoreWaitFlagBits.html
type SemaphoreWaitFlagBits int32

// SemaphoreWaitFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSemaphoreWaitFlagBits.html
const (
	SemaphoreWaitAnyBit          SemaphoreWaitFlagBits = 1
	SemaphoreWaitFlagBitsMaxEnum SemaphoreWaitFlagBits = 2147483647
)

// PresentMode as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPresentModeKHR
type PresentMode int32

// PresentMode enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPresentModeKHR
const (
	PresentModeImmediate               PresentMode = iota
	PresentModeMailbox                 PresentMode = 1
	PresentModeFifo                    PresentMode = 2
	PresentModeFifoRelaxed             PresentMode = 3
	PresentModeSharedDemandRefresh     PresentMode = 1000111000
	PresentModeSharedContinuousRefresh PresentMode = 1000111001
	PresentModeMaxEnum                 PresentMode = 2147483647
)

// ColorSpace as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkColorSpaceKHR
type ColorSpace int32

// ColorSpace enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkColorSpaceKHR
const (
	ColorSpaceSrgbNonlinear         ColorSpace = iota
	ColorSpaceDisplayP3Nonlinear    ColorSpace = 1000104001
	ColorSpaceExtendedSrgbLinear    ColorSpace = 1000104002
	ColorSpaceDisplayP3Linear       ColorSpace = 1000104003
	ColorSpaceDciP3Nonlinear        ColorSpace = 1000104004
	ColorSpaceBt709Linear           ColorSpace = 1000104005
	ColorSpaceBt709Nonlinear        ColorSpace = 1000104006
	ColorSpaceBt2020Linear          ColorSpace = 1000104007
	ColorSpaceHdr10St2084           ColorSpace = 1000104008
	ColorSpaceDolbyvision           ColorSpace = 1000104009
	ColorSpaceHdr10Hlg              ColorSpace = 1000104010
	ColorSpaceAdobergbLinear        ColorSpace = 1000104011
	ColorSpaceAdobergbNonlinear     ColorSpace = 1000104012
	ColorSpacePassThrough           ColorSpace = 1000104013
	ColorSpaceExtendedSrgbNonlinear ColorSpace = 1000104014
	ColorSpaceDisplayNativeAmd      ColorSpace = 1000213000
	ColorspaceSrgbNonlinear         ColorSpace = 0
	ColorSpaceDciP3Linear           ColorSpace = 1000104003
	ColorSpaceMaxEnum               ColorSpace = 2147483647
)

// SurfaceTransformFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSurfaceTransformFlagBitsKHR
type SurfaceTransformFlagBits int32

// SurfaceTransformFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSurfaceTransformFlagBitsKHR
const (
	SurfaceTransformIdentityBit                  SurfaceTransformFlagBits = 1
	SurfaceTransformRotate90Bit                  SurfaceTransformFlagBits = 2
	SurfaceTransformRotate180Bit                 SurfaceTransformFlagBits = 4
	SurfaceTransformRotate270Bit                 SurfaceTransformFlagBits = 8
	SurfaceTransformHorizontalMirrorBit          SurfaceTransformFlagBits = 16
	SurfaceTransformHorizontalMirrorRotate90Bit  SurfaceTransformFlagBits = 32
	SurfaceTransformHorizontalMirrorRotate180Bit SurfaceTransformFlagBits = 64
	SurfaceTransformHorizontalMirrorRotate270Bit SurfaceTransformFlagBits = 128
	SurfaceTransformInheritBit                   SurfaceTransformFlagBits = 256
	SurfaceTransformFlagBitsMaxEnum              SurfaceTransformFlagBits = 2147483647
)

// CompositeAlphaFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkCompositeAlphaFlagBitsKHR
type CompositeAlphaFlagBits int32

// CompositeAlphaFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkCompositeAlphaFlagBitsKHR
const (
	CompositeAlphaOpaqueBit         CompositeAlphaFlagBits = 1
	CompositeAlphaPreMultipliedBit  CompositeAlphaFlagBits = 2
	CompositeAlphaPostMultipliedBit CompositeAlphaFlagBits = 4
	CompositeAlphaInheritBit        CompositeAlphaFlagBits = 8
	CompositeAlphaFlagBitsMaxEnum   CompositeAlphaFlagBits = 2147483647
)

// SwapchainCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSwapchainCreateFlagBitsKHR
type SwapchainCreateFlagBits int32

// SwapchainCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSwapchainCreateFlagBitsKHR
const (
	SwapchainCreateSplitInstanceBindRegionsBit SwapchainCreateFlagBits = 1
	SwapchainCreateProtectedBit                SwapchainCreateFlagBits = 2
	SwapchainCreateMutableFormatBit            SwapchainCreateFlagBits = 4
	SwapchainCreateFlagBitsMaxEnum             SwapchainCreateFlagBits = 2147483647
)

// DeviceGroupPresentModeFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDeviceGroupPresentModeFlagBitsKHR
type DeviceGroupPresentModeFlagBits int32

// DeviceGroupPresentModeFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDeviceGroupPresentModeFlagBitsKHR
const (
	DeviceGroupPresentModeLocalBit            DeviceGroupPresentModeFlagBits = 1
	DeviceGroupPresentModeRemoteBit           DeviceGroupPresentModeFlagBits = 2
	DeviceGroupPresentModeSumBit              DeviceGroupPresentModeFlagBits = 4
	DeviceGroupPresentModeLocalMultiDeviceBit DeviceGroupPresentModeFlagBits = 8
	DeviceGroupPresentModeFlagBitsMaxEnum     DeviceGroupPresentModeFlagBits = 2147483647
)

// DisplayPlaneAlphaFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplayPlaneAlphaFlagBitsKHR
type DisplayPlaneAlphaFlagBits int32

// DisplayPlaneAlphaFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplayPlaneAlphaFlagBitsKHR
const (
	DisplayPlaneAlphaOpaqueBit                DisplayPlaneAlphaFlagBits = 1
	DisplayPlaneAlphaGlobalBit                DisplayPlaneAlphaFlagBits = 2
	DisplayPlaneAlphaPerPixelBit              DisplayPlaneAlphaFlagBits = 4
	DisplayPlaneAlphaPerPixelPremultipliedBit DisplayPlaneAlphaFlagBits = 8
	DisplayPlaneAlphaFlagBitsMaxEnum          DisplayPlaneAlphaFlagBits = 2147483647
)

// PerformanceCounterUnit as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPerformanceCounterUnitKHR
type PerformanceCounterUnit int32

// PerformanceCounterUnit enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPerformanceCounterUnitKHR
const (
	PerformanceCounterUnitGeneric        PerformanceCounterUnit = iota
	PerformanceCounterUnitPercentage     PerformanceCounterUnit = 1
	PerformanceCounterUnitNanoseconds    PerformanceCounterUnit = 2
	PerformanceCounterUnitBytes          PerformanceCounterUnit = 3
	PerformanceCounterUnitBytesPerSecond PerformanceCounterUnit = 4
	PerformanceCounterUnitKelvin         PerformanceCounterUnit = 5
	PerformanceCounterUnitWatts          PerformanceCounterUnit = 6
	PerformanceCounterUnitVolts          PerformanceCounterUnit = 7
	PerformanceCounterUnitAmps           PerformanceCounterUnit = 8
	PerformanceCounterUnitHertz          PerformanceCounterUnit = 9
	PerformanceCounterUnitCycles         PerformanceCounterUnit = 10
	PerformanceCounterUnitMaxEnum        PerformanceCounterUnit = 2147483647
)

// PerformanceCounterScope as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPerformanceCounterScopeKHR
type PerformanceCounterScope int32

// PerformanceCounterScope enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPerformanceCounterScopeKHR
const (
	PerformanceCounterScopeCommandBuffer PerformanceCounterScope = iota
	PerformanceCounterScopeRenderPass    PerformanceCounterScope = 1
	PerformanceCounterScopeCommand       PerformanceCounterScope = 2
	QueryScopeCommandBuffer              PerformanceCounterScope = 0
	QueryScopeRenderPass                 PerformanceCounterScope = 1
	QueryScopeCommand                    PerformanceCounterScope = 2
	PerformanceCounterScopeMaxEnum       PerformanceCounterScope = 2147483647
)

// PerformanceCounterStorage as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPerformanceCounterStorageKHR
type PerformanceCounterStorage int32

// PerformanceCounterStorage enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPerformanceCounterStorageKHR
const (
	PerformanceCounterStorageInt32   PerformanceCounterStorage = iota
	PerformanceCounterStorageInt64   PerformanceCounterStorage = 1
	PerformanceCounterStorageUint32  PerformanceCounterStorage = 2
	PerformanceCounterStorageUint64  PerformanceCounterStorage = 3
	PerformanceCounterStorageFloat32 PerformanceCounterStorage = 4
	PerformanceCounterStorageFloat64 PerformanceCounterStorage = 5
	PerformanceCounterStorageMaxEnum PerformanceCounterStorage = 2147483647
)

// PerformanceCounterDescriptionFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPerformanceCounterDescriptionFlagBitsKHR
type PerformanceCounterDescriptionFlagBits int32

// PerformanceCounterDescriptionFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPerformanceCounterDescriptionFlagBitsKHR
const (
	PerformanceCounterDescriptionPerformanceImpactingBit PerformanceCounterDescriptionFlagBits = 1
	PerformanceCounterDescriptionConcurrentlyImpactedBit PerformanceCounterDescriptionFlagBits = 2
	PerformanceCounterDescriptionPerformanceImpacting    PerformanceCounterDescriptionFlagBits = 1
	PerformanceCounterDescriptionConcurrentlyImpacted    PerformanceCounterDescriptionFlagBits = 2
	PerformanceCounterDescriptionFlagBitsMaxEnum         PerformanceCounterDescriptionFlagBits = 2147483647
)

// AcquireProfilingLockFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAcquireProfilingLockFlagBitsKHR
type AcquireProfilingLockFlagBits int32

// AcquireProfilingLockFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAcquireProfilingLockFlagBitsKHR
const (
	AcquireProfilingLockFlagBitsMaxEnum AcquireProfilingLockFlagBits = 2147483647
)

// FragmentShadingRateCombinerOp as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkFragmentShadingRateCombinerOpKHR
type FragmentShadingRateCombinerOp int32

// FragmentShadingRateCombinerOp enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkFragmentShadingRateCombinerOpKHR
const (
	FragmentShadingRateCombinerOpKeep    FragmentShadingRateCombinerOp = iota
	FragmentShadingRateCombinerOpReplace FragmentShadingRateCombinerOp = 1
	FragmentShadingRateCombinerOpMin     FragmentShadingRateCombinerOp = 2
	FragmentShadingRateCombinerOpMax     FragmentShadingRateCombinerOp = 3
	FragmentShadingRateCombinerOpMul     FragmentShadingRateCombinerOp = 4
	FragmentShadingRateCombinerOpMaxEnum FragmentShadingRateCombinerOp = 2147483647
)

// PipelineExecutableStatisticFormat as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPipelineExecutableStatisticFormatKHR
type PipelineExecutableStatisticFormat int32

// PipelineExecutableStatisticFormat enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPipelineExecutableStatisticFormatKHR
const (
	PipelineExecutableStatisticFormatBool32  PipelineExecutableStatisticFormat = iota
	PipelineExecutableStatisticFormatInt64   PipelineExecutableStatisticFormat = 1
	PipelineExecutableStatisticFormatUint64  PipelineExecutableStatisticFormat = 2
	PipelineExecutableStatisticFormatFloat64 PipelineExecutableStatisticFormat = 3
	PipelineExecutableStatisticFormatMaxEnum PipelineExecutableStatisticFormat = 2147483647
)

// SubmitFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSubmitFlagBitsKHR
type SubmitFlagBits int32

// SubmitFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSubmitFlagBitsKHR
const (
	SubmitProtectedBit    SubmitFlagBits = 1
	SubmitFlagBitsMaxEnum SubmitFlagBits = 2147483647
)

// DebugReportObjectType as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugReportObjectTypeEXT.html
type DebugReportObjectType int32

// DebugReportObjectType enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugReportObjectTypeEXT.html
const (
	DebugReportObjectTypeUnknown                     DebugReportObjectType = iota
	DebugReportObjectTypeInstance                    DebugReportObjectType = 1
	DebugReportObjectTypePhysicalDevice              DebugReportObjectType = 2
	DebugReportObjectTypeDevice                      DebugReportObjectType = 3
	DebugReportObjectTypeQueue                       DebugReportObjectType = 4
	DebugReportObjectTypeSemaphore                   DebugReportObjectType = 5
	DebugReportObjectTypeCommandBuffer               DebugReportObjectType = 6
	DebugReportObjectTypeFence                       DebugReportObjectType = 7
	DebugReportObjectTypeDeviceMemory                DebugReportObjectType = 8
	DebugReportObjectTypeBuffer                      DebugReportObjectType = 9
	DebugReportObjectTypeImage                       DebugReportObjectType = 10
	DebugReportObjectTypeEvent                       DebugReportObjectType = 11
	DebugReportObjectTypeQueryPool                   DebugReportObjectType = 12
	DebugReportObjectTypeBufferView                  DebugReportObjectType = 13
	DebugReportObjectTypeImageView                   DebugReportObjectType = 14
	DebugReportObjectTypeShaderModule                DebugReportObjectType = 15
	DebugReportObjectTypePipelineCache               DebugReportObjectType = 16
	DebugReportObjectTypePipelineLayout              DebugReportObjectType = 17
	DebugReportObjectTypeRenderPass                  DebugReportObjectType = 18
	DebugReportObjectTypePipeline                    DebugReportObjectType = 19
	DebugReportObjectTypeDescriptorSetLayout         DebugReportObjectType = 20
	DebugReportObjectTypeSampler                     DebugReportObjectType = 21
	DebugReportObjectTypeDescriptorPool              DebugReportObjectType = 22
	DebugReportObjectTypeDescriptorSet               DebugReportObjectType = 23
	DebugReportObjectTypeFramebuffer                 DebugReportObjectType = 24
	DebugReportObjectTypeCommandPool                 DebugReportObjectType = 25
	DebugReportObjectTypeSurfaceKhr                  DebugReportObjectType = 26
	DebugReportObjectTypeSwapchainKhr                DebugReportObjectType = 27
	DebugReportObjectTypeDebugReportCallbackExt      DebugReportObjectType = 28
	DebugReportObjectTypeDisplayKhr                  DebugReportObjectType = 29
	DebugReportObjectTypeDisplayModeKhr              DebugReportObjectType = 30
	DebugReportObjectTypeValidationCacheExt          DebugReportObjectType = 33
	DebugReportObjectTypeSamplerYcbcrConversion      DebugReportObjectType = 1000156000
	DebugReportObjectTypeDescriptorUpdateTemplate    DebugReportObjectType = 1000085000
	DebugReportObjectTypeAccelerationStructureKhr    DebugReportObjectType = 1000150000
	DebugReportObjectTypeAccelerationStructureNv     DebugReportObjectType = 1000165000
	DebugReportObjectTypeDebugReport                 DebugReportObjectType = 28
	DebugReportObjectTypeValidationCache             DebugReportObjectType = 33
	DebugReportObjectTypeDescriptorUpdateTemplateKhr DebugReportObjectType = 1000085000
	DebugReportObjectTypeSamplerYcbcrConversionKhr   DebugReportObjectType = 1000156000
	DebugReportObjectTypeMaxEnum                     DebugReportObjectType = 2147483647
)

// DebugReportFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugReportFlagBitsEXT.html
type DebugReportFlagBits int32

// DebugReportFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugReportFlagBitsEXT.html
const (
	DebugReportInformationBit        DebugReportFlagBits = 1
	DebugReportWarningBit            DebugReportFlagBits = 2
	DebugReportPerformanceWarningBit DebugReportFlagBits = 4
	DebugReportErrorBit              DebugReportFlagBits = 8
	DebugReportDebugBit              DebugReportFlagBits = 16
	DebugReportFlagBitsMaxEnum       DebugReportFlagBits = 2147483647
)

// RasterizationOrderAMD as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkRasterizationOrderAMD
type RasterizationOrderAMD int32

// RasterizationOrderAMD enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkRasterizationOrderAMD
const (
	RasterizationOrderStrictAmd  RasterizationOrderAMD = iota
	RasterizationOrderRelaxedAmd RasterizationOrderAMD = 1
	RasterizationOrderMaxEnumAmd RasterizationOrderAMD = 2147483647
)

// ShaderInfoTypeAMD as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkShaderInfoTypeAMD
type ShaderInfoTypeAMD int32

// ShaderInfoTypeAMD enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkShaderInfoTypeAMD
const (
	ShaderInfoTypeStatisticsAmd  ShaderInfoTypeAMD = iota
	ShaderInfoTypeBinaryAmd      ShaderInfoTypeAMD = 1
	ShaderInfoTypeDisassemblyAmd ShaderInfoTypeAMD = 2
	ShaderInfoTypeMaxEnumAmd     ShaderInfoTypeAMD = 2147483647
)

// ExternalMemoryHandleTypeFlagBitsNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html
type ExternalMemoryHandleTypeFlagBitsNV int32

// ExternalMemoryHandleTypeFlagBitsNV enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalMemoryHandleTypeFlagBitsNV.html
const (
	ExternalMemoryHandleTypeOpaqueWin32BitNv    ExternalMemoryHandleTypeFlagBitsNV = 1
	ExternalMemoryHandleTypeOpaqueWin32KmtBitNv ExternalMemoryHandleTypeFlagBitsNV = 2
	ExternalMemoryHandleTypeD3d11ImageBitNv     ExternalMemoryHandleTypeFlagBitsNV = 4
	ExternalMemoryHandleTypeD3d11ImageKmtBitNv  ExternalMemoryHandleTypeFlagBitsNV = 8
	ExternalMemoryHandleTypeFlagBitsMaxEnumNv   ExternalMemoryHandleTypeFlagBitsNV = 2147483647
)

// ExternalMemoryFeatureFlagBitsNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalMemoryFeatureFlagBitsNV.html
type ExternalMemoryFeatureFlagBitsNV int32

// ExternalMemoryFeatureFlagBitsNV enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalMemoryFeatureFlagBitsNV.html
const (
	ExternalMemoryFeatureDedicatedOnlyBitNv ExternalMemoryFeatureFlagBitsNV = 1
	ExternalMemoryFeatureExportableBitNv    ExternalMemoryFeatureFlagBitsNV = 2
	ExternalMemoryFeatureImportableBitNv    ExternalMemoryFeatureFlagBitsNV = 4
	ExternalMemoryFeatureFlagBitsMaxEnumNv  ExternalMemoryFeatureFlagBitsNV = 2147483647
)

// ValidationCheck as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkValidationCheckEXT.html
type ValidationCheck int32

// ValidationCheck enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkValidationCheckEXT.html
const (
	ValidationCheckAll     ValidationCheck = iota
	ValidationCheckShaders ValidationCheck = 1
	ValidationCheckMaxEnum ValidationCheck = 2147483647
)

// ConditionalRenderingFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkConditionalRenderingFlagBitsEXT.html
type ConditionalRenderingFlagBits int32

// ConditionalRenderingFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkConditionalRenderingFlagBitsEXT.html
const (
	ConditionalRenderingInvertedBit     ConditionalRenderingFlagBits = 1
	ConditionalRenderingFlagBitsMaxEnum ConditionalRenderingFlagBits = 2147483647
)

// SurfaceCounterFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSurfaceCounterFlagBitsEXT.html
type SurfaceCounterFlagBits int32

// SurfaceCounterFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSurfaceCounterFlagBitsEXT.html
const (
	SurfaceCounterVblankBit       SurfaceCounterFlagBits = 1
	SurfaceCounterVblank          SurfaceCounterFlagBits = 1
	SurfaceCounterFlagBitsMaxEnum SurfaceCounterFlagBits = 2147483647
)

// DisplayPowerState as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDisplayPowerStateEXT.html
type DisplayPowerState int32

// DisplayPowerState enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDisplayPowerStateEXT.html
const (
	DisplayPowerStateOff     DisplayPowerState = iota
	DisplayPowerStateSuspend DisplayPowerState = 1
	DisplayPowerStateOn      DisplayPowerState = 2
	DisplayPowerStateMaxEnum DisplayPowerState = 2147483647
)

// DeviceEventType as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceEventTypeEXT.html
type DeviceEventType int32

// DeviceEventType enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceEventTypeEXT.html
const (
	DeviceEventTypeDisplayHotplug DeviceEventType = iota
	DeviceEventTypeMaxEnum        DeviceEventType = 2147483647
)

// DisplayEventType as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDisplayEventTypeEXT.html
type DisplayEventType int32

// DisplayEventType enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDisplayEventTypeEXT.html
const (
	DisplayEventTypeFirstPixelOut DisplayEventType = iota
	DisplayEventTypeMaxEnum       DisplayEventType = 2147483647
)

// ViewportCoordinateSwizzleNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkViewportCoordinateSwizzleNV.html
type ViewportCoordinateSwizzleNV int32

// ViewportCoordinateSwizzleNV enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkViewportCoordinateSwizzleNV.html
const (
	ViewportCoordinateSwizzlePositiveXNv ViewportCoordinateSwizzleNV = iota
	ViewportCoordinateSwizzleNegativeXNv ViewportCoordinateSwizzleNV = 1
	ViewportCoordinateSwizzlePositiveYNv ViewportCoordinateSwizzleNV = 2
	ViewportCoordinateSwizzleNegativeYNv ViewportCoordinateSwizzleNV = 3
	ViewportCoordinateSwizzlePositiveZNv ViewportCoordinateSwizzleNV = 4
	ViewportCoordinateSwizzleNegativeZNv ViewportCoordinateSwizzleNV = 5
	ViewportCoordinateSwizzlePositiveWNv ViewportCoordinateSwizzleNV = 6
	ViewportCoordinateSwizzleNegativeWNv ViewportCoordinateSwizzleNV = 7
	ViewportCoordinateSwizzleMaxEnumNv   ViewportCoordinateSwizzleNV = 2147483647
)

// DiscardRectangleMode as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDiscardRectangleModeEXT.html
type DiscardRectangleMode int32

// DiscardRectangleMode enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDiscardRectangleModeEXT.html
const (
	DiscardRectangleModeInclusive DiscardRectangleMode = iota
	DiscardRectangleModeExclusive DiscardRectangleMode = 1
	DiscardRectangleModeMaxEnum   DiscardRectangleMode = 2147483647
)

// ConservativeRasterizationMode as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkConservativeRasterizationModeEXT.html
type ConservativeRasterizationMode int32

// ConservativeRasterizationMode enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkConservativeRasterizationModeEXT.html
const (
	ConservativeRasterizationModeDisabled      ConservativeRasterizationMode = iota
	ConservativeRasterizationModeOverestimate  ConservativeRasterizationMode = 1
	ConservativeRasterizationModeUnderestimate ConservativeRasterizationMode = 2
	ConservativeRasterizationModeMaxEnum       ConservativeRasterizationMode = 2147483647
)

// DebugUtilsMessageSeverityFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html
type DebugUtilsMessageSeverityFlagBits int32

// DebugUtilsMessageSeverityFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugUtilsMessageSeverityFlagBitsEXT.html
const (
	DebugUtilsMessageSeverityVerboseBit      DebugUtilsMessageSeverityFlagBits = 1
	DebugUtilsMessageSeverityInfoBit         DebugUtilsMessageSeverityFlagBits = 16
	DebugUtilsMessageSeverityWarningBit      DebugUtilsMessageSeverityFlagBits = 256
	DebugUtilsMessageSeverityErrorBit        DebugUtilsMessageSeverityFlagBits = 4096
	DebugUtilsMessageSeverityFlagBitsMaxEnum DebugUtilsMessageSeverityFlagBits = 2147483647
)

// DebugUtilsMessageTypeFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugUtilsMessageTypeFlagBitsEXT.html
type DebugUtilsMessageTypeFlagBits int32

// DebugUtilsMessageTypeFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugUtilsMessageTypeFlagBitsEXT.html
const (
	DebugUtilsMessageTypeGeneralBit      DebugUtilsMessageTypeFlagBits = 1
	DebugUtilsMessageTypeValidationBit   DebugUtilsMessageTypeFlagBits = 2
	DebugUtilsMessageTypePerformanceBit  DebugUtilsMessageTypeFlagBits = 4
	DebugUtilsMessageTypeFlagBitsMaxEnum DebugUtilsMessageTypeFlagBits = 2147483647
)

// BlendOverlap as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBlendOverlapEXT.html
type BlendOverlap int32

// BlendOverlap enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBlendOverlapEXT.html
const (
	BlendOverlapUncorrelated BlendOverlap = iota
	BlendOverlapDisjoint     BlendOverlap = 1
	BlendOverlapConjoint     BlendOverlap = 2
	BlendOverlapMaxEnum      BlendOverlap = 2147483647
)

// CoverageModulationModeNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCoverageModulationModeNV.html
type CoverageModulationModeNV int32

// CoverageModulationModeNV enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCoverageModulationModeNV.html
const (
	CoverageModulationModeNoneNv    CoverageModulationModeNV = iota
	CoverageModulationModeRgbNv     CoverageModulationModeNV = 1
	CoverageModulationModeAlphaNv   CoverageModulationModeNV = 2
	CoverageModulationModeRgbaNv    CoverageModulationModeNV = 3
	CoverageModulationModeMaxEnumNv CoverageModulationModeNV = 2147483647
)

// ValidationCacheHeaderVersion as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkValidationCacheHeaderVersionEXT.html
type ValidationCacheHeaderVersion int32

// ValidationCacheHeaderVersion enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkValidationCacheHeaderVersionEXT.html
const (
	ValidationCacheHeaderVersionOne     ValidationCacheHeaderVersion = 1
	ValidationCacheHeaderVersionMaxEnum ValidationCacheHeaderVersion = 2147483647
)

// ShadingRatePaletteEntryNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkShadingRatePaletteEntryNV.html
type ShadingRatePaletteEntryNV int32

// ShadingRatePaletteEntryNV enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkShadingRatePaletteEntryNV.html
const (
	ShadingRatePaletteEntryNoInvocationsNv           ShadingRatePaletteEntryNV = iota
	ShadingRatePaletteEntry16InvocationsPerPixelNv   ShadingRatePaletteEntryNV = 1
	ShadingRatePaletteEntry8InvocationsPerPixelNv    ShadingRatePaletteEntryNV = 2
	ShadingRatePaletteEntry4InvocationsPerPixelNv    ShadingRatePaletteEntryNV = 3
	ShadingRatePaletteEntry2InvocationsPerPixelNv    ShadingRatePaletteEntryNV = 4
	ShadingRatePaletteEntry1InvocationPerPixelNv     ShadingRatePaletteEntryNV = 5
	ShadingRatePaletteEntry1InvocationPer2x1PixelsNv ShadingRatePaletteEntryNV = 6
	ShadingRatePaletteEntry1InvocationPer1x2PixelsNv ShadingRatePaletteEntryNV = 7
	ShadingRatePaletteEntry1InvocationPer2x2PixelsNv ShadingRatePaletteEntryNV = 8
	ShadingRatePaletteEntry1InvocationPer4x2PixelsNv ShadingRatePaletteEntryNV = 9
	ShadingRatePaletteEntry1InvocationPer2x4PixelsNv ShadingRatePaletteEntryNV = 10
	ShadingRatePaletteEntry1InvocationPer4x4PixelsNv ShadingRatePaletteEntryNV = 11
	ShadingRatePaletteEntryMaxEnumNv                 ShadingRatePaletteEntryNV = 2147483647
)

// CoarseSampleOrderTypeNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCoarseSampleOrderTypeNV.html
type CoarseSampleOrderTypeNV int32

// CoarseSampleOrderTypeNV enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCoarseSampleOrderTypeNV.html
const (
	CoarseSampleOrderTypeDefaultNv     CoarseSampleOrderTypeNV = iota
	CoarseSampleOrderTypeCustomNv      CoarseSampleOrderTypeNV = 1
	CoarseSampleOrderTypePixelMajorNv  CoarseSampleOrderTypeNV = 2
	CoarseSampleOrderTypeSampleMajorNv CoarseSampleOrderTypeNV = 3
	CoarseSampleOrderTypeMaxEnumNv     CoarseSampleOrderTypeNV = 2147483647
)

// RayTracingShaderGroupType as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkRayTracingShaderGroupTypeKHR
type RayTracingShaderGroupType int32

// RayTracingShaderGroupType enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkRayTracingShaderGroupTypeKHR
const (
	RayTracingShaderGroupTypeGeneral              RayTracingShaderGroupType = iota
	RayTracingShaderGroupTypeTrianglesHitGroup    RayTracingShaderGroupType = 1
	RayTracingShaderGroupTypeProceduralHitGroup   RayTracingShaderGroupType = 2
	RayTracingShaderGroupTypeGeneralNv            RayTracingShaderGroupType = 0
	RayTracingShaderGroupTypeTrianglesHitGroupNv  RayTracingShaderGroupType = 1
	RayTracingShaderGroupTypeProceduralHitGroupNv RayTracingShaderGroupType = 2
	RayTracingShaderGroupTypeMaxEnum              RayTracingShaderGroupType = 2147483647
)

// GeometryType as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkGeometryTypeKHR
type GeometryType int32

// GeometryType enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkGeometryTypeKHR
const (
	GeometryTypeTriangles   GeometryType = iota
	GeometryTypeAabbs       GeometryType = 1
	GeometryTypeInstances   GeometryType = 2
	GeometryTypeTrianglesNv GeometryType = 0
	GeometryTypeAabbsNv     GeometryType = 1
	GeometryTypeMaxEnum     GeometryType = 2147483647
)

// AccelerationStructureType as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureTypeKHR
type AccelerationStructureType int32

// AccelerationStructureType enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureTypeKHR
const (
	AccelerationStructureTypeTopLevel      AccelerationStructureType = iota
	AccelerationStructureTypeBottomLevel   AccelerationStructureType = 1
	AccelerationStructureTypeGeneric       AccelerationStructureType = 2
	AccelerationStructureTypeTopLevelNv    AccelerationStructureType = 0
	AccelerationStructureTypeBottomLevelNv AccelerationStructureType = 1
	AccelerationStructureTypeMaxEnum       AccelerationStructureType = 2147483647
)

// CopyAccelerationStructureMode as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkCopyAccelerationStructureModeKHR
type CopyAccelerationStructureMode int32

// CopyAccelerationStructureMode enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkCopyAccelerationStructureModeKHR
const (
	CopyAccelerationStructureModeClone       CopyAccelerationStructureMode = iota
	CopyAccelerationStructureModeCompact     CopyAccelerationStructureMode = 1
	CopyAccelerationStructureModeSerialize   CopyAccelerationStructureMode = 2
	CopyAccelerationStructureModeDeserialize CopyAccelerationStructureMode = 3
	CopyAccelerationStructureModeCloneNv     CopyAccelerationStructureMode = 0
	CopyAccelerationStructureModeCompactNv   CopyAccelerationStructureMode = 1
	CopyAccelerationStructureModeMaxEnum     CopyAccelerationStructureMode = 2147483647
)

// AccelerationStructureMemoryRequirementsTypeNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAccelerationStructureMemoryRequirementsTypeNV.html
type AccelerationStructureMemoryRequirementsTypeNV int32

// AccelerationStructureMemoryRequirementsTypeNV enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAccelerationStructureMemoryRequirementsTypeNV.html
const (
	AccelerationStructureMemoryRequirementsTypeObjectNv        AccelerationStructureMemoryRequirementsTypeNV = iota
	AccelerationStructureMemoryRequirementsTypeBuildScratchNv  AccelerationStructureMemoryRequirementsTypeNV = 1
	AccelerationStructureMemoryRequirementsTypeUpdateScratchNv AccelerationStructureMemoryRequirementsTypeNV = 2
	AccelerationStructureMemoryRequirementsTypeMaxEnumNv       AccelerationStructureMemoryRequirementsTypeNV = 2147483647
)

// GeometryFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkGeometryFlagBitsKHR
type GeometryFlagBits int32

// GeometryFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkGeometryFlagBitsKHR
const (
	GeometryOpaqueBit                        GeometryFlagBits = 1
	GeometryNoDuplicateAnyHitInvocationBit   GeometryFlagBits = 2
	GeometryOpaqueBitNv                      GeometryFlagBits = 1
	GeometryNoDuplicateAnyHitInvocationBitNv GeometryFlagBits = 2
	GeometryFlagBitsMaxEnum                  GeometryFlagBits = 2147483647
)

// GeometryInstanceFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkGeometryInstanceFlagBitsKHR
type GeometryInstanceFlagBits int32

// GeometryInstanceFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkGeometryInstanceFlagBitsKHR
const (
	GeometryInstanceTriangleFacingCullDisableBit       GeometryInstanceFlagBits = 1
	GeometryInstanceTriangleFrontCounterclockwiseBit   GeometryInstanceFlagBits = 2
	GeometryInstanceForceOpaqueBit                     GeometryInstanceFlagBits = 4
	GeometryInstanceForceNoOpaqueBit                   GeometryInstanceFlagBits = 8
	GeometryInstanceTriangleCullDisableBitNv           GeometryInstanceFlagBits = 1
	GeometryInstanceTriangleFrontCounterclockwiseBitNv GeometryInstanceFlagBits = 2
	GeometryInstanceForceOpaqueBitNv                   GeometryInstanceFlagBits = 4
	GeometryInstanceForceNoOpaqueBitNv                 GeometryInstanceFlagBits = 8
	GeometryInstanceFlagBitsMaxEnum                    GeometryInstanceFlagBits = 2147483647
)

// BuildAccelerationStructureFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkBuildAccelerationStructureFlagBitsKHR
type BuildAccelerationStructureFlagBits int32

// BuildAccelerationStructureFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkBuildAccelerationStructureFlagBitsKHR
const (
	BuildAccelerationStructureAllowUpdateBit       BuildAccelerationStructureFlagBits = 1
	BuildAccelerationStructureAllowCompactionBit   BuildAccelerationStructureFlagBits = 2
	BuildAccelerationStructurePreferFastTraceBit   BuildAccelerationStructureFlagBits = 4
	BuildAccelerationStructurePreferFastBuildBit   BuildAccelerationStructureFlagBits = 8
	BuildAccelerationStructureLowMemoryBit         BuildAccelerationStructureFlagBits = 16
	BuildAccelerationStructureAllowUpdateBitNv     BuildAccelerationStructureFlagBits = 1
	BuildAccelerationStructureAllowCompactionBitNv BuildAccelerationStructureFlagBits = 2
	BuildAccelerationStructurePreferFastTraceBitNv BuildAccelerationStructureFlagBits = 4
	BuildAccelerationStructurePreferFastBuildBitNv BuildAccelerationStructureFlagBits = 8
	BuildAccelerationStructureLowMemoryBitNv       BuildAccelerationStructureFlagBits = 16
	BuildAccelerationStructureFlagBitsMaxEnum      BuildAccelerationStructureFlagBits = 2147483647
)

// QueueGlobalPriority as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueueGlobalPriorityEXT.html
type QueueGlobalPriority int32

// QueueGlobalPriority enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueueGlobalPriorityEXT.html
const (
	QueueGlobalPriorityLow      QueueGlobalPriority = 128
	QueueGlobalPriorityMedium   QueueGlobalPriority = 256
	QueueGlobalPriorityHigh     QueueGlobalPriority = 512
	QueueGlobalPriorityRealtime QueueGlobalPriority = 1024
	QueueGlobalPriorityMaxEnum  QueueGlobalPriority = 2147483647
)

// PipelineCompilerControlFlagBitsAMD as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkPipelineCompilerControlFlagBitsAMD
type PipelineCompilerControlFlagBitsAMD int32

// PipelineCompilerControlFlagBitsAMD enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkPipelineCompilerControlFlagBitsAMD
const (
	PipelineCompilerControlFlagBitsMaxEnumAmd PipelineCompilerControlFlagBitsAMD = 2147483647
)

// TimeDomain as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkTimeDomainEXT.html
type TimeDomain int32

// TimeDomain enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkTimeDomainEXT.html
const (
	TimeDomainDevice                  TimeDomain = iota
	TimeDomainClockMonotonic          TimeDomain = 1
	TimeDomainClockMonotonicRaw       TimeDomain = 2
	TimeDomainQueryPerformanceCounter TimeDomain = 3
	TimeDomainMaxEnum                 TimeDomain = 2147483647
)

// MemoryOverallocationBehaviorAMD as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkMemoryOverallocationBehaviorAMD
type MemoryOverallocationBehaviorAMD int32

// MemoryOverallocationBehaviorAMD enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkMemoryOverallocationBehaviorAMD
const (
	MemoryOverallocationBehaviorDefaultAmd    MemoryOverallocationBehaviorAMD = iota
	MemoryOverallocationBehaviorAllowedAmd    MemoryOverallocationBehaviorAMD = 1
	MemoryOverallocationBehaviorDisallowedAmd MemoryOverallocationBehaviorAMD = 2
	MemoryOverallocationBehaviorMaxEnumAmd    MemoryOverallocationBehaviorAMD = 2147483647
)

// PipelineCreationFeedbackFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCreationFeedbackFlagBitsEXT.html
type PipelineCreationFeedbackFlagBits int32

// PipelineCreationFeedbackFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCreationFeedbackFlagBitsEXT.html
const (
	PipelineCreationFeedbackValidBit                       PipelineCreationFeedbackFlagBits = 1
	PipelineCreationFeedbackApplicationPipelineCacheHitBit PipelineCreationFeedbackFlagBits = 2
	PipelineCreationFeedbackBasePipelineAccelerationBit    PipelineCreationFeedbackFlagBits = 4
	PipelineCreationFeedbackFlagBitsMaxEnum                PipelineCreationFeedbackFlagBits = 2147483647
)

// PerformanceConfigurationTypeINTEL as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPerformanceConfigurationTypeINTEL.html
type PerformanceConfigurationTypeINTEL int32

// PerformanceConfigurationTypeINTEL enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPerformanceConfigurationTypeINTEL.html
const (
	PerformanceConfigurationTypeCommandQueueMetricsDiscoveryActivatedIntel PerformanceConfigurationTypeINTEL = iota
	PerformanceConfigurationTypeMaxEnumIntel                               PerformanceConfigurationTypeINTEL = 2147483647
)

// QueryPoolSamplingModeINTEL as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueryPoolSamplingModeINTEL.html
type QueryPoolSamplingModeINTEL int32

// QueryPoolSamplingModeINTEL enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueryPoolSamplingModeINTEL.html
const (
	QueryPoolSamplingModeManualIntel  QueryPoolSamplingModeINTEL = iota
	QueryPoolSamplingModeMaxEnumIntel QueryPoolSamplingModeINTEL = 2147483647
)

// PerformanceOverrideTypeINTEL as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPerformanceOverrideTypeINTEL.html
type PerformanceOverrideTypeINTEL int32

// PerformanceOverrideTypeINTEL enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPerformanceOverrideTypeINTEL.html
const (
	PerformanceOverrideTypeNullHardwareIntel   PerformanceOverrideTypeINTEL = iota
	PerformanceOverrideTypeFlushGpuCachesIntel PerformanceOverrideTypeINTEL = 1
	PerformanceOverrideTypeMaxEnumIntel        PerformanceOverrideTypeINTEL = 2147483647
)

// PerformanceParameterTypeINTEL as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPerformanceParameterTypeINTEL.html
type PerformanceParameterTypeINTEL int32

// PerformanceParameterTypeINTEL enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPerformanceParameterTypeINTEL.html
const (
	PerformanceParameterTypeHwCountersSupportedIntel   PerformanceParameterTypeINTEL = iota
	PerformanceParameterTypeStreamMarkerValidBitsIntel PerformanceParameterTypeINTEL = 1
	PerformanceParameterTypeMaxEnumIntel               PerformanceParameterTypeINTEL = 2147483647
)

// PerformanceValueTypeINTEL as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPerformanceValueTypeINTEL.html
type PerformanceValueTypeINTEL int32

// PerformanceValueTypeINTEL enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPerformanceValueTypeINTEL.html
const (
	PerformanceValueTypeUint32Intel  PerformanceValueTypeINTEL = iota
	PerformanceValueTypeUint64Intel  PerformanceValueTypeINTEL = 1
	PerformanceValueTypeFloatIntel   PerformanceValueTypeINTEL = 2
	PerformanceValueTypeBoolIntel    PerformanceValueTypeINTEL = 3
	PerformanceValueTypeStringIntel  PerformanceValueTypeINTEL = 4
	PerformanceValueTypeMaxEnumIntel PerformanceValueTypeINTEL = 2147483647
)

// ShaderCorePropertiesFlagBitsAMD as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkShaderCorePropertiesFlagBitsAMD
type ShaderCorePropertiesFlagBitsAMD int32

// ShaderCorePropertiesFlagBitsAMD enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkShaderCorePropertiesFlagBitsAMD
const (
	ShaderCorePropertiesFlagBitsMaxEnumAmd ShaderCorePropertiesFlagBitsAMD = 2147483647
)

// ToolPurposeFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkToolPurposeFlagBitsEXT.html
type ToolPurposeFlagBits int32

// ToolPurposeFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkToolPurposeFlagBitsEXT.html
const (
	ToolPurposeValidationBit         ToolPurposeFlagBits = 1
	ToolPurposeProfilingBit          ToolPurposeFlagBits = 2
	ToolPurposeTracingBit            ToolPurposeFlagBits = 4
	ToolPurposeAdditionalFeaturesBit ToolPurposeFlagBits = 8
	ToolPurposeModifyingFeaturesBit  ToolPurposeFlagBits = 16
	ToolPurposeDebugReportingBit     ToolPurposeFlagBits = 32
	ToolPurposeDebugMarkersBit       ToolPurposeFlagBits = 64
	ToolPurposeFlagBitsMaxEnum       ToolPurposeFlagBits = 2147483647
)

// ValidationFeatureEnable as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkValidationFeatureEnableEXT.html
type ValidationFeatureEnable int32

// ValidationFeatureEnable enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkValidationFeatureEnableEXT.html
const (
	ValidationFeatureEnableGpuAssisted                   ValidationFeatureEnable = iota
	ValidationFeatureEnableGpuAssistedReserveBindingSlot ValidationFeatureEnable = 1
	ValidationFeatureEnableBestPractices                 ValidationFeatureEnable = 2
	ValidationFeatureEnableDebugPrintf                   ValidationFeatureEnable = 3
	ValidationFeatureEnableSynchronizationValidation     ValidationFeatureEnable = 4
	ValidationFeatureEnableMaxEnum                       ValidationFeatureEnable = 2147483647
)

// ValidationFeatureDisable as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkValidationFeatureDisableEXT.html
type ValidationFeatureDisable int32

// ValidationFeatureDisable enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkValidationFeatureDisableEXT.html
const (
	ValidationFeatureDisableAll             ValidationFeatureDisable = iota
	ValidationFeatureDisableShaders         ValidationFeatureDisable = 1
	ValidationFeatureDisableThreadSafety    ValidationFeatureDisable = 2
	ValidationFeatureDisableApiParameters   ValidationFeatureDisable = 3
	ValidationFeatureDisableObjectLifetimes ValidationFeatureDisable = 4
	ValidationFeatureDisableCoreChecks      ValidationFeatureDisable = 5
	ValidationFeatureDisableUniqueHandles   ValidationFeatureDisable = 6
	ValidationFeatureDisableMaxEnum         ValidationFeatureDisable = 2147483647
)

// ComponentTypeNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkComponentTypeNV.html
type ComponentTypeNV int32

// ComponentTypeNV enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkComponentTypeNV.html
const (
	ComponentTypeFloat16Nv ComponentTypeNV = iota
	ComponentTypeFloat32Nv ComponentTypeNV = 1
	ComponentTypeFloat64Nv ComponentTypeNV = 2
	ComponentTypeSint8Nv   ComponentTypeNV = 3
	ComponentTypeSint16Nv  ComponentTypeNV = 4
	ComponentTypeSint32Nv  ComponentTypeNV = 5
	ComponentTypeSint64Nv  ComponentTypeNV = 6
	ComponentTypeUint8Nv   ComponentTypeNV = 7
	ComponentTypeUint16Nv  ComponentTypeNV = 8
	ComponentTypeUint32Nv  ComponentTypeNV = 9
	ComponentTypeUint64Nv  ComponentTypeNV = 10
	ComponentTypeMaxEnumNv ComponentTypeNV = 2147483647
)

// ScopeNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkScopeNV.html
type ScopeNV int32

// ScopeNV enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkScopeNV.html
const (
	ScopeDeviceNv      ScopeNV = 1
	ScopeWorkgroupNv   ScopeNV = 2
	ScopeSubgroupNv    ScopeNV = 3
	ScopeQueueFamilyNv ScopeNV = 5
	ScopeMaxEnumNv     ScopeNV = 2147483647
)

// CoverageReductionModeNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCoverageReductionModeNV.html
type CoverageReductionModeNV int32

// CoverageReductionModeNV enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCoverageReductionModeNV.html
const (
	CoverageReductionModeMergeNv    CoverageReductionModeNV = iota
	CoverageReductionModeTruncateNv CoverageReductionModeNV = 1
	CoverageReductionModeMaxEnumNv  CoverageReductionModeNV = 2147483647
)

// ProvokingVertexMode as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkProvokingVertexModeEXT.html
type ProvokingVertexMode int32

// ProvokingVertexMode enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkProvokingVertexModeEXT.html
const (
	ProvokingVertexModeFirstVertex ProvokingVertexMode = iota
	ProvokingVertexModeLastVertex  ProvokingVertexMode = 1
	ProvokingVertexModeMaxEnum     ProvokingVertexMode = 2147483647
)

// LineRasterizationMode as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkLineRasterizationModeEXT.html
type LineRasterizationMode int32

// LineRasterizationMode enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkLineRasterizationModeEXT.html
const (
	LineRasterizationModeDefault           LineRasterizationMode = iota
	LineRasterizationModeRectangular       LineRasterizationMode = 1
	LineRasterizationModeBresenham         LineRasterizationMode = 2
	LineRasterizationModeRectangularSmooth LineRasterizationMode = 3
	LineRasterizationModeMaxEnum           LineRasterizationMode = 2147483647
)

// IndirectCommandsTokenTypeNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkIndirectCommandsTokenTypeNV.html
type IndirectCommandsTokenTypeNV int32

// IndirectCommandsTokenTypeNV enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkIndirectCommandsTokenTypeNV.html
const (
	IndirectCommandsTokenTypeShaderGroupNv  IndirectCommandsTokenTypeNV = iota
	IndirectCommandsTokenTypeStateFlagsNv   IndirectCommandsTokenTypeNV = 1
	IndirectCommandsTokenTypeIndexBufferNv  IndirectCommandsTokenTypeNV = 2
	IndirectCommandsTokenTypeVertexBufferNv IndirectCommandsTokenTypeNV = 3
	IndirectCommandsTokenTypePushConstantNv IndirectCommandsTokenTypeNV = 4
	IndirectCommandsTokenTypeDrawIndexedNv  IndirectCommandsTokenTypeNV = 5
	IndirectCommandsTokenTypeDrawNv         IndirectCommandsTokenTypeNV = 6
	IndirectCommandsTokenTypeDrawTasksNv    IndirectCommandsTokenTypeNV = 7
	IndirectCommandsTokenTypeMaxEnumNv      IndirectCommandsTokenTypeNV = 2147483647
)

// IndirectStateFlagBitsNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkIndirectStateFlagBitsNV.html
type IndirectStateFlagBitsNV int32

// IndirectStateFlagBitsNV enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkIndirectStateFlagBitsNV.html
const (
	IndirectStateFlagFrontfaceBitNv IndirectStateFlagBitsNV = 1
	IndirectStateFlagBitsMaxEnumNv  IndirectStateFlagBitsNV = 2147483647
)

// IndirectCommandsLayoutUsageFlagBitsNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkIndirectCommandsLayoutUsageFlagBitsNV.html
type IndirectCommandsLayoutUsageFlagBitsNV int32

// IndirectCommandsLayoutUsageFlagBitsNV enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkIndirectCommandsLayoutUsageFlagBitsNV.html
const (
	IndirectCommandsLayoutUsageExplicitPreprocessBitNv IndirectCommandsLayoutUsageFlagBitsNV = 1
	IndirectCommandsLayoutUsageIndexedSequencesBitNv   IndirectCommandsLayoutUsageFlagBitsNV = 2
	IndirectCommandsLayoutUsageUnorderedSequencesBitNv IndirectCommandsLayoutUsageFlagBitsNV = 4
	IndirectCommandsLayoutUsageFlagBitsMaxEnumNv       IndirectCommandsLayoutUsageFlagBitsNV = 2147483647
)

// DeviceMemoryReportEventType as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceMemoryReportEventTypeEXT.html
type DeviceMemoryReportEventType int32

// DeviceMemoryReportEventType enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceMemoryReportEventTypeEXT.html
const (
	DeviceMemoryReportEventTypeAllocate         DeviceMemoryReportEventType = iota
	DeviceMemoryReportEventTypeFree             DeviceMemoryReportEventType = 1
	DeviceMemoryReportEventTypeImport           DeviceMemoryReportEventType = 2
	DeviceMemoryReportEventTypeUnimport         DeviceMemoryReportEventType = 3
	DeviceMemoryReportEventTypeAllocationFailed DeviceMemoryReportEventType = 4
	DeviceMemoryReportEventTypeMaxEnum          DeviceMemoryReportEventType = 2147483647
)

// PrivateDataSlotCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPrivateDataSlotCreateFlagBitsEXT.html
type PrivateDataSlotCreateFlagBits int32

// PrivateDataSlotCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPrivateDataSlotCreateFlagBitsEXT.html
const (
	PrivateDataSlotCreateFlagBitsMaxEnum PrivateDataSlotCreateFlagBits = 2147483647
)

// DeviceDiagnosticsConfigFlagBitsNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceDiagnosticsConfigFlagBitsNV.html
type DeviceDiagnosticsConfigFlagBitsNV int32

// DeviceDiagnosticsConfigFlagBitsNV enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceDiagnosticsConfigFlagBitsNV.html
const (
	DeviceDiagnosticsConfigEnableShaderDebugInfoBitNv      DeviceDiagnosticsConfigFlagBitsNV = 1
	DeviceDiagnosticsConfigEnableResourceTrackingBitNv     DeviceDiagnosticsConfigFlagBitsNV = 2
	DeviceDiagnosticsConfigEnableAutomaticCheckpointsBitNv DeviceDiagnosticsConfigFlagBitsNV = 4
	DeviceDiagnosticsConfigFlagBitsMaxEnumNv               DeviceDiagnosticsConfigFlagBitsNV = 2147483647
)

// FragmentShadingRateTypeNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFragmentShadingRateTypeNV.html
type FragmentShadingRateTypeNV int32

// FragmentShadingRateTypeNV enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFragmentShadingRateTypeNV.html
const (
	FragmentShadingRateTypeFragmentSizeNv FragmentShadingRateTypeNV = iota
	FragmentShadingRateTypeEnumsNv        FragmentShadingRateTypeNV = 1
	FragmentShadingRateTypeMaxEnumNv      FragmentShadingRateTypeNV = 2147483647
)

// FragmentShadingRateNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFragmentShadingRateNV.html
type FragmentShadingRateNV int32

// FragmentShadingRateNV enumeration from https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFragmentShadingRateNV.html
const (
	FragmentShadingRate1InvocationPerPixelNv     FragmentShadingRateNV = iota
	FragmentShadingRate1InvocationPer1x2PixelsNv FragmentShadingRateNV = 1
	FragmentShadingRate1InvocationPer2x1PixelsNv FragmentShadingRateNV = 4
	FragmentShadingRate1InvocationPer2x2PixelsNv FragmentShadingRateNV = 5
	FragmentShadingRate1InvocationPer2x4PixelsNv FragmentShadingRateNV = 6
	FragmentShadingRate1InvocationPer4x2PixelsNv FragmentShadingRateNV = 9
	FragmentShadingRate1InvocationPer4x4PixelsNv FragmentShadingRateNV = 10
	FragmentShadingRate2InvocationsPerPixelNv    FragmentShadingRateNV = 11
	FragmentShadingRate4InvocationsPerPixelNv    FragmentShadingRateNV = 12
	FragmentShadingRate8InvocationsPerPixelNv    FragmentShadingRateNV = 13
	FragmentShadingRate16InvocationsPerPixelNv   FragmentShadingRateNV = 14
	FragmentShadingRateNoInvocationsNv           FragmentShadingRateNV = 15
	FragmentShadingRateMaxEnumNv                 FragmentShadingRateNV = 2147483647
)

// BuildAccelerationStructureMode as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkBuildAccelerationStructureModeKHR
type BuildAccelerationStructureMode int32

// BuildAccelerationStructureMode enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkBuildAccelerationStructureModeKHR
const (
	BuildAccelerationStructureModeBuild   BuildAccelerationStructureMode = iota
	BuildAccelerationStructureModeUpdate  BuildAccelerationStructureMode = 1
	BuildAccelerationStructureModeMaxEnum BuildAccelerationStructureMode = 2147483647
)

// AccelerationStructureBuildType as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureBuildTypeKHR
type AccelerationStructureBuildType int32

// AccelerationStructureBuildType enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureBuildTypeKHR
const (
	AccelerationStructureBuildTypeHost         AccelerationStructureBuildType = iota
	AccelerationStructureBuildTypeDevice       AccelerationStructureBuildType = 1
	AccelerationStructureBuildTypeHostOrDevice AccelerationStructureBuildType = 2
	AccelerationStructureBuildTypeMaxEnum      AccelerationStructureBuildType = 2147483647
)

// AccelerationStructureCompatibility as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureCompatibilityKHR
type AccelerationStructureCompatibility int32

// AccelerationStructureCompatibility enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureCompatibilityKHR
const (
	AccelerationStructureCompatibilityCompatible   AccelerationStructureCompatibility = iota
	AccelerationStructureCompatibilityIncompatible AccelerationStructureCompatibility = 1
	AccelerationStructureCompatibilityMaxEnum      AccelerationStructureCompatibility = 2147483647
)

// AccelerationStructureCreateFlagBits as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureCreateFlagBitsKHR
type AccelerationStructureCreateFlagBits int32

// AccelerationStructureCreateFlagBits enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureCreateFlagBitsKHR
const (
	AccelerationStructureCreateDeviceAddressCaptureReplayBit AccelerationStructureCreateFlagBits = 1
	AccelerationStructureCreateFlagBitsMaxEnum               AccelerationStructureCreateFlagBits = 2147483647
)

// ShaderGroupShader as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkShaderGroupShaderKHR
type ShaderGroupShader int32

// ShaderGroupShader enumeration from https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkShaderGroupShaderKHR
const (
	ShaderGroupShaderGeneral      ShaderGroupShader = iota
	ShaderGroupShaderClosestHit   ShaderGroupShader = 1
	ShaderGroupShaderAnyHit       ShaderGroupShader = 2
	ShaderGroupShaderIntersection ShaderGroupShader = 3
	ShaderGroupShaderMaxEnum      ShaderGroupShader = 2147483647
)
