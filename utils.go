package go_vulkan

import (
	"bytes"
	"fmt"
	"strings"
	"unsafe"
	"reflect"
)

// Max bounds of uint32 and uint64,
// declared as var so type would get checked.
var (
	MaxUint32 uint32 = 1<<32 - 1 // also ^uint32(0)
	MaxUint64 uint64 = 1<<64 - 1 // also ^uint64(0)
	bZero            = string([]byte{0})
)

func (b Bool32) B() bool {
	return b == Bool32(True)
}

type Version uint32

func (v Version) String() string {
	return fmt.Sprintf("%d.%d.%d", v.Major(), v.Minor(), v.Patch())
}

func (v Version) Major() int {
	return int(uint32(v) >> 22)
}

func (v Version) Minor() int {
	return int(uint32(v) >> 12 & 0x3FF)
}

func (v Version) Patch() int {
	return int(uint32(v) & 0xFFF)
}

func MakeVersion(major, minor, patch int) uint32 {
	return uint32(major)<<22 | uint32(minor)<<12 | uint32(patch)
}

func ToString(buf []byte) string {
	var str bytes.Buffer
	for i := range buf {
		if buf[i] == '\x00' {
			return str.String()
		}
		str.WriteByte(buf[i])
	}
	return str.String()
}

func TrimZeros(in string) string {
	return strings.Trim(in, bZero)
}

// comment

// // deprecated
// func FindMemoryTypeIndex(dev PhysicalDevice,
// 	typeBits uint32, reqMask MemoryPropertyFlagBits) (uint32, bool) {
//
// 	var memProperties PhysicalDeviceMemoryProperties
// 	GetPhysicalDeviceMemoryProperties(dev, &memProperties)
// 	memProperties.Deref()
//
// 	var memFlags = MemoryPropertyFlags(reqMask)
//
// 	// search memtypes to find the first index with those requirements
// 	for i := 0; i < 32; i++ {
// 		if typeBits&1 == 1 { // type is available
// 			memType := memProperties.MemoryTypes[i]
// 			memType.Deref()
// 			if memType.PropertyFlags&memFlags == memFlags {
// 				return uint32(i), true
// 			}
// 		}
// 		typeBits = typeBits >> 1
// 	}
// 	return 0, false
// }

// Memcopy is like a Go's built-in copy function, it copies data from src slice,
// but into a destination pointer. Useful to copy data into device memory.
func Memcopy(dst unsafe.Pointer, src []byte) int {
	const m = 0x7fffffff
	dstView := (*[m]byte)(dst)
	return copy(dstView[:len(src)], src)
}

func MemNcopy(dst unsafe.Pointer, src unsafe.Pointer, n int) int {
	var srcView []byte
	var dstView []byte
	bh := (*reflect.SliceHeader)(unsafe.Pointer(&srcView))
	bh.Data = uintptr(src)
	bh.Len = n
	bh.Cap = n
	bh = (*reflect.SliceHeader)(unsafe.Pointer(&dstView))
	bh.Data = uintptr(dst)
	bh.Len = n
	bh.Cap = n
	return copy(dstView, srcView)
}

// Memview returns a pointer to user data, so Vulkan runtime in userspace can peek.
func Memview(data []byte) unsafe.Pointer {
	return unsafe.Pointer((*sliceHeader)(unsafe.Pointer(&data)).Data)
}

func NewClearValueFloats(color [4]float32) ClearValue {
	var v ClearValue
	v.SetColorFloats(color)
	return v
}

func NewClearValueInts(color [4]int32) ClearValue {
	var v ClearValue
	v.SetColorInts(color)
	return v
}

func NewClearValueUInts(color [4]uint32) ClearValue {
	var v ClearValue
	v.SetColorUints(color)
	return v
}

func NewClearDepthStencil(depth float32, stencil uint32) ClearValue {
	var v ClearValue
	v.SetDepthStencil(ClearDepthStencilValue{
		Depth:   depth,
		Stencil: stencil,
	})
	return v
}

// SurfaceFromPointer casts a pointer to a Vulkan surface into a Surface.
// func SurfaceFromPointer(surface uintptr) Surface {
// 	return *(*Surface)(unsafe.Pointer(surface))
// }
