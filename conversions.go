package go_vulkan

import (
	"unsafe"
)

// #include <stdlib.h>
import "C"

type sliceHeader struct {
	Data unsafe.Pointer
	Len  int
	Cap  int
}

func ToBool32(in bool) C.uint {
	if in {
		return (C.uint)(True)
	}
	return (C.uint)(False)
}
