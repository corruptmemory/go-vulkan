package go_vulkan

/*
#cgo CFLAGS: -I. -DVK_NO_PROTOTYPES
#include <xcb/xcb.h>
#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>
#include "vulkan-includes/vulkan.h"
#include "vulkan-includes/vulkan_xcb.h"
#include "vulkan-includes/vulkan_xlib.h"
#include "vulkan-includes/vulkan_xlib_xrandr.h"
#include "vulkan-includes/vulkan_wayland.h"
#include <stdlib.h>
#include "vk_wrapper.h"
#include "vk_bridge.h"
static void write_bits(VkAccelerationStructureInstanceKHR *in,
                       uint32_t instanceCustomIndex,
                       uint32_t mask,
                       uint32_t instanceShaderBindingTableRecordOffset,
                       VkGeometryInstanceFlagsKHR flags)
{
	in->instanceCustomIndex = instanceCustomIndex;
	in->mask = mask;
	in->instanceShaderBindingTableRecordOffset = instanceShaderBindingTableRecordOffset;
	in->flags = flags;
}


static uint32_t get_instanceCustomIndex(VkAccelerationStructureInstanceKHR *in) {
	return in->instanceCustomIndex;
}

static uint32_t get_mask(VkAccelerationStructureInstanceKHR *in) {
	return in->mask;
}

static uint32_t get_instanceShaderBindingTableRecordOffset(VkAccelerationStructureInstanceKHR *in) {
	return in->instanceShaderBindingTableRecordOffset;
}

static uint32_t get_flags(VkAccelerationStructureInstanceKHR *in) {
	return in->flags;
}

static void vkClearColorValue_set_floats(VkClearColorValue *target, float *floats) {
	for (size_t i = 0; i < 4; i++) {
		target->float32[i] = floats[i];
	}
}

static void vkClearColorValue_set_ints(VkClearColorValue *target, int32_t *ints) {
	for (size_t i = 0; i < 4; i++) {
		target->int32[i] = ints[i];
	}
}

static void vkClearColorValue_set_uints(VkClearColorValue *target, uint32_t *uints) {
	for (size_t i = 0; i < 4; i++) {
		target->uint32[i] = uints[i];
	}
}

static void vkClearValue_write_color_floats(VkClearValue *target, float *floats) {
	vkClearColorValue_set_floats(&target->color, floats);
}

static void vkClearValue_write_color_ints(VkClearValue *target, int32_t *ints) {
	vkClearColorValue_set_ints(&target->color, ints);
}

static void vkClearValue_write_color_uints(VkClearValue *target, uint32_t *uints) {
	vkClearColorValue_set_uints(&target->color, uints);
}

static void vkClearValue_write_depth_stencil(VkClearValue *target, VkClearDepthStencilValue *ds) {
	target->depthStencil.depth = ds->depth;
	target->depthStencil.stencil = ds->stencil;
}

static VkSurfaceKHR *raw_pointer_to_VkSurfaceKHR(uint64_t in) {
	return (VkSurfaceKHR*)in;
}

*/
import "C"
import (
	"unsafe"

	"github.com/google/uuid"
)

// This is here to silence go vet
func RawPointerToSurface(in uintptr) Surface {
	return Surface(*C.raw_pointer_to_VkSurfaceKHR(C.uint64_t(in)))
}

// bool type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/Vkbool.html
type Bool32 uint32

// DeviceAddress type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceAddress.html
type DeviceAddress uint32

// DeviceSize type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceSize.html
type DeviceSize uint32

// Flags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFlags.html
type Flags uint32

// SampleMask type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSampleMask.html
type SampleMask uint32

// Buffer as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBuffer.html
type Buffer C.VkBuffer

// Image as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImage.html
type Image C.VkImage

// Instance as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkInstance.html
type Instance C.VkInstance

// PhysicalDevice as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDevice.html
type PhysicalDevice C.VkPhysicalDevice

// Device as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDevice.html
type Device C.VkDevice

// Queue as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueue.html
type Queue C.VkQueue

// Semaphore as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSemaphore.html
type Semaphore C.VkSemaphore

// CommandBuffer as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandBuffer.html
type CommandBuffer C.VkCommandBuffer

// Fence as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFence.html
type Fence C.VkFence

// DeviceMemory as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceMemory.html
type DeviceMemory C.VkDeviceMemory

// Event as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkEvent.html
type Event C.VkEvent

// QueryPool as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueryPool.html
type QueryPool C.VkQueryPool

// BufferView as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBufferView.html
type BufferView C.VkBufferView

// ImageView as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageView.html
type ImageView C.VkImageView

// ShaderModule as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkShaderModule.html
type ShaderModule C.VkShaderModule

// PipelineCache as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCache.html
type PipelineCache C.VkPipelineCache

// PipelineLayout as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineLayout.html
type PipelineLayout C.VkPipelineLayout

// Pipeline as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipeline.html
type Pipeline C.VkPipeline

// RenderPass as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkRenderPass.html
type RenderPass C.VkRenderPass

// DescriptorSetLayout as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorSetLayout.html
type DescriptorSetLayout C.VkDescriptorSetLayout

// Sampler as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSampler.html
type Sampler C.VkSampler

// DescriptorSet as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorSet.html
type DescriptorSet C.VkDescriptorSet

// DescriptorPool as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorPool.html
type DescriptorPool C.VkDescriptorPool

// Framebuffer as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFramebuffer.html
type Framebuffer C.VkFramebuffer

// CommandPool as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandPool.html
type CommandPool C.VkCommandPool

// AccessFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAccessFlags.html
type AccessFlags uint32

// ImageAspectFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageAspectFlags.html
type ImageAspectFlags uint32

// FormatFeatureFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFormatFeatureFlags.html
type FormatFeatureFlags uint32

// ImageCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageCreateFlags.html
type ImageCreateFlags uint32

// SampleCountFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSampleCountFlags.html
type SampleCountFlags uint32

// ImageUsageFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageUsageFlags.html
type ImageUsageFlags uint32

// InstanceCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkInstanceCreateFlags.html
type InstanceCreateFlags uint32

// MemoryHeapFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryHeapFlags.html
type MemoryHeapFlags uint32

// MemoryPropertyFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryPropertyFlags.html
type MemoryPropertyFlags uint32

// QueueFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueueFlags.html
type QueueFlags uint32

// DeviceCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceCreateFlags.html
type DeviceCreateFlags uint32

// DeviceQueueCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceQueueCreateFlags.html
type DeviceQueueCreateFlags uint32

// PipelineStageFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineStageFlags.html
type PipelineStageFlags uint32

// MemoryMapFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryMapFlags.html
type MemoryMapFlags uint32

// SparseMemoryBindFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSparseMemoryBindFlags.html
type SparseMemoryBindFlags uint32

// SparseImageFormatFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSparseImageFormatFlags.html
type SparseImageFormatFlags uint32

// FenceCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFenceCreateFlags.html
type FenceCreateFlags uint32

// SemaphoreCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSemaphoreCreateFlags.html
type SemaphoreCreateFlags uint32

// EventCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkEventCreateFlags.html
type EventCreateFlags uint32

// QueryPipelineStatisticFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueryPipelineStatisticFlags.html
type QueryPipelineStatisticFlags uint32

// QueryPoolCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueryPoolCreateFlags.html
type QueryPoolCreateFlags uint32

// QueryResultFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueryResultFlags.html
type QueryResultFlags uint32

// BufferCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBufferCreateFlags.html
type BufferCreateFlags uint32

// BufferUsageFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBufferUsageFlags.html
type BufferUsageFlags uint32

// BufferViewCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBufferViewCreateFlags.html
type BufferViewCreateFlags uint32

// ImageViewCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageViewCreateFlags.html
type ImageViewCreateFlags uint32

// ShaderModuleCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkShaderModuleCreateFlags.html
type ShaderModuleCreateFlags uint32

// PipelineCacheCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCacheCreateFlags.html
type PipelineCacheCreateFlags uint32

// ColorComponentFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkColorComponentFlags.html
type ColorComponentFlags uint32

// PipelineCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCreateFlags.html
type PipelineCreateFlags uint32

// PipelineShaderStageCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineShaderStageCreateFlags.html
type PipelineShaderStageCreateFlags uint32

// CullModeFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCullModeFlags.html
type CullModeFlags uint32

// PipelineVertexInputStateCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineVertexInputStateCreateFlags.html
type PipelineVertexInputStateCreateFlags uint32

// PipelineInputAssemblyStateCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineInputAssemblyStateCreateFlags.html
type PipelineInputAssemblyStateCreateFlags uint32

// PipelineTessellationStateCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineTessellationStateCreateFlags.html
type PipelineTessellationStateCreateFlags uint32

// PipelineViewportStateCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineViewportStateCreateFlags.html
type PipelineViewportStateCreateFlags uint32

// PipelineRasterizationStateCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineRasterizationStateCreateFlags.html
type PipelineRasterizationStateCreateFlags uint32

// PipelineMultisampleStateCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineMultisampleStateCreateFlags.html
type PipelineMultisampleStateCreateFlags uint32

// PipelineDepthStencilStateCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineDepthStencilStateCreateFlags.html
type PipelineDepthStencilStateCreateFlags uint32

// PipelineColorBlendStateCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineColorBlendStateCreateFlags.html
type PipelineColorBlendStateCreateFlags uint32

// PipelineDynamicStateCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineDynamicStateCreateFlags.html
type PipelineDynamicStateCreateFlags uint32

// PipelineLayoutCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineLayoutCreateFlags.html
type PipelineLayoutCreateFlags uint32

// ShaderStageFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkShaderStageFlags.html
type ShaderStageFlags uint32

// SamplerCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerCreateFlags.html
type SamplerCreateFlags uint32

// DescriptorPoolCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorPoolCreateFlags.html
type DescriptorPoolCreateFlags uint32

// DescriptorPoolResetFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorPoolResetFlags.html
type DescriptorPoolResetFlags uint32

// DescriptorSetLayoutCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorSetLayoutCreateFlags.html
type DescriptorSetLayoutCreateFlags uint32

// AttachmentDescriptionFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAttachmentDescriptionFlags.html
type AttachmentDescriptionFlags uint32

// DependencyFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDependencyFlags.html
type DependencyFlags uint32

// FramebufferCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFramebufferCreateFlags.html
type FramebufferCreateFlags uint32

// RenderPassCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkRenderPassCreateFlags.html
type RenderPassCreateFlags uint32

// SubpassDescriptionFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSubpassDescriptionFlags.html
type SubpassDescriptionFlags uint32

// CommandPoolCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandPoolCreateFlags.html
type CommandPoolCreateFlags uint32

// CommandPoolResetFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandPoolResetFlags.html
type CommandPoolResetFlags uint32

// CommandBufferUsageFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandBufferUsageFlags.html
type CommandBufferUsageFlags uint32

// QueryControlFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueryControlFlags.html
type QueryControlFlags uint32

// CommandBufferResetFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandBufferResetFlags.html
type CommandBufferResetFlags uint32

// StencilFaceFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkStencilFaceFlags.html
type StencilFaceFlags uint32

type AccelerationStructureTypeNV AccelerationStructureType

var noOpFree = func() {}

// Extent2D as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExtent2D.html
type Extent2D struct {
	Width  uint32
	Height uint32
}

func (e *Extent2D) From(in *C.VkExtent2D) {
	e.Width = uint32(in.width)
	e.Height = uint32(in.height)
}

func (e *Extent2D) To(mem VulkanClientMemory, target *C.VkExtent2D) {
	target.width = C.uint(e.Width)
	target.height = C.uint(e.Height)
}

// Extent3D as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExtent3D.html
type Extent3D struct {
	Width  uint32
	Height uint32
	Depth  uint32
}

func (e *Extent3D) From(in *C.VkExtent3D) {
	e.Width = uint32(in.width)
	e.Height = uint32(in.height)
	e.Depth = uint32(in.depth)
}

// Offset2D as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkOffset2D.html
type Offset2D struct {
	X int32
	Y int32
}

func (o *Offset2D) To(mem VulkanClientMemory, target *C.VkOffset2D) {
	target.x = C.int(o.X)
	target.y = C.int(o.Y)
}

// Offset3D as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkOffset3D.html
type Offset3D struct {
	X int32
	Y int32
	Z int32
}

func (o *Offset3D) To(mem VulkanClientMemory, target *C.VkOffset3D) {
	target.x = C.int32_t(o.X)
	target.y = C.int32_t(o.Y)
	target.z = C.int32_t(o.Z)
}

// Rect2D as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkRect2D.html
type Rect2D struct {
	Offset Offset2D
	Extent Extent2D
}

func (r *Rect2D) To(mem VulkanClientMemory, target *C.VkRect2D) {
	r.Offset.To(mem, &target.offset)
	r.Extent.To(mem, &target.extent)
}

// BaseInStructure as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBaseInStructure.html
type BaseInStructure struct {
	// SType StructureType
	PNext []BaseInStructure
}

// BaseOutStructure as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBaseOutStructure.html
type BaseOutStructure struct {
	// SType StructureType
	PNext []BaseOutStructure
}

// BufferMemoryBarrier as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBufferMemoryBarrier.html
type BufferMemoryBarrier struct {
	// SType               StructureType
	Next                unsafe.Pointer
	SrcAccessMask       AccessFlags
	DstAccessMask       AccessFlags
	SrcQueueFamilyIndex uint32
	DstQueueFamilyIndex uint32
	Buffer              Buffer
	Offset              DeviceSize
	Size                DeviceSize
}

// DispatchIndirectCommand as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDispatchIndirectCommand.html
type DispatchIndirectCommand struct {
	X uint32
	Y uint32
	Z uint32
}

// DrawIndexedIndirectCommand as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDrawIndexedIndirectCommand.html
type DrawIndexedIndirectCommand struct {
	IndexCount    uint32
	InstanceCount uint32
	FirstIndex    uint32
	VertexOffset  int32
	FirstInstance uint32
}

// AccelerationStructureInstance as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureInstanceKHR
type AccelerationStructureInstance struct {
	Transform                              TransformMatrix
	InstanceCustomIndex                    uint32
	Mask                                   uint32
	InstanceShaderBindingTableRecordOffset uint32
	Flags                                  GeometryInstanceFlags
	AccelerationStructureReference         uint32
}

type AccelerationStructureInstanceNV AccelerationStructureInstance

// DrawIndirectCommand as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDrawIndirectCommand.html
type DrawIndirectCommand struct {
	VertexCount   uint32
	InstanceCount uint32
	FirstVertex   uint32
	FirstInstance uint32
}

// ImageSubresourceRange as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageSubresourceRange.html
type ImageSubresourceRange struct {
	AspectMask     ImageAspectFlags
	BaseMipLevel   uint32
	LevelCount     uint32
	BaseArrayLayer uint32
	LayerCount     uint32
}

func (i *ImageSubresourceRange) To(mem VulkanClientMemory, target *C.VkImageSubresourceRange) {
	target.aspectMask = C.VkImageAspectFlags(i.AspectMask)
	target.baseMipLevel = C.uint32_t(i.BaseMipLevel)
	target.levelCount = C.uint32_t(i.LevelCount)
	target.baseArrayLayer = C.uint32_t(i.BaseArrayLayer)
	target.layerCount = C.uint32_t(i.LayerCount)
}

// ImageMemoryBarrier as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageMemoryBarrier.html
type ImageMemoryBarrier struct {
	// SType               StructureType
	Next                unsafe.Pointer
	SrcAccessMask       AccessFlags
	DstAccessMask       AccessFlags
	OldLayout           ImageLayout
	NewLayout           ImageLayout
	SrcQueueFamilyIndex uint32
	DstQueueFamilyIndex uint32
	Image               Image
	SubresourceRange    ImageSubresourceRange
}

// MemoryBarrier as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryBarrier.html
type MemoryBarrier struct {
	// SType         StructureType
	Next          unsafe.Pointer
	SrcAccessMask AccessFlags
	DstAccessMask AccessFlags
}

// AllocationCallbacks as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAllocationCallbacks.html
type AllocationCallbacks C.VkAllocationCallbacks

// ApplicationInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkApplicationInfo.html
type ApplicationInfo struct {
	Next               unsafe.Pointer
	ApplicationName    string
	ApplicationVersion uint32
	EngineName         string
	EngineVersion      uint32
	ApiVersion         uint32
}

func (i *ApplicationInfo) To(mem VulkanClientMemory, target *C.VkApplicationInfo) {
	target.sType = (C.VkStructureType)(StructureTypeApplicationInfo)
	target.pNext = i.Next
	if i.ApplicationName != "" {
		target.pApplicationName = mem.CString(i.ApplicationName)
	}
	target.applicationVersion = (C.uint)(i.ApplicationVersion)
	if i.EngineName != "" {
		target.pEngineName = mem.CString(i.EngineName)
	}
	target.engineVersion = (C.uint)(i.EngineVersion)
	target.apiVersion = (C.uint)(i.ApiVersion)
}

// FormatProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFormatProperties.html
type FormatProperties struct {
	LinearTilingFeatures  FormatFeatureFlags
	OptimalTilingFeatures FormatFeatureFlags
	BufferFeatures        FormatFeatureFlags
}

// ImageFormatProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageFormatProperties.html
type ImageFormatProperties struct {
	MaxExtent       Extent3D
	MaxMipLevels    uint32
	MaxArrayLayers  uint32
	SampleCounts    SampleCountFlags
	MaxResourceSize DeviceSize
}

// InstanceCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkInstanceCreateInfo.html
type InstanceCreateInfo struct {
	Next                  unsafe.Pointer
	Flags                 InstanceCreateFlags
	ApplicationInfo       *ApplicationInfo
	EnabledLayerNames     []string
	EnabledExtensionNames []string
}

func (i *InstanceCreateInfo) To(mem VulkanClientMemory, target *C.VkInstanceCreateInfo) {
	target.sType = (C.VkStructureType)(StructureTypeInstanceCreateInfo)
	target.pNext = i.Next
	target.flags = (C.uint)(i.Flags)
	if i.ApplicationInfo != nil {
		appInfo := (*C.VkApplicationInfo)(mem.Calloc(1, C.sizeof_VkApplicationInfo))
		i.ApplicationInfo.To(mem, appInfo)
		target.pApplicationInfo = appInfo
	}
	if len(i.EnabledLayerNames) > 0 {
		target.enabledLayerCount = (C.uint)(len(i.EnabledLayerNames))
		n := mem.CStrings(i.EnabledLayerNames)
		target.ppEnabledLayerNames = n
	}
	if len(i.EnabledExtensionNames) > 0 {
		target.enabledExtensionCount = (C.uint)(len(i.EnabledExtensionNames))
		n := mem.CStrings(i.EnabledExtensionNames)
		target.ppEnabledExtensionNames = n
	}
}

// MemoryHeap as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryHeap.html
type MemoryHeap struct {
	Size  DeviceSize
	Flags MemoryHeapFlags
}

func (m *MemoryHeap) From(in *C.VkMemoryHeap) {
	m.Size = DeviceSize(in.size)
	m.Flags = MemoryHeapFlags(in.flags)
}


// MemoryType as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryType.html
type MemoryType struct {
	PropertyFlags MemoryPropertyFlags
	HeapIndex     uint32
}

func (m *MemoryType) From(in *C.VkMemoryType) {
	m.PropertyFlags = MemoryPropertyFlags(in.propertyFlags)
	m.HeapIndex = uint32(in.heapIndex)
}

// PhysicalDeviceFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceFeatures.html
type PhysicalDeviceFeatures struct {
	RobustBufferAccess                      bool
	FullDrawIndexUint32                     bool
	ImageCubeArray                          bool
	IndependentBlend                        bool
	GeometryShader                          bool
	TessellationShader                      bool
	SampleRateShading                       bool
	DualSrcBlend                            bool
	LogicOp                                 bool
	MultiDrawIndirect                       bool
	DrawIndirectFirstInstance               bool
	DepthClamp                              bool
	DepthBiasClamp                          bool
	FillModeNonSolid                        bool
	DepthBounds                             bool
	WideLines                               bool
	LargePoints                             bool
	AlphaToOne                              bool
	MultiViewport                           bool
	SamplerAnisotropy                       bool
	TextureCompressionETC2                  bool
	TextureCompressionASTC_LDR              bool
	TextureCompressionBC                    bool
	OcclusionQueryPrecise                   bool
	PipelineStatisticsQuery                 bool
	VertexPipelineStoresAndAtomics          bool
	FragmentStoresAndAtomics                bool
	ShaderTessellationAndGeometryPointSize  bool
	ShaderImageGatherExtended               bool
	ShaderStorageImageExtendedFormats       bool
	ShaderStorageImageMultisample           bool
	ShaderStorageImageReadWithoutFormat     bool
	ShaderStorageImageWriteWithoutFormat    bool
	ShaderUniformBufferArrayDynamicIndexing bool
	ShaderSampledImageArrayDynamicIndexing  bool
	ShaderStorageBufferArrayDynamicIndexing bool
	ShaderStorageImageArrayDynamicIndexing  bool
	ShaderClipDistance                      bool
	ShaderCullDistance                      bool
	ShaderFloat64                           bool
	ShaderInt64                             bool
	ShaderInt16                             bool
	ShaderResourceResidency                 bool
	ShaderResourceMinLod                    bool
	SparseBinding                           bool
	SparseResidencyBuffer                   bool
	SparseResidencyImage2D                  bool
	SparseResidencyImage3D                  bool
	SparseResidency2Samples                 bool
	SparseResidency4Samples                 bool
	SparseResidency8Samples                 bool
	SparseResidency16Samples                bool
	SparseResidencyAliased                  bool
	VariableMultisampleRate                 bool
	InheritedQueries                        bool
}

func (p *PhysicalDeviceFeatures) To(mem VulkanClientMemory, target *C.VkPhysicalDeviceFeatures) {
	target.robustBufferAccess = ToBool32(p.RobustBufferAccess)
	target.fullDrawIndexUint32 = ToBool32(p.FullDrawIndexUint32)
	target.imageCubeArray = ToBool32(p.ImageCubeArray)
	target.independentBlend = ToBool32(p.IndependentBlend)
	target.geometryShader = ToBool32(p.GeometryShader)
	target.tessellationShader = ToBool32(p.TessellationShader)
	target.sampleRateShading = ToBool32(p.SampleRateShading)
	target.dualSrcBlend = ToBool32(p.DualSrcBlend)
	target.logicOp = ToBool32(p.LogicOp)
	target.multiDrawIndirect = ToBool32(p.MultiDrawIndirect)
	target.drawIndirectFirstInstance = ToBool32(p.DrawIndirectFirstInstance)
	target.depthClamp = ToBool32(p.DepthClamp)
	target.depthBiasClamp = ToBool32(p.DepthBiasClamp)
	target.fillModeNonSolid = ToBool32(p.FillModeNonSolid)
	target.depthBounds = ToBool32(p.DepthBounds)
	target.wideLines = ToBool32(p.WideLines)
	target.largePoints = ToBool32(p.LargePoints)
	target.alphaToOne = ToBool32(p.AlphaToOne)
	target.multiViewport = ToBool32(p.MultiViewport)
	target.samplerAnisotropy = ToBool32(p.SamplerAnisotropy)
	target.textureCompressionETC2 = ToBool32(p.TextureCompressionETC2)
	target.textureCompressionASTC_LDR = ToBool32(p.TextureCompressionASTC_LDR)
	target.textureCompressionBC = ToBool32(p.TextureCompressionBC)
	target.occlusionQueryPrecise = ToBool32(p.OcclusionQueryPrecise)
	target.pipelineStatisticsQuery = ToBool32(p.PipelineStatisticsQuery)
	target.vertexPipelineStoresAndAtomics = ToBool32(p.VertexPipelineStoresAndAtomics)
	target.fragmentStoresAndAtomics = ToBool32(p.FragmentStoresAndAtomics)
	target.shaderTessellationAndGeometryPointSize = ToBool32(p.ShaderTessellationAndGeometryPointSize)
	target.shaderImageGatherExtended = ToBool32(p.ShaderImageGatherExtended)
	target.shaderStorageImageExtendedFormats = ToBool32(p.ShaderStorageImageExtendedFormats)
	target.shaderStorageImageMultisample = ToBool32(p.ShaderStorageImageMultisample)
	target.shaderStorageImageReadWithoutFormat = ToBool32(p.ShaderStorageImageReadWithoutFormat)
	target.shaderStorageImageWriteWithoutFormat = ToBool32(p.ShaderStorageImageWriteWithoutFormat)
	target.shaderUniformBufferArrayDynamicIndexing = ToBool32(p.ShaderUniformBufferArrayDynamicIndexing)
	target.shaderSampledImageArrayDynamicIndexing = ToBool32(p.ShaderSampledImageArrayDynamicIndexing)
	target.shaderStorageBufferArrayDynamicIndexing = ToBool32(p.ShaderStorageBufferArrayDynamicIndexing)
	target.shaderStorageImageArrayDynamicIndexing = ToBool32(p.ShaderStorageImageArrayDynamicIndexing)
	target.shaderClipDistance = ToBool32(p.ShaderClipDistance)
	target.shaderCullDistance = ToBool32(p.ShaderCullDistance)
	target.shaderFloat64 = ToBool32(p.ShaderFloat64)
	target.shaderInt64 = ToBool32(p.ShaderInt64)
	target.shaderInt16 = ToBool32(p.ShaderInt16)
	target.shaderResourceResidency = ToBool32(p.ShaderResourceResidency)
	target.shaderResourceMinLod = ToBool32(p.ShaderResourceMinLod)
	target.sparseBinding = ToBool32(p.SparseBinding)
	target.sparseResidencyBuffer = ToBool32(p.SparseResidencyBuffer)
	target.sparseResidencyImage2D = ToBool32(p.SparseResidencyImage2D)
	target.sparseResidencyImage3D = ToBool32(p.SparseResidencyImage3D)
	target.sparseResidency2Samples = ToBool32(p.SparseResidency2Samples)
	target.sparseResidency4Samples = ToBool32(p.SparseResidency4Samples)
	target.sparseResidency8Samples = ToBool32(p.SparseResidency8Samples)
	target.sparseResidency16Samples = ToBool32(p.SparseResidency16Samples)
	target.sparseResidencyAliased = ToBool32(p.SparseResidencyAliased)
	target.variableMultisampleRate = ToBool32(p.VariableMultisampleRate)
	target.inheritedQueries = ToBool32(p.InheritedQueries)
}

// PhysicalDeviceLimits as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceLimits.html
type PhysicalDeviceLimits struct {
	MaxImageDimension1D                             uint32
	MaxImageDimension2D                             uint32
	MaxImageDimension3D                             uint32
	MaxImageDimensionCube                           uint32
	MaxImageArrayLayers                             uint32
	MaxTexelBufferElements                          uint32
	MaxUniformBufferRange                           uint32
	MaxStorageBufferRange                           uint32
	MaxPushConstantsSize                            uint32
	MaxMemoryAllocationCount                        uint32
	MaxSamplerAllocationCount                       uint32
	BufferImageGranularity                          DeviceSize
	SparseAddressSpaceSize                          DeviceSize
	MaxBoundDescriptorSets                          uint32
	MaxPerStageDescriptorSamplers                   uint32
	MaxPerStageDescriptorUniformBuffers             uint32
	MaxPerStageDescriptorStorageBuffers             uint32
	MaxPerStageDescriptorSampledImages              uint32
	MaxPerStageDescriptorStorageImages              uint32
	MaxPerStageDescriptorInputAttachments           uint32
	MaxPerStageResources                            uint32
	MaxDescriptorSetSamplers                        uint32
	MaxDescriptorSetUniformBuffers                  uint32
	MaxDescriptorSetUniformBuffersDynamic           uint32
	MaxDescriptorSetStorageBuffers                  uint32
	MaxDescriptorSetStorageBuffersDynamic           uint32
	MaxDescriptorSetSampledImages                   uint32
	MaxDescriptorSetStorageImages                   uint32
	MaxDescriptorSetInputAttachments                uint32
	MaxVertexInputAttributes                        uint32
	MaxVertexInputBindings                          uint32
	MaxVertexInputAttributeOffset                   uint32
	MaxVertexInputBindingStride                     uint32
	MaxVertexOutputComponents                       uint32
	MaxTessellationGenerationLevel                  uint32
	MaxTessellationPatchSize                        uint32
	MaxTessellationControlPerVertexInputComponents  uint32
	MaxTessellationControlPerVertexOutputComponents uint32
	MaxTessellationControlPerPatchOutputComponents  uint32
	MaxTessellationControlTotalOutputComponents     uint32
	MaxTessellationEvaluationInputComponents        uint32
	MaxTessellationEvaluationOutputComponents       uint32
	MaxGeometryShaderInvocations                    uint32
	MaxGeometryInputComponents                      uint32
	MaxGeometryOutputComponents                     uint32
	MaxGeometryOutputVertices                       uint32
	MaxGeometryTotalOutputComponents                uint32
	MaxFragmentInputComponents                      uint32
	MaxFragmentOutputAttachments                    uint32
	MaxFragmentDualSrcAttachments                   uint32
	MaxFragmentCombinedOutputResources              uint32
	MaxComputeSharedMemorySize                      uint32
	MaxComputeWorkGroupCount                        [3]uint32
	MaxComputeWorkGroupInvocations                  uint32
	MaxComputeWorkGroupSize                         [3]uint32
	SubPixelPrecisionBits                           uint32
	SubTexelPrecisionBits                           uint32
	MipmapPrecisionBits                             uint32
	MaxDrawIndexedIndexValue                        uint32
	MaxDrawIndirectCount                            uint32
	MaxSamplerLodBias                               float32
	MaxSamplerAnisotropy                            float32
	MaxViewports                                    uint32
	MaxViewportDimensions                           [2]uint32
	ViewportBoundsRange                             [2]float32
	ViewportSubPixelBits                            uint32
	MinMemoryMapAlignment                           uint32
	MinTexelBufferOffsetAlignment                   DeviceSize
	MinUniformBufferOffsetAlignment                 DeviceSize
	MinStorageBufferOffsetAlignment                 DeviceSize
	MinTexelOffset                                  int32
	MaxTexelOffset                                  uint32
	MinTexelGatherOffset                            int32
	MaxTexelGatherOffset                            uint32
	MinInterpolationOffset                          float32
	MaxInterpolationOffset                          float32
	SubPixelInterpolationOffsetBits                 uint32
	MaxFramebufferWidth                             uint32
	MaxFramebufferHeight                            uint32
	MaxFramebufferLayers                            uint32
	FramebufferColorSampleCounts                    SampleCountFlags
	FramebufferDepthSampleCounts                    SampleCountFlags
	FramebufferStencilSampleCounts                  SampleCountFlags
	FramebufferNoAttachmentsSampleCounts            SampleCountFlags
	MaxColorAttachments                             uint32
	SampledImageColorSampleCounts                   SampleCountFlags
	SampledImageIntegerSampleCounts                 SampleCountFlags
	SampledImageDepthSampleCounts                   SampleCountFlags
	SampledImageStencilSampleCounts                 SampleCountFlags
	StorageImageSampleCounts                        SampleCountFlags
	MaxSampleMaskWords                              uint32
	TimestampComputeAndGraphics                     bool
	TimestampPeriod                                 float32
	MaxClipDistances                                uint32
	MaxCullDistances                                uint32
	MaxCombinedClipAndCullDistances                 uint32
	DiscreteQueuePriorities                         uint32
	PointSizeRange                                  [2]float32
	LineWidthRange                                  [2]float32
	PointSizeGranularity                            float32
	LineWidthGranularity                            float32
	StrictLines                                     bool
	StandardSampleLocations                         bool
	OptimalBufferCopyOffsetAlignment                DeviceSize
	OptimalBufferCopyRowPitchAlignment              DeviceSize
	NonCoherentAtomSize                             DeviceSize
}

func (p *PhysicalDeviceLimits) From(in *C.VkPhysicalDeviceLimits) {
	p.MaxImageDimension1D = uint32(in.maxImageDimension1D)
	p.MaxImageDimension2D = uint32(in.maxImageDimension2D)
	p.MaxImageDimension3D = uint32(in.maxImageDimension3D)
	p.MaxImageDimensionCube = uint32(in.maxImageDimensionCube)
	p.MaxImageArrayLayers = uint32(in.maxImageArrayLayers)
	p.MaxTexelBufferElements = uint32(in.maxTexelBufferElements)
	p.MaxUniformBufferRange = uint32(in.maxUniformBufferRange)
	p.MaxStorageBufferRange = uint32(in.maxStorageBufferRange)
	p.MaxPushConstantsSize = uint32(in.maxPushConstantsSize)
	p.MaxMemoryAllocationCount = uint32(in.maxMemoryAllocationCount)
	p.MaxSamplerAllocationCount = uint32(in.maxSamplerAllocationCount)
	p.BufferImageGranularity = DeviceSize(in.bufferImageGranularity)
	p.SparseAddressSpaceSize = DeviceSize(in.sparseAddressSpaceSize)
	p.MaxBoundDescriptorSets = uint32(in.maxBoundDescriptorSets)
	p.MaxPerStageDescriptorSamplers = uint32(in.maxPerStageDescriptorSamplers)
	p.MaxPerStageDescriptorUniformBuffers = uint32(in.maxPerStageDescriptorUniformBuffers)
	p.MaxPerStageDescriptorStorageBuffers = uint32(in.maxPerStageDescriptorStorageBuffers)
	p.MaxPerStageDescriptorSampledImages = uint32(in.maxPerStageDescriptorSampledImages)
	p.MaxPerStageDescriptorStorageImages = uint32(in.maxPerStageDescriptorStorageImages)
	p.MaxPerStageDescriptorInputAttachments = uint32(in.maxPerStageDescriptorInputAttachments)
	p.MaxPerStageResources = uint32(in.maxPerStageResources)
	p.MaxDescriptorSetSamplers = uint32(in.maxDescriptorSetSamplers)
	p.MaxDescriptorSetUniformBuffers = uint32(in.maxDescriptorSetUniformBuffers)
	p.MaxDescriptorSetUniformBuffersDynamic = uint32(in.maxDescriptorSetUniformBuffersDynamic)
	p.MaxDescriptorSetStorageBuffers = uint32(in.maxDescriptorSetStorageBuffers)
	p.MaxDescriptorSetStorageBuffersDynamic = uint32(in.maxDescriptorSetStorageBuffersDynamic)
	p.MaxDescriptorSetSampledImages = uint32(in.maxDescriptorSetSampledImages)
	p.MaxDescriptorSetStorageImages = uint32(in.maxDescriptorSetStorageImages)
	p.MaxDescriptorSetInputAttachments = uint32(in.maxDescriptorSetInputAttachments)
	p.MaxVertexInputAttributes = uint32(in.maxVertexInputAttributes)
	p.MaxVertexInputBindings = uint32(in.maxVertexInputBindings)
	p.MaxVertexInputAttributeOffset = uint32(in.maxVertexInputAttributeOffset)
	p.MaxVertexInputBindingStride = uint32(in.maxVertexInputBindingStride)
	p.MaxVertexOutputComponents = uint32(in.maxVertexOutputComponents)
	p.MaxTessellationGenerationLevel = uint32(in.maxTessellationGenerationLevel)
	p.MaxTessellationPatchSize = uint32(in.maxTessellationPatchSize)
	p.MaxTessellationControlPerVertexInputComponents = uint32(in.maxTessellationControlPerVertexInputComponents)
	p.MaxTessellationControlPerVertexOutputComponents = uint32(in.maxTessellationControlPerVertexOutputComponents)
	p.MaxTessellationControlPerPatchOutputComponents = uint32(in.maxTessellationControlPerPatchOutputComponents)
	p.MaxTessellationControlTotalOutputComponents = uint32(in.maxTessellationControlTotalOutputComponents)
	p.MaxTessellationEvaluationInputComponents = uint32(in.maxTessellationEvaluationInputComponents)
	p.MaxTessellationEvaluationOutputComponents = uint32(in.maxTessellationEvaluationOutputComponents)
	p.MaxGeometryShaderInvocations = uint32(in.maxGeometryShaderInvocations)
	p.MaxGeometryInputComponents = uint32(in.maxGeometryInputComponents)
	p.MaxGeometryOutputComponents = uint32(in.maxGeometryOutputComponents)
	p.MaxGeometryOutputVertices = uint32(in.maxGeometryOutputVertices)
	p.MaxGeometryTotalOutputComponents = uint32(in.maxGeometryTotalOutputComponents)
	p.MaxFragmentInputComponents = uint32(in.maxFragmentInputComponents)
	p.MaxFragmentOutputAttachments = uint32(in.maxFragmentOutputAttachments)
	p.MaxFragmentDualSrcAttachments = uint32(in.maxFragmentDualSrcAttachments)
	p.MaxFragmentCombinedOutputResources = uint32(in.maxFragmentCombinedOutputResources)
	p.MaxComputeSharedMemorySize = uint32(in.maxComputeSharedMemorySize)
	p.MaxComputeWorkGroupCount = [3]uint32{uint32(in.maxComputeWorkGroupCount[0]), uint32(in.maxComputeWorkGroupCount[1]), uint32(in.maxComputeWorkGroupCount[2])}
	p.MaxComputeWorkGroupInvocations = uint32(in.maxComputeWorkGroupInvocations)
	p.MaxComputeWorkGroupSize = [3]uint32{uint32(in.maxComputeWorkGroupSize[0]), uint32(in.maxComputeWorkGroupSize[1]), uint32(in.maxComputeWorkGroupSize[2])}
	p.SubPixelPrecisionBits = uint32(in.subPixelPrecisionBits)
	p.SubTexelPrecisionBits = uint32(in.subTexelPrecisionBits)
	p.MipmapPrecisionBits = uint32(in.mipmapPrecisionBits)
	p.MaxDrawIndexedIndexValue = uint32(in.maxDrawIndexedIndexValue)
	p.MaxDrawIndirectCount = uint32(in.maxDrawIndirectCount)
	p.MaxSamplerLodBias = float32(in.maxSamplerLodBias)
	p.MaxSamplerAnisotropy = float32(in.maxSamplerAnisotropy)
	p.MaxViewports = uint32(in.maxViewports)
	p.MaxViewportDimensions = [2]uint32{uint32(in.maxViewportDimensions[0]), uint32(in.maxViewportDimensions[1])}
	p.ViewportBoundsRange = [2]float32{float32(in.viewportBoundsRange[0]), float32(in.viewportBoundsRange[1])}
	p.ViewportSubPixelBits = uint32(in.viewportSubPixelBits)
	p.MinMemoryMapAlignment = uint32(in.minMemoryMapAlignment)
	p.MinTexelBufferOffsetAlignment = DeviceSize(in.minTexelBufferOffsetAlignment)
	p.MinUniformBufferOffsetAlignment = DeviceSize(in.minUniformBufferOffsetAlignment)
	p.MinStorageBufferOffsetAlignment = DeviceSize(in.minStorageBufferOffsetAlignment)
	p.MinTexelOffset = int32(in.minTexelOffset)
	p.MaxTexelOffset = uint32(in.maxTexelOffset)
	p.MinTexelGatherOffset = int32(in.minTexelGatherOffset)
	p.MaxTexelGatherOffset = uint32(in.maxTexelGatherOffset)
	p.MinInterpolationOffset = float32(in.minInterpolationOffset)
	p.MaxInterpolationOffset = float32(in.maxInterpolationOffset)
	p.SubPixelInterpolationOffsetBits = uint32(in.subPixelInterpolationOffsetBits)
	p.MaxFramebufferWidth = uint32(in.maxFramebufferWidth)
	p.MaxFramebufferHeight = uint32(in.maxFramebufferHeight)
	p.MaxFramebufferLayers = uint32(in.maxFramebufferLayers)
	p.FramebufferColorSampleCounts = SampleCountFlags(in.framebufferColorSampleCounts)
	p.FramebufferDepthSampleCounts = SampleCountFlags(in.framebufferDepthSampleCounts)
	p.FramebufferStencilSampleCounts = SampleCountFlags(in.framebufferStencilSampleCounts)
	p.FramebufferNoAttachmentsSampleCounts = SampleCountFlags(in.framebufferNoAttachmentsSampleCounts)
	p.MaxColorAttachments = uint32(in.maxColorAttachments)
	p.SampledImageColorSampleCounts = SampleCountFlags(in.sampledImageColorSampleCounts)
	p.SampledImageIntegerSampleCounts = SampleCountFlags(in.sampledImageIntegerSampleCounts)
	p.SampledImageDepthSampleCounts = SampleCountFlags(in.sampledImageDepthSampleCounts)
	p.SampledImageStencilSampleCounts = SampleCountFlags(in.sampledImageStencilSampleCounts)
	p.StorageImageSampleCounts = SampleCountFlags(in.storageImageSampleCounts)
	p.MaxSampleMaskWords = uint32(in.maxSampleMaskWords)
	p.TimestampComputeAndGraphics = Bool32(in.timestampComputeAndGraphics).B()
	p.TimestampPeriod = float32(in.timestampPeriod)
	p.MaxClipDistances = uint32(in.maxClipDistances)
	p.MaxCullDistances = uint32(in.maxCullDistances)
	p.MaxCombinedClipAndCullDistances = uint32(in.maxCombinedClipAndCullDistances)
	p.DiscreteQueuePriorities = uint32(in.discreteQueuePriorities)
	p.PointSizeRange = [2]float32{float32(in.pointSizeRange[0]), float32(in.pointSizeRange[1])}
	p.LineWidthRange = [2]float32{float32(in.lineWidthRange[0]), float32(in.lineWidthRange[1])}
	p.PointSizeGranularity = float32(in.pointSizeGranularity)
	p.LineWidthGranularity = float32(in.lineWidthGranularity)
	p.StrictLines = Bool32(in.strictLines).B()
	p.StandardSampleLocations = Bool32(in.standardSampleLocations).B()
	p.OptimalBufferCopyOffsetAlignment = DeviceSize(in.optimalBufferCopyOffsetAlignment)
	p.OptimalBufferCopyRowPitchAlignment = DeviceSize(in.optimalBufferCopyRowPitchAlignment)
	p.NonCoherentAtomSize = DeviceSize(in.nonCoherentAtomSize)
}

// PhysicalDeviceMemoryProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceMemoryProperties.html
type PhysicalDeviceMemoryProperties struct {
	MemoryTypes     []MemoryType
	MemoryHeaps     []MemoryHeap
}

func (p *PhysicalDeviceMemoryProperties) From(in *C.VkPhysicalDeviceMemoryProperties) {
	if in.memoryTypeCount > 0 {
		p.MemoryTypes = make([]MemoryType, 0, in.memoryTypeCount)
		for i := 0; i < int(in.memoryTypeCount); i++ {
			mt := MemoryType{}
			mt.From(&in.memoryTypes[i])
			p.MemoryTypes = append(p.MemoryTypes,mt)
		}
	}

	if in.memoryHeapCount > 0 {
		p.MemoryHeaps = make([]MemoryHeap, 0, in.memoryHeapCount)
		for i := 0; i < int(in.memoryHeapCount); i++ {
			mh := MemoryHeap{}
			mh.From(&in.memoryHeaps[i])
			p.MemoryHeaps = append(p.MemoryHeaps,mh)
		}
	}
}

// PhysicalDeviceSparseProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceSparseProperties.html
type PhysicalDeviceSparseProperties struct {
	ResidencyStandard2DBlockShape            bool
	ResidencyStandard2DMultisampleBlockShape bool
	ResidencyStandard3DBlockShape            bool
	ResidencyAlignedMipSize                  bool
	ResidencyNonResidentStrict               bool
}

func (p *PhysicalDeviceSparseProperties) From(in *C.VkPhysicalDeviceSparseProperties) {
	p.ResidencyStandard2DBlockShape = in.residencyStandard2DBlockShape != 0
	p.ResidencyStandard2DMultisampleBlockShape = in.residencyStandard2DMultisampleBlockShape != 0
	p.ResidencyStandard3DBlockShape = in.residencyStandard3DBlockShape != 0
	p.ResidencyAlignedMipSize = in.residencyAlignedMipSize != 0
	p.ResidencyNonResidentStrict = in.residencyNonResidentStrict != 0
}

// PhysicalDeviceProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceProperties.html
type PhysicalDeviceProperties struct {
	ApiVersion        uint32
	DriverVersion     uint32
	VendorID          uint32
	DeviceID          uint32
	DeviceType        PhysicalDeviceType
	DeviceName        string
	PipelineCacheUUID uuid.UUID
	Limits            PhysicalDeviceLimits
	SparseProperties  PhysicalDeviceSparseProperties
}

func (p *PhysicalDeviceProperties) From(in *C.VkPhysicalDeviceProperties) {
	p.ApiVersion = uint32(in.apiVersion)
	p.DriverVersion = uint32(in.driverVersion)
	p.VendorID = uint32(in.vendorID)
	p.DeviceID = uint32(in.deviceID)
	p.DeviceType = PhysicalDeviceType(in.deviceType)
	p.DeviceName = TrimZeros(C.GoString(&in.deviceName[0]))
	u, _ := uuid.FromBytes(C.GoBytes(unsafe.Pointer(&in.pipelineCacheUUID[0]), 16))
	p.PipelineCacheUUID = u
	p.Limits.From(&in.limits)
	p.SparseProperties.From(&in.sparseProperties)
}

// QueueFamilyProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueueFamilyProperties.html
type QueueFamilyProperties struct {
	QueueFlags                  QueueFlags
	QueueCount                  uint32
	TimestampValidBits          uint32
	MinImageTransferGranularity Extent3D
}

func (q *QueueFamilyProperties) From(in *C.VkQueueFamilyProperties) {
	q.QueueFlags = QueueFlags(in.queueFlags)
	q.QueueCount = uint32(in.queueCount)
	q.TimestampValidBits = uint32(in.timestampValidBits)
	q.MinImageTransferGranularity.From(&in.minImageTransferGranularity)
}

// DeviceQueueCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceQueueCreateInfo.html
type DeviceQueueCreateInfo struct {
	Next             unsafe.Pointer
	Flags            DeviceQueueCreateFlags
	QueueFamilyIndex uint32
	QueuePriorities  []float32
}

func (q *DeviceQueueCreateInfo) To(mem VulkanClientMemory, target *C.VkDeviceQueueCreateInfo) func() {
	target.sType = (C.VkStructureType)(StructureTypeDeviceQueueCreateInfo)
	target.pNext = q.Next
	target.flags = (C.VkDeviceQueueCreateFlags)(q.Flags)
	target.queueFamilyIndex = (C.uint)(q.QueueFamilyIndex)
	target.queueCount = (C.uint)(len(q.QueuePriorities))
	qp := (*C.float)(mem.Calloc(len(q.QueuePriorities), C.sizeof_float))
	target.pQueuePriorities = qp
	for _, iqp := range q.QueuePriorities {
		*qp = C.float(iqp)
		qp = (*C.float)((unsafe.Pointer)(uintptr(unsafe.Pointer(qp)) + C.sizeof_float))
	}
	return func() {
		C.free(unsafe.Pointer(target.pQueuePriorities))
	}
}

// DeviceCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceCreateInfo.html
type DeviceCreateInfo struct {
	Next                  unsafe.Pointer
	Flags                 DeviceCreateFlags
	QueueCreateInfos      []DeviceQueueCreateInfo
	EnabledLayerNames     []string
	EnabledExtensionNames []string
	EnabledFeatures       []PhysicalDeviceFeatures
}

func (d *DeviceCreateInfo) To(mem VulkanClientMemory, in *C.VkDeviceCreateInfo) {
	in.sType = (C.VkStructureType)(StructureTypeDeviceCreateInfo)
	in.pNext = d.Next
	in.flags = (C.VkDeviceCreateFlags)(d.Flags)
	if len(d.QueueCreateInfos) > 0 {
		in.queueCreateInfoCount = (C.uint32_t)(len(d.QueueCreateInfos))
		qis := (*C.VkDeviceQueueCreateInfo)(mem.Calloc(len(d.QueueCreateInfos), C.sizeof_VkDeviceQueueCreateInfo))
		in.pQueueCreateInfos = qis
		for _, qi := range d.QueueCreateInfos {
			qi.To(mem, qis)
			qis = (*C.VkDeviceQueueCreateInfo)((unsafe.Pointer)(uintptr(unsafe.Pointer(qis)) + C.sizeof_VkDeviceQueueCreateInfo))
		}
	}
	if len(d.EnabledLayerNames) > 0 {
		in.enabledLayerCount = (C.uint32_t)(len(d.EnabledLayerNames))
		ppe := mem.CStrings(d.EnabledLayerNames)
		in.ppEnabledLayerNames = ppe
	}
	if len(d.EnabledExtensionNames) > 0 {
		in.enabledExtensionCount = (C.uint32_t)(len(d.EnabledExtensionNames))
		een := mem.CStrings(d.EnabledExtensionNames)
		in.ppEnabledExtensionNames = een
	}
	efs := (*C.VkPhysicalDeviceFeatures)(mem.Calloc(len(d.EnabledFeatures), C.sizeof_VkPhysicalDeviceFeatures))
	in.pEnabledFeatures = efs
	for _, e := range d.EnabledFeatures {
		e.To(mem, efs)
		efs = (*C.VkPhysicalDeviceFeatures)((unsafe.Pointer)(uintptr(unsafe.Pointer(efs)) + C.sizeof_VkPhysicalDeviceFeatures))
	}
}

// ExtensionProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExtensionProperties.html
type ExtensionProperties struct {
	ExtensionName string
	SpecVersion   uint32
}

func (e *ExtensionProperties) From(in *C.VkExtensionProperties) {
	e.ExtensionName = TrimZeros(C.GoString(&in.extensionName[0]))
	e.SpecVersion = uint32(in.specVersion)
}

// LayerProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkLayerProperties.html
type LayerProperties struct {
	LayerName             string
	SpecVersion           uint32
	ImplementationVersion uint32
	Description           string
}

func (l *LayerProperties) From(in *C.VkLayerProperties) {
	l.LayerName = TrimZeros(C.GoString((*C.char)(&in.layerName[0])))
	l.SpecVersion = uint32(in.specVersion)
	l.ImplementationVersion = uint32(in.implementationVersion)
	l.Description = TrimZeros(C.GoString((*C.char)(&in.description[0])))
}

// SubmitInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSubmitInfo.html
type SubmitInfo struct {
	Next             unsafe.Pointer
	WaitSemaphores   []Semaphore
	WaitDstStageMask []PipelineStageFlags
	CommandBuffers   []CommandBuffer
	SignalSemaphores []Semaphore
}

func (s *SubmitInfo) To(mem VulkanClientMemory, target *C.VkSubmitInfo) error {
	target.sType = (C.VkStructureType)(StructureTypeSubmitInfo)
	target.pNext = s.Next
	target.waitSemaphoreCount = (C.uint32_t)(len(s.WaitSemaphores))
	if len(s.WaitSemaphores) > 0 {
		ws := (*C.VkSemaphore)(mem.Calloc(len(s.WaitSemaphores), C.sizeof_VkSemaphore))
		target.pWaitSemaphores = ws
		for _, sem := range s.WaitSemaphores {
			*ws = C.VkSemaphore(sem)
			ws = (*C.VkSemaphore)(unsafe.Pointer(uintptr(unsafe.Pointer(ws)) + C.sizeof_VkSemaphore))
		}
		wd := (*C.VkPipelineStageFlags)(mem.Calloc(len(s.WaitSemaphores), C.sizeof_VkPipelineStageFlags))
		target.pWaitDstStageMask = wd
		for _, mask := range s.WaitDstStageMask {
			*wd = C.VkPipelineStageFlags(mask)
			wd = (*C.VkPipelineStageFlags)(unsafe.Pointer(uintptr(unsafe.Pointer(wd)) + C.sizeof_VkPipelineStageFlags))
		}
	}

	target.commandBufferCount = (C.uint32_t)(len(s.CommandBuffers))
	if len(s.CommandBuffers) > 0 {
		cb := (*C.VkCommandBuffer)(mem.Calloc(len(s.CommandBuffers), C.sizeof_VkCommandBuffer))
		target.pCommandBuffers = cb
		for _, commandBuffer := range s.CommandBuffers {
			*cb = C.VkCommandBuffer(commandBuffer)
			cb = (*C.VkCommandBuffer)(unsafe.Pointer(uintptr(unsafe.Pointer(cb)) + C.sizeof_VkCommandBuffer))
		}
	}
	target.signalSemaphoreCount = (C.uint32_t)(len(s.SignalSemaphores))
	if len(s.SignalSemaphores) > 0 {
		ss := (*C.VkSemaphore)(mem.Calloc(len(s.SignalSemaphores), C.sizeof_VkSemaphore))
		target.pSignalSemaphores = ss
		for _, sem := range s.SignalSemaphores {
			*ss = C.VkSemaphore(sem)
			ss = (*C.VkSemaphore)(unsafe.Pointer(uintptr(unsafe.Pointer(ss)) + C.sizeof_VkSemaphore))
		}
	}
	return nil
}

// MappedMemoryRange as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMappedMemoryRange.html
type MappedMemoryRange struct {
	// SType  StructureType
	Next   unsafe.Pointer
	Memory DeviceMemory
	Offset DeviceSize
	Size   DeviceSize
}

// MemoryAllocateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryAllocateInfo.html
type MemoryAllocateInfo struct {
	Next            unsafe.Pointer
	AllocationSize  DeviceSize
	MemoryTypeIndex uint32
}

func (m *MemoryAllocateInfo) To(target *C.VkMemoryAllocateInfo) {
	target.sType = (C.VkStructureType)(StructureTypeMemoryAllocateInfo)
	target.pNext = m.Next
	target.allocationSize = C.VkDeviceSize(m.AllocationSize)
	target.memoryTypeIndex = C.uint32_t(m.MemoryTypeIndex)
}

// MemoryRequirements as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryRequirements.html
type MemoryRequirements struct {
	Size           DeviceSize
	Alignment      DeviceSize
	MemoryTypeBits uint32
}

func (m *MemoryRequirements) From(in *C.VkMemoryRequirements) {
	m.Size = DeviceSize(in.size)
	m.Alignment = DeviceSize(in.alignment)
	m.MemoryTypeBits = uint32(in.memoryTypeBits)
}

// SparseMemoryBind as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSparseMemoryBind.html
type SparseMemoryBind struct {
	ResourceOffset DeviceSize
	Size           DeviceSize
	Memory         DeviceMemory
	MemoryOffset   DeviceSize
	Flags          SparseMemoryBindFlags
}

// SparseBufferMemoryBindInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSparseBufferMemoryBindInfo.html
type SparseBufferMemoryBindInfo struct {
	Buffer    Buffer
	BindCount uint32
	PBinds    []SparseMemoryBind
}

// SparseImageOpaqueMemoryBindInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSparseImageOpaqueMemoryBindInfo.html
type SparseImageOpaqueMemoryBindInfo struct {
	Image     Image
	BindCount uint32
	PBinds    []SparseMemoryBind
}

// ImageSubresource as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageSubresource.html
type ImageSubresource struct {
	AspectMask ImageAspectFlags
	MipLevel   uint32
	ArrayLayer uint32
}

// SparseImageMemoryBind as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSparseImageMemoryBind.html
type SparseImageMemoryBind struct {
	Subresource  ImageSubresource
	Offset       Offset3D
	Extent       Extent3D
	Memory       DeviceMemory
	MemoryOffset DeviceSize
	Flags        SparseMemoryBindFlags
}

// SparseImageMemoryBindInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSparseImageMemoryBindInfo.html
type SparseImageMemoryBindInfo struct {
	Image     Image
	BindCount uint32
	PBinds    []SparseImageMemoryBind
}

// BindSparseInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBindSparseInfo.html
type BindSparseInfo struct {
	// SType                StructureType
	Next                 unsafe.Pointer
	WaitSemaphoreCount   uint32
	PWaitSemaphores      []Semaphore
	BufferBindCount      uint32
	PBufferBinds         []SparseBufferMemoryBindInfo
	ImageOpaqueBindCount uint32
	PImageOpaqueBinds    []SparseImageOpaqueMemoryBindInfo
	ImageBindCount       uint32
	PImageBinds          []SparseImageMemoryBindInfo
	SignalSemaphoreCount uint32
	PSignalSemaphores    []Semaphore
}

// SparseImageFormatProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSparseImageFormatProperties.html
type SparseImageFormatProperties struct {
	AspectMask       ImageAspectFlags
	ImageGranularity Extent3D
	Flags            SparseImageFormatFlags
}

// SparseImageMemoryRequirements as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSparseImageMemoryRequirements.html
type SparseImageMemoryRequirements struct {
	FormatProperties     SparseImageFormatProperties
	ImageMipTailFirstLod uint32
	ImageMipTailSize     DeviceSize
	ImageMipTailOffset   DeviceSize
	ImageMipTailStride   DeviceSize
}

// FenceCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFenceCreateInfo.html
type FenceCreateInfo struct {
	Next  unsafe.Pointer
	Flags FenceCreateFlags
}

func (f *FenceCreateInfo) To(mem VulkanClientMemory, target *C.VkFenceCreateInfo) {
	target.sType = (C.VkStructureType)(StructureTypeFenceCreateInfo)
	target.pNext = f.Next
	target.flags = (C.VkFenceCreateFlags)(f.Flags)
}

// SemaphoreCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSemaphoreCreateInfo.html
type SemaphoreCreateInfo struct {
	Next  unsafe.Pointer
	Flags SemaphoreCreateFlags
}

func (s *SemaphoreCreateInfo) To(mem VulkanClientMemory, target *C.VkSemaphoreCreateInfo) {
	target.sType = (C.VkStructureType)(StructureTypeSemaphoreCreateInfo)
	target.pNext = s.Next
	target.flags = (C.VkSemaphoreCreateFlags)(s.Flags)
}

// EventCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkEventCreateInfo.html
type EventCreateInfo struct {
	// SType StructureType
	Next  unsafe.Pointer
	Flags EventCreateFlags
}

// QueryPoolCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueryPoolCreateInfo.html
type QueryPoolCreateInfo struct {
	// SType              StructureType
	Next               unsafe.Pointer
	Flags              QueryPoolCreateFlags
	QueryType          QueryType
	QueryCount         uint32
	PipelineStatistics QueryPipelineStatisticFlags
}

// BufferCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBufferCreateInfo.html
type BufferCreateInfo struct {
	Next                  unsafe.Pointer
	Flags                 BufferCreateFlags
	Size                  DeviceSize
	Usage                 BufferUsageFlags
	SharingMode           SharingMode
	QueueFamilyIndices   []uint32
}

func (b *BufferCreateInfo) To(mem VulkanClientMemory, target *C.VkBufferCreateInfo) {
	target.sType = (C.VkStructureType)(StructureTypeBufferCreateInfo)
	target.pNext = b.Next
	target.flags = (C.VkBufferCreateFlags)(b.Flags)
	target.size = (C.VkDeviceSize)(b.Size)
	target.usage = (C.VkBufferUsageFlags)(b.Usage)
	target.sharingMode = (C.VkSharingMode)(b.SharingMode)
	if len(b.QueueFamilyIndices) > 0 {
		target.queueFamilyIndexCount = (C.uint)(len(b.QueueFamilyIndices))
		pp := (*C.uint32_t)(mem.Calloc(len(b.QueueFamilyIndices), C.sizeof_uint32_t))
		target.pQueueFamilyIndices = pp
		for _, idx := range b.QueueFamilyIndices {
			*pp = C.uint32_t(idx)
			pp = (*C.uint32_t)(unsafe.Pointer(uintptr(unsafe.Pointer(pp)) + C.sizeof_uint32_t))
		}
	}
}

// BufferViewCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBufferViewCreateInfo.html
type BufferViewCreateInfo struct {
	// SType  StructureType
	Next   unsafe.Pointer
	Flags  BufferViewCreateFlags
	Buffer Buffer
	Format Format
	Offset DeviceSize
	Range  DeviceSize
}

// ImageCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageCreateInfo.html
type ImageCreateInfo struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	Flags                 ImageCreateFlags
	ImageType             ImageType
	Format                Format
	Extent                Extent3D
	MipLevels             uint32
	ArrayLayers           uint32
	Samples               SampleCountFlagBits
	Tiling                ImageTiling
	Usage                 ImageUsageFlags
	SharingMode           SharingMode
	QueueFamilyIndexCount uint32
	PQueueFamilyIndices   []uint32
	InitialLayout         ImageLayout
}

// SubresourceLayout as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSubresourceLayout.html
type SubresourceLayout struct {
	Offset     DeviceSize
	Size       DeviceSize
	RowPitch   DeviceSize
	ArrayPitch DeviceSize
	DepthPitch DeviceSize
}

// ComponentMapping as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkComponentMapping.html
type ComponentMapping struct {
	R ComponentSwizzle
	G ComponentSwizzle
	B ComponentSwizzle
	A ComponentSwizzle
}

func (c *ComponentMapping) To(mem VulkanClientMemory, target *C.VkComponentMapping) {
	target.r = C.VkComponentSwizzle(c.R)
	target.g = C.VkComponentSwizzle(c.G)
	target.b = C.VkComponentSwizzle(c.B)
	target.a = C.VkComponentSwizzle(c.A)
}

// ImageViewCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageViewCreateInfo.html
type ImageViewCreateInfo struct {
	Next             unsafe.Pointer
	Flags            ImageViewCreateFlags
	Image            Image
	ViewType         ImageViewType
	Format           Format
	Components       ComponentMapping
	SubresourceRange ImageSubresourceRange
}

func (i *ImageViewCreateInfo) To(mem VulkanClientMemory, target *C.VkImageViewCreateInfo) {
	target.sType = (C.VkStructureType)(StructureTypeImageViewCreateInfo)
	target.pNext = i.Next
	target.flags = (C.VkImageViewCreateFlags)(i.Flags)
	target.image = (C.VkImage)(i.Image)
	target.viewType = (C.VkImageViewType)(i.ViewType)
	target.format = (C.VkFormat)(i.Format)
	i.Components.To(mem, &target.components)
	i.SubresourceRange.To(mem, &target.subresourceRange)
}

// ShaderModuleCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkShaderModuleCreateInfo.html
type ShaderModuleCreateInfo struct {
	Next  unsafe.Pointer
	Flags ShaderModuleCreateFlags
	Code  []byte
}

func (s *ShaderModuleCreateInfo) To(mem VulkanClientMemory, target *C.VkShaderModuleCreateInfo) error {
	target.sType = (C.VkStructureType)(StructureTypeShaderModuleCreateInfo)
	target.pNext = s.Next
	target.flags = (C.VkShaderModuleCreateFlags)(s.Flags)
	target.codeSize = (C.size_t)(len(s.Code))
	target.pCode = (*C.uint32_t)(unsafe.Pointer(&s.Code[0]))
	return nil
}

// PipelineCacheCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCacheCreateInfo.html
type PipelineCacheCreateInfo struct {
	// SType           StructureType
	Next            unsafe.Pointer
	Flags           PipelineCacheCreateFlags
	InitialDataSize uint32
	PInitialData    unsafe.Pointer
}

// SpecializationMapEntry as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSpecializationMapEntry.html
type SpecializationMapEntry struct {
	ConstantID uint32
	Offset     uint32
	Size       uint32
}

func (s *SpecializationMapEntry) To(mem VulkanClientMemory, target *C.VkSpecializationMapEntry) {
	target.constantID = C.uint32_t(s.ConstantID)
	target.offset = C.uint32_t(s.Offset)
	target.size = C.size_t(s.Size)
}

// SpecializationInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSpecializationInfo.html
type SpecializationInfo struct {
	MapEntries []SpecializationMapEntry
	Data       []byte
}

func (s *SpecializationInfo) To(mem VulkanClientMemory, target *C.VkSpecializationInfo) error {
	target.mapEntryCount = (C.uint32_t)(len(s.MapEntries))
	me := (*C.VkSpecializationMapEntry)(mem.Calloc(len(s.MapEntries), C.sizeof_VkSpecializationMapEntry))
	target.pMapEntries = me
	for _, gme := range s.MapEntries {
		gme.To(mem, me)
		me = (*C.VkSpecializationMapEntry)((unsafe.Pointer)(uintptr(unsafe.Pointer(me)) + C.sizeof_VkSpecializationMapEntry))
	}
	target.dataSize = (C.size_t)(len(s.Data))
	target.pData = unsafe.Pointer(&s.Data[0])
	return nil
}

// PipelineShaderStageCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineShaderStageCreateInfo.html
type PipelineShaderStageCreateInfo struct {
	Next               unsafe.Pointer
	Flags              PipelineShaderStageCreateFlags
	Stage              ShaderStageFlagBits
	Module             ShaderModule
	Name               string
	SpecializationInfo []SpecializationInfo
}

func (p *PipelineShaderStageCreateInfo) To(mem VulkanClientMemory, target *C.VkPipelineShaderStageCreateInfo) error {
	target.sType = (C.VkStructureType)(StructureTypePipelineShaderStageCreateInfo)
	target.pNext = p.Next
	target.flags = (C.VkPipelineShaderStageCreateFlags)(p.Flags)
	target.stage = (C.VkShaderStageFlagBits)(p.Stage)
	target.module = (C.VkShaderModule)(p.Module)
	target.pName = mem.CString(p.Name)
	if len(p.SpecializationInfo) > 0 {
		si := (*C.VkSpecializationInfo)(mem.Calloc(len(p.SpecializationInfo), C.sizeof_VkSpecializationInfo))
		target.pSpecializationInfo = si
		for _, gsi := range p.SpecializationInfo {
			err := gsi.To(mem, si)
			if err != nil {
				return err
			}
			si = (*C.VkSpecializationInfo)((unsafe.Pointer)(uintptr(unsafe.Pointer(si)) + C.sizeof_VkSpecializationInfo))
		}
	}
	return nil
}

// ComputePipelineCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkComputePipelineCreateInfo.html
type ComputePipelineCreateInfo struct {
	// SType              StructureType
	Next               unsafe.Pointer
	Flags              PipelineCreateFlags
	Stage              PipelineShaderStageCreateInfo
	Layout             PipelineLayout
	BasePipelineHandle Pipeline
	BasePipelineIndex  int32
}

// VertexInputBindingDescription as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkVertexInputBindingDescription.html
type VertexInputBindingDescription struct {
	Binding   uint32
	Stride    uint32
	InputRate VertexInputRate
}

func (v *VertexInputBindingDescription) To(mem VulkanClientMemory, target *C.VkVertexInputBindingDescription) {
	target.binding = (C.uint32_t)(v.Binding)
	target.stride = (C.uint32_t)(v.Stride)
	target.inputRate = (C.VkVertexInputRate)(v.InputRate)
}

// VertexInputAttributeDescription as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkVertexInputAttributeDescription.html
type VertexInputAttributeDescription struct {
	Location uint32
	Binding  uint32
	Format   Format
	Offset   uint32
}

func (v *VertexInputAttributeDescription) To(mem VulkanClientMemory, target *C.VkVertexInputAttributeDescription) {
	target.location = (C.uint32_t)(v.Location)
	target.binding = (C.uint32_t)(v.Binding)
	target.format = (C.VkFormat)(v.Format)
	target.offset = (C.uint32_t)(v.Offset)
}

// PipelineVertexInputStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineVertexInputStateCreateInfo.html
type PipelineVertexInputStateCreateInfo struct {
	Next                        unsafe.Pointer
	Flags                       PipelineVertexInputStateCreateFlags
	VertexBindingDescriptions   []VertexInputBindingDescription
	VertexAttributeDescriptions []VertexInputAttributeDescription
}

func (p *PipelineVertexInputStateCreateInfo) To(mem VulkanClientMemory, target *C.VkPipelineVertexInputStateCreateInfo) error {
	target.sType = (C.VkStructureType)(StructureTypePipelineVertexInputStateCreateInfo)
	target.pNext = p.Next
	target.flags = (C.VkPipelineVertexInputStateCreateFlags)(p.Flags)
	target.vertexBindingDescriptionCount = (C.uint32_t)(len(p.VertexBindingDescriptions))
	if len(p.VertexBindingDescriptions) > 0 {
		cvbds := make([]C.VkVertexInputBindingDescription, len(p.VertexBindingDescriptions))
		for i, vbd := range p.VertexBindingDescriptions {
			vbd.To(mem, &cvbds[i])
		}
		target.pVertexBindingDescriptions = (*C.VkVertexInputBindingDescription)(unsafe.Pointer(&cvbds[0]))
	}
	target.vertexAttributeDescriptionCount = (C.uint32_t)(len(p.VertexAttributeDescriptions))
	if len(p.VertexAttributeDescriptions) > 0 {
		cvads := make([]C.VkVertexInputAttributeDescription, len(p.VertexAttributeDescriptions))
		for i, vad := range p.VertexAttributeDescriptions {
			vad.To(mem, &cvads[i])
		}
		target.pVertexAttributeDescriptions = (*C.VkVertexInputAttributeDescription)(unsafe.Pointer(&cvads[0]))
	}
	return nil
}

// PipelineInputAssemblyStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineInputAssemblyStateCreateInfo.html
type PipelineInputAssemblyStateCreateInfo struct {
	Next                   unsafe.Pointer
	Flags                  PipelineInputAssemblyStateCreateFlags
	Topology               PrimitiveTopology
	PrimitiveRestartEnable bool
}

func (p *PipelineInputAssemblyStateCreateInfo) To(mem VulkanClientMemory, target *C.VkPipelineInputAssemblyStateCreateInfo) {
	target.sType = C.VkStructureType(StructureTypePipelineInputAssemblyStateCreateInfo)
	target.pNext = p.Next
	target.flags = (C.VkPipelineInputAssemblyStateCreateFlags)(p.Flags)
	target.topology = (C.VkPrimitiveTopology)(p.Topology)
	target.primitiveRestartEnable = ToBool32(p.PrimitiveRestartEnable)
}

// PipelineTessellationStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineTessellationStateCreateInfo.html
type PipelineTessellationStateCreateInfo struct {
	Next               unsafe.Pointer
	Flags              PipelineTessellationStateCreateFlags
	PatchControlPoints uint32
}

func (t *PipelineTessellationStateCreateInfo) To(mem VulkanClientMemory, target *C.VkPipelineTessellationStateCreateInfo) {
	target.sType = C.VkStructureType(StructureTypePipelineTessellationStateCreateInfo)
	target.pNext = t.Next
	target.flags = (C.VkPipelineTessellationStateCreateFlags)(t.Flags)
	target.patchControlPoints = (C.uint32_t)(t.PatchControlPoints)
}

// Viewport as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkViewport.html
type Viewport struct {
	X        float32
	Y        float32
	Width    float32
	Height   float32
	MinDepth float32
	MaxDepth float32
}

func (v *Viewport) To(mem VulkanClientMemory, target *C.VkViewport) {
	target.x = (C.float)(v.X)
	target.y = (C.float)(v.Y)
	target.width = (C.float)(v.Width)
	target.height = (C.float)(v.Height)
	target.minDepth = (C.float)(v.MinDepth)
	target.maxDepth = (C.float)(v.MaxDepth)
}

// PipelineViewportStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineViewportStateCreateInfo.html
type PipelineViewportStateCreateInfo struct {
	Next      unsafe.Pointer
	Flags     PipelineViewportStateCreateFlags
	Viewports []Viewport
	Scissors  []Rect2D
}

func (p *PipelineViewportStateCreateInfo) To(mem VulkanClientMemory, target *C.VkPipelineViewportStateCreateInfo) error {
	target.sType = (C.VkStructureType)(StructureTypePipelineViewportStateCreateInfo)
	target.pNext = p.Next
	target.flags = C.VkPipelineViewportStateCreateFlags(p.Flags)
	target.viewportCount = C.uint32_t(len(p.Viewports))
	if len(p.Viewports) > 0 {
		pv := (*C.VkViewport)(mem.Calloc(len(p.Viewports), C.sizeof_VkViewport))
		target.pViewports = pv
		for _, v := range p.Viewports {
			v.To(mem, pv)
			pv = (*C.VkViewport)((unsafe.Pointer)(uintptr(unsafe.Pointer(pv)) + C.sizeof_VkViewport))
		}
	}
	target.scissorCount = C.uint32_t(len(p.Scissors))
	if len(p.Scissors) > 0 {
		ps := (*C.VkRect2D)(mem.Calloc(len(p.Viewports), C.sizeof_VkRect2D))
		target.pScissors = ps
		for _, s := range p.Scissors {
			s.To(mem, ps)
			ps = (*C.VkRect2D)((unsafe.Pointer)(uintptr(unsafe.Pointer(ps)) + C.sizeof_VkRect2D))
		}
	}
	return nil
}

// PipelineRasterizationStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineRasterizationStateCreateInfo.html
type PipelineRasterizationStateCreateInfo struct {
	Next                    unsafe.Pointer
	Flags                   PipelineRasterizationStateCreateFlags
	DepthClampEnable        bool
	RasterizerDiscardEnable bool
	PolygonMode             PolygonMode
	CullMode                CullModeFlags
	FrontFace               FrontFace
	DepthBiasEnable         bool
	DepthBiasConstantFactor float32
	DepthBiasClamp          float32
	DepthBiasSlopeFactor    float32
	LineWidth               float32
}

func (p *PipelineRasterizationStateCreateInfo) To(mem VulkanClientMemory, target *C.VkPipelineRasterizationStateCreateInfo) {
	target.sType = (C.VkStructureType)(StructureTypePipelineRasterizationStateCreateInfo)
	target.pNext = p.Next
	target.flags = (C.VkPipelineRasterizationStateCreateFlags)(p.Flags)
	target.depthClampEnable = ToBool32(p.DepthClampEnable)
	target.rasterizerDiscardEnable = ToBool32(p.RasterizerDiscardEnable)
	target.polygonMode = (C.VkPolygonMode)(p.PolygonMode)
	target.cullMode = (C.VkCullModeFlags)(p.CullMode)
	target.frontFace = (C.VkFrontFace)(p.FrontFace)
	target.depthBiasEnable = ToBool32(p.DepthBiasEnable)
	target.depthBiasConstantFactor = (C.float)(p.DepthBiasConstantFactor)
	target.depthBiasClamp = (C.float)(p.DepthBiasClamp)
	target.depthBiasSlopeFactor = (C.float)(p.DepthBiasSlopeFactor)
	target.lineWidth = (C.float)(p.LineWidth)
}

// PipelineMultisampleStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineMultisampleStateCreateInfo.html
type PipelineMultisampleStateCreateInfo struct {
	Next                  unsafe.Pointer
	Flags                 PipelineMultisampleStateCreateFlags
	RasterizationSamples  SampleCountFlagBits
	SampleShadingEnable   bool
	MinSampleShading      float32
	SampleMask            []SampleMask
	AlphaToCoverageEnable bool
	AlphaToOneEnable      bool
}

func (p *PipelineMultisampleStateCreateInfo) To(mem VulkanClientMemory, target *C.VkPipelineMultisampleStateCreateInfo) error {
	target.sType = (C.VkStructureType)(StructureTypePipelineMultisampleStateCreateInfo)
	target.pNext = p.Next
	target.flags = (C.VkPipelineMultisampleStateCreateFlags)(p.Flags)
	target.rasterizationSamples = (C.VkSampleCountFlagBits)(p.RasterizationSamples)
	target.sampleShadingEnable = ToBool32(p.SampleShadingEnable)
	target.minSampleShading = (C.float)(p.MinSampleShading)
	if len(p.SampleMask) > 0 {
		target.pSampleMask = (*C.VkSampleMask)(unsafe.Pointer(&p.SampleMask[0]))
	}
	target.alphaToCoverageEnable = ToBool32(p.AlphaToCoverageEnable)
	target.alphaToOneEnable = ToBool32(p.AlphaToOneEnable)
	return nil
}

// StencilOpState as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkStencilOpState.html
type StencilOpState struct {
	FailOp      StencilOp
	PassOp      StencilOp
	DepthFailOp StencilOp
	CompareOp   CompareOp
	CompareMask uint32
	WriteMask   uint32
	Reference   uint32
}

func (s *StencilOpState) To(mem VulkanClientMemory, target *C.VkStencilOpState) {
	target.failOp = (C.VkStencilOp)(s.FailOp)
	target.passOp = (C.VkStencilOp)(s.PassOp)
	target.depthFailOp = (C.VkStencilOp)(s.DepthFailOp)
	target.compareOp = (C.VkCompareOp)(s.CompareOp)
	target.compareMask = (C.uint32_t)(s.CompareMask)
	target.writeMask = (C.uint32_t)(s.WriteMask)
	target.reference = (C.uint32_t)(s.Reference)
}

// PipelineDepthStencilStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineDepthStencilStateCreateInfo.html
type PipelineDepthStencilStateCreateInfo struct {
	Next                  unsafe.Pointer
	Flags                 PipelineDepthStencilStateCreateFlags
	DepthTestEnable       bool
	DepthWriteEnable      bool
	DepthCompareOp        CompareOp
	DepthBoundsTestEnable bool
	StencilTestEnable     bool
	Front                 StencilOpState
	Back                  StencilOpState
	MinDepthBounds        float32
	MaxDepthBounds        float32
}

func (p *PipelineDepthStencilStateCreateInfo) To(mem VulkanClientMemory, target *C.VkPipelineDepthStencilStateCreateInfo) {
	target.sType = (C.VkStructureType)(StructureTypePipelineDepthStencilStateCreateInfo)
	target.pNext = p.Next
	target.flags = (C.VkPipelineDepthStencilStateCreateFlags)(p.Flags)
	target.depthTestEnable = ToBool32(p.DepthTestEnable)
	target.depthWriteEnable = ToBool32(p.DepthWriteEnable)
	target.depthCompareOp = (C.VkCompareOp)(p.DepthCompareOp)
	target.depthBoundsTestEnable = ToBool32(p.DepthBoundsTestEnable)
	target.stencilTestEnable = ToBool32(p.StencilTestEnable)
	p.Front.To(mem, &target.front)
	p.Back.To(mem, &target.back)
	target.minDepthBounds = (C.float)(p.MinDepthBounds)
	target.maxDepthBounds = (C.float)(p.MaxDepthBounds)
}

// PipelineColorBlendAttachmentState as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineColorBlendAttachmentState.html
type PipelineColorBlendAttachmentState struct {
	BlendEnable         bool
	SrcColorBlendFactor BlendFactor
	DstColorBlendFactor BlendFactor
	ColorBlendOp        BlendOp
	SrcAlphaBlendFactor BlendFactor
	DstAlphaBlendFactor BlendFactor
	AlphaBlendOp        BlendOp
	ColorWriteMask      ColorComponentFlags
}

func (p *PipelineColorBlendAttachmentState) To(mem VulkanClientMemory, target *C.VkPipelineColorBlendAttachmentState) {
	target.blendEnable = ToBool32(p.BlendEnable)
	target.srcColorBlendFactor = (C.VkBlendFactor)(p.SrcColorBlendFactor)
	target.dstColorBlendFactor = (C.VkBlendFactor)(p.DstColorBlendFactor)
	target.colorBlendOp = (C.VkBlendOp)(p.ColorBlendOp)
	target.srcAlphaBlendFactor = (C.VkBlendFactor)(p.SrcAlphaBlendFactor)
	target.dstAlphaBlendFactor = (C.VkBlendFactor)(p.DstAlphaBlendFactor)
	target.alphaBlendOp = (C.VkBlendOp)(p.AlphaBlendOp)
	target.colorWriteMask = (C.VkColorComponentFlags)(p.ColorWriteMask)
}

// PipelineColorBlendStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineColorBlendStateCreateInfo.html
type PipelineColorBlendStateCreateInfo struct {
	Next           unsafe.Pointer
	Flags          PipelineColorBlendStateCreateFlags
	LogicOpEnable  bool
	LogicOp        LogicOp
	Attachments    []PipelineColorBlendAttachmentState
	BlendConstants [4]float32
}

func (p *PipelineColorBlendStateCreateInfo) To(mem VulkanClientMemory, target *C.VkPipelineColorBlendStateCreateInfo) error {
	target.sType = (C.VkStructureType)(StructureTypePipelineColorBlendStateCreateInfo)
	target.pNext = p.Next
	target.flags = (C.VkPipelineColorBlendStateCreateFlags)(p.Flags)
	target.logicOpEnable = ToBool32(p.LogicOpEnable)
	target.logicOp = (C.VkLogicOp)(p.LogicOp)
	target.attachmentCount = (C.uint32_t)(len(p.Attachments))
	if len(p.Attachments) > 0 {
		pa := (*C.VkPipelineColorBlendAttachmentState)(mem.Calloc(len(p.Attachments), C.sizeof_VkPipelineColorBlendAttachmentState))
		target.pAttachments = pa
		for _, a := range p.Attachments {
			a.To(mem, pa)
			pa = (*C.VkPipelineColorBlendAttachmentState)((unsafe.Pointer)(uintptr(unsafe.Pointer(pa)) + C.sizeof_VkPipelineColorBlendAttachmentState))
		}
	}
	for i, v := range p.BlendConstants {
		target.blendConstants[i] = C.float(v)
	}
	return nil
}

// PipelineDynamicStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineDynamicStateCreateInfo.html
type PipelineDynamicStateCreateInfo struct {
	Next          unsafe.Pointer
	Flags         PipelineDynamicStateCreateFlags
	DynamicStates []DynamicState
}

func (p *PipelineDynamicStateCreateInfo) To(mem VulkanClientMemory, target *C.VkPipelineDynamicStateCreateInfo) func() {
	target.sType = (C.VkStructureType)(StructureTypePipelineDynamicStateCreateInfo)
	target.pNext = p.Next
	target.flags = (C.VkPipelineDynamicStateCreateFlags)(p.Flags)
	target.dynamicStateCount = (C.uint32_t)(len(p.DynamicStates))
	if len(p.DynamicStates) > 0 {
		mem := (*C.VkDynamicState)(mem.Calloc(len(p.DynamicStates), C.sizeof_VkDynamicState))
		target.pDynamicStates = mem
		for _, d := range p.DynamicStates {
			*mem = C.VkDynamicState(d)
			mem = (*C.VkDynamicState)((unsafe.Pointer)(uintptr(unsafe.Pointer(mem)) + C.sizeof_VkDynamicState))
		}
		return func() { C.free(unsafe.Pointer(target.pDynamicStates)) }
	}
	return func() {}
}

// GraphicsPipelineCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkGraphicsPipelineCreateInfo.html
type GraphicsPipelineCreateInfo struct {
	Next               unsafe.Pointer
	Flags              PipelineCreateFlags
	Stages             []PipelineShaderStageCreateInfo
	VertexInputState   *PipelineVertexInputStateCreateInfo
	InputAssemblyState *PipelineInputAssemblyStateCreateInfo
	TessellationState  *PipelineTessellationStateCreateInfo
	ViewportState      *PipelineViewportStateCreateInfo
	RasterizationState *PipelineRasterizationStateCreateInfo
	MultisampleState   *PipelineMultisampleStateCreateInfo
	DepthStencilState  *PipelineDepthStencilStateCreateInfo
	ColorBlendState    *PipelineColorBlendStateCreateInfo
	DynamicState       *PipelineDynamicStateCreateInfo
	Layout             PipelineLayout
	RenderPass         RenderPass
	Subpass            uint32
	BasePipelineHandle Pipeline
	BasePipelineIndex  int32
}

func (g *GraphicsPipelineCreateInfo) To(mem VulkanClientMemory, target *C.VkGraphicsPipelineCreateInfo) error {
	target.sType = (C.VkStructureType)(StructureTypeGraphicsPipelineCreateInfo)
	target.pNext = g.Next
	target.flags = (C.VkPipelineCreateFlags)(g.Flags)
	target.stageCount = (C.uint32_t)(len(g.Stages))
	if len(g.Stages) > 0 {
		paddr := (*C.VkPipelineShaderStageCreateInfo)(mem.Calloc(len(g.Stages), C.sizeof_VkPipelineShaderStageCreateInfo))
		target.pStages = paddr
		for _, s := range g.Stages {
			err := s.To(mem, paddr)
			if err != nil {
				return err
			}
			paddr = (*C.VkPipelineShaderStageCreateInfo)((unsafe.Pointer)(uintptr(unsafe.Pointer(paddr)) + C.sizeof_VkPipelineShaderStageCreateInfo))
		}
	}
	if g.VertexInputState != nil {
		lmem := (*C.VkPipelineVertexInputStateCreateInfo)(mem.Calloc(1, C.sizeof_VkPipelineVertexInputStateCreateInfo))
		err := g.VertexInputState.To(mem, lmem)
		if err != nil {
			return err
		}
		target.pVertexInputState = lmem
	}
	if g.InputAssemblyState != nil {
		lmem := (*C.VkPipelineInputAssemblyStateCreateInfo)(mem.Calloc(1, C.sizeof_VkPipelineInputAssemblyStateCreateInfo))
		g.InputAssemblyState.To(mem, lmem)
		target.pInputAssemblyState = lmem
	}
	if g.TessellationState != nil {
		lmem := (*C.VkPipelineTessellationStateCreateInfo)(mem.Calloc(1, C.sizeof_VkPipelineTessellationStateCreateInfo))
		g.TessellationState.To(mem, lmem)
		target.pTessellationState = lmem
	}
	if g.ViewportState != nil {
		lmem := (*C.VkPipelineViewportStateCreateInfo)(mem.Calloc(1, C.sizeof_VkPipelineViewportStateCreateInfo))
		err := g.ViewportState.To(mem, lmem)
		if err != nil {
			return err
		}
		target.pViewportState = lmem
	}
	if g.RasterizationState != nil {
		lmem := (*C.VkPipelineRasterizationStateCreateInfo)(mem.Calloc(1, C.sizeof_VkPipelineRasterizationStateCreateInfo))
		g.RasterizationState.To(mem, lmem)
		target.pRasterizationState = lmem
	}
	if g.MultisampleState != nil {
		lmem := (*C.VkPipelineMultisampleStateCreateInfo)(mem.Calloc(1, C.sizeof_VkPipelineMultisampleStateCreateInfo))
		err := g.MultisampleState.To(mem, lmem)
		if err != nil {
			return err
		}
		target.pMultisampleState = lmem
	}
	if g.DepthStencilState != nil {
		lmem := (*C.VkPipelineDepthStencilStateCreateInfo)(mem.Calloc(1, C.sizeof_VkPipelineDepthStencilStateCreateInfo))
		g.DepthStencilState.To(mem, lmem)
		target.pDepthStencilState = lmem
	}
	if g.ColorBlendState != nil {
		lmem := (*C.VkPipelineColorBlendStateCreateInfo)(mem.Calloc(1, C.sizeof_VkPipelineColorBlendStateCreateInfo))
		err := g.ColorBlendState.To(mem, lmem)
		if err != nil {
			return err
		}
		target.pColorBlendState = lmem
	}
	if g.DynamicState != nil {
		lmem := (*C.VkPipelineDynamicStateCreateInfo)(mem.Calloc(1, C.sizeof_VkPipelineDynamicStateCreateInfo))
		g.DynamicState.To(mem, lmem)
		target.pDynamicState = lmem
	}
	target.layout = (C.VkPipelineLayout)(g.Layout)
	target.renderPass = (C.VkRenderPass)(g.RenderPass)
	target.subpass = (C.uint32_t)(g.Subpass)
	target.basePipelineHandle = (C.VkPipeline)(g.BasePipelineHandle)
	target.basePipelineIndex = (C.int32_t)(g.BasePipelineIndex)
	return nil
}

// PushConstantRange as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPushConstantRange.html
type PushConstantRange struct {
	StageFlags ShaderStageFlags
	Offset     uint32
	Size       uint32
}

func (p *PushConstantRange) To(mem VulkanClientMemory, target *C.VkPushConstantRange) {
	target.stageFlags = (C.VkShaderStageFlags)(p.StageFlags)
	target.offset = (C.uint32_t)(p.Offset)
	target.size = (C.uint32_t)(p.Size)
}

// PipelineLayoutCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineLayoutCreateInfo.html
type PipelineLayoutCreateInfo struct {
	Next               unsafe.Pointer
	Flags              PipelineLayoutCreateFlags
	SetLayouts         []DescriptorSetLayout
	PushConstantRanges []PushConstantRange
}

func (p *PipelineLayoutCreateInfo) To(mem VulkanClientMemory, target *C.VkPipelineLayoutCreateInfo) error {
	target.sType = (C.VkStructureType)(StructureTypePipelineLayoutCreateInfo)
	target.pNext = p.Next
	target.flags = (C.VkPipelineLayoutCreateFlags)(p.Flags)
	target.setLayoutCount = (C.uint32_t)(len(p.SetLayouts))
	if len(p.SetLayouts) > 0 {
		target.pSetLayouts = (*C.VkDescriptorSetLayout)(unsafe.Pointer(&p.SetLayouts[0]))
	}
	target.pushConstantRangeCount = (C.uint32_t)(len(p.PushConstantRanges))
	if len(p.PushConstantRanges) > 0 {
		pcrs := make([]C.VkPushConstantRange, len(p.PushConstantRanges))
		for i, pcr := range p.PushConstantRanges {
			pcr.To(mem, &pcrs[i])
		}
		target.pPushConstantRanges = (*C.VkPushConstantRange)(unsafe.Pointer(&pcrs[0]))
	}
	return nil
}

// SamplerCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerCreateInfo.html
type SamplerCreateInfo struct {
	// SType                   StructureType
	Next                    unsafe.Pointer
	Flags                   SamplerCreateFlags
	MagFilter               Filter
	MinFilter               Filter
	MipmapMode              SamplerMipmapMode
	AddressModeU            SamplerAddressMode
	AddressModeV            SamplerAddressMode
	AddressModeW            SamplerAddressMode
	MipLodBias              float32
	AnisotropyEnable        bool
	MaxAnisotropy           float32
	CompareEnable           bool
	CompareOp               CompareOp
	MinLod                  float32
	MaxLod                  float32
	BorderColor             BorderColor
	UnnormalizedCoordinates bool
}

// CopyDescriptorSet as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCopyDescriptorSet.html
type CopyDescriptorSet struct {
	// SType           StructureType
	Next            unsafe.Pointer
	SrcSet          DescriptorSet
	SrcBinding      uint32
	SrcArrayElement uint32
	DstSet          DescriptorSet
	DstBinding      uint32
	DstArrayElement uint32
	DescriptorCount uint32
}

// DescriptorBufferInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorBufferInfo.html
type DescriptorBufferInfo struct {
	Buffer Buffer
	Offset DeviceSize
	Range  DeviceSize
}

// DescriptorImageInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorImageInfo.html
type DescriptorImageInfo struct {
	Sampler     Sampler
	ImageView   ImageView
	ImageLayout ImageLayout
}

// DescriptorPoolSize as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorPoolSize.html
type DescriptorPoolSize struct {
	Type            DescriptorType
	DescriptorCount uint32
}

// DescriptorPoolCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorPoolCreateInfo.html
type DescriptorPoolCreateInfo struct {
	// SType         StructureType
	Next          unsafe.Pointer
	Flags         DescriptorPoolCreateFlags
	MaxSets       uint32
	PoolSizeCount uint32
	PPoolSizes    []DescriptorPoolSize
}

// DescriptorSetAllocateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorSetAllocateInfo.html
type DescriptorSetAllocateInfo struct {
	// SType              StructureType
	Next               unsafe.Pointer
	DescriptorPool     DescriptorPool
	DescriptorSetCount uint32
	PSetLayouts        []DescriptorSetLayout
}

// DescriptorSetLayoutBinding as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorSetLayoutBinding.html
type DescriptorSetLayoutBinding struct {
	Binding            uint32
	DescriptorType     DescriptorType
	DescriptorCount    uint32
	StageFlags         ShaderStageFlags
	PImmutableSamplers []Sampler
}

// DescriptorSetLayoutCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorSetLayoutCreateInfo.html
type DescriptorSetLayoutCreateInfo struct {
	// SType        StructureType
	Next         unsafe.Pointer
	Flags        DescriptorSetLayoutCreateFlags
	BindingCount uint32
	PBindings    []DescriptorSetLayoutBinding
}

// WriteDescriptorSet as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkWriteDescriptorSet.html
type WriteDescriptorSet struct {
	// SType            StructureType
	Next             unsafe.Pointer
	DstSet           DescriptorSet
	DstBinding       uint32
	DstArrayElement  uint32
	DescriptorCount  uint32
	DescriptorType   DescriptorType
	PImageInfo       []DescriptorImageInfo
	PBufferInfo      []DescriptorBufferInfo
	PTexelBufferView []BufferView
}

// AttachmentDescription as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAttachmentDescription.html
type AttachmentDescription struct {
	Flags          AttachmentDescriptionFlags
	Format         Format
	Samples        SampleCountFlagBits
	LoadOp         AttachmentLoadOp
	StoreOp        AttachmentStoreOp
	StencilLoadOp  AttachmentLoadOp
	StencilStoreOp AttachmentStoreOp
	InitialLayout  ImageLayout
	FinalLayout    ImageLayout
}

func (a *AttachmentDescription) To(mem VulkanClientMemory, target *C.VkAttachmentDescription) {
	target.flags = (C.VkAttachmentDescriptionFlags)(a.Flags)
	target.format = (C.VkFormat)(a.Format)
	target.samples = (C.VkSampleCountFlagBits)(a.Samples)
	target.loadOp = (C.VkAttachmentLoadOp)(a.LoadOp)
	target.storeOp = (C.VkAttachmentStoreOp)(a.StoreOp)
	target.stencilLoadOp = (C.VkAttachmentLoadOp)(a.StencilLoadOp)
	target.stencilStoreOp = (C.VkAttachmentStoreOp)(a.StencilStoreOp)
	target.initialLayout = (C.VkImageLayout)(a.InitialLayout)
	target.finalLayout = (C.VkImageLayout)(a.FinalLayout)
}

// AttachmentReference as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAttachmentReference.html
type AttachmentReference struct {
	Attachment uint32
	Layout     ImageLayout
}

func (a *AttachmentReference) To(mem VulkanClientMemory, target *C.VkAttachmentReference) {
	target.attachment = (C.uint32_t)(a.Attachment)
	target.layout = (C.VkImageLayout)(a.Layout)
}

// FramebufferCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFramebufferCreateInfo.html
type FramebufferCreateInfo struct {
	Next        unsafe.Pointer
	Flags       FramebufferCreateFlags
	RenderPass  RenderPass
	Attachments []ImageView
	Width       uint32
	Height      uint32
	Layers      uint32
}

func (f *FramebufferCreateInfo) To(mem VulkanClientMemory, target *C.VkFramebufferCreateInfo) error {
	target.sType = (C.VkStructureType)(StructureTypeFramebufferCreateInfo)
	target.pNext = f.Next
	target.flags = (C.VkFramebufferCreateFlags)(f.Flags)
	target.renderPass = (C.VkRenderPass)(f.RenderPass)
	target.attachmentCount = (C.uint32_t)(len(f.Attachments))
	if len(f.Attachments) > 0 {
		lmem := (*C.VkImageView)(mem.Calloc(len(f.Attachments), C.sizeof_VkImageView))
		target.pAttachments = lmem
		for _, iv := range f.Attachments {
			*lmem = (C.VkImageView)(iv)
			lmem = (*C.VkImageView)((unsafe.Pointer)(uintptr(unsafe.Pointer(lmem)) + C.sizeof_VkImageView))
		}
	}
	target.width = (C.uint32_t)(f.Width)
	target.height = (C.uint32_t)(f.Height)
	target.layers = (C.uint32_t)(f.Layers)
	return nil
}

// SubpassDescription as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSubpassDescription.html
type SubpassDescription struct {
	Flags                  SubpassDescriptionFlags
	PipelineBindPoint      PipelineBindPoint
	InputAttachments       []AttachmentReference
	ColorAttachments       []AttachmentReference
	ResolveAttachments     []AttachmentReference
	DepthStencilAttachment *AttachmentReference
	PreserveAttachments    []uint32
}

func (s *SubpassDescription) To(mem VulkanClientMemory, target *C.VkSubpassDescription) error {
	target.flags = (C.VkSubpassDescriptionFlags)(s.Flags)
	target.pipelineBindPoint = (C.VkPipelineBindPoint)(s.PipelineBindPoint)
	target.inputAttachmentCount = (C.uint32_t)(len(s.InputAttachments))
	if len(s.InputAttachments) > 0 {
		ars := (*C.VkAttachmentReference)(mem.Calloc(len(s.InputAttachments), C.sizeof_VkAttachmentReference))
		target.pInputAttachments = ars
		for _, a := range s.InputAttachments {
			a.To(mem, ars)
			ars = (*C.VkAttachmentReference)((unsafe.Pointer)(uintptr(unsafe.Pointer(ars)) + C.sizeof_VkAttachmentReference))
		}
	}
	target.colorAttachmentCount = (C.uint32_t)(len(s.ColorAttachments))
	if len(s.ColorAttachments) > 0 {
		ars := (*C.VkAttachmentReference)(mem.Calloc(len(s.ColorAttachments), C.sizeof_VkAttachmentReference))
		target.pColorAttachments = ars
		for _, a := range s.ColorAttachments {
			a.To(mem, ars)
			ars = (*C.VkAttachmentReference)((unsafe.Pointer)(uintptr(unsafe.Pointer(ars)) + C.sizeof_VkAttachmentReference))
		}
	}

	if len(s.ResolveAttachments) > 0 {
		ars := (*C.VkAttachmentReference)(mem.Calloc(len(s.ResolveAttachments), C.sizeof_VkAttachmentReference))
		target.pResolveAttachments = ars
		for _, a := range s.ResolveAttachments {
			a.To(mem, ars)
			ars = (*C.VkAttachmentReference)((unsafe.Pointer)(uintptr(unsafe.Pointer(ars)) + C.sizeof_VkAttachmentReference))
		}
	}
	if s.DepthStencilAttachment != nil {
		var a C.VkAttachmentReference
		s.DepthStencilAttachment.To(mem, &a)
		target.pDepthStencilAttachment = (*C.VkAttachmentReference)(unsafe.Pointer(&a))
	}
	target.preserveAttachmentCount = (C.uint32_t)(len(s.PreserveAttachments))
	if len(s.PreserveAttachments) > 0 {
		ars := (*C.uint32_t)(mem.Calloc(len(s.PreserveAttachments), C.sizeof_uint32_t))
		target.pPreserveAttachments = ars
		for _, a := range s.PreserveAttachments {
			*ars = C.uint32_t(a)
			ars = (*C.uint32_t)((unsafe.Pointer)(uintptr(unsafe.Pointer(ars)) + C.sizeof_uint32_t))
		}
	}
	return nil
}

// SubpassDependency as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSubpassDependency.html
type SubpassDependency struct {
	SrcSubpass      uint32
	DstSubpass      uint32
	SrcStageMask    PipelineStageFlags
	DstStageMask    PipelineStageFlags
	SrcAccessMask   AccessFlags
	DstAccessMask   AccessFlags
	DependencyFlags DependencyFlags
}

func (s *SubpassDependency) To(mem VulkanClientMemory, target *C.VkSubpassDependency) {
	target.srcSubpass = (C.uint32_t)(s.SrcSubpass)
	target.dstSubpass = (C.uint32_t)(s.DstSubpass)
	target.srcStageMask = (C.VkPipelineStageFlags)(s.SrcStageMask)
	target.dstStageMask = (C.VkPipelineStageFlags)(s.DstStageMask)
	target.srcAccessMask = (C.VkAccessFlags)(s.SrcAccessMask)
	target.dstAccessMask = (C.VkAccessFlags)(s.DstAccessMask)
	target.dependencyFlags = (C.VkDependencyFlags)(s.DependencyFlags)
}

// RenderPassCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkRenderPassCreateInfo.html
type RenderPassCreateInfo struct {
	Next         unsafe.Pointer
	Flags        RenderPassCreateFlags
	Attachments  []AttachmentDescription
	Subpasses    []SubpassDescription
	Dependencies []SubpassDependency
}

func (r *RenderPassCreateInfo) To(mem VulkanClientMemory, target *C.VkRenderPassCreateInfo) error {
	target.sType = (C.VkStructureType)(StructureTypeRenderPassCreateInfo)
	target.pNext = r.Next
	target.flags = (C.VkRenderPassCreateFlags)(r.Flags)
	target.attachmentCount = (C.uint32_t)(len(r.Attachments))
	if len(r.Attachments) > 0 {
		ars := (*C.VkAttachmentDescription)(mem.Calloc(len(r.Attachments), C.sizeof_VkAttachmentDescription))
		target.pAttachments = ars
		for _, a := range r.Attachments {
			a.To(mem, ars)
			ars = (*C.VkAttachmentDescription)((unsafe.Pointer)(uintptr(unsafe.Pointer(ars)) + C.sizeof_VkAttachmentDescription))
		}
	}
	target.subpassCount = (C.uint32_t)(len(r.Subpasses))
	if len(r.Subpasses) > 0 {
		ars := (*C.VkSubpassDescription)(mem.Calloc(len(r.Subpasses), C.sizeof_VkSubpassDescription))
		target.pSubpasses = ars
		for _, a := range r.Subpasses {
			err := a.To(mem, ars)
			if err != nil {
				return err
			}
			ars = (*C.VkSubpassDescription)((unsafe.Pointer)(uintptr(unsafe.Pointer(ars)) + C.sizeof_VkSubpassDescription))
		}
	}

	target.dependencyCount = (C.uint32_t)(len(r.Dependencies))
	if len(r.Dependencies) > 0 {
		ars := (*C.VkSubpassDependency)(mem.Calloc(len(r.Attachments), C.sizeof_VkSubpassDependency))
		target.pDependencies = ars
		for _, a := range r.Dependencies {
			a.To(mem, ars)
			ars = (*C.VkSubpassDependency)((unsafe.Pointer)(uintptr(unsafe.Pointer(ars)) + C.sizeof_VkSubpassDependency))
		}
	}

	return nil
}

// CommandPoolCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandPoolCreateInfo.html
type CommandPoolCreateInfo struct {
	Next             unsafe.Pointer
	Flags            CommandPoolCreateFlags
	QueueFamilyIndex uint32
}

func (c *CommandPoolCreateInfo) To(mem VulkanClientMemory, target *C.VkCommandPoolCreateInfo) {
	target.sType = (C.VkStructureType)(StructureTypeCommandPoolCreateInfo)
	target.pNext = c.Next
	target.flags = (C.VkCommandPoolCreateFlags)(c.Flags)
	target.queueFamilyIndex = (C.uint32_t)(c.QueueFamilyIndex)
}

// CommandBufferAllocateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandBufferAllocateInfo.html
type CommandBufferAllocateInfo struct {
	Next               unsafe.Pointer
	CommandPool        CommandPool
	Level              CommandBufferLevel
	CommandBufferCount uint32
}

func (c *CommandBufferAllocateInfo) To(mem VulkanClientMemory, target *C.VkCommandBufferAllocateInfo) {
	target.sType = (C.VkStructureType)(StructureTypeCommandBufferAllocateInfo)
	target.pNext = c.Next
	target.commandPool = (C.VkCommandPool)(c.CommandPool)
	target.level = (C.VkCommandBufferLevel)(c.Level)
	target.commandBufferCount = (C.uint32_t)(c.CommandBufferCount)
}

// CommandBufferInheritanceInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandBufferInheritanceInfo.html
type CommandBufferInheritanceInfo struct {
	Next                 unsafe.Pointer
	RenderPass           RenderPass
	Subpass              uint32
	Framebuffer          Framebuffer
	OcclusionQueryEnable bool
	QueryFlags           QueryControlFlags
	PipelineStatistics   QueryPipelineStatisticFlags
}

func (b *CommandBufferInheritanceInfo) To(mem VulkanClientMemory, target *C.VkCommandBufferInheritanceInfo) {
	target.sType = (C.VkStructureType)(StructureTypeCommandBufferInheritanceInfo)
	target.pNext = b.Next
	target.renderPass = (C.VkRenderPass)(b.RenderPass)
	target.subpass = (C.uint32_t)(b.Subpass)
	target.framebuffer = (C.VkFramebuffer)(b.Framebuffer)
	target.occlusionQueryEnable = ToBool32(b.OcclusionQueryEnable)
	target.queryFlags = (C.VkQueryControlFlags)(b.QueryFlags)
	target.pipelineStatistics = (C.VkQueryPipelineStatisticFlags)(b.PipelineStatistics)
}

// CommandBufferBeginInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandBufferBeginInfo.html
type CommandBufferBeginInfo struct {
	Next            unsafe.Pointer
	Flags           CommandBufferUsageFlags
	InheritanceInfo *CommandBufferInheritanceInfo
}

func (b *CommandBufferBeginInfo) To(mem VulkanClientMemory, target *C.VkCommandBufferBeginInfo) error {
	target.sType = (C.VkStructureType)(StructureTypeCommandBufferBeginInfo)
	target.pNext = b.Next
	target.flags = (C.VkCommandBufferUsageFlags)(b.Flags)
	if b.InheritanceInfo != nil {
		temp := (*C.VkCommandBufferInheritanceInfo)(mem.Calloc(1, C.sizeof_VkCommandBufferInheritanceInfo))
		b.InheritanceInfo.To(mem, temp)
		target.pInheritanceInfo = temp
		return nil
	}
	return nil
}

// BufferCopy as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBufferCopy.html
type BufferCopy struct {
	SrcOffset DeviceSize
	DstOffset DeviceSize
	Size      DeviceSize
}

// ImageSubresourceLayers as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageSubresourceLayers.html
type ImageSubresourceLayers struct {
	AspectMask     ImageAspectFlags
	MipLevel       uint32
	BaseArrayLayer uint32
	LayerCount     uint32
}

// BufferImageCopy as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBufferImageCopy.html
type BufferImageCopy struct {
	BufferOffset      DeviceSize
	BufferRowLength   uint32
	BufferImageHeight uint32
	ImageSubresource  ImageSubresourceLayers
	ImageOffset       Offset3D
	ImageExtent       Extent3D
}

// ClearColorValue as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkClearColorValue.html
const sizeofClearColorValue = unsafe.Sizeof(C.VkClearColorValue{})

type ClearColorValue [sizeofClearColorValue]byte

// ClearDepthStencilValue as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkClearDepthStencilValue.html
type ClearDepthStencilValue struct {
	Depth   float32
	Stencil uint32
}

func (d *ClearDepthStencilValue) To(target *C.VkClearDepthStencilValue) {
	target.depth = C.float(d.Depth)
	target.stencil = C.uint32_t(d.Stencil)
}

// ClearValue as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkClearValue.html
const sizeofClearValue = unsafe.Sizeof(C.VkClearValue{})

type ClearValue [sizeofClearValue]byte

func (c *ClearValue) SetColorFloats(floats [4]float32) {
	C.vkClearValue_write_color_floats((*C.VkClearValue)(unsafe.Pointer(&c[0])), (*C.float)(unsafe.Pointer(&floats[0])))
}

func (c *ClearValue) SetColorInts(ints [4]int32) {
	C.vkClearValue_write_color_ints((*C.VkClearValue)(unsafe.Pointer(&c[0])), (*C.int32_t)(unsafe.Pointer(&ints[0])))
}

func (c *ClearValue) SetColorUints(uints [4]uint32) {
	C.vkClearValue_write_color_uints((*C.VkClearValue)(unsafe.Pointer(&c[0])), (*C.uint32_t)(unsafe.Pointer(&uints[0])))
}

func (c *ClearValue) SetDepthStencil(ds ClearDepthStencilValue) {
	var cDSV C.VkClearDepthStencilValue
	ds.To(&cDSV)
	C.vkClearValue_write_depth_stencil((*C.VkClearValue)(unsafe.Pointer(&c[0])), (*C.VkClearDepthStencilValue)(&cDSV))
}

// ClearAttachment as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkClearAttachment.html
type ClearAttachment struct {
	AspectMask      ImageAspectFlags
	ColorAttachment uint32
	ClearValue      ClearValue
}

// ClearRect as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkClearRect.html
type ClearRect struct {
	Rect           Rect2D
	BaseArrayLayer uint32
	LayerCount     uint32
}

// ImageBlit as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageBlit.html
type ImageBlit struct {
	SrcSubresource ImageSubresourceLayers
	SrcOffsets     [2]Offset3D
	DstSubresource ImageSubresourceLayers
	DstOffsets     [2]Offset3D
}

// ImageCopy as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageCopy.html
type ImageCopy struct {
	SrcSubresource ImageSubresourceLayers
	SrcOffset      Offset3D
	DstSubresource ImageSubresourceLayers
	DstOffset      Offset3D
	Extent         Extent3D
}

// ImageResolve as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageResolve.html
type ImageResolve struct {
	SrcSubresource ImageSubresourceLayers
	SrcOffset      Offset3D
	DstSubresource ImageSubresourceLayers
	DstOffset      Offset3D
	Extent         Extent3D
}

// RenderPassBeginInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkRenderPassBeginInfo.html
type RenderPassBeginInfo struct {
	Type        StructureType
	Next        unsafe.Pointer
	RenderPass  RenderPass
	Framebuffer Framebuffer
	RenderArea  Rect2D
	ClearValues []ClearValue
}

func (r *RenderPassBeginInfo) To(mem VulkanClientMemory, target *C.VkRenderPassBeginInfo) {
	target.sType = (C.VkStructureType)(StructureTypeRenderPassBeginInfo)
	target.pNext = r.Next
	target.renderPass = (C.VkRenderPass)(r.RenderPass)
	target.framebuffer = (C.VkFramebuffer)(r.Framebuffer)
	r.RenderArea.To(mem, &target.renderArea)
	target.clearValueCount = (C.uint32_t)(len(r.ClearValues))
	if len(r.ClearValues) > 0 {
		pcv := (*C.VkClearValue)(mem.Calloc(len(r.ClearValues), C.sizeof_VkClearValue))
		target.pClearValues = pcv
		for _, b := range r.ClearValues {
			Memcopy(unsafe.Pointer(pcv), b[:])
			pcv = (*C.VkClearValue)((unsafe.Pointer)(uintptr(unsafe.Pointer(pcv)) + C.sizeof_VkClearValue))
		}
		return
	}
	return
}

// SamplerYcbcrConversion as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerYcbcrConversion.html
type SamplerYcbcrConversion C.VkSamplerYcbcrConversion

// DescriptorUpdateTemplate as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorUpdateTemplate.html
type DescriptorUpdateTemplate C.VkDescriptorUpdateTemplate

// SubgroupFeatureFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSubgroupFeatureFlags.html
type SubgroupFeatureFlags uint32

// PeerMemoryFeatureFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPeerMemoryFeatureFlags.html
type PeerMemoryFeatureFlags uint32

// MemoryAllocateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryAllocateFlags.html
type MemoryAllocateFlags uint32

// CommandPoolTrimFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandPoolTrimFlags.html
type CommandPoolTrimFlags uint32

// DescriptorUpdateTemplateCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorUpdateTemplateCreateFlags.html
type DescriptorUpdateTemplateCreateFlags uint32

// ExternalMemoryHandleTypeFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalMemoryHandleTypeFlags.html
type ExternalMemoryHandleTypeFlags uint32

// ExternalMemoryFeatureFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalMemoryFeatureFlags.html
type ExternalMemoryFeatureFlags uint32

// ExternalFenceHandleTypeFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalFenceHandleTypeFlags.html
type ExternalFenceHandleTypeFlags uint32

// ExternalFenceFeatureFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalFenceFeatureFlags.html
type ExternalFenceFeatureFlags uint32

// FenceImportFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFenceImportFlags.html
type FenceImportFlags uint32

// SemaphoreImportFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSemaphoreImportFlags.html
type SemaphoreImportFlags uint32

// ExternalSemaphoreHandleTypeFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalSemaphoreHandleTypeFlags.html
type ExternalSemaphoreHandleTypeFlags uint32

// ExternalSemaphoreFeatureFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalSemaphoreFeatureFlags.html
type ExternalSemaphoreFeatureFlags uint32

// PhysicalDeviceSubgroupProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceSubgroupProperties.html
type PhysicalDeviceSubgroupProperties struct {
	// SType                     StructureType
	Next                      unsafe.Pointer
	SubgroupSize              uint32
	SupportedStages           ShaderStageFlags
	SupportedOperations       SubgroupFeatureFlags
	QuadOperationsInAllStages bool
}

// BindBufferMemoryInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBindBufferMemoryInfo.html
type BindBufferMemoryInfo struct {
	// SType        StructureType
	Next         unsafe.Pointer
	Buffer       Buffer
	Memory       DeviceMemory
	MemoryOffset DeviceSize
}

// BindImageMemoryInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBindImageMemoryInfo.html
type BindImageMemoryInfo struct {
	// SType        StructureType
	Next         unsafe.Pointer
	Image        Image
	Memory       DeviceMemory
	MemoryOffset DeviceSize
}

// PhysicalDevice16BitStorageFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDevice16BitStorageFeatures.html
type PhysicalDevice16BitStorageFeatures struct {
	// SType                              StructureType
	Next                               unsafe.Pointer
	StorageBuffer16BitAccess           bool
	UniformAndStorageBuffer16BitAccess bool
	StoragePushConstant16              bool
	StorageInputOutput16               bool
}

// MemoryDedicatedRequirements as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryDedicatedRequirements.html
type MemoryDedicatedRequirements struct {
	// SType                       StructureType
	Next                        unsafe.Pointer
	PrefersDedicatedAllocation  bool
	RequiresDedicatedAllocation bool
}

// MemoryDedicatedAllocateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryDedicatedAllocateInfo.html
type MemoryDedicatedAllocateInfo struct {
	// SType  StructureType
	Next   unsafe.Pointer
	Image  Image
	Buffer Buffer
}

// MemoryAllocateFlagsInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryAllocateFlagsInfo.html
type MemoryAllocateFlagsInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	Flags      MemoryAllocateFlags
	DeviceMask uint32
}

// DeviceGroupRenderPassBeginInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceGroupRenderPassBeginInfo.html
type DeviceGroupRenderPassBeginInfo struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	DeviceMask            uint32
	DeviceRenderAreaCount uint32
	PDeviceRenderAreas    []Rect2D
}

// DeviceGroupCommandBufferBeginInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceGroupCommandBufferBeginInfo.html
type DeviceGroupCommandBufferBeginInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	DeviceMask uint32
}

// DeviceGroupSubmitInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceGroupSubmitInfo.html
type DeviceGroupSubmitInfo struct {
	// SType                         StructureType
	Next                          unsafe.Pointer
	WaitSemaphoreCount            uint32
	PWaitSemaphoreDeviceIndices   []uint32
	CommandBufferCount            uint32
	PCommandBufferDeviceMasks     []uint32
	SignalSemaphoreCount          uint32
	PSignalSemaphoreDeviceIndices []uint32
}

// DeviceGroupBindSparseInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceGroupBindSparseInfo.html
type DeviceGroupBindSparseInfo struct {
	// SType               StructureType
	Next                unsafe.Pointer
	ResourceDeviceIndex uint32
	MemoryDeviceIndex   uint32
}

// BindBufferMemoryDeviceGroupInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBindBufferMemoryDeviceGroupInfo.html
type BindBufferMemoryDeviceGroupInfo struct {
	// SType            StructureType
	Next             unsafe.Pointer
	DeviceIndexCount uint32
	PDeviceIndices   []uint32
}

// BindImageMemoryDeviceGroupInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBindImageMemoryDeviceGroupInfo.html
type BindImageMemoryDeviceGroupInfo struct {
	// SType                        StructureType
	Next                         unsafe.Pointer
	DeviceIndexCount             uint32
	PDeviceIndices               []uint32
	SplitInstanceBindRegionCount uint32
	PSplitInstanceBindRegions    []Rect2D
}

// PhysicalDeviceGroupProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceGroupProperties.html
type PhysicalDeviceGroupProperties struct {
	// SType               StructureType
	Next                unsafe.Pointer
	PhysicalDeviceCount uint32
	PhysicalDevices     [32]PhysicalDevice
	SubsetAllocation    bool
}

// DeviceGroupDeviceCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceGroupDeviceCreateInfo.html
type DeviceGroupDeviceCreateInfo struct {
	// SType               StructureType
	Next                unsafe.Pointer
	PhysicalDeviceCount uint32
	PPhysicalDevices    []PhysicalDevice
}

// BufferMemoryRequirementsInfo2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBufferMemoryRequirementsInfo2.html
type BufferMemoryRequirementsInfo2 struct {
	// SType  StructureType
	Next   unsafe.Pointer
	Buffer Buffer
}

// ImageMemoryRequirementsInfo2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageMemoryRequirementsInfo2.html
type ImageMemoryRequirementsInfo2 struct {
	// SType StructureType
	Next  unsafe.Pointer
	Image Image
}

// ImageSparseMemoryRequirementsInfo2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageSparseMemoryRequirementsInfo2.html
type ImageSparseMemoryRequirementsInfo2 struct {
	// SType StructureType
	Next  unsafe.Pointer
	Image Image
}

// MemoryRequirements2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryRequirements2.html
type MemoryRequirements2 struct {
	// SType              StructureType
	Next               unsafe.Pointer
	MemoryRequirements MemoryRequirements
}

// SparseImageMemoryRequirements2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSparseImageMemoryRequirements2.html
type SparseImageMemoryRequirements2 struct {
	// SType              StructureType
	Next               unsafe.Pointer
	MemoryRequirements SparseImageMemoryRequirements
}

// PhysicalDeviceFeatures2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceFeatures2.html
type PhysicalDeviceFeatures2 struct {
	// SType    StructureType
	Next     unsafe.Pointer
	Features PhysicalDeviceFeatures
}

// PhysicalDeviceProperties2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceProperties2.html
type PhysicalDeviceProperties2 struct {
	// SType      StructureType
	Next       unsafe.Pointer
	Properties PhysicalDeviceProperties
}

// FormatProperties2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFormatProperties2.html
type FormatProperties2 struct {
	// SType            StructureType
	Next             unsafe.Pointer
	FormatProperties FormatProperties
}

// ImageFormatProperties2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageFormatProperties2.html
type ImageFormatProperties2 struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	ImageFormatProperties ImageFormatProperties
}

// PhysicalDeviceImageFormatInfo2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceImageFormatInfo2.html
type PhysicalDeviceImageFormatInfo2 struct {
	// SType  StructureType
	Next   unsafe.Pointer
	Format Format
	Type   ImageType
	Tiling ImageTiling
	Usage  ImageUsageFlags
	Flags  ImageCreateFlags
}

// QueueFamilyProperties2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueueFamilyProperties2.html
type QueueFamilyProperties2 struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	QueueFamilyProperties QueueFamilyProperties
}

// PhysicalDeviceMemoryProperties2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceMemoryProperties2.html
type PhysicalDeviceMemoryProperties2 struct {
	// SType            StructureType
	Next             unsafe.Pointer
	MemoryProperties PhysicalDeviceMemoryProperties
}

// SparseImageFormatProperties2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSparseImageFormatProperties2.html
type SparseImageFormatProperties2 struct {
	// SType      StructureType
	Next       unsafe.Pointer
	Properties SparseImageFormatProperties
}

// PhysicalDeviceSparseImageFormatInfo2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceSparseImageFormatInfo2.html
type PhysicalDeviceSparseImageFormatInfo2 struct {
	// SType   StructureType
	Next    unsafe.Pointer
	Format  Format
	Type    ImageType
	Samples SampleCountFlagBits
	Usage   ImageUsageFlags
	Tiling  ImageTiling
}

// PhysicalDevicePointClippingProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDevicePointClippingProperties.html
type PhysicalDevicePointClippingProperties struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	PointClippingBehavior PointClippingBehavior
}

// InputAttachmentAspectReference as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkInputAttachmentAspectReference.html
type InputAttachmentAspectReference struct {
	Subpass              uint32
	InputAttachmentIndex uint32
	AspectMask           ImageAspectFlags
}

// RenderPassInputAttachmentAspectCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkRenderPassInputAttachmentAspectCreateInfo.html
type RenderPassInputAttachmentAspectCreateInfo struct {
	// SType                StructureType
	Next                 unsafe.Pointer
	AspectReferenceCount uint32
	PAspectReferences    []InputAttachmentAspectReference
}

// ImageViewUsageCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageViewUsageCreateInfo.html
type ImageViewUsageCreateInfo struct {
	// SType StructureType
	Next  unsafe.Pointer
	Usage ImageUsageFlags
}

// PipelineTessellationDomainOriginStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineTessellationDomainOriginStateCreateInfo.html
type PipelineTessellationDomainOriginStateCreateInfo struct {
	// SType        StructureType
	Next         unsafe.Pointer
	DomainOrigin TessellationDomainOrigin
}

// RenderPassMultiviewCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkRenderPassMultiviewCreateInfo.html
type RenderPassMultiviewCreateInfo struct {
	// SType                StructureType
	Next                 unsafe.Pointer
	SubpassCount         uint32
	PViewMasks           []uint32
	DependencyCount      uint32
	PViewOffsets         []int32
	CorrelationMaskCount uint32
	PCorrelationMasks    []uint32
}

// PhysicalDeviceMultiviewFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceMultiviewFeatures.html
type PhysicalDeviceMultiviewFeatures struct {
	// SType                       StructureType
	Next                        unsafe.Pointer
	Multiview                   bool
	MultiviewGeometryShader     bool
	MultiviewTessellationShader bool
}

// PhysicalDeviceMultiviewProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceMultiviewProperties.html
type PhysicalDeviceMultiviewProperties struct {
	// SType                     StructureType
	Next                      unsafe.Pointer
	MaxMultiviewViewCount     uint32
	MaxMultiviewInstanceIndex uint32
}

// PhysicalDeviceVariablePointersFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceVariablePointersFeatures.html
type PhysicalDeviceVariablePointersFeatures struct {
	// SType                         StructureType
	Next                          unsafe.Pointer
	VariablePointersStorageBuffer bool
	VariablePointers              bool
}

// PhysicalDeviceVariablePointerFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceVariablePointerFeatures.html
type PhysicalDeviceVariablePointerFeatures struct {
	// SType                         StructureType
	Next                          unsafe.Pointer
	VariablePointersStorageBuffer bool
	VariablePointers              bool
}

// PhysicalDeviceProtectedMemoryFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceProtectedMemoryFeatures.html
type PhysicalDeviceProtectedMemoryFeatures struct {
	// SType           StructureType
	Next            unsafe.Pointer
	ProtectedMemory bool
}

// PhysicalDeviceProtectedMemoryProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceProtectedMemoryProperties.html
type PhysicalDeviceProtectedMemoryProperties struct {
	// SType            StructureType
	Next             unsafe.Pointer
	ProtectedNoFault bool
}

// DeviceQueueInfo2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceQueueInfo2.html
type DeviceQueueInfo2 struct {
	// SType            StructureType
	Next             unsafe.Pointer
	Flags            DeviceQueueCreateFlags
	QueueFamilyIndex uint32
	QueueIndex       uint32
}

// ProtectedSubmitInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkProtectedSubmitInfo.html
type ProtectedSubmitInfo struct {
	// SType           StructureType
	Next            unsafe.Pointer
	ProtectedSubmit bool
}

// SamplerYcbcrConversionCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerYcbcrConversionCreateInfo.html
type SamplerYcbcrConversionCreateInfo struct {
	// SType                       StructureType
	Next                        unsafe.Pointer
	Format                      Format
	YcbcrModel                  SamplerYcbcrModelConversion
	YcbcrRange                  SamplerYcbcrRange
	Components                  ComponentMapping
	XChromaOffset               ChromaLocation
	YChromaOffset               ChromaLocation
	ChromaFilter                Filter
	ForceExplicitReconstruction bool
}

// SamplerYcbcrConversionInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerYcbcrConversionInfo.html
type SamplerYcbcrConversionInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	Conversion SamplerYcbcrConversion
}

// BindImagePlaneMemoryInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBindImagePlaneMemoryInfo.html
type BindImagePlaneMemoryInfo struct {
	// SType       StructureType
	Next        unsafe.Pointer
	PlaneAspect ImageAspectFlagBits
}

// ImagePlaneMemoryRequirementsInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImagePlaneMemoryRequirementsInfo.html
type ImagePlaneMemoryRequirementsInfo struct {
	// SType       StructureType
	Next        unsafe.Pointer
	PlaneAspect ImageAspectFlagBits
}

// PhysicalDeviceSamplerYcbcrConversionFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceSamplerYcbcrConversionFeatures.html
type PhysicalDeviceSamplerYcbcrConversionFeatures struct {
	// SType                  StructureType
	Next                   unsafe.Pointer
	SamplerYcbcrConversion bool
}

// SamplerYcbcrConversionImageFormatProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerYcbcrConversionImageFormatProperties.html
type SamplerYcbcrConversionImageFormatProperties struct {
	// SType                               StructureType
	Next                                unsafe.Pointer
	CombinedImageSamplerDescriptorCount uint32
}

// DescriptorUpdateTemplateEntry as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorUpdateTemplateEntry.html
type DescriptorUpdateTemplateEntry struct {
	DstBinding      uint32
	DstArrayElement uint32
	DescriptorCount uint32
	DescriptorType  DescriptorType
	Offset          uint32
	Stride          uint32
}

// DescriptorUpdateTemplateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorUpdateTemplateCreateInfo.html
type DescriptorUpdateTemplateCreateInfo struct {
	// SType                      StructureType
	Next                       unsafe.Pointer
	Flags                      DescriptorUpdateTemplateCreateFlags
	DescriptorUpdateEntryCount uint32
	PDescriptorUpdateEntries   []DescriptorUpdateTemplateEntry
	TemplateType               DescriptorUpdateTemplateType
	DescriptorSetLayout        DescriptorSetLayout
	PipelineBindPoint          PipelineBindPoint
	PipelineLayout             PipelineLayout
	Set                        uint32
}

// ExternalMemoryProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalMemoryProperties.html
type ExternalMemoryProperties struct {
	ExternalMemoryFeatures        ExternalMemoryFeatureFlags
	ExportFromImportedHandleTypes ExternalMemoryHandleTypeFlags
	CompatibleHandleTypes         ExternalMemoryHandleTypeFlags
}

// PhysicalDeviceExternalImageFormatInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceExternalImageFormatInfo.html
type PhysicalDeviceExternalImageFormatInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	HandleType ExternalMemoryHandleTypeFlagBits
}

// ExternalImageFormatProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalImageFormatProperties.html
type ExternalImageFormatProperties struct {
	// SType                    StructureType
	Next                     unsafe.Pointer
	ExternalMemoryProperties ExternalMemoryProperties
}

// PhysicalDeviceExternalBufferInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceExternalBufferInfo.html
type PhysicalDeviceExternalBufferInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	Flags      BufferCreateFlags
	Usage      BufferUsageFlags
	HandleType ExternalMemoryHandleTypeFlagBits
}

// ExternalBufferProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalBufferProperties.html
type ExternalBufferProperties struct {
	// SType                    StructureType
	Next                     unsafe.Pointer
	ExternalMemoryProperties ExternalMemoryProperties
}

// PhysicalDeviceIDProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceIDProperties.html
type PhysicalDeviceIDProperties struct {
	// SType           StructureType
	Next            unsafe.Pointer
	DeviceUUID      [16]byte
	DriverUUID      [16]byte
	DeviceLUID      [8]byte
	DeviceNodeMask  uint32
	DeviceLUIDValid bool
}

// ExternalMemoryImageCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalMemoryImageCreateInfo.html
type ExternalMemoryImageCreateInfo struct {
	// SType       StructureType
	Next        unsafe.Pointer
	HandleTypes ExternalMemoryHandleTypeFlags
}

// ExternalMemoryBufferCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalMemoryBufferCreateInfo.html
type ExternalMemoryBufferCreateInfo struct {
	// SType       StructureType
	Next        unsafe.Pointer
	HandleTypes ExternalMemoryHandleTypeFlags
}

// ExportMemoryAllocateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExportMemoryAllocateInfo.html
type ExportMemoryAllocateInfo struct {
	// SType       StructureType
	Next        unsafe.Pointer
	HandleTypes ExternalMemoryHandleTypeFlags
}

// PhysicalDeviceExternalFenceInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceExternalFenceInfo.html
type PhysicalDeviceExternalFenceInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	HandleType ExternalFenceHandleTypeFlagBits
}

// ExternalFenceProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalFenceProperties.html
type ExternalFenceProperties struct {
	// SType                         StructureType
	Next                          unsafe.Pointer
	ExportFromImportedHandleTypes ExternalFenceHandleTypeFlags
	CompatibleHandleTypes         ExternalFenceHandleTypeFlags
	ExternalFenceFeatures         ExternalFenceFeatureFlags
}

// ExportFenceCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExportFenceCreateInfo.html
type ExportFenceCreateInfo struct {
	// SType       StructureType
	Next        unsafe.Pointer
	HandleTypes ExternalFenceHandleTypeFlags
}

// ExportSemaphoreCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExportSemaphoreCreateInfo.html
type ExportSemaphoreCreateInfo struct {
	// SType       StructureType
	Next        unsafe.Pointer
	HandleTypes ExternalSemaphoreHandleTypeFlags
}

// PhysicalDeviceExternalSemaphoreInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceExternalSemaphoreInfo.html
type PhysicalDeviceExternalSemaphoreInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	HandleType ExternalSemaphoreHandleTypeFlagBits
}

// ExternalSemaphoreProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalSemaphoreProperties.html
type ExternalSemaphoreProperties struct {
	// SType                         StructureType
	Next                          unsafe.Pointer
	ExportFromImportedHandleTypes ExternalSemaphoreHandleTypeFlags
	CompatibleHandleTypes         ExternalSemaphoreHandleTypeFlags
	ExternalSemaphoreFeatures     ExternalSemaphoreFeatureFlags
}

// PhysicalDeviceMaintenance3Properties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceMaintenance3Properties.html
type PhysicalDeviceMaintenance3Properties struct {
	// SType                   StructureType
	Next                    unsafe.Pointer
	MaxPerSetDescriptors    uint32
	MaxMemoryAllocationSize DeviceSize
}

// DescriptorSetLayoutSupport as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorSetLayoutSupport.html
type DescriptorSetLayoutSupport struct {
	// SType     StructureType
	Next      unsafe.Pointer
	Supported bool
}

// PhysicalDeviceShaderDrawParametersFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceShaderDrawParametersFeatures.html
type PhysicalDeviceShaderDrawParametersFeatures struct {
	// SType                StructureType
	Next                 unsafe.Pointer
	ShaderDrawParameters bool
}

// PhysicalDeviceShaderDrawParameterFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceShaderDrawParameterFeatures.html
type PhysicalDeviceShaderDrawParameterFeatures struct {
	// SType                StructureType
	Next                 unsafe.Pointer
	ShaderDrawParameters bool
}

// ResolveModeFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkResolveModeFlags.html
type ResolveModeFlags uint32

// DescriptorBindingFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorBindingFlags.html
type DescriptorBindingFlags uint32

// SemaphoreWaitFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSemaphoreWaitFlags.html
type SemaphoreWaitFlags uint32

// PhysicalDeviceVulkan11Features as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceVulkan11Features.html
type PhysicalDeviceVulkan11Features struct {
	// SType                              StructureType
	Next                               unsafe.Pointer
	StorageBuffer16BitAccess           bool
	UniformAndStorageBuffer16BitAccess bool
	StoragePushConstant16              bool
	StorageInputOutput16               bool
	Multiview                          bool
	MultiviewGeometryShader            bool
	MultiviewTessellationShader        bool
	VariablePointersStorageBuffer      bool
	VariablePointers                   bool
	ProtectedMemory                    bool
	SamplerYcbcrConversion             bool
	ShaderDrawParameters               bool
}

// PhysicalDeviceVulkan11Properties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceVulkan11Properties.html
type PhysicalDeviceVulkan11Properties struct {
	// SType                             StructureType
	Next                              unsafe.Pointer
	DeviceUUID                        [16]byte
	DriverUUID                        [16]byte
	DeviceLUID                        [8]byte
	DeviceNodeMask                    uint32
	DeviceLUIDValid                   bool
	SubgroupSize                      uint32
	SubgroupSupportedStages           ShaderStageFlags
	SubgroupSupportedOperations       SubgroupFeatureFlags
	SubgroupQuadOperationsInAllStages bool
	PointClippingBehavior             PointClippingBehavior
	MaxMultiviewViewCount             uint32
	MaxMultiviewInstanceIndex         uint32
	ProtectedNoFault                  bool
	MaxPerSetDescriptors              uint32
	MaxMemoryAllocationSize           DeviceSize
}

// PhysicalDeviceVulkan12Features as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceVulkan12Features.html
type PhysicalDeviceVulkan12Features struct {
	// SType                                              StructureType
	Next                                               unsafe.Pointer
	SamplerMirrorClampToEdge                           bool
	DrawIndirectCount                                  bool
	StorageBuffer8BitAccess                            bool
	UniformAndStorageBuffer8BitAccess                  bool
	StoragePushConstant8                               bool
	ShaderBufferInt64Atomics                           bool
	ShaderSharedInt64Atomics                           bool
	ShaderFloat16                                      bool
	ShaderInt8                                         bool
	DescriptorIndexing                                 bool
	ShaderInputAttachmentArrayDynamicIndexing          bool
	ShaderUniformTexelBufferArrayDynamicIndexing       bool
	ShaderStorageTexelBufferArrayDynamicIndexing       bool
	ShaderUniformBufferArrayNonUniformIndexing         bool
	ShaderSampledImageArrayNonUniformIndexing          bool
	ShaderStorageBufferArrayNonUniformIndexing         bool
	ShaderStorageImageArrayNonUniformIndexing          bool
	ShaderInputAttachmentArrayNonUniformIndexing       bool
	ShaderUniformTexelBufferArrayNonUniformIndexing    bool
	ShaderStorageTexelBufferArrayNonUniformIndexing    bool
	DescriptorBindingUniformBufferUpdateAfterBind      bool
	DescriptorBindingSampledImageUpdateAfterBind       bool
	DescriptorBindingStorageImageUpdateAfterBind       bool
	DescriptorBindingStorageBufferUpdateAfterBind      bool
	DescriptorBindingUniformTexelBufferUpdateAfterBind bool
	DescriptorBindingStorageTexelBufferUpdateAfterBind bool
	DescriptorBindingUpdateUnusedWhilePending          bool
	DescriptorBindingPartiallyBound                    bool
	DescriptorBindingVariableDescriptorCount           bool
	RuntimeDescriptorArray                             bool
	SamplerFilterMinmax                                bool
	ScalarBlockLayout                                  bool
	ImagelessFramebuffer                               bool
	UniformBufferStandardLayout                        bool
	ShaderSubgroupExtendedTypes                        bool
	SeparateDepthStencilLayouts                        bool
	HostQueryReset                                     bool
	TimelineSemaphore                                  bool
	BufferDeviceAddress                                bool
	BufferDeviceAddressCaptureReplay                   bool
	BufferDeviceAddressMultiDevice                     bool
	VulkanMemoryModel                                  bool
	VulkanMemoryModelDeviceScope                       bool
	VulkanMemoryModelAvailabilityVisibilityChains      bool
	ShaderOutputViewportIndex                          bool
	ShaderOutputLayer                                  bool
	SubgroupBroadcastDynamicId                         bool
}

// ConformanceVersion as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkConformanceVersion.html
type ConformanceVersion struct {
	Major    byte
	Minor    byte
	Subminor byte
	Patch    byte
}

// PhysicalDeviceVulkan12Properties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceVulkan12Properties.html
type PhysicalDeviceVulkan12Properties struct {
	// SType                                                StructureType
	Next                                                 unsafe.Pointer
	DriverID                                             DriverId
	DriverName                                           [256]byte
	DriverInfo                                           [256]byte
	ConformanceVersion                                   ConformanceVersion
	DenormBehaviorIndependence                           ShaderFloatControlsIndependence
	RoundingModeIndependence                             ShaderFloatControlsIndependence
	ShaderSignedZeroInfNanPreserveFloat16                bool
	ShaderSignedZeroInfNanPreserveFloat32                bool
	ShaderSignedZeroInfNanPreserveFloat64                bool
	ShaderDenormPreserveFloat16                          bool
	ShaderDenormPreserveFloat32                          bool
	ShaderDenormPreserveFloat64                          bool
	ShaderDenormFlushToZeroFloat16                       bool
	ShaderDenormFlushToZeroFloat32                       bool
	ShaderDenormFlushToZeroFloat64                       bool
	ShaderRoundingModeRTEFloat16                         bool
	ShaderRoundingModeRTEFloat32                         bool
	ShaderRoundingModeRTEFloat64                         bool
	ShaderRoundingModeRTZFloat16                         bool
	ShaderRoundingModeRTZFloat32                         bool
	ShaderRoundingModeRTZFloat64                         bool
	MaxUpdateAfterBindDescriptorsInAllPools              uint32
	ShaderUniformBufferArrayNonUniformIndexingNative     bool
	ShaderSampledImageArrayNonUniformIndexingNative      bool
	ShaderStorageBufferArrayNonUniformIndexingNative     bool
	ShaderStorageImageArrayNonUniformIndexingNative      bool
	ShaderInputAttachmentArrayNonUniformIndexingNative   bool
	RobustBufferAccessUpdateAfterBind                    bool
	QuadDivergentImplicitLod                             bool
	MaxPerStageDescriptorUpdateAfterBindSamplers         uint32
	MaxPerStageDescriptorUpdateAfterBindUniformBuffers   uint32
	MaxPerStageDescriptorUpdateAfterBindStorageBuffers   uint32
	MaxPerStageDescriptorUpdateAfterBindSampledImages    uint32
	MaxPerStageDescriptorUpdateAfterBindStorageImages    uint32
	MaxPerStageDescriptorUpdateAfterBindInputAttachments uint32
	MaxPerStageUpdateAfterBindResources                  uint32
	MaxDescriptorSetUpdateAfterBindSamplers              uint32
	MaxDescriptorSetUpdateAfterBindUniformBuffers        uint32
	MaxDescriptorSetUpdateAfterBindUniformBuffersDynamic uint32
	MaxDescriptorSetUpdateAfterBindStorageBuffers        uint32
	MaxDescriptorSetUpdateAfterBindStorageBuffersDynamic uint32
	MaxDescriptorSetUpdateAfterBindSampledImages         uint32
	MaxDescriptorSetUpdateAfterBindStorageImages         uint32
	MaxDescriptorSetUpdateAfterBindInputAttachments      uint32
	SupportedDepthResolveModes                           ResolveModeFlags
	SupportedStencilResolveModes                         ResolveModeFlags
	IndependentResolveNone                               bool
	IndependentResolve                                   bool
	FilterMinmaxSingleComponentFormats                   bool
	FilterMinmaxImageComponentMapping                    bool
	MaxTimelineSemaphoreValueDifference                  uint32
	FramebufferIntegerColorSampleCounts                  SampleCountFlags
}

// ImageFormatListCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageFormatListCreateInfo.html
type ImageFormatListCreateInfo struct {
	// SType           StructureType
	Next            unsafe.Pointer
	ViewFormatCount uint32
	PViewFormats    []Format
}

// AttachmentDescription2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAttachmentDescription2.html
type AttachmentDescription2 struct {
	// SType          StructureType
	Next           unsafe.Pointer
	Flags          AttachmentDescriptionFlags
	Format         Format
	Samples        SampleCountFlagBits
	LoadOp         AttachmentLoadOp
	StoreOp        AttachmentStoreOp
	StencilLoadOp  AttachmentLoadOp
	StencilStoreOp AttachmentStoreOp
	InitialLayout  ImageLayout
	FinalLayout    ImageLayout
}

// AttachmentReference2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAttachmentReference2.html
type AttachmentReference2 struct {
	// SType      StructureType
	Next       unsafe.Pointer
	Attachment uint32
	Layout     ImageLayout
	AspectMask ImageAspectFlags
}

// SubpassDescription2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSubpassDescription2.html
type SubpassDescription2 struct {
	// SType                   StructureType
	Next                    unsafe.Pointer
	Flags                   SubpassDescriptionFlags
	PipelineBindPoint       PipelineBindPoint
	ViewMask                uint32
	InputAttachmentCount    uint32
	PInputAttachments       []AttachmentReference2
	ColorAttachmentCount    uint32
	PColorAttachments       []AttachmentReference2
	PResolveAttachments     []AttachmentReference2
	PDepthStencilAttachment []AttachmentReference2
	PreserveAttachmentCount uint32
	PPreserveAttachments    []uint32
}

// SubpassDependency2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSubpassDependency2.html
type SubpassDependency2 struct {
	// SType           StructureType
	Next            unsafe.Pointer
	SrcSubpass      uint32
	DstSubpass      uint32
	SrcStageMask    PipelineStageFlags
	DstStageMask    PipelineStageFlags
	SrcAccessMask   AccessFlags
	DstAccessMask   AccessFlags
	DependencyFlags DependencyFlags
	ViewOffset      int32
}

// RenderPassCreateInfo2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkRenderPassCreateInfo2.html
type RenderPassCreateInfo2 struct {
	// SType                   StructureType
	Next                    unsafe.Pointer
	Flags                   RenderPassCreateFlags
	AttachmentCount         uint32
	PAttachments            []AttachmentDescription2
	SubpassCount            uint32
	PSubpasses              []SubpassDescription2
	DependencyCount         uint32
	PDependencies           []SubpassDependency2
	CorrelatedViewMaskCount uint32
	PCorrelatedViewMasks    []uint32
}

// SubpassBeginInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSubpassBeginInfo.html
type SubpassBeginInfo struct {
	// SType    StructureType
	Next     unsafe.Pointer
	Contents SubpassContents
}

// SubpassEndInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSubpassEndInfo.html
type SubpassEndInfo struct {
	// SType StructureType
	Next unsafe.Pointer
}

// PhysicalDevice8BitStorageFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDevice8BitStorageFeatures.html
type PhysicalDevice8BitStorageFeatures struct {
	// SType                             StructureType
	Next                              unsafe.Pointer
	StorageBuffer8BitAccess           bool
	UniformAndStorageBuffer8BitAccess bool
	StoragePushConstant8              bool
}

// PhysicalDeviceDriverProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceDriverProperties.html
type PhysicalDeviceDriverProperties struct {
	// SType              StructureType
	Next               unsafe.Pointer
	DriverID           DriverId
	DriverName         [256]byte
	DriverInfo         [256]byte
	ConformanceVersion ConformanceVersion
}

// PhysicalDeviceShaderAtomicInt64Features as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceShaderAtomicInt64Features.html
type PhysicalDeviceShaderAtomicInt64Features struct {
	// SType                    StructureType
	Next                     unsafe.Pointer
	ShaderBufferInt64Atomics bool
	ShaderSharedInt64Atomics bool
}

// PhysicalDeviceShaderFloat16Int8Features as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceShaderFloat16Int8Features.html
type PhysicalDeviceShaderFloat16Int8Features struct {
	// SType         StructureType
	Next          unsafe.Pointer
	ShaderFloat16 bool
	ShaderInt8    bool
}

// PhysicalDeviceFloatControlsProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceFloatControlsProperties.html
type PhysicalDeviceFloatControlsProperties struct {
	// SType                                 StructureType
	Next                                  unsafe.Pointer
	DenormBehaviorIndependence            ShaderFloatControlsIndependence
	RoundingModeIndependence              ShaderFloatControlsIndependence
	ShaderSignedZeroInfNanPreserveFloat16 bool
	ShaderSignedZeroInfNanPreserveFloat32 bool
	ShaderSignedZeroInfNanPreserveFloat64 bool
	ShaderDenormPreserveFloat16           bool
	ShaderDenormPreserveFloat32           bool
	ShaderDenormPreserveFloat64           bool
	ShaderDenormFlushToZeroFloat16        bool
	ShaderDenormFlushToZeroFloat32        bool
	ShaderDenormFlushToZeroFloat64        bool
	ShaderRoundingModeRTEFloat16          bool
	ShaderRoundingModeRTEFloat32          bool
	ShaderRoundingModeRTEFloat64          bool
	ShaderRoundingModeRTZFloat16          bool
	ShaderRoundingModeRTZFloat32          bool
	ShaderRoundingModeRTZFloat64          bool
}

// DescriptorSetLayoutBindingFlagsCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorSetLayoutBindingFlagsCreateInfo.html
type DescriptorSetLayoutBindingFlagsCreateInfo struct {
	// SType         StructureType
	Next          unsafe.Pointer
	BindingCount  uint32
	PBindingFlags []DescriptorBindingFlags
}

// PhysicalDeviceDescriptorIndexingFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceDescriptorIndexingFeatures.html
type PhysicalDeviceDescriptorIndexingFeatures struct {
	// SType                                              StructureType
	Next                                               unsafe.Pointer
	ShaderInputAttachmentArrayDynamicIndexing          bool
	ShaderUniformTexelBufferArrayDynamicIndexing       bool
	ShaderStorageTexelBufferArrayDynamicIndexing       bool
	ShaderUniformBufferArrayNonUniformIndexing         bool
	ShaderSampledImageArrayNonUniformIndexing          bool
	ShaderStorageBufferArrayNonUniformIndexing         bool
	ShaderStorageImageArrayNonUniformIndexing          bool
	ShaderInputAttachmentArrayNonUniformIndexing       bool
	ShaderUniformTexelBufferArrayNonUniformIndexing    bool
	ShaderStorageTexelBufferArrayNonUniformIndexing    bool
	DescriptorBindingUniformBufferUpdateAfterBind      bool
	DescriptorBindingSampledImageUpdateAfterBind       bool
	DescriptorBindingStorageImageUpdateAfterBind       bool
	DescriptorBindingStorageBufferUpdateAfterBind      bool
	DescriptorBindingUniformTexelBufferUpdateAfterBind bool
	DescriptorBindingStorageTexelBufferUpdateAfterBind bool
	DescriptorBindingUpdateUnusedWhilePending          bool
	DescriptorBindingPartiallyBound                    bool
	DescriptorBindingVariableDescriptorCount           bool
	RuntimeDescriptorArray                             bool
}

// PhysicalDeviceDescriptorIndexingProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceDescriptorIndexingProperties.html
type PhysicalDeviceDescriptorIndexingProperties struct {
	// SType                                                StructureType
	Next                                                 unsafe.Pointer
	MaxUpdateAfterBindDescriptorsInAllPools              uint32
	ShaderUniformBufferArrayNonUniformIndexingNative     bool
	ShaderSampledImageArrayNonUniformIndexingNative      bool
	ShaderStorageBufferArrayNonUniformIndexingNative     bool
	ShaderStorageImageArrayNonUniformIndexingNative      bool
	ShaderInputAttachmentArrayNonUniformIndexingNative   bool
	RobustBufferAccessUpdateAfterBind                    bool
	QuadDivergentImplicitLod                             bool
	MaxPerStageDescriptorUpdateAfterBindSamplers         uint32
	MaxPerStageDescriptorUpdateAfterBindUniformBuffers   uint32
	MaxPerStageDescriptorUpdateAfterBindStorageBuffers   uint32
	MaxPerStageDescriptorUpdateAfterBindSampledImages    uint32
	MaxPerStageDescriptorUpdateAfterBindStorageImages    uint32
	MaxPerStageDescriptorUpdateAfterBindInputAttachments uint32
	MaxPerStageUpdateAfterBindResources                  uint32
	MaxDescriptorSetUpdateAfterBindSamplers              uint32
	MaxDescriptorSetUpdateAfterBindUniformBuffers        uint32
	MaxDescriptorSetUpdateAfterBindUniformBuffersDynamic uint32
	MaxDescriptorSetUpdateAfterBindStorageBuffers        uint32
	MaxDescriptorSetUpdateAfterBindStorageBuffersDynamic uint32
	MaxDescriptorSetUpdateAfterBindSampledImages         uint32
	MaxDescriptorSetUpdateAfterBindStorageImages         uint32
	MaxDescriptorSetUpdateAfterBindInputAttachments      uint32
}

// DescriptorSetVariableDescriptorCountAllocateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorSetVariableDescriptorCountAllocateInfo.html
type DescriptorSetVariableDescriptorCountAllocateInfo struct {
	// SType              StructureType
	Next               unsafe.Pointer
	DescriptorSetCount uint32
	PDescriptorCounts  []uint32
}

// DescriptorSetVariableDescriptorCountLayoutSupport as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorSetVariableDescriptorCountLayoutSupport.html
type DescriptorSetVariableDescriptorCountLayoutSupport struct {
	// SType                      StructureType
	Next                       unsafe.Pointer
	MaxVariableDescriptorCount uint32
}

// SubpassDescriptionDepthStencilResolve as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSubpassDescriptionDepthStencilResolve.html
type SubpassDescriptionDepthStencilResolve struct {
	// SType                          StructureType
	Next                           unsafe.Pointer
	DepthResolveMode               ResolveModeFlagBits
	StencilResolveMode             ResolveModeFlagBits
	PDepthStencilResolveAttachment []AttachmentReference2
}

// PhysicalDeviceDepthStencilResolveProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceDepthStencilResolveProperties.html
type PhysicalDeviceDepthStencilResolveProperties struct {
	// SType                        StructureType
	Next                         unsafe.Pointer
	SupportedDepthResolveModes   ResolveModeFlags
	SupportedStencilResolveModes ResolveModeFlags
	IndependentResolveNone       bool
	IndependentResolve           bool
}

// PhysicalDeviceScalarBlockLayoutFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceScalarBlockLayoutFeatures.html
type PhysicalDeviceScalarBlockLayoutFeatures struct {
	// SType             StructureType
	Next              unsafe.Pointer
	ScalarBlockLayout bool
}

// ImageStencilUsageCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageStencilUsageCreateInfo.html
type ImageStencilUsageCreateInfo struct {
	// SType        StructureType
	Next         unsafe.Pointer
	StencilUsage ImageUsageFlags
}

// SamplerReductionModeCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerReductionModeCreateInfo.html
type SamplerReductionModeCreateInfo struct {
	// SType         StructureType
	Next          unsafe.Pointer
	ReductionMode SamplerReductionMode
}

// PhysicalDeviceSamplerFilterMinmaxProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceSamplerFilterMinmaxProperties.html
type PhysicalDeviceSamplerFilterMinmaxProperties struct {
	// SType                              StructureType
	Next                               unsafe.Pointer
	FilterMinmaxSingleComponentFormats bool
	FilterMinmaxImageComponentMapping  bool
}

// PhysicalDeviceVulkanMemoryModelFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceVulkanMemoryModelFeatures.html
type PhysicalDeviceVulkanMemoryModelFeatures struct {
	// SType                                         StructureType
	Next                                          unsafe.Pointer
	VulkanMemoryModel                             bool
	VulkanMemoryModelDeviceScope                  bool
	VulkanMemoryModelAvailabilityVisibilityChains bool
}

// PhysicalDeviceImagelessFramebufferFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceImagelessFramebufferFeatures.html
type PhysicalDeviceImagelessFramebufferFeatures struct {
	// SType                StructureType
	Next                 unsafe.Pointer
	ImagelessFramebuffer bool
}

// FramebufferAttachmentImageInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFramebufferAttachmentImageInfo.html
type FramebufferAttachmentImageInfo struct {
	// SType           StructureType
	Next            unsafe.Pointer
	Flags           ImageCreateFlags
	Usage           ImageUsageFlags
	Width           uint32
	Height          uint32
	LayerCount      uint32
	ViewFormatCount uint32
	PViewFormats    []Format
}

// FramebufferAttachmentsCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFramebufferAttachmentsCreateInfo.html
type FramebufferAttachmentsCreateInfo struct {
	// SType                    StructureType
	Next                     unsafe.Pointer
	AttachmentImageInfoCount uint32
	PAttachmentImageInfos    []FramebufferAttachmentImageInfo
}

// RenderPassAttachmentBeginInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkRenderPassAttachmentBeginInfo.html
type RenderPassAttachmentBeginInfo struct {
	// SType           StructureType
	Next            unsafe.Pointer
	AttachmentCount uint32
	PAttachments    []ImageView
}

// PhysicalDeviceUniformBufferStandardLayoutFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceUniformBufferStandardLayoutFeatures.html
type PhysicalDeviceUniformBufferStandardLayoutFeatures struct {
	// SType                       StructureType
	Next                        unsafe.Pointer
	UniformBufferStandardLayout bool
}

// PhysicalDeviceShaderSubgroupExtendedTypesFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceShaderSubgroupExtendedTypesFeatures.html
type PhysicalDeviceShaderSubgroupExtendedTypesFeatures struct {
	// SType                       StructureType
	Next                        unsafe.Pointer
	ShaderSubgroupExtendedTypes bool
}

// PhysicalDeviceSeparateDepthStencilLayoutsFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceSeparateDepthStencilLayoutsFeatures.html
type PhysicalDeviceSeparateDepthStencilLayoutsFeatures struct {
	// SType                       StructureType
	Next                        unsafe.Pointer
	SeparateDepthStencilLayouts bool
}

// AttachmentReferenceStencilLayout as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAttachmentReferenceStencilLayout.html
type AttachmentReferenceStencilLayout struct {
	// SType         StructureType
	Next          unsafe.Pointer
	StencilLayout ImageLayout
}

// AttachmentDescriptionStencilLayout as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAttachmentDescriptionStencilLayout.html
type AttachmentDescriptionStencilLayout struct {
	// SType                StructureType
	Next                 unsafe.Pointer
	StencilInitialLayout ImageLayout
	StencilFinalLayout   ImageLayout
}

// PhysicalDeviceHostQueryResetFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceHostQueryResetFeatures.html
type PhysicalDeviceHostQueryResetFeatures struct {
	// SType          StructureType
	Next           unsafe.Pointer
	HostQueryReset bool
}

// PhysicalDeviceTimelineSemaphoreFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceTimelineSemaphoreFeatures.html
type PhysicalDeviceTimelineSemaphoreFeatures struct {
	// SType             StructureType
	Next              unsafe.Pointer
	TimelineSemaphore bool
}

// PhysicalDeviceTimelineSemaphoreProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceTimelineSemaphoreProperties.html
type PhysicalDeviceTimelineSemaphoreProperties struct {
	// SType                               StructureType
	Next                                unsafe.Pointer
	MaxTimelineSemaphoreValueDifference uint32
}

// SemaphoreTypeCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSemaphoreTypeCreateInfo.html
type SemaphoreTypeCreateInfo struct {
	// SType         StructureType
	Next          unsafe.Pointer
	SemaphoreType SemaphoreType
	InitialValue  uint32
}

// TimelineSemaphoreSubmitInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkTimelineSemaphoreSubmitInfo.html
type TimelineSemaphoreSubmitInfo struct {
	// SType                     StructureType
	Next                      unsafe.Pointer
	WaitSemaphoreValueCount   uint32
	PWaitSemaphoreValues      []uint32
	SignalSemaphoreValueCount uint32
	PSignalSemaphoreValues    []uint32
}

// SemaphoreWaitInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSemaphoreWaitInfo.html
type SemaphoreWaitInfo struct {
	// SType          StructureType
	Next           unsafe.Pointer
	Flags          SemaphoreWaitFlags
	SemaphoreCount uint32
	PSemaphores    []Semaphore
	PValues        []uint32
}

// SemaphoreSignalInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSemaphoreSignalInfo.html
type SemaphoreSignalInfo struct {
	// SType     StructureType
	Next      unsafe.Pointer
	Semaphore Semaphore
	Value     uint32
}

// PhysicalDeviceBufferDeviceAddressFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceBufferDeviceAddressFeatures.html
type PhysicalDeviceBufferDeviceAddressFeatures struct {
	// SType                            StructureType
	Next                             unsafe.Pointer
	BufferDeviceAddress              bool
	BufferDeviceAddressCaptureReplay bool
	BufferDeviceAddressMultiDevice   bool
}

// BufferDeviceAddressInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBufferDeviceAddressInfo.html
type BufferDeviceAddressInfo struct {
	// SType  StructureType
	Next   unsafe.Pointer
	Buffer Buffer
}

// BufferOpaqueCaptureAddressCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBufferOpaqueCaptureAddressCreateInfo.html
type BufferOpaqueCaptureAddressCreateInfo struct {
	// SType                StructureType
	Next                 unsafe.Pointer
	OpaqueCaptureAddress uint32
}

// MemoryOpaqueCaptureAddressAllocateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryOpaqueCaptureAddressAllocateInfo.html
type MemoryOpaqueCaptureAddressAllocateInfo struct {
	// SType                StructureType
	Next                 unsafe.Pointer
	OpaqueCaptureAddress uint32
}

// DeviceMemoryOpaqueCaptureAddressInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceMemoryOpaqueCaptureAddressInfo.html
type DeviceMemoryOpaqueCaptureAddressInfo struct {
	// SType  StructureType
	Next   unsafe.Pointer
	Memory DeviceMemory
}

// Surface as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSurfaceKHR
type Surface C.VkSurfaceKHR

// CompositeAlphaFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkCompositeAlphaFlagsKHR
type CompositeAlphaFlags uint32

// SurfaceTransformFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSurfaceTransformFlagsKHR
type SurfaceTransformFlags uint32

// SurfaceCapabilities as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSurfaceCapabilitiesKHR
type SurfaceCapabilities struct {
	MinImageCount           uint32
	MaxImageCount           uint32
	CurrentExtent           Extent2D
	MinImageExtent          Extent2D
	MaxImageExtent          Extent2D
	MaxImageArrayLayers     uint32
	SupportedTransforms     SurfaceTransformFlags
	CurrentTransform        SurfaceTransformFlagBits
	SupportedCompositeAlpha CompositeAlphaFlags
	SupportedUsageFlags     ImageUsageFlags
}

func (s *SurfaceCapabilities) From(in *C.VkSurfaceCapabilitiesKHR) {
	s.MinImageCount = uint32(in.minImageCount)
	s.MaxImageCount = uint32(in.maxImageCount)
	s.CurrentExtent.From(&in.currentExtent)
	s.MinImageExtent.From(&in.minImageExtent)
	s.MaxImageExtent.From(&in.maxImageExtent)
	s.MaxImageArrayLayers = uint32(in.maxImageArrayLayers)
	s.SupportedTransforms = SurfaceTransformFlags(in.supportedTransforms)
	s.CurrentTransform = SurfaceTransformFlagBits(in.currentTransform)
	s.SupportedCompositeAlpha = CompositeAlphaFlags(in.supportedCompositeAlpha)
	s.SupportedUsageFlags = ImageUsageFlags(in.supportedUsageFlags)
}

// SurfaceFormat as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSurfaceFormatKHR
type SurfaceFormat struct {
	Format     Format
	ColorSpace ColorSpace
}

func (s *SurfaceFormat) From(in *C.VkSurfaceFormatKHR) {
	s.Format = Format(in.format)
	s.ColorSpace = ColorSpace(in.colorSpace)
}

// Swapchain as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSwapchainKHR
type Swapchain C.VkSwapchainKHR

// SwapchainCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSwapchainCreateFlagsKHR
type SwapchainCreateFlags uint32

// DeviceGroupPresentModeFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDeviceGroupPresentModeFlagsKHR
type DeviceGroupPresentModeFlags uint32

// SwapchainCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSwapchainCreateInfoKHR
type SwapchainCreateInfo struct {
	Next                  unsafe.Pointer
	Flags                 SwapchainCreateFlags
	Surface               Surface
	MinImageCount         uint32
	ImageFormat           Format
	ImageColorSpace       ColorSpace
	ImageExtent           Extent2D
	ImageArrayLayers      uint32
	ImageUsage            ImageUsageFlags
	ImageSharingMode      SharingMode
	QueueFamilyIndexCount uint32
	QueueFamilyIndices    []uint32
	PreTransform          SurfaceTransformFlagBits
	CompositeAlpha        CompositeAlphaFlagBits
	PresentMode           PresentMode
	Clipped               bool
	OldSwapchain          Swapchain
}

func (s *SwapchainCreateInfo) To(mem VulkanClientMemory, target *C.VkSwapchainCreateInfoKHR) error {
	target.sType = (C.VkStructureType)(StructureTypeSwapchainCreateInfo)
	target.pNext = s.Next
	target.flags = (C.VkSwapchainCreateFlagsKHR)(s.Flags)
	target.surface = (C.VkSurfaceKHR)(s.Surface)
	target.minImageCount = (C.uint32_t)(s.MinImageCount)
	target.imageFormat = (C.VkFormat)(s.ImageFormat)
	target.imageColorSpace = (C.VkColorSpaceKHR)(s.ImageColorSpace)
	s.ImageExtent.To(mem, &target.imageExtent)
	target.imageArrayLayers = (C.uint32_t)(s.ImageArrayLayers)
	target.imageUsage = (C.VkImageUsageFlags)(s.ImageUsage)
	target.imageSharingMode = (C.VkSharingMode)(s.ImageSharingMode)
	target.queueFamilyIndexCount = (C.uint32_t)(s.QueueFamilyIndexCount)
	if s.QueueFamilyIndices != nil {
		target.pQueueFamilyIndices = (*C.uint32_t)(unsafe.Pointer(&s.QueueFamilyIndices[0]))
	}
	target.preTransform = (C.VkSurfaceTransformFlagBitsKHR)(s.PreTransform)
	target.compositeAlpha = (C.VkCompositeAlphaFlagBitsKHR)(s.CompositeAlpha)
	target.presentMode = (C.VkPresentModeKHR)(s.PresentMode)
	target.clipped = (C.VkBool32)(ToBool32(s.Clipped))
	target.oldSwapchain = (C.VkSwapchainKHR)(s.OldSwapchain)
	return nil
}

// PresentInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPresentInfoKHR
type PresentInfo struct {
	Next           unsafe.Pointer
	WaitSemaphores []Semaphore
	Swapchains     []Swapchain
	ImageIndices   []uint32
	Results        []Result
}

func (p *PresentInfo) Errors() (r []error) {
	for _, re := range p.Results {
		r = append(r, VkSuccess(re))
	}
	return
}

func (p *PresentInfo) To(mem VulkanClientMemory, target *C.VkPresentInfoKHR) error {
	target.sType = (C.VkStructureType)(StructureTypePresentInfo)
	target.pNext = p.Next
	target.waitSemaphoreCount = (C.uint32_t)(len(p.WaitSemaphores))
	if len(p.WaitSemaphores) > 0 {
		ptr := (*C.VkSemaphore)(mem.Calloc(len(p.WaitSemaphores), C.sizeof_VkSemaphore))
		target.pWaitSemaphores = ptr
		for _, pp := range p.WaitSemaphores {
			*ptr = C.VkSemaphore(pp)
			ptr = (*C.VkSemaphore)(unsafe.Pointer(uintptr(unsafe.Pointer(ptr)) + C.sizeof_VkSemaphore))
		}
	}
	target.swapchainCount = (C.uint32_t)(len(p.Swapchains))
	if len(p.Swapchains) > 0 {
		ptr := (*C.VkSwapchainKHR)(mem.Calloc(len(p.Swapchains), C.sizeof_VkSwapchainKHR))
		target.pSwapchains = ptr
		for _, pp := range p.Swapchains {
			*ptr = C.VkSwapchainKHR(pp)
			ptr = (*C.VkSwapchainKHR)(unsafe.Pointer(uintptr(unsafe.Pointer(ptr)) + C.sizeof_VkSwapchainKHR))
		}
		ptr1 := (*C.uint32_t)(mem.Calloc(len(p.Swapchains), C.sizeof_uint32_t))
		target.pImageIndices = ptr1
		for _, pp := range p.ImageIndices {
			*ptr1 = C.uint32_t(pp)
			ptr1 = (*C.uint32_t)(unsafe.Pointer(uintptr(unsafe.Pointer(ptr1)) + C.sizeof_uint32_t))
		}
	}
	if len(p.Results) > 0 {
		ptr := (*C.VkResult)(mem.Calloc(len(p.Results), C.sizeof_VkResult))
		target.pResults = ptr
		for _, pp := range p.Results {
			*ptr = C.VkResult(pp)
			ptr = (*C.VkResult)(unsafe.Pointer(uintptr(unsafe.Pointer(ptr)) + C.sizeof_VkResult))
		}
	}
	return nil
}

// ImageSwapchainCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkImageSwapchainCreateInfoKHR
type ImageSwapchainCreateInfo struct {
	// SType     StructureType
	Next      unsafe.Pointer
	Swapchain Swapchain
}

// BindImageMemorySwapchainInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkBindImageMemorySwapchainInfoKHR
type BindImageMemorySwapchainInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	Swapchain  Swapchain
	ImageIndex uint32
}

// AcquireNextImageInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAcquireNextImageInfoKHR
type AcquireNextImageInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	Swapchain  Swapchain
	Timeout    uint32
	Semaphore  Semaphore
	Fence      Fence
	DeviceMask uint32
}

// DeviceGroupPresentCapabilities as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDeviceGroupPresentCapabilitiesKHR
type DeviceGroupPresentCapabilities struct {
	// SType       StructureType
	Next        unsafe.Pointer
	PresentMask [32]uint32
	Modes       DeviceGroupPresentModeFlags
}

// DeviceGroupPresentInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDeviceGroupPresentInfoKHR
type DeviceGroupPresentInfo struct {
	// SType          StructureType
	Next           unsafe.Pointer
	SwapchainCount uint32
	PDeviceMasks   []uint32
	Mode           DeviceGroupPresentModeFlagBits
}

// DeviceGroupSwapchainCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDeviceGroupSwapchainCreateInfoKHR
type DeviceGroupSwapchainCreateInfo struct {
	// SType StructureType
	Next  unsafe.Pointer
	Modes DeviceGroupPresentModeFlags
}

// Display as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplayKHR
type Display C.VkDisplayKHR

// DisplayMode as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplayModeKHR
type DisplayMode C.VkDisplayModeKHR

// DisplayModeCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplayModeCreateFlagsKHR
type DisplayModeCreateFlags uint32

// DisplayPlaneAlphaFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplayPlaneAlphaFlagsKHR
type DisplayPlaneAlphaFlags uint32

// DisplaySurfaceCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplaySurfaceCreateFlagsKHR
type DisplaySurfaceCreateFlags uint32

// DisplayModeParameters as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplayModeParametersKHR
type DisplayModeParameters struct {
	VisibleRegion Extent2D
	RefreshRate   uint32
}

// DisplayModeCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplayModeCreateInfoKHR
type DisplayModeCreateInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	Flags      DisplayModeCreateFlags
	Parameters DisplayModeParameters
}

// DisplayModeProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplayModePropertiesKHR
type DisplayModeProperties struct {
	DisplayMode DisplayMode
	Parameters  DisplayModeParameters
}

// DisplayPlaneCapabilities as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplayPlaneCapabilitiesKHR
type DisplayPlaneCapabilities struct {
	SupportedAlpha DisplayPlaneAlphaFlags
	MinSrcPosition Offset2D
	MaxSrcPosition Offset2D
	MinSrcExtent   Extent2D
	MaxSrcExtent   Extent2D
	MinDstPosition Offset2D
	MaxDstPosition Offset2D
	MinDstExtent   Extent2D
	MaxDstExtent   Extent2D
}

// DisplayPlaneProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplayPlanePropertiesKHR
type DisplayPlaneProperties struct {
	CurrentDisplay    Display
	CurrentStackIndex uint32
}

// DisplayProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplayPropertiesKHR
type DisplayProperties struct {
	Display              Display
	DisplayName          string
	PhysicalDimensions   Extent2D
	PhysicalResolution   Extent2D
	SupportedTransforms  SurfaceTransformFlags
	PlaneReorderPossible bool
	PersistentContent    bool
}

// DisplaySurfaceCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplaySurfaceCreateInfoKHR
type DisplaySurfaceCreateInfo struct {
	// SType           StructureType
	Next            unsafe.Pointer
	Flags           DisplaySurfaceCreateFlags
	DisplayMode     DisplayMode
	PlaneIndex      uint32
	PlaneStackIndex uint32
	Transform       SurfaceTransformFlagBits
	GlobalAlpha     float32
	AlphaMode       DisplayPlaneAlphaFlagBits
	ImageExtent     Extent2D
}

// DisplayPresentInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplayPresentInfoKHR
type DisplayPresentInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	SrcRect    Rect2D
	DstRect    Rect2D
	Persistent bool
}

// ImportMemoryFdInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkImportMemoryFdInfoKHR
type ImportMemoryFdInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	HandleType ExternalMemoryHandleTypeFlagBits
	Fd         int32
}

// MemoryFdProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkMemoryFdPropertiesKHR
type MemoryFdProperties struct {
	// SType          StructureType
	Next           unsafe.Pointer
	MemoryTypeBits uint32
}

// MemoryGetFdInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkMemoryGetFdInfoKHR
type MemoryGetFdInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	Memory     DeviceMemory
	HandleType ExternalMemoryHandleTypeFlagBits
}

// ImportSemaphoreFdInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkImportSemaphoreFdInfoKHR
type ImportSemaphoreFdInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	Semaphore  Semaphore
	Flags      SemaphoreImportFlags
	HandleType ExternalSemaphoreHandleTypeFlagBits
	Fd         int32
}

// SemaphoreGetFdInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSemaphoreGetFdInfoKHR
type SemaphoreGetFdInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	Semaphore  Semaphore
	HandleType ExternalSemaphoreHandleTypeFlagBits
}

// PhysicalDevicePushDescriptorProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDevicePushDescriptorPropertiesKHR
type PhysicalDevicePushDescriptorProperties struct {
	// SType              StructureType
	Next               unsafe.Pointer
	MaxPushDescriptors uint32
}

// PhysicalDeviceFloat16Int8Features as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDeviceFloat16Int8FeaturesKHR
type PhysicalDeviceFloat16Int8Features struct {
	// SType         StructureType
	Next          unsafe.Pointer
	ShaderFloat16 bool
	ShaderInt8    bool
}

// RectLayer as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkRectLayerKHR
type RectLayer struct {
	Offset Offset2D
	Extent Extent2D
	Layer  uint32
}

// PresentRegion as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPresentRegionKHR
type PresentRegion struct {
	RectangleCount uint32
	PRectangles    []RectLayer
}

// PresentRegions as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPresentRegionsKHR
type PresentRegions struct {
	// SType          StructureType
	Next           unsafe.Pointer
	SwapchainCount uint32
	PRegions       []PresentRegion
}

// SharedPresentSurfaceCapabilities as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSharedPresentSurfaceCapabilitiesKHR
type SharedPresentSurfaceCapabilities struct {
	// SType                            StructureType
	Next                             unsafe.Pointer
	SharedPresentSupportedUsageFlags ImageUsageFlags
}

// ImportFenceFdInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkImportFenceFdInfoKHR
type ImportFenceFdInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	Fence      Fence
	Flags      FenceImportFlags
	HandleType ExternalFenceHandleTypeFlagBits
	Fd         int32
}

// FenceGetFdInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkFenceGetFdInfoKHR
type FenceGetFdInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	Fence      Fence
	HandleType ExternalFenceHandleTypeFlagBits
}

// PerformanceCounterDescriptionFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPerformanceCounterDescriptionFlagsKHR
type PerformanceCounterDescriptionFlags uint32

// AcquireProfilingLockFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAcquireProfilingLockFlagsKHR
type AcquireProfilingLockFlags uint32

// PhysicalDevicePerformanceQueryFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDevicePerformanceQueryFeaturesKHR
type PhysicalDevicePerformanceQueryFeatures struct {
	// SType                                StructureType
	Next                                 unsafe.Pointer
	PerformanceCounterQueryPools         bool
	PerformanceCounterMultipleQueryPools bool
}

// PhysicalDevicePerformanceQueryProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDevicePerformanceQueryPropertiesKHR
type PhysicalDevicePerformanceQueryProperties struct {
	// SType                         StructureType
	Next                          unsafe.Pointer
	AllowCommandBufferQueryCopies bool
}

// PerformanceCounter as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPerformanceCounterKHR
type PerformanceCounter struct {
	// SType   StructureType
	Next    unsafe.Pointer
	Unit    PerformanceCounterUnit
	Scope   PerformanceCounterScope
	Storage PerformanceCounterStorage
	Uuid    [16]byte
}

// PerformanceCounterDescription as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPerformanceCounterDescriptionKHR
type PerformanceCounterDescription struct {
	// SType       StructureType
	Next        unsafe.Pointer
	Flags       PerformanceCounterDescriptionFlags
	Name        [256]byte
	Category    [256]byte
	Description [256]byte
}

// QueryPoolPerformanceCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkQueryPoolPerformanceCreateInfoKHR
type QueryPoolPerformanceCreateInfo struct {
	// SType             StructureType
	Next              unsafe.Pointer
	QueueFamilyIndex  uint32
	CounterIndexCount uint32
	PCounterIndices   []uint32
}

// PerformanceCounterResult as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPerformanceCounterResultKHR
const sizeofPerformanceCounterResult = unsafe.Sizeof(C.VkPerformanceCounterResultKHR{})

type PerformanceCounterResult [sizeofPerformanceCounterResult]byte

// AcquireProfilingLockInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAcquireProfilingLockInfoKHR
type AcquireProfilingLockInfo struct {
	// SType   StructureType
	Next    unsafe.Pointer
	Flags   AcquireProfilingLockFlags
	Timeout uint32
}

// PerformanceQuerySubmitInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPerformanceQuerySubmitInfoKHR
type PerformanceQuerySubmitInfo struct {
	// SType            StructureType
	Next             unsafe.Pointer
	CounterPassIndex uint32
}

// PhysicalDeviceSurfaceInfo2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDeviceSurfaceInfo2KHR
type PhysicalDeviceSurfaceInfo2 struct {
	// SType   StructureType
	Next    unsafe.Pointer
	Surface Surface
}

// SurfaceCapabilities2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSurfaceCapabilities2KHR
type SurfaceCapabilities2 struct {
	// SType               StructureType
	Next                unsafe.Pointer
	SurfaceCapabilities SurfaceCapabilities
}

// SurfaceFormat2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSurfaceFormat2KHR
type SurfaceFormat2 struct {
	// SType         StructureType
	Next          unsafe.Pointer
	SurfaceFormat SurfaceFormat
}

// DisplayProperties2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplayProperties2KHR
type DisplayProperties2 struct {
	// SType             StructureType
	Next              unsafe.Pointer
	DisplayProperties DisplayProperties
}

// DisplayPlaneProperties2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplayPlaneProperties2KHR
type DisplayPlaneProperties2 struct {
	// SType                  StructureType
	Next                   unsafe.Pointer
	DisplayPlaneProperties DisplayPlaneProperties
}

// DisplayModeProperties2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplayModeProperties2KHR
type DisplayModeProperties2 struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	DisplayModeProperties DisplayModeProperties
}

// DisplayPlaneInfo2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplayPlaneInfo2KHR
type DisplayPlaneInfo2 struct {
	// SType      StructureType
	Next       unsafe.Pointer
	Mode       DisplayMode
	PlaneIndex uint32
}

// DisplayPlaneCapabilities2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDisplayPlaneCapabilities2KHR
type DisplayPlaneCapabilities2 struct {
	// SType        StructureType
	Next         unsafe.Pointer
	Capabilities DisplayPlaneCapabilities
}

// PhysicalDeviceShaderClockFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDeviceShaderClockFeaturesKHR
type PhysicalDeviceShaderClockFeatures struct {
	// SType               StructureType
	Next                unsafe.Pointer
	ShaderSubgroupClock bool
	ShaderDeviceClock   bool
}

// PhysicalDeviceShaderTerminateInvocationFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDeviceShaderTerminateInvocationFeaturesKHR
type PhysicalDeviceShaderTerminateInvocationFeatures struct {
	// SType                     StructureType
	Next                      unsafe.Pointer
	ShaderTerminateInvocation bool
}

// FragmentShadingRateAttachmentInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkFragmentShadingRateAttachmentInfoKHR
type FragmentShadingRateAttachmentInfo struct {
	// SType                          StructureType
	Next                           unsafe.Pointer
	PFragmentShadingRateAttachment []AttachmentReference2
	ShadingRateAttachmentTexelSize Extent2D
}

// PipelineFragmentShadingRateStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPipelineFragmentShadingRateStateCreateInfoKHR
type PipelineFragmentShadingRateStateCreateInfo struct {
	// SType        StructureType
	Next         unsafe.Pointer
	FragmentSize Extent2D
	CombinerOps  [2]FragmentShadingRateCombinerOp
}

// PhysicalDeviceFragmentShadingRateFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDeviceFragmentShadingRateFeaturesKHR
type PhysicalDeviceFragmentShadingRateFeatures struct {
	// SType                         StructureType
	Next                          unsafe.Pointer
	PipelineFragmentShadingRate   bool
	PrimitiveFragmentShadingRate  bool
	AttachmentFragmentShadingRate bool
}

// PhysicalDeviceFragmentShadingRateProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDeviceFragmentShadingRatePropertiesKHR
type PhysicalDeviceFragmentShadingRateProperties struct {
	// SType                                                StructureType
	Next                                                 unsafe.Pointer
	MinFragmentShadingRateAttachmentTexelSize            Extent2D
	MaxFragmentShadingRateAttachmentTexelSize            Extent2D
	MaxFragmentShadingRateAttachmentTexelSizeAspectRatio uint32
	PrimitiveFragmentShadingRateWithMultipleViewports    bool
	LayeredShadingRateAttachments                        bool
	FragmentShadingRateNonTrivialCombinerOps             bool
	MaxFragmentSize                                      Extent2D
	MaxFragmentSizeAspectRatio                           uint32
	MaxFragmentShadingRateCoverageSamples                uint32
	MaxFragmentShadingRateRasterizationSamples           SampleCountFlagBits
	FragmentShadingRateWithShaderDepthStencilWrites      bool
	FragmentShadingRateWithSampleMask                    bool
	FragmentShadingRateWithShaderSampleMask              bool
	FragmentShadingRateWithConservativeRasterization     bool
	FragmentShadingRateWithFragmentShaderInterlock       bool
	FragmentShadingRateWithCustomSampleLocations         bool
	FragmentShadingRateStrictMultiplyCombiner            bool
}

// PhysicalDeviceFragmentShadingRate as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDeviceFragmentShadingRateKHR
type PhysicalDeviceFragmentShadingRate struct {
	// SType        StructureType
	Next         unsafe.Pointer
	SampleCounts SampleCountFlags
	FragmentSize Extent2D
}

// SurfaceProtectedCapabilities as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSurfaceProtectedCapabilitiesKHR
type SurfaceProtectedCapabilities struct {
	// SType             StructureType
	Next              unsafe.Pointer
	SupportsProtected bool
}

// DeferredOperation as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDeferredOperationKHR
type DeferredOperation C.VkDeferredOperationKHR

// PhysicalDevicePipelineExecutablePropertiesFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDevicePipelineExecutablePropertiesFeaturesKHR
type PhysicalDevicePipelineExecutablePropertiesFeatures struct {
	// SType                  StructureType
	Next                   unsafe.Pointer
	PipelineExecutableInfo bool
}

// PipelineInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPipelineInfoKHR
type PipelineInfo struct {
	// SType    StructureType
	Next     unsafe.Pointer
	Pipeline Pipeline
}

// PipelineExecutableProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPipelineExecutablePropertiesKHR
type PipelineExecutableProperties struct {
	// SType        StructureType
	Next         unsafe.Pointer
	Stages       ShaderStageFlags
	Name         [256]byte
	Description  [256]byte
	SubgroupSize uint32
}

// PipelineExecutableInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPipelineExecutableInfoKHR
type PipelineExecutableInfo struct {
	// SType           StructureType
	Next            unsafe.Pointer
	Pipeline        Pipeline
	ExecutableIndex uint32
}

// PipelineExecutableStatisticValue as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPipelineExecutableStatisticValueKHR
const sizeofPipelineExecutableStatisticValue = unsafe.Sizeof(C.VkPipelineExecutableStatisticValueKHR{})

type PipelineExecutableStatisticValue [sizeofPipelineExecutableStatisticValue]byte

// PipelineExecutableStatistic as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPipelineExecutableStatisticKHR
type PipelineExecutableStatistic struct {
	// SType       StructureType
	Next        unsafe.Pointer
	Name        [256]byte
	Description [256]byte
	Format      PipelineExecutableStatisticFormat
	Value       PipelineExecutableStatisticValue
}

// PipelineExecutableInternalRepresentation as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPipelineExecutableInternalRepresentationKHR
type PipelineExecutableInternalRepresentation struct {
	// SType       StructureType
	Next        unsafe.Pointer
	Name        [256]byte
	Description [256]byte
	IsText      bool
	DataSize    uint32
	PData       unsafe.Pointer
}

// PipelineLibraryCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPipelineLibraryCreateInfoKHR
type PipelineLibraryCreateInfo struct {
	// SType        StructureType
	Next         unsafe.Pointer
	LibraryCount uint32
	PLibraries   []Pipeline
}

// Flags64 type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFlags64.html
type Flags64 uint32

// PipelineStageFlags2 type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPipelineStageFlags2KHR
type PipelineStageFlags2 uint32

// PipelineStageFlagBits2 type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPipelineStageFlagBits2KHR
type PipelineStageFlagBits2 uint32

// AccessFlags2 type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccessFlags2KHR
type AccessFlags2 uint32

// AccessFlagBits2 type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccessFlagBits2KHR
type AccessFlagBits2 uint32

// SubmitFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSubmitFlagsKHR
type SubmitFlags uint32

// MemoryBarrier2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkMemoryBarrier2KHR
type MemoryBarrier2 struct {
	// SType         StructureType
	Next          unsafe.Pointer
	SrcStageMask  PipelineStageFlags2
	SrcAccessMask AccessFlags2
	DstStageMask  PipelineStageFlags2
	DstAccessMask AccessFlags2
}

// BufferMemoryBarrier2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkBufferMemoryBarrier2KHR
type BufferMemoryBarrier2 struct {
	// SType               StructureType
	Next                unsafe.Pointer
	SrcStageMask        PipelineStageFlags2
	SrcAccessMask       AccessFlags2
	DstStageMask        PipelineStageFlags2
	DstAccessMask       AccessFlags2
	SrcQueueFamilyIndex uint32
	DstQueueFamilyIndex uint32
	Buffer              Buffer
	Offset              DeviceSize
	Size                DeviceSize
}

// ImageMemoryBarrier2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkImageMemoryBarrier2KHR
type ImageMemoryBarrier2 struct {
	// SType               StructureType
	Next                unsafe.Pointer
	SrcStageMask        PipelineStageFlags2
	SrcAccessMask       AccessFlags2
	DstStageMask        PipelineStageFlags2
	DstAccessMask       AccessFlags2
	OldLayout           ImageLayout
	NewLayout           ImageLayout
	SrcQueueFamilyIndex uint32
	DstQueueFamilyIndex uint32
	Image               Image
	SubresourceRange    ImageSubresourceRange
}

// DependencyInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDependencyInfoKHR
type DependencyInfo struct {
	// SType                    StructureType
	Next                     unsafe.Pointer
	DependencyFlags          DependencyFlags
	MemoryBarrierCount       uint32
	PMemoryBarriers          []MemoryBarrier2
	BufferMemoryBarrierCount uint32
	PBufferMemoryBarriers    []BufferMemoryBarrier2
	ImageMemoryBarrierCount  uint32
	PImageMemoryBarriers     []ImageMemoryBarrier2
}

// SemaphoreSubmitInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSemaphoreSubmitInfoKHR
type SemaphoreSubmitInfo struct {
	// SType       StructureType
	Next        unsafe.Pointer
	Semaphore   Semaphore
	Value       uint32
	StageMask   PipelineStageFlags2
	DeviceIndex uint32
}

// CommandBufferSubmitInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkCommandBufferSubmitInfoKHR
type CommandBufferSubmitInfo struct {
	// SType         StructureType
	Next          unsafe.Pointer
	CommandBuffer CommandBuffer
	DeviceMask    uint32
}

// SubmitInfo2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkSubmitInfo2KHR
type SubmitInfo2 struct {
	// SType                    StructureType
	Next                     unsafe.Pointer
	Flags                    SubmitFlags
	WaitSemaphoreInfoCount   uint32
	PWaitSemaphoreInfos      []SemaphoreSubmitInfo
	CommandBufferInfoCount   uint32
	PCommandBufferInfos      []CommandBufferSubmitInfo
	SignalSemaphoreInfoCount uint32
	PSignalSemaphoreInfos    []SemaphoreSubmitInfo
}

// PhysicalDeviceSynchronization2Features as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDeviceSynchronization2FeaturesKHR
type PhysicalDeviceSynchronization2Features struct {
	// SType            StructureType
	Next             unsafe.Pointer
	Synchronization2 bool
}

// QueueFamilyCheckpointProperties2NV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueueFamilyCheckpointProperties2NV.html
type QueueFamilyCheckpointProperties2NV struct {
	// SType                        StructureType
	Next                         unsafe.Pointer
	CheckpointExecutionStageMask PipelineStageFlags2
}

// CheckpointData2NV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCheckpointData2NV.html
type CheckpointData2NV struct {
	// SType             StructureType
	Next              unsafe.Pointer
	Stage             PipelineStageFlags2
	PCheckpointMarker unsafe.Pointer
}

// PhysicalDeviceZeroInitializeWorkgroupMemoryFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKHR
type PhysicalDeviceZeroInitializeWorkgroupMemoryFeatures struct {
	// SType                               StructureType
	Next                                unsafe.Pointer
	ShaderZeroInitializeWorkgroupMemory bool
}

// PhysicalDeviceWorkgroupMemoryExplicitLayoutFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKHR
type PhysicalDeviceWorkgroupMemoryExplicitLayoutFeatures struct {
	// SType                                          StructureType
	Next                                           unsafe.Pointer
	WorkgroupMemoryExplicitLayout                  bool
	WorkgroupMemoryExplicitLayoutScalarBlockLayout bool
	WorkgroupMemoryExplicitLayout8BitAccess        bool
	WorkgroupMemoryExplicitLayout16BitAccess       bool
}

// BufferCopy2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkBufferCopy2KHR
type BufferCopy2 struct {
	// SType     StructureType
	Next      unsafe.Pointer
	SrcOffset DeviceSize
	DstOffset DeviceSize
	Size      DeviceSize
}

// CopyBufferInfo2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkCopyBufferInfo2KHR
type CopyBufferInfo2 struct {
	// SType       StructureType
	Next        unsafe.Pointer
	SrcBuffer   Buffer
	DstBuffer   Buffer
	RegionCount uint32
	PRegions    []BufferCopy2
}

// ImageCopy2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkImageCopy2KHR
type ImageCopy2 struct {
	// SType          StructureType
	Next           unsafe.Pointer
	SrcSubresource ImageSubresourceLayers
	SrcOffset      Offset3D
	DstSubresource ImageSubresourceLayers
	DstOffset      Offset3D
	Extent         Extent3D
}

// CopyImageInfo2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkCopyImageInfo2KHR
type CopyImageInfo2 struct {
	// SType          StructureType
	Next           unsafe.Pointer
	SrcImage       Image
	SrcImageLayout ImageLayout
	DstImage       Image
	DstImageLayout ImageLayout
	RegionCount    uint32
	PRegions       []ImageCopy2
}

// BufferImageCopy2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkBufferImageCopy2KHR
type BufferImageCopy2 struct {
	// SType             StructureType
	Next              unsafe.Pointer
	BufferOffset      DeviceSize
	BufferRowLength   uint32
	BufferImageHeight uint32
	ImageSubresource  ImageSubresourceLayers
	ImageOffset       Offset3D
	ImageExtent       Extent3D
}

// CopyBufferToImageInfo2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkCopyBufferToImageInfo2KHR
type CopyBufferToImageInfo2 struct {
	// SType          StructureType
	Next           unsafe.Pointer
	SrcBuffer      Buffer
	DstImage       Image
	DstImageLayout ImageLayout
	RegionCount    uint32
	PRegions       []BufferImageCopy2
}

// CopyImageToBufferInfo2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkCopyImageToBufferInfo2KHR
type CopyImageToBufferInfo2 struct {
	// SType          StructureType
	Next           unsafe.Pointer
	SrcImage       Image
	SrcImageLayout ImageLayout
	DstBuffer      Buffer
	RegionCount    uint32
	PRegions       []BufferImageCopy2
}

// ImageBlit2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkImageBlit2KHR
type ImageBlit2 struct {
	// SType          StructureType
	Next           unsafe.Pointer
	SrcSubresource ImageSubresourceLayers
	SrcOffsets     [2]Offset3D
	DstSubresource ImageSubresourceLayers
	DstOffsets     [2]Offset3D
}

// BlitImageInfo2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkBlitImageInfo2KHR
type BlitImageInfo2 struct {
	// SType          StructureType
	Next           unsafe.Pointer
	SrcImage       Image
	SrcImageLayout ImageLayout
	DstImage       Image
	DstImageLayout ImageLayout
	RegionCount    uint32
	PRegions       []ImageBlit2
	Filter         Filter
}

// ImageResolve2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkImageResolve2KHR
type ImageResolve2 struct {
	// SType          StructureType
	Next           unsafe.Pointer
	SrcSubresource ImageSubresourceLayers
	SrcOffset      Offset3D
	DstSubresource ImageSubresourceLayers
	DstOffset      Offset3D
	Extent         Extent3D
}

// ResolveImageInfo2 as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkResolveImageInfo2KHR
type ResolveImageInfo2 struct {
	// SType          StructureType
	Next           unsafe.Pointer
	SrcImage       Image
	SrcImageLayout ImageLayout
	DstImage       Image
	DstImageLayout ImageLayout
	RegionCount    uint32
	PRegions       []ImageResolve2
}

// DebugReportCallback as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugReportCallbackEXT.html
type DebugReportCallback C.VkDebugReportCallbackEXT

// DebugReportFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugReportFlagsEXT.html
type DebugReportFlags uint32

// DebugReportCallbackFunc type as declared in vulkan/vulkan_core.h:8320
type DebugReportCallbackFunc func(flags DebugReportFlags, objectType DebugReportObjectType, object uint32, location uint32, messageCode int32, pLayerPrefix string, pMessage string, pUserData unsafe.Pointer) bool

// DebugReportCallbackCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugReportCallbackCreateInfoEXT.html
type DebugReportCallbackCreateInfo struct {
	// SType       StructureType
	Next        unsafe.Pointer
	Flags       DebugReportFlags
	PfnCallback DebugReportCallbackFunc
	PUserData   unsafe.Pointer
}

// PipelineRasterizationStateRasterizationOrderAMD as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkPipelineRasterizationStateRasterizationOrderAMD
type PipelineRasterizationStateRasterizationOrderAMD struct {
	// SType              StructureType
	Next               unsafe.Pointer
	RasterizationOrder RasterizationOrderAMD
}

// DebugMarkerObjectNameInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugMarkerObjectNameInfoEXT.html
type DebugMarkerObjectNameInfo struct {
	// SType       StructureType
	Next        unsafe.Pointer
	ObjectType  DebugReportObjectType
	Object      uint32
	PObjectName string
}

// DebugMarkerObjectTagInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugMarkerObjectTagInfoEXT.html
type DebugMarkerObjectTagInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	ObjectType DebugReportObjectType
	Object     uint32
	TagName    uint32
	TagSize    uint32
	PTag       unsafe.Pointer
}

// DebugMarkerMarkerInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugMarkerMarkerInfoEXT.html
type DebugMarkerMarkerInfo struct {
	// SType       StructureType
	Next        unsafe.Pointer
	PMarkerName string
	Color       [4]float32
}

// DedicatedAllocationImageCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDedicatedAllocationImageCreateInfoNV.html
type DedicatedAllocationImageCreateInfoNV struct {
	// SType               StructureType
	Next                unsafe.Pointer
	DedicatedAllocation bool
}

// DedicatedAllocationBufferCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDedicatedAllocationBufferCreateInfoNV.html
type DedicatedAllocationBufferCreateInfoNV struct {
	// SType               StructureType
	Next                unsafe.Pointer
	DedicatedAllocation bool
}

// DedicatedAllocationMemoryAllocateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDedicatedAllocationMemoryAllocateInfoNV.html
type DedicatedAllocationMemoryAllocateInfoNV struct {
	// SType  StructureType
	Next   unsafe.Pointer
	Image  Image
	Buffer Buffer
}

// PipelineRasterizationStateStreamCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineRasterizationStateStreamCreateFlagsEXT.html
type PipelineRasterizationStateStreamCreateFlags uint32

// PhysicalDeviceTransformFeedbackFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceTransformFeedbackFeaturesEXT.html
type PhysicalDeviceTransformFeedbackFeatures struct {
	// SType             StructureType
	Next              unsafe.Pointer
	TransformFeedback bool
	GeometryStreams   bool
}

// PhysicalDeviceTransformFeedbackProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceTransformFeedbackPropertiesEXT.html
type PhysicalDeviceTransformFeedbackProperties struct {
	// SType                                      StructureType
	Next                                       unsafe.Pointer
	MaxTransformFeedbackStreams                uint32
	MaxTransformFeedbackBuffers                uint32
	MaxTransformFeedbackBufferSize             DeviceSize
	MaxTransformFeedbackStreamDataSize         uint32
	MaxTransformFeedbackBufferDataSize         uint32
	MaxTransformFeedbackBufferDataStride       uint32
	TransformFeedbackQueries                   bool
	TransformFeedbackStreamsLinesTriangles     bool
	TransformFeedbackRasterizationStreamSelect bool
	TransformFeedbackDraw                      bool
}

// PipelineRasterizationStateStreamCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineRasterizationStateStreamCreateInfoEXT.html
type PipelineRasterizationStateStreamCreateInfo struct {
	// SType               StructureType
	Next                unsafe.Pointer
	Flags               PipelineRasterizationStateStreamCreateFlags
	RasterizationStream uint32
}

// ImageViewHandleInfoNVX as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkImageViewHandleInfoNVX
type ImageViewHandleInfoNVX struct {
	// SType          StructureType
	Next           unsafe.Pointer
	ImageView      ImageView
	DescriptorType DescriptorType
	Sampler        Sampler
}

// ImageViewAddressPropertiesNVX as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkImageViewAddressPropertiesNVX
type ImageViewAddressPropertiesNVX struct {
	// SType         StructureType
	Next          unsafe.Pointer
	DeviceAddress DeviceAddress
	Size          DeviceSize
}

// TextureLODGatherFormatPropertiesAMD as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkTextureLODGatherFormatPropertiesAMD
type TextureLODGatherFormatPropertiesAMD struct {
	// SType                           StructureType
	Next                            unsafe.Pointer
	SupportsTextureGatherLODBiasAMD bool
}

// ShaderResourceUsageAMD as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkShaderResourceUsageAMD
type ShaderResourceUsageAMD struct {
	NumUsedVgprs             uint32
	NumUsedSgprs             uint32
	LdsSizePerLocalWorkGroup uint32
	LdsUsageSizeInBytes      uint32
	ScratchMemUsageInBytes   uint32
}

// ShaderStatisticsInfoAMD as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkShaderStatisticsInfoAMD
type ShaderStatisticsInfoAMD struct {
	ShaderStageMask      ShaderStageFlags
	ResourceUsage        ShaderResourceUsageAMD
	NumPhysicalVgprs     uint32
	NumPhysicalSgprs     uint32
	NumAvailableVgprs    uint32
	NumAvailableSgprs    uint32
	ComputeWorkGroupSize [3]uint32
}

// PhysicalDeviceCornerSampledImageFeaturesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceCornerSampledImageFeaturesNV.html
type PhysicalDeviceCornerSampledImageFeaturesNV struct {
	// SType              StructureType
	Next               unsafe.Pointer
	CornerSampledImage bool
}

// ExternalMemoryHandleTypeFlagsNV type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalMemoryHandleTypeFlagsNV.html
type ExternalMemoryHandleTypeFlagsNV uint32

// ExternalMemoryFeatureFlagsNV type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalMemoryFeatureFlagsNV.html
type ExternalMemoryFeatureFlagsNV uint32

// ExternalImageFormatPropertiesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalImageFormatPropertiesNV.html
type ExternalImageFormatPropertiesNV struct {
	ImageFormatProperties         ImageFormatProperties
	ExternalMemoryFeatures        ExternalMemoryFeatureFlagsNV
	ExportFromImportedHandleTypes ExternalMemoryHandleTypeFlagsNV
	CompatibleHandleTypes         ExternalMemoryHandleTypeFlagsNV
}

// ExternalMemoryImageCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExternalMemoryImageCreateInfoNV.html
type ExternalMemoryImageCreateInfoNV struct {
	// SType       StructureType
	Next        unsafe.Pointer
	HandleTypes ExternalMemoryHandleTypeFlagsNV
}

// ExportMemoryAllocateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkExportMemoryAllocateInfoNV.html
type ExportMemoryAllocateInfoNV struct {
	// SType       StructureType
	Next        unsafe.Pointer
	HandleTypes ExternalMemoryHandleTypeFlagsNV
}

// ValidationFlags as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkValidationFlagsEXT.html
type ValidationFlags struct {
	// SType                        StructureType
	Next                         unsafe.Pointer
	DisabledValidationCheckCount uint32
	PDisabledValidationChecks    []ValidationCheck
}

// PhysicalDeviceTextureCompressionASTCHDRFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceTextureCompressionASTCHDRFeaturesEXT.html
type PhysicalDeviceTextureCompressionASTCHDRFeatures struct {
	// SType                      StructureType
	Next                       unsafe.Pointer
	TextureCompressionASTC_HDR bool
}

// ImageViewASTCDecodeMode as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageViewASTCDecodeModeEXT.html
type ImageViewASTCDecodeMode struct {
	// SType      StructureType
	Next       unsafe.Pointer
	DecodeMode Format
}

// PhysicalDeviceASTCDecodeFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceASTCDecodeFeaturesEXT.html
type PhysicalDeviceASTCDecodeFeatures struct {
	// SType                    StructureType
	Next                     unsafe.Pointer
	DecodeModeSharedExponent bool
}

// ConditionalRenderingFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkConditionalRenderingFlagsEXT.html
type ConditionalRenderingFlags uint32

// ConditionalRenderingBeginInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkConditionalRenderingBeginInfoEXT.html
type ConditionalRenderingBeginInfo struct {
	// SType  StructureType
	Next   unsafe.Pointer
	Buffer Buffer
	Offset DeviceSize
	Flags  ConditionalRenderingFlags
}

// PhysicalDeviceConditionalRenderingFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceConditionalRenderingFeaturesEXT.html
type PhysicalDeviceConditionalRenderingFeatures struct {
	// SType                         StructureType
	Next                          unsafe.Pointer
	ConditionalRendering          bool
	InheritedConditionalRendering bool
}

// CommandBufferInheritanceConditionalRenderingInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandBufferInheritanceConditionalRenderingInfoEXT.html
type CommandBufferInheritanceConditionalRenderingInfo struct {
	// SType                      StructureType
	Next                       unsafe.Pointer
	ConditionalRenderingEnable bool
}

// ViewportWScalingNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkViewportWScalingNV.html
type ViewportWScalingNV struct {
	Xcoeff float32
	Ycoeff float32
}

// PipelineViewportWScalingStateCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineViewportWScalingStateCreateInfoNV.html
type PipelineViewportWScalingStateCreateInfoNV struct {
	// SType                  StructureType
	Next                   unsafe.Pointer
	ViewportWScalingEnable bool
	ViewportCount          uint32
	PViewportWScalings     []ViewportWScalingNV
}

// SurfaceCounterFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSurfaceCounterFlagsEXT.html
type SurfaceCounterFlags uint32

// DisplayPowerInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDisplayPowerInfoEXT.html
type DisplayPowerInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	PowerState DisplayPowerState
}

// DeviceEventInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceEventInfoEXT.html
type DeviceEventInfo struct {
	// SType       StructureType
	Next        unsafe.Pointer
	DeviceEvent DeviceEventType
}

// DisplayEventInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDisplayEventInfoEXT.html
type DisplayEventInfo struct {
	// SType        StructureType
	Next         unsafe.Pointer
	DisplayEvent DisplayEventType
}

// SwapchainCounterCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSwapchainCounterCreateInfoEXT.html
type SwapchainCounterCreateInfo struct {
	// SType           StructureType
	Next            unsafe.Pointer
	SurfaceCounters SurfaceCounterFlags
}

// RefreshCycleDurationGOOGLE as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkRefreshCycleDurationGOOGLE.html
type RefreshCycleDurationGOOGLE struct {
	RefreshDuration uint32
}

// PastPresentationTimingGOOGLE as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPastPresentationTimingGOOGLE.html
type PastPresentationTimingGOOGLE struct {
	PresentID           uint32
	DesiredPresentTime  uint32
	ActualPresentTime   uint32
	EarliestPresentTime uint32
	PresentMargin       uint32
}

// PresentTimeGOOGLE as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPresentTimeGOOGLE.html
type PresentTimeGOOGLE struct {
	PresentID          uint32
	DesiredPresentTime uint32
}

// PresentTimesInfoGOOGLE as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPresentTimesInfoGOOGLE.html
type PresentTimesInfoGOOGLE struct {
	// SType          StructureType
	Next           unsafe.Pointer
	SwapchainCount uint32
	PTimes         []PresentTimeGOOGLE
}

// PhysicalDeviceMultiviewPerViewAttributesPropertiesNVX as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkPhysicalDeviceMultiviewPerViewAttributesPropertiesNVX
type PhysicalDeviceMultiviewPerViewAttributesPropertiesNVX struct {
	// SType                        StructureType
	Next                         unsafe.Pointer
	PerViewPositionAllComponents bool
}

// PipelineViewportSwizzleStateCreateFlagsNV type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineViewportSwizzleStateCreateFlagsNV.html
type PipelineViewportSwizzleStateCreateFlagsNV uint32

// ViewportSwizzleNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkViewportSwizzleNV.html
type ViewportSwizzleNV struct {
	X ViewportCoordinateSwizzleNV
	Y ViewportCoordinateSwizzleNV
	Z ViewportCoordinateSwizzleNV
	W ViewportCoordinateSwizzleNV
}

// PipelineViewportSwizzleStateCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineViewportSwizzleStateCreateInfoNV.html
type PipelineViewportSwizzleStateCreateInfoNV struct {
	// SType             StructureType
	Next              unsafe.Pointer
	Flags             PipelineViewportSwizzleStateCreateFlagsNV
	ViewportCount     uint32
	PViewportSwizzles []ViewportSwizzleNV
}

// PipelineDiscardRectangleStateCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineDiscardRectangleStateCreateFlagsEXT.html
type PipelineDiscardRectangleStateCreateFlags uint32

// PhysicalDeviceDiscardRectangleProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceDiscardRectanglePropertiesEXT.html
type PhysicalDeviceDiscardRectangleProperties struct {
	// SType                StructureType
	Next                 unsafe.Pointer
	MaxDiscardRectangles uint32
}

// PipelineDiscardRectangleStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineDiscardRectangleStateCreateInfoEXT.html
type PipelineDiscardRectangleStateCreateInfo struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	Flags                 PipelineDiscardRectangleStateCreateFlags
	DiscardRectangleMode  DiscardRectangleMode
	DiscardRectangleCount uint32
	PDiscardRectangles    []Rect2D
}

// PipelineRasterizationConservativeStateCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineRasterizationConservativeStateCreateFlagsEXT.html
type PipelineRasterizationConservativeStateCreateFlags uint32

// PhysicalDeviceConservativeRasterizationProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceConservativeRasterizationPropertiesEXT.html
type PhysicalDeviceConservativeRasterizationProperties struct {
	// SType                                       StructureType
	Next                                        unsafe.Pointer
	PrimitiveOverestimationSize                 float32
	MaxExtraPrimitiveOverestimationSize         float32
	ExtraPrimitiveOverestimationSizeGranularity float32
	PrimitiveUnderestimation                    bool
	ConservativePointAndLineRasterization       bool
	DegenerateTrianglesRasterized               bool
	DegenerateLinesRasterized                   bool
	FullyCoveredFragmentShaderInputVariable     bool
	ConservativeRasterizationPostDepthCoverage  bool
}

// PipelineRasterizationConservativeStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineRasterizationConservativeStateCreateInfoEXT.html
type PipelineRasterizationConservativeStateCreateInfo struct {
	// SType                            StructureType
	Next                             unsafe.Pointer
	Flags                            PipelineRasterizationConservativeStateCreateFlags
	ConservativeRasterizationMode    ConservativeRasterizationMode
	ExtraPrimitiveOverestimationSize float32
}

// PipelineRasterizationDepthClipStateCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineRasterizationDepthClipStateCreateFlagsEXT.html
type PipelineRasterizationDepthClipStateCreateFlags uint32

// PhysicalDeviceDepthClipEnableFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceDepthClipEnableFeaturesEXT.html
type PhysicalDeviceDepthClipEnableFeatures struct {
	// SType           StructureType
	Next            unsafe.Pointer
	DepthClipEnable bool
}

// PipelineRasterizationDepthClipStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineRasterizationDepthClipStateCreateInfoEXT.html
type PipelineRasterizationDepthClipStateCreateInfo struct {
	// SType           StructureType
	Next            unsafe.Pointer
	Flags           PipelineRasterizationDepthClipStateCreateFlags
	DepthClipEnable bool
}

// XYColor as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkXYColorEXT.html
type XYColor struct {
	X float32
	Y float32
}

// HdrMetadata as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkHdrMetadataEXT.html
type HdrMetadata struct {
	// SType                     StructureType
	Next                      unsafe.Pointer
	DisplayPrimaryRed         XYColor
	DisplayPrimaryGreen       XYColor
	DisplayPrimaryBlue        XYColor
	WhitePoint                XYColor
	MaxLuminance              float32
	MinLuminance              float32
	MaxContentLightLevel      float32
	MaxFrameAverageLightLevel float32
}

// DebugUtilsMessageTypeFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugUtilsMessageTypeFlagsEXT.html
type DebugUtilsMessageTypeFlags uint32

// DebugUtilsMessageSeverityFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugUtilsMessageSeverityFlagsEXT.html
type DebugUtilsMessageSeverityFlags uint32

// DebugUtilsLabel as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugUtilsLabelEXT.html
type DebugUtilsLabel struct {
	// SType      StructureType
	Next       unsafe.Pointer
	PLabelName string
	Color      [4]float32
}

// DebugUtilsObjectNameInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugUtilsObjectNameInfoEXT.html
type DebugUtilsObjectNameInfo struct {
	// SType        StructureType
	Next         unsafe.Pointer
	ObjectType   ObjectType
	ObjectHandle uint32
	PObjectName  string
}

// DebugUtilsObjectTagInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDebugUtilsObjectTagInfoEXT.html
type DebugUtilsObjectTagInfo struct {
	// SType        StructureType
	Next         unsafe.Pointer
	ObjectType   ObjectType
	ObjectHandle uint32
	TagName      uint32
	TagSize      uint32
	PTag         unsafe.Pointer
}

// PhysicalDeviceInlineUniformBlockFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceInlineUniformBlockFeaturesEXT.html
type PhysicalDeviceInlineUniformBlockFeatures struct {
	// SType                                              StructureType
	Next                                               unsafe.Pointer
	InlineUniformBlock                                 bool
	DescriptorBindingInlineUniformBlockUpdateAfterBind bool
}

// PhysicalDeviceInlineUniformBlockProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceInlineUniformBlockPropertiesEXT.html
type PhysicalDeviceInlineUniformBlockProperties struct {
	// SType                                                   StructureType
	Next                                                    unsafe.Pointer
	MaxInlineUniformBlockSize                               uint32
	MaxPerStageDescriptorInlineUniformBlocks                uint32
	MaxPerStageDescriptorUpdateAfterBindInlineUniformBlocks uint32
	MaxDescriptorSetInlineUniformBlocks                     uint32
	MaxDescriptorSetUpdateAfterBindInlineUniformBlocks      uint32
}

// WriteDescriptorSetInlineUniformBlock as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkWriteDescriptorSetInlineUniformBlockEXT.html
type WriteDescriptorSetInlineUniformBlock struct {
	// SType    StructureType
	Next     unsafe.Pointer
	DataSize uint32
	PData    unsafe.Pointer
}

// DescriptorPoolInlineUniformBlockCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDescriptorPoolInlineUniformBlockCreateInfoEXT.html
type DescriptorPoolInlineUniformBlockCreateInfo struct {
	// SType                         StructureType
	Next                          unsafe.Pointer
	MaxInlineUniformBlockBindings uint32
}

// SampleLocation as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSampleLocationEXT.html
type SampleLocation struct {
	X float32
	Y float32
}

// SampleLocationsInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSampleLocationsInfoEXT.html
type SampleLocationsInfo struct {
	// SType                   StructureType
	Next                    unsafe.Pointer
	SampleLocationsPerPixel SampleCountFlagBits
	SampleLocationGridSize  Extent2D
	SampleLocationsCount    uint32
	PSampleLocations        []SampleLocation
}

// AttachmentSampleLocations as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAttachmentSampleLocationsEXT.html
type AttachmentSampleLocations struct {
	AttachmentIndex     uint32
	SampleLocationsInfo SampleLocationsInfo
}

// SubpassSampleLocations as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSubpassSampleLocationsEXT.html
type SubpassSampleLocations struct {
	SubpassIndex        uint32
	SampleLocationsInfo SampleLocationsInfo
}

// RenderPassSampleLocationsBeginInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkRenderPassSampleLocationsBeginInfoEXT.html
type RenderPassSampleLocationsBeginInfo struct {
	// SType                                 StructureType
	Next                                  unsafe.Pointer
	AttachmentInitialSampleLocationsCount uint32
	PAttachmentInitialSampleLocations     []AttachmentSampleLocations
	PostSubpassSampleLocationsCount       uint32
	PPostSubpassSampleLocations           []SubpassSampleLocations
}

// PipelineSampleLocationsStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineSampleLocationsStateCreateInfoEXT.html
type PipelineSampleLocationsStateCreateInfo struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	SampleLocationsEnable bool
	SampleLocationsInfo   SampleLocationsInfo
}

// PhysicalDeviceSampleLocationsProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceSampleLocationsPropertiesEXT.html
type PhysicalDeviceSampleLocationsProperties struct {
	// SType                         StructureType
	Next                          unsafe.Pointer
	SampleLocationSampleCounts    SampleCountFlags
	MaxSampleLocationGridSize     Extent2D
	SampleLocationCoordinateRange [2]float32
	SampleLocationSubPixelBits    uint32
	VariableSampleLocations       bool
}

// MultisampleProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMultisamplePropertiesEXT.html
type MultisampleProperties struct {
	// SType                     StructureType
	Next                      unsafe.Pointer
	MaxSampleLocationGridSize Extent2D
}

// PhysicalDeviceBlendOperationAdvancedFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceBlendOperationAdvancedFeaturesEXT.html
type PhysicalDeviceBlendOperationAdvancedFeatures struct {
	// SType                           StructureType
	Next                            unsafe.Pointer
	AdvancedBlendCoherentOperations bool
}

// PhysicalDeviceBlendOperationAdvancedProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceBlendOperationAdvancedPropertiesEXT.html
type PhysicalDeviceBlendOperationAdvancedProperties struct {
	// SType                                 StructureType
	Next                                  unsafe.Pointer
	AdvancedBlendMaxColorAttachments      uint32
	AdvancedBlendIndependentBlend         bool
	AdvancedBlendNonPremultipliedSrcColor bool
	AdvancedBlendNonPremultipliedDstColor bool
	AdvancedBlendCorrelatedOverlap        bool
	AdvancedBlendAllOperations            bool
}

// PipelineColorBlendAdvancedStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineColorBlendAdvancedStateCreateInfoEXT.html
type PipelineColorBlendAdvancedStateCreateInfo struct {
	// SType            StructureType
	Next             unsafe.Pointer
	SrcPremultiplied bool
	DstPremultiplied bool
	BlendOverlap     BlendOverlap
}

// PipelineCoverageToColorStateCreateFlagsNV type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCoverageToColorStateCreateFlagsNV.html
type PipelineCoverageToColorStateCreateFlagsNV uint32

// PipelineCoverageToColorStateCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCoverageToColorStateCreateInfoNV.html
type PipelineCoverageToColorStateCreateInfoNV struct {
	// SType                   StructureType
	Next                    unsafe.Pointer
	Flags                   PipelineCoverageToColorStateCreateFlagsNV
	CoverageToColorEnable   bool
	CoverageToColorLocation uint32
}

// PipelineCoverageModulationStateCreateFlagsNV type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCoverageModulationStateCreateFlagsNV.html
type PipelineCoverageModulationStateCreateFlagsNV uint32

// PipelineCoverageModulationStateCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCoverageModulationStateCreateInfoNV.html
type PipelineCoverageModulationStateCreateInfoNV struct {
	// SType                         StructureType
	Next                          unsafe.Pointer
	Flags                         PipelineCoverageModulationStateCreateFlagsNV
	CoverageModulationMode        CoverageModulationModeNV
	CoverageModulationTableEnable bool
	CoverageModulationTableCount  uint32
	PCoverageModulationTable      []float32
}

// PhysicalDeviceShaderSMBuiltinsPropertiesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceShaderSMBuiltinsPropertiesNV.html
type PhysicalDeviceShaderSMBuiltinsPropertiesNV struct {
	// SType            StructureType
	Next             unsafe.Pointer
	ShaderSMCount    uint32
	ShaderWarpsPerSM uint32
}

// PhysicalDeviceShaderSMBuiltinsFeaturesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceShaderSMBuiltinsFeaturesNV.html
type PhysicalDeviceShaderSMBuiltinsFeaturesNV struct {
	// SType            StructureType
	Next             unsafe.Pointer
	ShaderSMBuiltins bool
}

// DrmFormatModifierProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDrmFormatModifierPropertiesEXT.html
type DrmFormatModifierProperties struct {
	DrmFormatModifier               uint32
	DrmFormatModifierPlaneCount     uint32
	DrmFormatModifierTilingFeatures FormatFeatureFlags
}

// DrmFormatModifierPropertiesList as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDrmFormatModifierPropertiesListEXT.html
type DrmFormatModifierPropertiesList struct {
	// SType                        StructureType
	Next                         unsafe.Pointer
	DrmFormatModifierCount       uint32
	PDrmFormatModifierProperties []DrmFormatModifierProperties
}

// PhysicalDeviceImageDrmFormatModifierInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceImageDrmFormatModifierInfoEXT.html
type PhysicalDeviceImageDrmFormatModifierInfo struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	DrmFormatModifier     uint32
	SharingMode           SharingMode
	QueueFamilyIndexCount uint32
	PQueueFamilyIndices   []uint32
}

// ImageDrmFormatModifierListCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageDrmFormatModifierListCreateInfoEXT.html
type ImageDrmFormatModifierListCreateInfo struct {
	// SType                  StructureType
	Next                   unsafe.Pointer
	DrmFormatModifierCount uint32
	PDrmFormatModifiers    []uint32
}

// ImageDrmFormatModifierExplicitCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageDrmFormatModifierExplicitCreateInfoEXT.html
type ImageDrmFormatModifierExplicitCreateInfo struct {
	// SType                       StructureType
	Next                        unsafe.Pointer
	DrmFormatModifier           uint32
	DrmFormatModifierPlaneCount uint32
	PPlaneLayouts               []SubresourceLayout
}

// ImageDrmFormatModifierProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImageDrmFormatModifierPropertiesEXT.html
type ImageDrmFormatModifierProperties struct {
	// SType             StructureType
	Next              unsafe.Pointer
	DrmFormatModifier uint32
}

// ValidationCache as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkValidationCacheEXT.html
type ValidationCache C.VkValidationCacheEXT

// ValidationCacheCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkValidationCacheCreateFlagsEXT.html
type ValidationCacheCreateFlags uint32

// ValidationCacheCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkValidationCacheCreateInfoEXT.html
type ValidationCacheCreateInfo struct {
	// SType           StructureType
	Next            unsafe.Pointer
	Flags           ValidationCacheCreateFlags
	InitialDataSize uint32
	PInitialData    unsafe.Pointer
}

// ShaderModuleValidationCacheCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkShaderModuleValidationCacheCreateInfoEXT.html
type ShaderModuleValidationCacheCreateInfo struct {
	// SType           StructureType
	Next            unsafe.Pointer
	ValidationCache ValidationCache
}

// ShadingRatePaletteNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkShadingRatePaletteNV.html
type ShadingRatePaletteNV struct {
	ShadingRatePaletteEntryCount uint32
	PShadingRatePaletteEntries   []ShadingRatePaletteEntryNV
}

// PipelineViewportShadingRateImageStateCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineViewportShadingRateImageStateCreateInfoNV.html
type PipelineViewportShadingRateImageStateCreateInfoNV struct {
	// SType                  StructureType
	Next                   unsafe.Pointer
	ShadingRateImageEnable bool
	ViewportCount          uint32
	PShadingRatePalettes   []ShadingRatePaletteNV
}

// PhysicalDeviceShadingRateImageFeaturesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceShadingRateImageFeaturesNV.html
type PhysicalDeviceShadingRateImageFeaturesNV struct {
	// SType                        StructureType
	Next                         unsafe.Pointer
	ShadingRateImage             bool
	ShadingRateCoarseSampleOrder bool
}

// PhysicalDeviceShadingRateImagePropertiesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceShadingRateImagePropertiesNV.html
type PhysicalDeviceShadingRateImagePropertiesNV struct {
	// SType                       StructureType
	Next                        unsafe.Pointer
	ShadingRateTexelSize        Extent2D
	ShadingRatePaletteSize      uint32
	ShadingRateMaxCoarseSamples uint32
}

// CoarseSampleLocationNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCoarseSampleLocationNV.html
type CoarseSampleLocationNV struct {
	PixelX uint32
	PixelY uint32
	Sample uint32
}

// CoarseSampleOrderCustomNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCoarseSampleOrderCustomNV.html
type CoarseSampleOrderCustomNV struct {
	ShadingRate         ShadingRatePaletteEntryNV
	SampleCount         uint32
	SampleLocationCount uint32
	PSampleLocations    []CoarseSampleLocationNV
}

// PipelineViewportCoarseSampleOrderStateCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineViewportCoarseSampleOrderStateCreateInfoNV.html
type PipelineViewportCoarseSampleOrderStateCreateInfoNV struct {
	// SType                  StructureType
	Next                   unsafe.Pointer
	SampleOrderType        CoarseSampleOrderTypeNV
	CustomSampleOrderCount uint32
	PCustomSampleOrders    []CoarseSampleOrderCustomNV
}

// AccelerationStructureNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAccelerationStructureNV.html
type AccelerationStructureNV C.VkAccelerationStructureNV

// GeometryFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkGeometryFlagsKHR
type GeometryFlags uint32

// GeometryFlagsNV type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkGeometryFlagsNV.html
type GeometryFlagsNV uint32

// GeometryInstanceFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkGeometryInstanceFlagsKHR
type GeometryInstanceFlags uint32

// GeometryInstanceFlagsNV type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkGeometryInstanceFlagsNV.html
type GeometryInstanceFlagsNV uint32

// BuildAccelerationStructureFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkBuildAccelerationStructureFlagsKHR
type BuildAccelerationStructureFlags uint32

// BuildAccelerationStructureFlagsNV type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBuildAccelerationStructureFlagsNV.html
type BuildAccelerationStructureFlagsNV uint32

// RayTracingShaderGroupCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkRayTracingShaderGroupCreateInfoNV.html
type RayTracingShaderGroupCreateInfoNV struct {
	// SType              StructureType
	Next               unsafe.Pointer
	Type               RayTracingShaderGroupType
	GeneralShader      uint32
	ClosestHitShader   uint32
	AnyHitShader       uint32
	IntersectionShader uint32
}

// RayTracingPipelineCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkRayTracingPipelineCreateInfoNV.html
type RayTracingPipelineCreateInfoNV struct {
	// SType              StructureType
	Next               unsafe.Pointer
	Flags              PipelineCreateFlags
	StageCount         uint32
	PStages            []PipelineShaderStageCreateInfo
	GroupCount         uint32
	PGroups            []RayTracingShaderGroupCreateInfoNV
	MaxRecursionDepth  uint32
	Layout             PipelineLayout
	BasePipelineHandle Pipeline
	BasePipelineIndex  int32
}

// GeometryTrianglesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkGeometryTrianglesNV.html
type GeometryTrianglesNV struct {
	// SType           StructureType
	Next            unsafe.Pointer
	VertexData      Buffer
	VertexOffset    DeviceSize
	VertexCount     uint32
	VertexStride    DeviceSize
	VertexFormat    Format
	IndexData       Buffer
	IndexOffset     DeviceSize
	IndexCount      uint32
	IndexType       IndexType
	TransformData   Buffer
	TransformOffset DeviceSize
}

// GeometryAABBNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkGeometryAABBNV.html
type GeometryAABBNV struct {
	// SType    StructureType
	Next     unsafe.Pointer
	AabbData Buffer
	NumAABBs uint32
	Stride   uint32
	Offset   DeviceSize
}

// GeometryDataNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkGeometryDataNV.html
type GeometryDataNV struct {
	Triangles GeometryTrianglesNV
	Aabbs     GeometryAABBNV
}

// GeometryNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkGeometryNV.html
type GeometryNV struct {
	// SType        StructureType
	Next         unsafe.Pointer
	GeometryType GeometryType
	Geometry     GeometryDataNV
	Flags        GeometryFlags
}

// AccelerationStructureInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAccelerationStructureInfoNV.html
type AccelerationStructureInfoNV struct {
	// SType         StructureType
	Next          unsafe.Pointer
	Type          AccelerationStructureTypeNV
	Flags         BuildAccelerationStructureFlagsNV
	InstanceCount uint32
	GeometryCount uint32
	PGeometries   []GeometryNV
}

// AccelerationStructureCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAccelerationStructureCreateInfoNV.html
type AccelerationStructureCreateInfoNV struct {
	// SType         StructureType
	Next          unsafe.Pointer
	CompactedSize DeviceSize
	Info          AccelerationStructureInfoNV
}

// BindAccelerationStructureMemoryInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBindAccelerationStructureMemoryInfoNV.html
type BindAccelerationStructureMemoryInfoNV struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	AccelerationStructure AccelerationStructureNV
	Memory                DeviceMemory
	MemoryOffset          DeviceSize
	DeviceIndexCount      uint32
	PDeviceIndices        []uint32
}

// WriteDescriptorSetAccelerationStructureNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkWriteDescriptorSetAccelerationStructureNV.html
type WriteDescriptorSetAccelerationStructureNV struct {
	// SType                      StructureType
	Next                       unsafe.Pointer
	AccelerationStructureCount uint32
	PAccelerationStructures    []AccelerationStructureNV
}

// AccelerationStructureMemoryRequirementsInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAccelerationStructureMemoryRequirementsInfoNV.html
type AccelerationStructureMemoryRequirementsInfoNV struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	Type                  AccelerationStructureMemoryRequirementsTypeNV
	AccelerationStructure AccelerationStructureNV
}

// PhysicalDeviceRayTracingPropertiesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceRayTracingPropertiesNV.html
type PhysicalDeviceRayTracingPropertiesNV struct {
	// SType                                  StructureType
	Next                                   unsafe.Pointer
	ShaderGroupHandleSize                  uint32
	MaxRecursionDepth                      uint32
	MaxShaderGroupStride                   uint32
	ShaderGroupBaseAlignment               uint32
	MaxGeometryCount                       uint32
	MaxInstanceCount                       uint32
	MaxTriangleCount                       uint32
	MaxDescriptorSetAccelerationStructures uint32
}

// TransformMatrix as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkTransformMatrixKHR
type TransformMatrix struct {
	Matrix [3][4]float32
}

// TransformMatrixNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkTransformMatrixNV.html
type TransformMatrixNV struct {
	Matrix [3][4]float32
}

// AabbPositions as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAabbPositionsKHR
type AabbPositions struct {
	MinX float32
	MinY float32
	MinZ float32
	MaxX float32
	MaxY float32
	MaxZ float32
}

// AabbPositionsNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkAabbPositionsNV.html
type AabbPositionsNV struct {
	MinX float32
	MinY float32
	MinZ float32
	MaxX float32
	MaxY float32
	MaxZ float32
}

// PhysicalDeviceRepresentativeFragmentTestFeaturesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceRepresentativeFragmentTestFeaturesNV.html
type PhysicalDeviceRepresentativeFragmentTestFeaturesNV struct {
	// SType                      StructureType
	Next                       unsafe.Pointer
	RepresentativeFragmentTest bool
}

// PipelineRepresentativeFragmentTestStateCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineRepresentativeFragmentTestStateCreateInfoNV.html
type PipelineRepresentativeFragmentTestStateCreateInfoNV struct {
	// SType                            StructureType
	Next                             unsafe.Pointer
	RepresentativeFragmentTestEnable bool
}

// PhysicalDeviceImageViewImageFormatInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceImageViewImageFormatInfoEXT.html
type PhysicalDeviceImageViewImageFormatInfo struct {
	// SType         StructureType
	Next          unsafe.Pointer
	ImageViewType ImageViewType
}

// FilterCubicImageViewImageFormatProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFilterCubicImageViewImageFormatPropertiesEXT.html
type FilterCubicImageViewImageFormatProperties struct {
	// SType             StructureType
	Next              unsafe.Pointer
	FilterCubic       bool
	FilterCubicMinmax bool
}

// DeviceQueueGlobalPriorityCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceQueueGlobalPriorityCreateInfoEXT.html
type DeviceQueueGlobalPriorityCreateInfo struct {
	// SType          StructureType
	Next           unsafe.Pointer
	GlobalPriority QueueGlobalPriority
}

// ImportMemoryHostPointerInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkImportMemoryHostPointerInfoEXT.html
type ImportMemoryHostPointerInfo struct {
	// SType        StructureType
	Next         unsafe.Pointer
	HandleType   ExternalMemoryHandleTypeFlagBits
	PHostPointer unsafe.Pointer
}

// MemoryHostPointerProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryHostPointerPropertiesEXT.html
type MemoryHostPointerProperties struct {
	// SType          StructureType
	Next           unsafe.Pointer
	MemoryTypeBits uint32
}

// PhysicalDeviceExternalMemoryHostProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceExternalMemoryHostPropertiesEXT.html
type PhysicalDeviceExternalMemoryHostProperties struct {
	// SType                           StructureType
	Next                            unsafe.Pointer
	MinImportedHostPointerAlignment DeviceSize
}

// PipelineCompilerControlFlagsAMD type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkPipelineCompilerControlFlagsAMD
type PipelineCompilerControlFlagsAMD uint32

// PipelineCompilerControlCreateInfoAMD as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkPipelineCompilerControlCreateInfoAMD
type PipelineCompilerControlCreateInfoAMD struct {
	// SType                StructureType
	Next                 unsafe.Pointer
	CompilerControlFlags PipelineCompilerControlFlagsAMD
}

// CalibratedTimestampInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCalibratedTimestampInfoEXT.html
type CalibratedTimestampInfo struct {
	// SType      StructureType
	Next       unsafe.Pointer
	TimeDomain TimeDomain
}

// PhysicalDeviceShaderCorePropertiesAMD as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkPhysicalDeviceShaderCorePropertiesAMD
type PhysicalDeviceShaderCorePropertiesAMD struct {
	// SType                      StructureType
	Next                       unsafe.Pointer
	ShaderEngineCount          uint32
	ShaderArraysPerEngineCount uint32
	ComputeUnitsPerShaderArray uint32
	SimdPerComputeUnit         uint32
	WavefrontsPerSimd          uint32
	WavefrontSize              uint32
	SgprsPerSimd               uint32
	MinSgprAllocation          uint32
	MaxSgprAllocation          uint32
	SgprAllocationGranularity  uint32
	VgprsPerSimd               uint32
	MinVgprAllocation          uint32
	MaxVgprAllocation          uint32
	VgprAllocationGranularity  uint32
}

// DeviceMemoryOverallocationCreateInfoAMD as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkDeviceMemoryOverallocationCreateInfoAMD
type DeviceMemoryOverallocationCreateInfoAMD struct {
	// SType                  StructureType
	Next                   unsafe.Pointer
	OverallocationBehavior MemoryOverallocationBehaviorAMD
}

// PhysicalDeviceVertexAttributeDivisorProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceVertexAttributeDivisorPropertiesEXT.html
type PhysicalDeviceVertexAttributeDivisorProperties struct {
	// SType                  StructureType
	Next                   unsafe.Pointer
	MaxVertexAttribDivisor uint32
}

// VertexInputBindingDivisorDescription as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkVertexInputBindingDivisorDescriptionEXT.html
type VertexInputBindingDivisorDescription struct {
	Binding uint32
	Divisor uint32
}

// PipelineVertexInputDivisorStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineVertexInputDivisorStateCreateInfoEXT.html
type PipelineVertexInputDivisorStateCreateInfo struct {
	// SType                     StructureType
	Next                      unsafe.Pointer
	VertexBindingDivisorCount uint32
	PVertexBindingDivisors    []VertexInputBindingDivisorDescription
}

// PhysicalDeviceVertexAttributeDivisorFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceVertexAttributeDivisorFeaturesEXT.html
type PhysicalDeviceVertexAttributeDivisorFeatures struct {
	// SType                                  StructureType
	Next                                   unsafe.Pointer
	VertexAttributeInstanceRateDivisor     bool
	VertexAttributeInstanceRateZeroDivisor bool
}

// PipelineCreationFeedbackFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCreationFeedbackFlagsEXT.html
type PipelineCreationFeedbackFlags uint32

// PipelineCreationFeedback as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCreationFeedbackEXT.html
type PipelineCreationFeedback struct {
	Flags    PipelineCreationFeedbackFlags
	Duration uint32
}

// PipelineCreationFeedbackCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCreationFeedbackCreateInfoEXT.html
type PipelineCreationFeedbackCreateInfo struct {
	// SType                              StructureType
	Next                               unsafe.Pointer
	PPipelineCreationFeedback          []PipelineCreationFeedback
	PipelineStageCreationFeedbackCount uint32
	PPipelineStageCreationFeedbacks    []PipelineCreationFeedback
}

// PhysicalDeviceComputeShaderDerivativesFeaturesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceComputeShaderDerivativesFeaturesNV.html
type PhysicalDeviceComputeShaderDerivativesFeaturesNV struct {
	// SType                        StructureType
	Next                         unsafe.Pointer
	ComputeDerivativeGroupQuads  bool
	ComputeDerivativeGroupLinear bool
}

// PhysicalDeviceMeshShaderFeaturesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceMeshShaderFeaturesNV.html
type PhysicalDeviceMeshShaderFeaturesNV struct {
	// SType      StructureType
	Next       unsafe.Pointer
	TaskShader bool
	MeshShader bool
}

// PhysicalDeviceMeshShaderPropertiesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceMeshShaderPropertiesNV.html
type PhysicalDeviceMeshShaderPropertiesNV struct {
	// SType                             StructureType
	Next                              unsafe.Pointer
	MaxDrawMeshTasksCount             uint32
	MaxTaskWorkGroupInvocations       uint32
	MaxTaskWorkGroupSize              [3]uint32
	MaxTaskTotalMemorySize            uint32
	MaxTaskOutputCount                uint32
	MaxMeshWorkGroupInvocations       uint32
	MaxMeshWorkGroupSize              [3]uint32
	MaxMeshTotalMemorySize            uint32
	MaxMeshOutputVertices             uint32
	MaxMeshOutputPrimitives           uint32
	MaxMeshMultiviewViewCount         uint32
	MeshOutputPerVertexGranularity    uint32
	MeshOutputPerPrimitiveGranularity uint32
}

// DrawMeshTasksIndirectCommandNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDrawMeshTasksIndirectCommandNV.html
type DrawMeshTasksIndirectCommandNV struct {
	TaskCount uint32
	FirstTask uint32
}

// PhysicalDeviceFragmentShaderBarycentricFeaturesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceFragmentShaderBarycentricFeaturesNV.html
type PhysicalDeviceFragmentShaderBarycentricFeaturesNV struct {
	// SType                     StructureType
	Next                      unsafe.Pointer
	FragmentShaderBarycentric bool
}

// PhysicalDeviceShaderImageFootprintFeaturesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceShaderImageFootprintFeaturesNV.html
type PhysicalDeviceShaderImageFootprintFeaturesNV struct {
	// SType          StructureType
	Next           unsafe.Pointer
	ImageFootprint bool
}

// PipelineViewportExclusiveScissorStateCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineViewportExclusiveScissorStateCreateInfoNV.html
type PipelineViewportExclusiveScissorStateCreateInfoNV struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	ExclusiveScissorCount uint32
	PExclusiveScissors    []Rect2D
}

// PhysicalDeviceExclusiveScissorFeaturesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceExclusiveScissorFeaturesNV.html
type PhysicalDeviceExclusiveScissorFeaturesNV struct {
	// SType            StructureType
	Next             unsafe.Pointer
	ExclusiveScissor bool
}

// QueueFamilyCheckpointPropertiesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueueFamilyCheckpointPropertiesNV.html
type QueueFamilyCheckpointPropertiesNV struct {
	// SType                        StructureType
	Next                         unsafe.Pointer
	CheckpointExecutionStageMask PipelineStageFlags
}

// CheckpointDataNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCheckpointDataNV.html
type CheckpointDataNV struct {
	// SType             StructureType
	Next              unsafe.Pointer
	Stage             PipelineStageFlagBits
	PCheckpointMarker unsafe.Pointer
}

// PhysicalDeviceShaderIntegerFunctions2FeaturesINTEL as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceShaderIntegerFunctions2FeaturesINTEL.html
type PhysicalDeviceShaderIntegerFunctions2FeaturesINTEL struct {
	// SType                   StructureType
	Next                    unsafe.Pointer
	ShaderIntegerFunctions2 bool
}

// PerformanceConfigurationINTEL as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPerformanceConfigurationINTEL.html
type PerformanceConfigurationINTEL C.VkPerformanceConfigurationINTEL

// PerformanceValueDataINTEL as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPerformanceValueDataINTEL.html
const sizeofPerformanceValueDataINTEL = unsafe.Sizeof(C.VkPerformanceValueDataINTEL{})

type PerformanceValueDataINTEL [sizeofPerformanceValueDataINTEL]byte

// PerformanceValueINTEL as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPerformanceValueINTEL.html
type PerformanceValueINTEL struct {
	Type PerformanceValueTypeINTEL
	Data PerformanceValueDataINTEL
}

// InitializePerformanceApiInfoINTEL as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkInitializePerformanceApiInfoINTEL.html
type InitializePerformanceApiInfoINTEL struct {
	// SType     StructureType
	Next      unsafe.Pointer
	PUserData unsafe.Pointer
}

// QueryPoolPerformanceQueryCreateInfoINTEL as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueryPoolPerformanceQueryCreateInfoINTEL.html
type QueryPoolPerformanceQueryCreateInfoINTEL struct {
	// SType                       StructureType
	Next                        unsafe.Pointer
	PerformanceCountersSampling QueryPoolSamplingModeINTEL
}

// QueryPoolCreateInfoINTEL as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkQueryPoolCreateInfoINTEL.html
type QueryPoolCreateInfoINTEL struct {
	// SType                       StructureType
	Next                        unsafe.Pointer
	PerformanceCountersSampling QueryPoolSamplingModeINTEL
}

// PerformanceMarkerInfoINTEL as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPerformanceMarkerInfoINTEL.html
type PerformanceMarkerInfoINTEL struct {
	// SType  StructureType
	Next   unsafe.Pointer
	Marker uint32
}

// PerformanceStreamMarkerInfoINTEL as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPerformanceStreamMarkerInfoINTEL.html
type PerformanceStreamMarkerInfoINTEL struct {
	// SType  StructureType
	Next   unsafe.Pointer
	Marker uint32
}

// PerformanceOverrideInfoINTEL as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPerformanceOverrideInfoINTEL.html
type PerformanceOverrideInfoINTEL struct {
	// SType     StructureType
	Next      unsafe.Pointer
	Type      PerformanceOverrideTypeINTEL
	Enable    bool
	Parameter uint32
}

// PerformanceConfigurationAcquireInfoINTEL as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPerformanceConfigurationAcquireInfoINTEL.html
type PerformanceConfigurationAcquireInfoINTEL struct {
	// SType StructureType
	Next unsafe.Pointer
	Type PerformanceConfigurationTypeINTEL
}

// PhysicalDevicePCIBusInfoProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDevicePCIBusInfoPropertiesEXT.html
type PhysicalDevicePCIBusInfoProperties struct {
	// SType       StructureType
	Next        unsafe.Pointer
	PciDomain   uint32
	PciBus      uint32
	PciDevice   uint32
	PciFunction uint32
}

// DisplayNativeHdrSurfaceCapabilitiesAMD as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkDisplayNativeHdrSurfaceCapabilitiesAMD
type DisplayNativeHdrSurfaceCapabilitiesAMD struct {
	// SType               StructureType
	Next                unsafe.Pointer
	LocalDimmingSupport bool
}

// SwapchainDisplayNativeHdrCreateInfoAMD as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkSwapchainDisplayNativeHdrCreateInfoAMD
type SwapchainDisplayNativeHdrCreateInfoAMD struct {
	// SType              StructureType
	Next               unsafe.Pointer
	LocalDimmingEnable bool
}

// PhysicalDeviceFragmentDensityMapFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceFragmentDensityMapFeaturesEXT.html
type PhysicalDeviceFragmentDensityMapFeatures struct {
	// SType                                 StructureType
	Next                                  unsafe.Pointer
	FragmentDensityMap                    bool
	FragmentDensityMapDynamic             bool
	FragmentDensityMapNonSubsampledImages bool
}

// PhysicalDeviceFragmentDensityMapProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceFragmentDensityMapPropertiesEXT.html
type PhysicalDeviceFragmentDensityMapProperties struct {
	// SType                       StructureType
	Next                        unsafe.Pointer
	MinFragmentDensityTexelSize Extent2D
	MaxFragmentDensityTexelSize Extent2D
	FragmentDensityInvocations  bool
}

// RenderPassFragmentDensityMapCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkRenderPassFragmentDensityMapCreateInfoEXT.html
type RenderPassFragmentDensityMapCreateInfo struct {
	// SType                        StructureType
	Next                         unsafe.Pointer
	FragmentDensityMapAttachment AttachmentReference
}

// PhysicalDeviceSubgroupSizeControlFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceSubgroupSizeControlFeaturesEXT.html
type PhysicalDeviceSubgroupSizeControlFeatures struct {
	// SType                StructureType
	Next                 unsafe.Pointer
	SubgroupSizeControl  bool
	ComputeFullSubgroups bool
}

// PhysicalDeviceSubgroupSizeControlProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceSubgroupSizeControlPropertiesEXT.html
type PhysicalDeviceSubgroupSizeControlProperties struct {
	// SType                        StructureType
	Next                         unsafe.Pointer
	MinSubgroupSize              uint32
	MaxSubgroupSize              uint32
	MaxComputeWorkgroupSubgroups uint32
	RequiredSubgroupSizeStages   ShaderStageFlags
}

// PipelineShaderStageRequiredSubgroupSizeCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineShaderStageRequiredSubgroupSizeCreateInfoEXT.html
type PipelineShaderStageRequiredSubgroupSizeCreateInfo struct {
	// SType                StructureType
	Next                 unsafe.Pointer
	RequiredSubgroupSize uint32
}

// ShaderCorePropertiesFlagsAMD type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkShaderCorePropertiesFlagsAMD
type ShaderCorePropertiesFlagsAMD uint32

// PhysicalDeviceShaderCoreProperties2AMD as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkPhysicalDeviceShaderCoreProperties2AMD
type PhysicalDeviceShaderCoreProperties2AMD struct {
	// SType                  StructureType
	Next                   unsafe.Pointer
	ShaderCoreFeatures     ShaderCorePropertiesFlagsAMD
	ActiveComputeUnitCount uint32
}

// PhysicalDeviceCoherentMemoryFeaturesAMD as declared in https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/vkspec.html#VkPhysicalDeviceCoherentMemoryFeaturesAMD
type PhysicalDeviceCoherentMemoryFeaturesAMD struct {
	// SType                StructureType
	Next                 unsafe.Pointer
	DeviceCoherentMemory bool
}

// PhysicalDeviceShaderImageAtomicInt64Features as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceShaderImageAtomicInt64FeaturesEXT.html
type PhysicalDeviceShaderImageAtomicInt64Features struct {
	// SType                   StructureType
	Next                    unsafe.Pointer
	ShaderImageInt64Atomics bool
	SparseImageInt64Atomics bool
}

// PhysicalDeviceMemoryBudgetProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceMemoryBudgetPropertiesEXT.html
type PhysicalDeviceMemoryBudgetProperties struct {
	// SType      StructureType
	Next       unsafe.Pointer
	HeapBudget [16]DeviceSize
	HeapUsage  [16]DeviceSize
}

// PhysicalDeviceMemoryPriorityFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceMemoryPriorityFeaturesEXT.html
type PhysicalDeviceMemoryPriorityFeatures struct {
	// SType          StructureType
	Next           unsafe.Pointer
	MemoryPriority bool
}

// MemoryPriorityAllocateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMemoryPriorityAllocateInfoEXT.html
type MemoryPriorityAllocateInfo struct {
	// SType    StructureType
	Next     unsafe.Pointer
	Priority float32
}

// PhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV.html
type PhysicalDeviceDedicatedAllocationImageAliasingFeaturesNV struct {
	// SType                            StructureType
	Next                             unsafe.Pointer
	DedicatedAllocationImageAliasing bool
}

// PhysicalDeviceBufferAddressFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceBufferAddressFeaturesEXT.html
type PhysicalDeviceBufferAddressFeatures struct {
	// SType                            StructureType
	Next                             unsafe.Pointer
	BufferDeviceAddress              bool
	BufferDeviceAddressCaptureReplay bool
	BufferDeviceAddressMultiDevice   bool
}

// BufferDeviceAddressCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBufferDeviceAddressCreateInfoEXT.html
type BufferDeviceAddressCreateInfo struct {
	// SType         StructureType
	Next          unsafe.Pointer
	DeviceAddress DeviceAddress
}

// ToolPurposeFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkToolPurposeFlagsEXT.html
type ToolPurposeFlags uint32

// PhysicalDeviceToolProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceToolPropertiesEXT.html
type PhysicalDeviceToolProperties struct {
	// SType       StructureType
	Next        unsafe.Pointer
	Name        [256]byte
	Version     [256]byte
	Purposes    ToolPurposeFlags
	Description [256]byte
	Layer       [256]byte
}

// ValidationFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkValidationFeaturesEXT.html
type ValidationFeatures struct {
	// SType                          StructureType
	Next                           unsafe.Pointer
	EnabledValidationFeatureCount  uint32
	PEnabledValidationFeatures     []ValidationFeatureEnable
	DisabledValidationFeatureCount uint32
	PDisabledValidationFeatures    []ValidationFeatureDisable
}

// CooperativeMatrixPropertiesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCooperativeMatrixPropertiesNV.html
type CooperativeMatrixPropertiesNV struct {
	// SType StructureType
	Next  unsafe.Pointer
	MSize uint32
	NSize uint32
	KSize uint32
	AType ComponentTypeNV
	BType ComponentTypeNV
	CType ComponentTypeNV
	DType ComponentTypeNV
	Scope ScopeNV
}

// PhysicalDeviceCooperativeMatrixFeaturesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceCooperativeMatrixFeaturesNV.html
type PhysicalDeviceCooperativeMatrixFeaturesNV struct {
	// SType                               StructureType
	Next                                unsafe.Pointer
	CooperativeMatrix                   bool
	CooperativeMatrixRobustBufferAccess bool
}

// PhysicalDeviceCooperativeMatrixPropertiesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceCooperativeMatrixPropertiesNV.html
type PhysicalDeviceCooperativeMatrixPropertiesNV struct {
	// SType                            StructureType
	Next                             unsafe.Pointer
	CooperativeMatrixSupportedStages ShaderStageFlags
}

// PipelineCoverageReductionStateCreateFlagsNV type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCoverageReductionStateCreateFlagsNV.html
type PipelineCoverageReductionStateCreateFlagsNV uint32

// PhysicalDeviceCoverageReductionModeFeaturesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceCoverageReductionModeFeaturesNV.html
type PhysicalDeviceCoverageReductionModeFeaturesNV struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	CoverageReductionMode bool
}

// PipelineCoverageReductionStateCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineCoverageReductionStateCreateInfoNV.html
type PipelineCoverageReductionStateCreateInfoNV struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	Flags                 PipelineCoverageReductionStateCreateFlagsNV
	CoverageReductionMode CoverageReductionModeNV
}

// FramebufferMixedSamplesCombinationNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFramebufferMixedSamplesCombinationNV.html
type FramebufferMixedSamplesCombinationNV struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	CoverageReductionMode CoverageReductionModeNV
	RasterizationSamples  SampleCountFlagBits
	DepthStencilSamples   SampleCountFlags
	ColorSamples          SampleCountFlags
}

// PhysicalDeviceFragmentShaderInterlockFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceFragmentShaderInterlockFeaturesEXT.html
type PhysicalDeviceFragmentShaderInterlockFeatures struct {
	// SType                              StructureType
	Next                               unsafe.Pointer
	FragmentShaderSampleInterlock      bool
	FragmentShaderPixelInterlock       bool
	FragmentShaderShadingRateInterlock bool
}

// PhysicalDeviceYcbcrImageArraysFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceYcbcrImageArraysFeaturesEXT.html
type PhysicalDeviceYcbcrImageArraysFeatures struct {
	// SType            StructureType
	Next             unsafe.Pointer
	YcbcrImageArrays bool
}

// PhysicalDeviceProvokingVertexFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceProvokingVertexFeaturesEXT.html
type PhysicalDeviceProvokingVertexFeatures struct {
	// SType                                     StructureType
	Next                                      unsafe.Pointer
	ProvokingVertexLast                       bool
	TransformFeedbackPreservesProvokingVertex bool
}

// PhysicalDeviceProvokingVertexProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceProvokingVertexPropertiesEXT.html
type PhysicalDeviceProvokingVertexProperties struct {
	// SType                                                StructureType
	Next                                                 unsafe.Pointer
	ProvokingVertexModePerPipeline                       bool
	TransformFeedbackPreservesTriangleFanProvokingVertex bool
}

// PipelineRasterizationProvokingVertexStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineRasterizationProvokingVertexStateCreateInfoEXT.html
type PipelineRasterizationProvokingVertexStateCreateInfo struct {
	// SType               StructureType
	Next                unsafe.Pointer
	ProvokingVertexMode ProvokingVertexMode
}

// HeadlessSurfaceCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkHeadlessSurfaceCreateFlagsEXT.html
type HeadlessSurfaceCreateFlags uint32

// HeadlessSurfaceCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkHeadlessSurfaceCreateInfoEXT.html
type HeadlessSurfaceCreateInfo struct {
	// SType StructureType
	Next  unsafe.Pointer
	Flags HeadlessSurfaceCreateFlags
}

// PhysicalDeviceLineRasterizationFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceLineRasterizationFeaturesEXT.html
type PhysicalDeviceLineRasterizationFeatures struct {
	// SType                    StructureType
	Next                     unsafe.Pointer
	RectangularLines         bool
	BresenhamLines           bool
	SmoothLines              bool
	StippledRectangularLines bool
	StippledBresenhamLines   bool
	StippledSmoothLines      bool
}

// PhysicalDeviceLineRasterizationProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceLineRasterizationPropertiesEXT.html
type PhysicalDeviceLineRasterizationProperties struct {
	// SType                     StructureType
	Next                      unsafe.Pointer
	LineSubPixelPrecisionBits uint32
}

// PipelineRasterizationLineStateCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineRasterizationLineStateCreateInfoEXT.html
type PipelineRasterizationLineStateCreateInfo struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	LineRasterizationMode LineRasterizationMode
	StippledLineEnable    bool
	LineStippleFactor     uint32
	LineStipplePattern    uint16
}

// PhysicalDeviceShaderAtomicFloatFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceShaderAtomicFloatFeaturesEXT.html
type PhysicalDeviceShaderAtomicFloatFeatures struct {
	// SType                        StructureType
	Next                         unsafe.Pointer
	ShaderBufferFloat32Atomics   bool
	ShaderBufferFloat32AtomicAdd bool
	ShaderBufferFloat64Atomics   bool
	ShaderBufferFloat64AtomicAdd bool
	ShaderSharedFloat32Atomics   bool
	ShaderSharedFloat32AtomicAdd bool
	ShaderSharedFloat64Atomics   bool
	ShaderSharedFloat64AtomicAdd bool
	ShaderImageFloat32Atomics    bool
	ShaderImageFloat32AtomicAdd  bool
	SparseImageFloat32Atomics    bool
	SparseImageFloat32AtomicAdd  bool
}

// PhysicalDeviceIndexTypeUint8Features as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceIndexTypeUint8FeaturesEXT.html
type PhysicalDeviceIndexTypeUint8Features struct {
	// SType          StructureType
	Next           unsafe.Pointer
	IndexTypeUint8 bool
}

// PhysicalDeviceExtendedDynamicStateFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceExtendedDynamicStateFeaturesEXT.html
type PhysicalDeviceExtendedDynamicStateFeatures struct {
	// SType                StructureType
	Next                 unsafe.Pointer
	ExtendedDynamicState bool
}

// PhysicalDeviceShaderDemoteToHelperInvocationFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceShaderDemoteToHelperInvocationFeaturesEXT.html
type PhysicalDeviceShaderDemoteToHelperInvocationFeatures struct {
	// SType                          StructureType
	Next                           unsafe.Pointer
	ShaderDemoteToHelperInvocation bool
}

// IndirectCommandsLayoutNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkIndirectCommandsLayoutNV.html
type IndirectCommandsLayoutNV C.VkIndirectCommandsLayoutNV

// IndirectStateFlagsNV type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkIndirectStateFlagsNV.html
type IndirectStateFlagsNV uint32

// IndirectCommandsLayoutUsageFlagsNV type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkIndirectCommandsLayoutUsageFlagsNV.html
type IndirectCommandsLayoutUsageFlagsNV uint32

// PhysicalDeviceDeviceGeneratedCommandsPropertiesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceDeviceGeneratedCommandsPropertiesNV.html
type PhysicalDeviceDeviceGeneratedCommandsPropertiesNV struct {
	// SType                                    StructureType
	Next                                     unsafe.Pointer
	MaxGraphicsShaderGroupCount              uint32
	MaxIndirectSequenceCount                 uint32
	MaxIndirectCommandsTokenCount            uint32
	MaxIndirectCommandsStreamCount           uint32
	MaxIndirectCommandsTokenOffset           uint32
	MaxIndirectCommandsStreamStride          uint32
	MinSequencesCountBufferOffsetAlignment   uint32
	MinSequencesIndexBufferOffsetAlignment   uint32
	MinIndirectCommandsBufferOffsetAlignment uint32
}

// PhysicalDeviceDeviceGeneratedCommandsFeaturesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceDeviceGeneratedCommandsFeaturesNV.html
type PhysicalDeviceDeviceGeneratedCommandsFeaturesNV struct {
	// SType                   StructureType
	Next                    unsafe.Pointer
	DeviceGeneratedCommands bool
}

// GraphicsShaderGroupCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkGraphicsShaderGroupCreateInfoNV.html
type GraphicsShaderGroupCreateInfoNV struct {
	// SType              StructureType
	Next               unsafe.Pointer
	StageCount         uint32
	PStages            []PipelineShaderStageCreateInfo
	PVertexInputState  []PipelineVertexInputStateCreateInfo
	PTessellationState []PipelineTessellationStateCreateInfo
}

// GraphicsPipelineShaderGroupsCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkGraphicsPipelineShaderGroupsCreateInfoNV.html
type GraphicsPipelineShaderGroupsCreateInfoNV struct {
	// SType         StructureType
	Next          unsafe.Pointer
	GroupCount    uint32
	PGroups       []GraphicsShaderGroupCreateInfoNV
	PipelineCount uint32
	PPipelines    []Pipeline
}

// BindShaderGroupIndirectCommandNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBindShaderGroupIndirectCommandNV.html
type BindShaderGroupIndirectCommandNV struct {
	GroupIndex uint32
}

// BindIndexBufferIndirectCommandNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBindIndexBufferIndirectCommandNV.html
type BindIndexBufferIndirectCommandNV struct {
	BufferAddress DeviceAddress
	Size          uint32
	IndexType     IndexType
}

// BindVertexBufferIndirectCommandNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkBindVertexBufferIndirectCommandNV.html
type BindVertexBufferIndirectCommandNV struct {
	BufferAddress DeviceAddress
	Size          uint32
	Stride        uint32
}

// SetStateFlagsIndirectCommandNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSetStateFlagsIndirectCommandNV.html
type SetStateFlagsIndirectCommandNV struct {
	Data uint32
}

// IndirectCommandsStreamNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkIndirectCommandsStreamNV.html
type IndirectCommandsStreamNV struct {
	Buffer Buffer
	Offset DeviceSize
}

// IndirectCommandsLayoutTokenNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkIndirectCommandsLayoutTokenNV.html
type IndirectCommandsLayoutTokenNV struct {
	// SType                        StructureType
	Next                         unsafe.Pointer
	TokenType                    IndirectCommandsTokenTypeNV
	Stream                       uint32
	Offset                       uint32
	VertexBindingUnit            uint32
	VertexDynamicStride          bool
	PushconstantPipelineLayout   PipelineLayout
	PushconstantShaderStageFlags ShaderStageFlags
	PushconstantOffset           uint32
	PushconstantSize             uint32
	IndirectStateFlags           IndirectStateFlagsNV
	IndexTypeCount               uint32
	PIndexTypes                  []IndexType
	PIndexTypeValues             []uint32
}

// IndirectCommandsLayoutCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkIndirectCommandsLayoutCreateInfoNV.html
type IndirectCommandsLayoutCreateInfoNV struct {
	// SType             StructureType
	Next              unsafe.Pointer
	Flags             IndirectCommandsLayoutUsageFlagsNV
	PipelineBindPoint PipelineBindPoint
	TokenCount        uint32
	PTokens           []IndirectCommandsLayoutTokenNV
	StreamCount       uint32
	PStreamStrides    []uint32
}

// GeneratedCommandsInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkGeneratedCommandsInfoNV.html
type GeneratedCommandsInfoNV struct {
	// SType                  StructureType
	Next                   unsafe.Pointer
	PipelineBindPoint      PipelineBindPoint
	Pipeline               Pipeline
	IndirectCommandsLayout IndirectCommandsLayoutNV
	StreamCount            uint32
	PStreams               []IndirectCommandsStreamNV
	SequencesCount         uint32
	PreprocessBuffer       Buffer
	PreprocessOffset       DeviceSize
	PreprocessSize         DeviceSize
	SequencesCountBuffer   Buffer
	SequencesCountOffset   DeviceSize
	SequencesIndexBuffer   Buffer
	SequencesIndexOffset   DeviceSize
}

// GeneratedCommandsMemoryRequirementsInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkGeneratedCommandsMemoryRequirementsInfoNV.html
type GeneratedCommandsMemoryRequirementsInfoNV struct {
	// SType                  StructureType
	Next                   unsafe.Pointer
	PipelineBindPoint      PipelineBindPoint
	Pipeline               Pipeline
	IndirectCommandsLayout IndirectCommandsLayoutNV
	MaxSequencesCount      uint32
}

// PhysicalDeviceInheritedViewportScissorFeaturesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceInheritedViewportScissorFeaturesNV.html
type PhysicalDeviceInheritedViewportScissorFeaturesNV struct {
	// SType                      StructureType
	Next                       unsafe.Pointer
	InheritedViewportScissor2D bool
}

// CommandBufferInheritanceViewportScissorInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandBufferInheritanceViewportScissorInfoNV.html
type CommandBufferInheritanceViewportScissorInfoNV struct {
	// SType              StructureType
	Next               unsafe.Pointer
	ViewportScissor2D  bool
	ViewportDepthCount uint32
	PViewportDepths    []Viewport
}

// PhysicalDeviceTexelBufferAlignmentFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceTexelBufferAlignmentFeaturesEXT.html
type PhysicalDeviceTexelBufferAlignmentFeatures struct {
	// SType                StructureType
	Next                 unsafe.Pointer
	TexelBufferAlignment bool
}

// PhysicalDeviceTexelBufferAlignmentProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceTexelBufferAlignmentPropertiesEXT.html
type PhysicalDeviceTexelBufferAlignmentProperties struct {
	// SType                                        StructureType
	Next                                         unsafe.Pointer
	StorageTexelBufferOffsetAlignmentBytes       DeviceSize
	StorageTexelBufferOffsetSingleTexelAlignment bool
	UniformTexelBufferOffsetAlignmentBytes       DeviceSize
	UniformTexelBufferOffsetSingleTexelAlignment bool
}

// RenderPassTransformBeginInfoQCOM as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkRenderPassTransformBeginInfoQCOM.html
type RenderPassTransformBeginInfoQCOM struct {
	// SType     StructureType
	Next      unsafe.Pointer
	Transform SurfaceTransformFlagBits
}

// CommandBufferInheritanceRenderPassTransformInfoQCOM as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCommandBufferInheritanceRenderPassTransformInfoQCOM.html
type CommandBufferInheritanceRenderPassTransformInfoQCOM struct {
	// SType      StructureType
	Next       unsafe.Pointer
	Transform  SurfaceTransformFlagBits
	RenderArea Rect2D
}

// DeviceMemoryReportFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceMemoryReportFlagsEXT.html
type DeviceMemoryReportFlags uint32

// PhysicalDeviceDeviceMemoryReportFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceDeviceMemoryReportFeaturesEXT.html
type PhysicalDeviceDeviceMemoryReportFeatures struct {
	// SType              StructureType
	Next               unsafe.Pointer
	DeviceMemoryReport bool
}

// DeviceMemoryReportCallbackData as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceMemoryReportCallbackDataEXT.html
type DeviceMemoryReportCallbackData C.VkDeviceMemoryReportCallbackDataEXT

// DeviceMemoryReportCallback type as declared in vulkan/vulkan_core.h:11772
type DeviceMemoryReportCallback func(pCallbackData []DeviceMemoryReportCallbackData, pUserData unsafe.Pointer)

// DeviceDeviceMemoryReportCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceDeviceMemoryReportCreateInfoEXT.html
type DeviceDeviceMemoryReportCreateInfo struct {
	// SType           StructureType
	Next            unsafe.Pointer
	Flags           DeviceMemoryReportFlags
	PfnUserCallback DeviceMemoryReportCallback
	PUserData       unsafe.Pointer
}

// PhysicalDeviceRobustness2Features as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceRobustness2FeaturesEXT.html
type PhysicalDeviceRobustness2Features struct {
	// SType               StructureType
	Next                unsafe.Pointer
	RobustBufferAccess2 bool
	RobustImageAccess2  bool
	NullDescriptor      bool
}

// PhysicalDeviceRobustness2Properties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceRobustness2PropertiesEXT.html
type PhysicalDeviceRobustness2Properties struct {
	// SType                                  StructureType
	Next                                   unsafe.Pointer
	RobustStorageBufferAccessSizeAlignment DeviceSize
	RobustUniformBufferAccessSizeAlignment DeviceSize
}

// SamplerCustomBorderColorCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkSamplerCustomBorderColorCreateInfoEXT.html
type SamplerCustomBorderColorCreateInfo struct {
	// SType             StructureType
	Next              unsafe.Pointer
	CustomBorderColor ClearColorValue
	Format            Format
}

// PhysicalDeviceCustomBorderColorProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceCustomBorderColorPropertiesEXT.html
type PhysicalDeviceCustomBorderColorProperties struct {
	// SType                        StructureType
	Next                         unsafe.Pointer
	MaxCustomBorderColorSamplers uint32
}

// PhysicalDeviceCustomBorderColorFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceCustomBorderColorFeaturesEXT.html
type PhysicalDeviceCustomBorderColorFeatures struct {
	// SType                          StructureType
	Next                           unsafe.Pointer
	CustomBorderColors             bool
	CustomBorderColorWithoutFormat bool
}

// PrivateDataSlot as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPrivateDataSlotEXT.html
type PrivateDataSlot C.VkPrivateDataSlotEXT

// PrivateDataSlotCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPrivateDataSlotCreateFlagsEXT.html
type PrivateDataSlotCreateFlags uint32

// PhysicalDevicePrivateDataFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDevicePrivateDataFeaturesEXT.html
type PhysicalDevicePrivateDataFeatures struct {
	// SType       StructureType
	Next        unsafe.Pointer
	PrivateData bool
}

// DevicePrivateDataCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDevicePrivateDataCreateInfoEXT.html
type DevicePrivateDataCreateInfo struct {
	// SType                       StructureType
	Next                        unsafe.Pointer
	PrivateDataSlotRequestCount uint32
}

// PrivateDataSlotCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPrivateDataSlotCreateInfoEXT.html
type PrivateDataSlotCreateInfo struct {
	// SType StructureType
	Next  unsafe.Pointer
	Flags PrivateDataSlotCreateFlags
}

// PhysicalDevicePipelineCreationCacheControlFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDevicePipelineCreationCacheControlFeaturesEXT.html
type PhysicalDevicePipelineCreationCacheControlFeatures struct {
	// SType                        StructureType
	Next                         unsafe.Pointer
	PipelineCreationCacheControl bool
}

// DeviceDiagnosticsConfigFlagsNV type as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceDiagnosticsConfigFlagsNV.html
type DeviceDiagnosticsConfigFlagsNV uint32

// PhysicalDeviceDiagnosticsConfigFeaturesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceDiagnosticsConfigFeaturesNV.html
type PhysicalDeviceDiagnosticsConfigFeaturesNV struct {
	// SType             StructureType
	Next              unsafe.Pointer
	DiagnosticsConfig bool
}

// DeviceDiagnosticsConfigCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkDeviceDiagnosticsConfigCreateInfoNV.html
type DeviceDiagnosticsConfigCreateInfoNV struct {
	// SType StructureType
	Next  unsafe.Pointer
	Flags DeviceDiagnosticsConfigFlagsNV
}

// PhysicalDeviceFragmentShadingRateEnumsFeaturesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceFragmentShadingRateEnumsFeaturesNV.html
type PhysicalDeviceFragmentShadingRateEnumsFeaturesNV struct {
	// SType                            StructureType
	Next                             unsafe.Pointer
	FragmentShadingRateEnums         bool
	SupersampleFragmentShadingRates  bool
	NoInvocationFragmentShadingRates bool
}

// PhysicalDeviceFragmentShadingRateEnumsPropertiesNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceFragmentShadingRateEnumsPropertiesNV.html
type PhysicalDeviceFragmentShadingRateEnumsPropertiesNV struct {
	// SType                                 StructureType
	Next                                  unsafe.Pointer
	MaxFragmentShadingRateInvocationCount SampleCountFlagBits
}

// PipelineFragmentShadingRateEnumStateCreateInfoNV as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineFragmentShadingRateEnumStateCreateInfoNV.html
type PipelineFragmentShadingRateEnumStateCreateInfoNV struct {
	// SType           StructureType
	Next            unsafe.Pointer
	ShadingRateType FragmentShadingRateTypeNV
	ShadingRate     FragmentShadingRateNV
	CombinerOps     [2]FragmentShadingRateCombinerOp
}

// PhysicalDeviceYcbcr2Plane444FormatsFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceYcbcr2Plane444FormatsFeaturesEXT.html
type PhysicalDeviceYcbcr2Plane444FormatsFeatures struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	Ycbcr2plane444Formats bool
}

// PhysicalDeviceFragmentDensityMap2Features as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceFragmentDensityMap2FeaturesEXT.html
type PhysicalDeviceFragmentDensityMap2Features struct {
	// SType                      StructureType
	Next                       unsafe.Pointer
	FragmentDensityMapDeferred bool
}

// PhysicalDeviceFragmentDensityMap2Properties as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceFragmentDensityMap2PropertiesEXT.html
type PhysicalDeviceFragmentDensityMap2Properties struct {
	// SType                                     StructureType
	Next                                      unsafe.Pointer
	SubsampledLoads                           bool
	SubsampledCoarseReconstructionEarlyAccess bool
	MaxSubsampledArrayLayers                  uint32
	MaxDescriptorSetSubsampledSamplers        uint32
}

// CopyCommandTransformInfoQCOM as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkCopyCommandTransformInfoQCOM.html
type CopyCommandTransformInfoQCOM struct {
	// SType     StructureType
	Next      unsafe.Pointer
	Transform SurfaceTransformFlagBits
}

// PhysicalDeviceImageRobustnessFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceImageRobustnessFeaturesEXT.html
type PhysicalDeviceImageRobustnessFeatures struct {
	// SType             StructureType
	Next              unsafe.Pointer
	RobustImageAccess bool
}

// PhysicalDevice4444FormatsFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDevice4444FormatsFeaturesEXT.html
type PhysicalDevice4444FormatsFeatures struct {
	// SType          StructureType
	Next           unsafe.Pointer
	FormatA4R4G4B4 bool
	FormatA4B4G4R4 bool
}

// PhysicalDeviceMutableDescriptorTypeFeaturesVALVE as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceMutableDescriptorTypeFeaturesVALVE.html
type PhysicalDeviceMutableDescriptorTypeFeaturesVALVE struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	MutableDescriptorType bool
}

// MutableDescriptorTypeListVALVE as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMutableDescriptorTypeListVALVE.html
type MutableDescriptorTypeListVALVE struct {
	DescriptorTypeCount uint32
	PDescriptorTypes    []DescriptorType
}

// MutableDescriptorTypeCreateInfoVALVE as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkMutableDescriptorTypeCreateInfoVALVE.html
type MutableDescriptorTypeCreateInfoVALVE struct {
	// SType                          StructureType
	Next                           unsafe.Pointer
	MutableDescriptorTypeListCount uint32
	PMutableDescriptorTypeLists    []MutableDescriptorTypeListVALVE
}

// PhysicalDeviceVertexInputDynamicStateFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceVertexInputDynamicStateFeaturesEXT.html
type PhysicalDeviceVertexInputDynamicStateFeatures struct {
	// SType                   StructureType
	Next                    unsafe.Pointer
	VertexInputDynamicState bool
}

// VertexInputBindingDescription2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkVertexInputBindingDescription2EXT.html
type VertexInputBindingDescription2 struct {
	// SType     StructureType
	Next      unsafe.Pointer
	Binding   uint32
	Stride    uint32
	InputRate VertexInputRate
	Divisor   uint32
}

// VertexInputAttributeDescription2 as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkVertexInputAttributeDescription2EXT.html
type VertexInputAttributeDescription2 struct {
	// SType    StructureType
	Next     unsafe.Pointer
	Location uint32
	Binding  uint32
	Format   Format
	Offset   uint32
}

// PhysicalDeviceExtendedDynamicState2Features as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceExtendedDynamicState2FeaturesEXT.html
type PhysicalDeviceExtendedDynamicState2Features struct {
	// SType                                   StructureType
	Next                                    unsafe.Pointer
	ExtendedDynamicState2                   bool
	ExtendedDynamicState2LogicOp            bool
	ExtendedDynamicState2PatchControlPoints bool
}

// PhysicalDeviceColorWriteEnableFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPhysicalDeviceColorWriteEnableFeaturesEXT.html
type PhysicalDeviceColorWriteEnableFeatures struct {
	// SType            StructureType
	Next             unsafe.Pointer
	ColorWriteEnable bool
}

// PipelineColorWriteCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkPipelineColorWriteCreateInfoEXT.html
type PipelineColorWriteCreateInfo struct {
	// SType              StructureType
	Next               unsafe.Pointer
	AttachmentCount    uint32
	PColorWriteEnables []bool
}

// AccelerationStructure as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureKHR
type AccelerationStructure C.VkAccelerationStructureKHR

// AccelerationStructureCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureCreateFlagsKHR
type AccelerationStructureCreateFlags uint32

// DeviceOrHostAddress as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDeviceOrHostAddressKHR
const sizeofDeviceOrHostAddress = unsafe.Sizeof(C.VkDeviceOrHostAddressKHR{})

type DeviceOrHostAddress [sizeofDeviceOrHostAddress]byte

// DeviceOrHostAddressConst as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkDeviceOrHostAddressConstKHR
const sizeofDeviceOrHostAddressConst = unsafe.Sizeof(C.VkDeviceOrHostAddressConstKHR{})

type DeviceOrHostAddressConst [sizeofDeviceOrHostAddressConst]byte

// AccelerationStructureBuildRangeInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureBuildRangeInfoKHR
type AccelerationStructureBuildRangeInfo struct {
	PrimitiveCount  uint32
	PrimitiveOffset uint32
	FirstVertex     uint32
	TransformOffset uint32
}

// AccelerationStructureGeometryTrianglesData as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureGeometryTrianglesDataKHR
type AccelerationStructureGeometryTrianglesData struct {
	// SType         StructureType
	Next          unsafe.Pointer
	VertexFormat  Format
	VertexData    DeviceOrHostAddressConst
	VertexStride  DeviceSize
	MaxVertex     uint32
	IndexType     IndexType
	IndexData     DeviceOrHostAddressConst
	TransformData DeviceOrHostAddressConst
}

// AccelerationStructureGeometryAabbsData as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureGeometryAabbsDataKHR
type AccelerationStructureGeometryAabbsData struct {
	// SType  StructureType
	Next   unsafe.Pointer
	Data   DeviceOrHostAddressConst
	Stride DeviceSize
}

// AccelerationStructureGeometryInstancesData as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureGeometryInstancesDataKHR
type AccelerationStructureGeometryInstancesData struct {
	// SType           StructureType
	Next            unsafe.Pointer
	ArrayOfPointers bool
	Data            DeviceOrHostAddressConst
}

// AccelerationStructureGeometryData as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureGeometryDataKHR
const sizeofAccelerationStructureGeometryData = unsafe.Sizeof(C.VkAccelerationStructureGeometryDataKHR{})

type AccelerationStructureGeometryData [sizeofAccelerationStructureGeometryData]byte

// AccelerationStructureGeometry as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureGeometryKHR
type AccelerationStructureGeometry struct {
	// SType        StructureType
	Next         unsafe.Pointer
	GeometryType GeometryType
	Geometry     AccelerationStructureGeometryData
	Flags        GeometryFlags
}

// AccelerationStructureBuildGeometryInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureBuildGeometryInfoKHR
type AccelerationStructureBuildGeometryInfo struct {
	// SType                    StructureType
	Next                     unsafe.Pointer
	Type                     AccelerationStructureType
	Flags                    BuildAccelerationStructureFlags
	Mode                     BuildAccelerationStructureMode
	SrcAccelerationStructure AccelerationStructure
	DstAccelerationStructure AccelerationStructure
	GeometryCount            uint32
	PGeometries              []AccelerationStructureGeometry
	PpGeometries             [][]AccelerationStructureGeometry
	ScratchData              DeviceOrHostAddress
}

// AccelerationStructureCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureCreateInfoKHR
type AccelerationStructureCreateInfo struct {
	// SType         StructureType
	Next          unsafe.Pointer
	CreateFlags   AccelerationStructureCreateFlags
	Buffer        Buffer
	Offset        DeviceSize
	Size          DeviceSize
	Type          AccelerationStructureType
	DeviceAddress DeviceAddress
}

// WriteDescriptorSetAccelerationStructure as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkWriteDescriptorSetAccelerationStructureKHR
type WriteDescriptorSetAccelerationStructure struct {
	// SType                      StructureType
	Next                       unsafe.Pointer
	AccelerationStructureCount uint32
	PAccelerationStructures    []AccelerationStructure
}

// PhysicalDeviceAccelerationStructureFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDeviceAccelerationStructureFeaturesKHR
type PhysicalDeviceAccelerationStructureFeatures struct {
	// SType                                                 StructureType
	Next                                                  unsafe.Pointer
	AccelerationStructure                                 bool
	AccelerationStructureCaptureReplay                    bool
	AccelerationStructureIndirectBuild                    bool
	AccelerationStructureHostCommands                     bool
	DescriptorBindingAccelerationStructureUpdateAfterBind bool
}

// PhysicalDeviceAccelerationStructureProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDeviceAccelerationStructurePropertiesKHR
type PhysicalDeviceAccelerationStructureProperties struct {
	// SType                                                      StructureType
	Next                                                       unsafe.Pointer
	MaxGeometryCount                                           uint32
	MaxInstanceCount                                           uint32
	MaxPrimitiveCount                                          uint32
	MaxPerStageDescriptorAccelerationStructures                uint32
	MaxPerStageDescriptorUpdateAfterBindAccelerationStructures uint32
	MaxDescriptorSetAccelerationStructures                     uint32
	MaxDescriptorSetUpdateAfterBindAccelerationStructures      uint32
	MinAccelerationStructureScratchOffsetAlignment             uint32
}

// AccelerationStructureDeviceAddressInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureDeviceAddressInfoKHR
type AccelerationStructureDeviceAddressInfo struct {
	// SType                 StructureType
	Next                  unsafe.Pointer
	AccelerationStructure AccelerationStructure
}

// AccelerationStructureVersionInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureVersionInfoKHR
type AccelerationStructureVersionInfo struct {
	// SType        StructureType
	Next         unsafe.Pointer
	PVersionData []byte
}

// CopyAccelerationStructureToMemoryInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkCopyAccelerationStructureToMemoryInfoKHR
type CopyAccelerationStructureToMemoryInfo struct {
	// SType StructureType
	Next unsafe.Pointer
	Src  AccelerationStructure
	Dst  DeviceOrHostAddress
	Mode CopyAccelerationStructureMode
}

// CopyMemoryToAccelerationStructureInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkCopyMemoryToAccelerationStructureInfoKHR
type CopyMemoryToAccelerationStructureInfo struct {
	// SType StructureType
	Next unsafe.Pointer
	Src  DeviceOrHostAddressConst
	Dst  AccelerationStructure
	Mode CopyAccelerationStructureMode
}

// CopyAccelerationStructureInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkCopyAccelerationStructureInfoKHR
type CopyAccelerationStructureInfo struct {
	// SType StructureType
	Next unsafe.Pointer
	Src  AccelerationStructure
	Dst  AccelerationStructure
	Mode CopyAccelerationStructureMode
}

// AccelerationStructureBuildSizesInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkAccelerationStructureBuildSizesInfoKHR
type AccelerationStructureBuildSizesInfo struct {
	// SType                     StructureType
	Next                      unsafe.Pointer
	AccelerationStructureSize DeviceSize
	UpdateScratchSize         DeviceSize
	BuildScratchSize          DeviceSize
}

// RayTracingShaderGroupCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkRayTracingShaderGroupCreateInfoKHR
type RayTracingShaderGroupCreateInfo struct {
	// SType                           StructureType
	Next                            unsafe.Pointer
	Type                            RayTracingShaderGroupType
	GeneralShader                   uint32
	ClosestHitShader                uint32
	AnyHitShader                    uint32
	IntersectionShader              uint32
	PShaderGroupCaptureReplayHandle unsafe.Pointer
}

// RayTracingPipelineInterfaceCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkRayTracingPipelineInterfaceCreateInfoKHR
type RayTracingPipelineInterfaceCreateInfo struct {
	// SType                          StructureType
	Next                           unsafe.Pointer
	MaxPipelineRayPayloadSize      uint32
	MaxPipelineRayHitAttributeSize uint32
}

// RayTracingPipelineCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkRayTracingPipelineCreateInfoKHR
type RayTracingPipelineCreateInfo struct {
	// SType                        StructureType
	Next                         unsafe.Pointer
	Flags                        PipelineCreateFlags
	StageCount                   uint32
	PStages                      []PipelineShaderStageCreateInfo
	GroupCount                   uint32
	PGroups                      []RayTracingShaderGroupCreateInfo
	MaxPipelineRayRecursionDepth uint32
	PLibraryInfo                 []PipelineLibraryCreateInfo
	PLibraryInterface            []RayTracingPipelineInterfaceCreateInfo
	PDynamicState                []PipelineDynamicStateCreateInfo
	Layout                       PipelineLayout
	BasePipelineHandle           Pipeline
	BasePipelineIndex            int32
}

// PhysicalDeviceRayTracingPipelineFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDeviceRayTracingPipelineFeaturesKHR
type PhysicalDeviceRayTracingPipelineFeatures struct {
	// SType                                                 StructureType
	Next                                                  unsafe.Pointer
	RayTracingPipeline                                    bool
	RayTracingPipelineShaderGroupHandleCaptureReplay      bool
	RayTracingPipelineShaderGroupHandleCaptureReplayMixed bool
	RayTracingPipelineTraceRaysIndirect                   bool
	RayTraversalPrimitiveCulling                          bool
}

// PhysicalDeviceRayTracingPipelineProperties as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDeviceRayTracingPipelinePropertiesKHR
type PhysicalDeviceRayTracingPipelineProperties struct {
	// SType                              StructureType
	Next                               unsafe.Pointer
	ShaderGroupHandleSize              uint32
	MaxRayRecursionDepth               uint32
	MaxShaderGroupStride               uint32
	ShaderGroupBaseAlignment           uint32
	ShaderGroupHandleCaptureReplaySize uint32
	MaxRayDispatchInvocationCount      uint32
	ShaderGroupHandleAlignment         uint32
	MaxRayHitAttributeSize             uint32
}

// StridedDeviceAddressRegion as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkStridedDeviceAddressRegionKHR
type StridedDeviceAddressRegion struct {
	DeviceAddress DeviceAddress
	Stride        DeviceSize
	Size          DeviceSize
}

// TraceRaysIndirectCommand as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkTraceRaysIndirectCommandKHR
type TraceRaysIndirectCommand struct {
	Width  uint32
	Height uint32
	Depth  uint32
}

// PhysicalDeviceRayQueryFeatures as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkPhysicalDeviceRayQueryFeaturesKHR
type PhysicalDeviceRayQueryFeatures struct {
	// SType    StructureType
	Next     unsafe.Pointer
	RayQuery bool
}

// XcbSurfaceCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkXcbSurfaceCreateFlagsKHR
type XcbSurfaceCreateFlags uint32

// XcbSurfaceCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkXcbSurfaceCreateInfoKHR
type XcbSurfaceCreateInfo struct {
	// SType  StructureType
	Next       unsafe.Pointer
	Flags      XcbSurfaceCreateFlags
	Connection unsafe.Pointer
	Window     uint32
}

// XlibSurfaceCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkXlibSurfaceCreateFlagsKHR
type XlibSurfaceCreateFlags uint32

// XlibSurfaceCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkXlibSurfaceCreateInfoKHR
type XlibSurfaceCreateInfo struct {
	// SType  StructureType
	Next   unsafe.Pointer
	Flags  XlibSurfaceCreateFlags
	Window uint32
}

// WaylandSurfaceCreateFlags type as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkWaylandSurfaceCreateFlagsKHR
type WaylandSurfaceCreateFlags uint32

// WaylandSurfaceCreateInfo as declared in https://www.khronos.org/registry/vulkan/specs/1.2-khr-extensions/html/vkspec.html#VkWaylandSurfaceCreateInfoKHR
type WaylandSurfaceCreateInfo struct {
	// SType StructureType
	Next  unsafe.Pointer
	Flags WaylandSurfaceCreateFlags
}
