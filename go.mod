module gitlab.com/corruptmemory/go-vulkan

go 1.16

require (
	github.com/google/uuid v1.2.0
	github.com/jcmturner/gokrb5/v8 v8.4.2
)
